﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Data.OleDb;
using System.IO;




namespace Gestion_AP
{
    class Logins
    {


        // Se utiliza para el vencimiento de las Claves de Usuario. Modificar segun politicas!
        public static int DiasVencimiento = 365;
        

        public static void Login(string Usuario, string Dominio, DateTime Fecha, string Accion)



        {
            DateTime dt = DateTime.Today;

            string path = Directory.GetCurrentDirectory() + @"\Log_Process\" + dt.ToString("yyyyMMdd") + ".txt";
            if (!File.Exists(path))
            {

                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.Write("USUARIO;ACCION;DOMINIO;FECHA");
                    sw.WriteLine();
                    sw.Write(Usuario + ";" + Accion + ";" + Dominio + ";" + Fecha);
                    sw.WriteLine();


                }


            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.Write(Usuario + ";" + Accion + ";" + Dominio + ";" + Fecha);
                    sw.WriteLine();
                }
            }







        }
        public static void LogError(Exception error)

        {
            DateTime dt = DateTime.Today;

            string path = Directory.GetCurrentDirectory() + @"\Log_Exceptions\" + dt.ToString("yyyyMMdd") + ".txt";
            if (!File.Exists(path))
            {

                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.Write("USUARIO LOGUEADO;USUARIO PC;NOMBRE MAQUINA;FECHA;ERROR####");
                    sw.WriteLine();
                    sw.Write(Ingreso.UsuarioLogueado + ";" + Environment.UserName + ";" + Environment.MachineName + ";" + DateTime.Now + ";" + Error(error));
                    sw.WriteLine();


                }


            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.Write(Ingreso.UsuarioLogueado + ";" + Environment.UserName + ";" + Environment.MachineName + ";" + DateTime.Now + ";" + Error(error));
                    sw.WriteLine();
                }
            }



        }


        public static string Error(Exception ex)


        {
            StringBuilder _error = new StringBuilder();
            _error.Append("#"+ex.Message.Trim()).Append("#" + ex.StackTrace.Replace("\n","").Trim()).Append("#"+ex.Source.Trim()).Append("#"+ex.TargetSite);

            return _error.ToString();


        }

        public static void Estadistica(string Accion, string ID)
        {

            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cn.Open();
                cmd.CommandText = "INSERT INTO ESTADISTICAS(USUARIO,FECHA,ACCION,ID_TRAM) VALUES (@US,@FE,@ACC,@ID_TRAM)";
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@ACC", Accion);
                cmd.Parameters.AddWithValue("@ID_TRAM", ID);
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception EX)
            {

                LogError(EX);
            }
         
            


        }

        public const int LONG_CONTRASENIA = 10;
        public const bool CASE_CONTRASENIA = true;
        public const string CONTRASENIA_BLANQUEADA = "IngresoPorBlanqueo";

        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }





    }
}
