﻿namespace Gestion_AP
{
    partial class Mails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mails));
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.cmbGestion = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.btnCargar = new System.Windows.Forms.Button();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.lblWF_Asociado = new System.Windows.Forms.TextBox();
            this.txtIdControl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chbUrgente = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtComAdic = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbAdm = new System.Windows.Forms.ComboBox();
            this.chbLiberar = new System.Windows.Forms.CheckBox();
            this.txtIdVincular = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnValidarVinc = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowser
            // 
            this.webBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser.Location = new System.Drawing.Point(12, 219);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.ScriptErrorsSuppressed = true;
            this.webBrowser.Size = new System.Drawing.Size(647, 385);
            this.webBrowser.TabIndex = 0;
            // 
            // cmbGestion
            // 
            this.cmbGestion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGestion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGestion.FormattingEnabled = true;
            this.cmbGestion.Location = new System.Drawing.Point(121, 41);
            this.cmbGestion.Name = "cmbGestion";
            this.cmbGestion.Size = new System.Drawing.Size(480, 24);
            this.cmbGestion.TabIndex = 1;
            this.cmbGestion.SelectedValueChanged += new System.EventHandler(this.cmbGestion_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(44, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "GESTION";
            // 
            // txtPath
            // 
            this.txtPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPath.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtPath.Location = new System.Drawing.Point(18, 44);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(10, 23);
            this.txtPath.TabIndex = 6;
            this.txtPath.Visible = false;
            // 
            // btnCargar
            // 
            this.btnCargar.BackgroundImage = global::Gestion_AP.Properties.Resources.searchico;
            this.btnCargar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCargar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargar.Location = new System.Drawing.Point(516, 71);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(44, 24);
            this.btnCargar.TabIndex = 8;
            this.btnCargar.UseVisualStyleBackColor = true;
            this.btnCargar.Click += new System.EventHandler(this.btnCargar_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrabar.Location = new System.Drawing.Point(567, 71);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(93, 24);
            this.btnGrabar.TabIndex = 9;
            this.btnGrabar.Text = "GRABAR";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // lblWF_Asociado
            // 
            this.lblWF_Asociado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblWF_Asociado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblWF_Asociado.Location = new System.Drawing.Point(607, 42);
            this.lblWF_Asociado.Name = "lblWF_Asociado";
            this.lblWF_Asociado.ReadOnly = true;
            this.lblWF_Asociado.Size = new System.Drawing.Size(52, 23);
            this.lblWF_Asociado.TabIndex = 2;
            // 
            // txtIdControl
            // 
            this.txtIdControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIdControl.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtIdControl.Location = new System.Drawing.Point(121, 11);
            this.txtIdControl.Name = "txtIdControl";
            this.txtIdControl.ReadOnly = true;
            this.txtIdControl.Size = new System.Drawing.Size(213, 23);
            this.txtIdControl.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 19);
            this.label3.TabIndex = 10;
            this.label3.Text = "ID CONTROL";
            // 
            // chbUrgente
            // 
            this.chbUrgente.AutoSize = true;
            this.chbUrgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbUrgente.Location = new System.Drawing.Point(340, 76);
            this.chbUrgente.Name = "chbUrgente";
            this.chbUrgente.Size = new System.Drawing.Size(86, 17);
            this.chbUrgente.TabIndex = 4;
            this.chbUrgente.Text = "URGENTE";
            this.chbUrgente.UseVisualStyleBackColor = true;
            this.chbUrgente.CheckedChanged += new System.EventHandler(this.chbUrgente_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtComAdic);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 101);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(645, 110);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Comentarios Adicionales";
            // 
            // txtComAdic
            // 
            this.txtComAdic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComAdic.Location = new System.Drawing.Point(6, 19);
            this.txtComAdic.Multiline = true;
            this.txtComAdic.Name = "txtComAdic";
            this.txtComAdic.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtComAdic.Size = new System.Drawing.Size(633, 85);
            this.txtComAdic.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(72, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 19);
            this.label4.TabIndex = 15;
            this.label4.Text = "ADM";
            // 
            // cmbAdm
            // 
            this.cmbAdm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAdm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAdm.FormattingEnabled = true;
            this.cmbAdm.Items.AddRange(new object[] {
            "VISA",
            "AMERICAN EXPRESS",
            "MASTERCARD"});
            this.cmbAdm.Location = new System.Drawing.Point(121, 71);
            this.cmbAdm.Name = "cmbAdm";
            this.cmbAdm.Size = new System.Drawing.Size(213, 24);
            this.cmbAdm.TabIndex = 5;
            // 
            // chbLiberar
            // 
            this.chbLiberar.AutoSize = true;
            this.chbLiberar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbLiberar.Location = new System.Drawing.Point(431, 76);
            this.chbLiberar.Name = "chbLiberar";
            this.chbLiberar.Size = new System.Drawing.Size(79, 17);
            this.chbLiberar.TabIndex = 16;
            this.chbLiberar.Text = "LIBERAR";
            this.chbLiberar.UseVisualStyleBackColor = true;
            this.chbLiberar.CheckedChanged += new System.EventHandler(this.chbLiberar_CheckedChanged);
            // 
            // txtIdVincular
            // 
            this.txtIdVincular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIdVincular.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtIdVincular.Location = new System.Drawing.Point(443, 11);
            this.txtIdVincular.Name = "txtIdVincular";
            this.txtIdVincular.Size = new System.Drawing.Size(158, 23);
            this.txtIdVincular.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(339, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 19);
            this.label2.TabIndex = 18;
            this.label2.Text = "VINCULADO";
            // 
            // btnValidarVinc
            // 
            this.btnValidarVinc.BackgroundImage = global::Gestion_AP.Properties.Resources.Entypo_d83d_0__1283;
            this.btnValidarVinc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnValidarVinc.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidarVinc.Location = new System.Drawing.Point(607, 10);
            this.btnValidarVinc.Name = "btnValidarVinc";
            this.btnValidarVinc.Size = new System.Drawing.Size(53, 24);
            this.btnValidarVinc.TabIndex = 19;
            this.btnValidarVinc.UseVisualStyleBackColor = true;
            this.btnValidarVinc.Click += new System.EventHandler(this.btnValidarVinc_Click);
            // 
            // Mails
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(669, 616);
            this.Controls.Add(this.btnValidarVinc);
            this.Controls.Add(this.txtIdVincular);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chbLiberar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbAdm);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chbUrgente);
            this.Controls.Add(this.txtIdControl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblWF_Asociado);
            this.Controls.Add(this.btnGrabar);
            this.Controls.Add(this.btnCargar);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbGestion);
            this.Controls.Add(this.webBrowser);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Mails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trámite E-Mail";
            this.Load += new System.EventHandler(this.Mails_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.ComboBox cmbGestion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Button btnCargar;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.TextBox lblWF_Asociado;
        private System.Windows.Forms.TextBox txtIdControl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chbUrgente;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtComAdic;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbAdm;
        private System.Windows.Forms.CheckBox chbLiberar;
        private System.Windows.Forms.TextBox txtIdVincular;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnValidarVinc;
    }
}