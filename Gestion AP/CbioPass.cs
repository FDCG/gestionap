﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;


namespace Gestion_AP
{
    public partial class CbioPass : Form
    {
        private UsuarioDaoImpl daoUsuario;

        public CbioPass()
        {
            InitializeComponent();

            daoUsuario = new UsuarioDaoImpl();

        }

        public void cleanControls()
        {

            txtNueva.Clear();
            txtRepetir.Clear();
            txtNueva.Focus();

        }


        private void processChangePassword(Usuario usuario, string contrasenia, string repContrasenia)
        {

            if (contrasenia == "" || repContrasenia == "")
            {
                throw new ArgumentException("Todos los campos deben completarse.");
            }
            else if (!contrasenia.Equals(repContrasenia))
            {
                throw new ArgumentException("Las contraseñas ingresadas no coinciden.");
            }
            else if (usuario.Contrasenia.Equals(contrasenia))
            {
                throw new ArgumentException("La nueva contraseña debe ser distinta a la anterior.");
            }
            else
            {

                daoUsuario.updateUsuario(setUpdateLogin(usuario, contrasenia));

                Usuario.usuarioSingleton = usuario;

                MessageBox.Show("Contraseña modificada.");

            }


        }

        private Usuario setUpdateLogin(Usuario usuario, string nuevaContrasenia)
        {

            usuario.Contrasenia = nuevaContrasenia;
            usuario.VtoClave = DateTime.Today.AddDays(Logins.DiasVencimiento);

            return usuario;

        }



        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            string contrasenia, repContrasenia;

            contrasenia = txtNueva.Text.Trim();
            repContrasenia = txtRepetir.Text.Trim();

            cleanControls();

            try
            {

                processChangePassword(Usuario.usuarioSingleton, contrasenia, repContrasenia);

            }
            catch (ArgumentException argException)
            {
                MessageBox.Show(argException.Message);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

        }

        private void pbSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
