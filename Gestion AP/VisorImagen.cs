﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Gestion_AP
{
    public partial class VisorImagen : Form
    {
        public VisorImagen(string imagen)
        {
            InitializeComponent();
            pictureBox1.Image = Image.FromFile(imagen);
        }
    }
}
