﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;



namespace Gestion_AP
{
    public partial class ModificaUsuario : Form
    {
        public ModificaUsuario(string Usuario, string Nombre, string Apellido, string Nivel)
        {

		//INICIALIZA FORMULARIO CON NIVEL DEL USUARIO LOGUEADO

            InitializeComponent();
            txtUsuario.Text = Usuario;
            txtNombre.Text = Nombre;
            txtApellido.Text = Apellido;
            switch (Nivel == "00")
            {
                case true:
                    rbtAnalista.Checked = true;
                    break;
                case false:
                    rbtAdministrador.Checked = true;
                    break;


            }


        }

        public static string strUsuario = "";

        private void ModificaUsuario_Load(object sender, EventArgs e)
        {
		
		// INICIALIZA FORMULARIO CON UPPER PARA TODOS LOS CAMPOS Y SETEA STRUSUARIO

            txtUsuario.CharacterCasing = CharacterCasing.Upper;
            txtNombre.CharacterCasing = CharacterCasing.Upper;
            txtApellido.CharacterCasing = CharacterCasing.Upper;
            strUsuario = txtUsuario.Text.Trim();


        }



        private void btnConfirmar_Click(object sender, EventArgs e)
        {

            try
            {

			// ABRE CONEXION Y ENVIA COMANDO UPdAtE PARA MODIFICACION			

                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnUsuario;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "UPDATE Usuarios SET Usuario=@Usuario,Nombre=@Nombre,Apellido=@Apellido,NIVEL=@NIVEL WHERE Usuario='" + strUsuario + "'";
                cmd.Parameters.AddWithValue("@Usuario", txtUsuario.Text.Trim());
                cmd.Parameters.AddWithValue("@Nombre", txtNombre.Text.Trim());
                cmd.Parameters.AddWithValue("@Apellido", txtApellido.Text.Trim());
                switch (rbtAnalista.Checked == true)
                {

                    case true:
                        cmd.Parameters.AddWithValue("@NIVEL", "00");
                        break;
                    case false:
                        cmd.Parameters.AddWithValue("@NIVEL", "99");
                        break;

                }

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
                MessageBox.Show("Datos actualizados correctamente", "Actualización Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();



            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo actualizar la tabla Usuarios. Error: " + ex.Message);
                Logins.LogError(ex);

            }


        }

        private void ModificaUsuario_Leave(object sender, EventArgs e)
        {


        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

      

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
