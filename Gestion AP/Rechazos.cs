﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;


namespace Gestion_AP
{
    public partial class Rechazos : Form
    {
        public Rechazos()
        {
            InitializeComponent();
        }
        public static int TotRech;
        public static int TotUs;


        public DataTable tabla;
        private void Rechazos_Load(object sender, EventArgs e)
        {
            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;

            cn.Open();

            try
            {


                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM GESTIONES WHERE ESTADO='RECHAZADO'";
                DataTable dt = new DataTable();
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);
                tabla = dt;
                if (dt.Rows.Count != 0)
                {
                    TotRech = dt.Rows.Count;
                    
                    dgvRechazos.DataSource = dt;
                    dgvRechazos.ReadOnly = true;
                    dgvRechazos.Columns["Id"].Visible = false;
                    dgvRechazos.Columns["US_CONTROL"].Visible = false;
                    dgvRechazos.Columns["FECHA_CONTROL"].Visible = false;
                    dgvRechazos.Columns["US_REPROCESO"].Visible = false;
                    dgvRechazos.Columns["FECHA_REPROCESO"].Visible = false;
                    dgvRechazos.Columns["DOC_ASOCIADO"].Visible = false;

                    dgvRechazos.Columns["ESTADO"].Visible = false;
                    dgvRechazos.Columns["A_CONTROLAR"].Visible = false;
                    dgvRechazos.Columns["CANAL_INGRESO"].HeaderText = "CANAL INGRESO";

                    dgvRechazos.Columns["NRO_TRAMITE"].HeaderText = "NRO.TRAMITE";
                    dgvRechazos.Columns["US_INICIO"].HeaderText = "USUARIO PROCESO";
                    dgvRechazos.Columns["FECHA_INICIO"].HeaderText = "FECHA TRAMITE";
                    dgvRechazos.Columns["WF"].HeaderText = "TIPO";
                    AspectoTabla.AspectoDGV(dgvRechazos, Color.Silver);
                    dgvRechazos.ScrollBars = ScrollBars.Both;
                    cmd.ExecuteNonQuery();
                    string fieldName = string.Concat("[", tabla.Columns[1].ColumnName, "]");
                    tabla.DefaultView.Sort = fieldName;
                    DataView view = tabla.DefaultView;
                    view.RowFilter = string.Empty;

                    view.RowFilter = fieldName + " = '" + Ingreso.UsuarioLogueado + "'";
                    dgvRechazos.DataSource = view;
                    TotUs = dgvRechazos.Rows.Count;

                    txtTotRech.Text = TotRech.ToString();
                    txtTotUs.Text = TotUs.ToString();





                }
               



            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);

            }
            finally
            {
                cn.Close();
            }
        }

        private void btnVertodo_Click(object sender, EventArgs e)
        {
            DataView view = tabla.DefaultView;
            view.RowFilter = string.Empty;
            dgvRechazos.DataSource = view;
        }
        public static DataGridView dgv;
        public static void EliminarRegistro(string ID)

        {

            foreach (DataGridViewRow fila in dgv.Rows)
            {
                if (fila.Cells[10].Value.ToString() == ID)
                {
                    dgv.Rows.Remove(fila);
                }

            }


        }

        private void Rechazos_FormClosed(object sender, FormClosedEventArgs e)
        {

            //try
            //{
            //    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Principal").SingleOrDefault<Form>();
            //    if (existe != null)
            //    {
                    Principal frm = new Principal();
                    frm.Show();
            //    }
            //}
            //catch (Exception ex) { Logins.LogError(ex); }

        }

        public void TextoFiltro(int columna)
        {

            string fieldName = string.Concat("[", tabla.Columns[columna].ColumnName, "]");
            tabla.DefaultView.Sort = fieldName;
            DataView view = tabla.DefaultView;
            view.RowFilter = string.Empty;
            if (txtFiltro.Text != string.Empty)
                view.RowFilter = fieldName + " LIKE '%" + txtFiltro.Text + "%'";
            dgvRechazos.DataSource = view;

        }
        public int Index()
        {
            int ind = 99;
            if (rbtPrio.Checked)
            {
                ind = 12;
            }

            if (rbtCanal.Checked)
            {
                ind = 13;

            }
            if (rbtAdm.Checked)
            {
                ind = 7;
            }
            if (rbtTram.Checked)
            {
                ind = 8;
            }
            if (rbtWf.Checked)
            {
                ind = 9;

            }
            if (rbtUser.Checked)
            {
                ind = 1;
            }


            if (rbtNro.Checked)
            {
                ind = 10;
            }


            return ind;


        }
        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextoFiltro(Index());
            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }

        }
        private void dgvRechazos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                dgv = dgvRechazos;
                string CanalIngreso = Convert.ToString(dgvRechazos.CurrentRow.Cells["CANAL_INGRESO"].Value);
                string IdTramite = Convert.ToString(dgvRechazos.CurrentRow.Cells["NRO_TRAMITE"].Value);
                string Estado = Convert.ToString(dgvRechazos.CurrentRow.Cells["ESTADO"].Value);
                string Prioridad = Convert.ToString(dgvRechazos.CurrentRow.Cells["PRIORIDAD"].Value);

                if (CanalIngreso == "SEGURIDAD" || CanalIngreso == "CALLCENTER/FALLECIDOS")
                {
                    BajasFall frm = new BajasFall(IdTramite, Estado);
                    frm.Show();

                }

                if (CanalIngreso == "MAIL" || CanalIngreso == "VARIOS" || CanalIngreso == "ADJUNTOS" || CanalIngreso == "BATCH")
                {
                    try
                    {
                        GenericoDocs frm = new GenericoDocs(IdTramite, Estado);
                        frm.Show();
                    }
                    catch (Exception ex)
                    {

                        Logins.LogError(ex);
                    }

                }

                if (CanalIngreso == "WORKFLOW")
                {

                    ModuloPurpura frm = new ModuloPurpura(IdTramite, Estado, Prioridad);
                    frm.Show();

                }
                if (CanalIngreso == "ERR-PREVALID")
                {

                    ModuloPrevalid frm = new ModuloPrevalid(IdTramite, Estado, Prioridad);
                    frm.Show();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo falló al abrir trámite. Contactá al administrador.\n" + ex.Message);
                Logins.LogError(ex);
            }

        }
        private void dgvRechazos_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {

                dgvRechazos_CellContentDoubleClick(null, null);

            }
        }

        private void Rechazos_Enter(object sender, EventArgs e)
        {
            if (TotRech>=0)
            {
                txtTotRech.Text = TotRech.ToString();
            }
            if (TotUs>=0)
            {
                txtTotUs.Text = TotUs.ToString();
            }
           
            
        }

        private void btnDelegar_Click(object sender, EventArgs e)
        {
            try
            {
                string Tramite = dgvRechazos.CurrentRow.Cells[10].Value.ToString();
                CambioEstado frm = new CambioEstado(Tramite);
                frm.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Para delegar debe posicionarse sobre un trámite previamente");
                Logins.LogError(ex);
            }
            
        }

        private void btnWorklist_Click(object sender, EventArgs e)
        {
            RechList frm = new RechList (txtTotRech.Text);
            frm.Show();
        }
    }
    }
