﻿namespace Gestion_AP
{
    partial class ABMUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ABMUsuarios));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pbSalir = new System.Windows.Forms.PictureBox();
            this.btnDarBaja = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvVigentes = new System.Windows.Forms.DataGridView();
            this.btnReactivar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvNoVigentes = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVigentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNoVigentes)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Menu;
            this.splitContainer1.Panel1.Controls.Add(this.pbSalir);
            this.splitContainer1.Panel1.Controls.Add(this.btnDarBaja);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dgvVigentes);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.MenuBar;
            this.splitContainer1.Panel2.Controls.Add(this.btnReactivar);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.dgvNoVigentes);
            this.splitContainer1.Size = new System.Drawing.Size(972, 519);
            this.splitContainer1.SplitterDistance = 340;
            this.splitContainer1.TabIndex = 0;
            // 
            // pbSalir
            // 
            this.pbSalir.Image = global::Gestion_AP.Properties.Resources.Entypo_e759_0__128;
            this.pbSalir.Location = new System.Drawing.Point(12, 12);
            this.pbSalir.Name = "pbSalir";
            this.pbSalir.Size = new System.Drawing.Size(59, 50);
            this.pbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSalir.TabIndex = 3;
            this.pbSalir.TabStop = false;
            this.pbSalir.Click += new System.EventHandler(this.pbSalir_Click);
            // 
            // btnDarBaja
            // 
            this.btnDarBaja.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnDarBaja.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDarBaja.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDarBaja.ForeColor = System.Drawing.SystemColors.Control;
            this.btnDarBaja.Location = new System.Drawing.Point(731, 72);
            this.btnDarBaja.Name = "btnDarBaja";
            this.btnDarBaja.Size = new System.Drawing.Size(105, 25);
            this.btnDarBaja.TabIndex = 2;
            this.btnDarBaja.Text = "ELIMINAR";
            this.btnDarBaja.UseVisualStyleBackColor = false;
            this.btnDarBaja.Click += new System.EventHandler(this.btnDarBaja_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(19, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "USUARIOS VIGENTES";
            // 
            // dgvVigentes
            // 
            this.dgvVigentes.AllowUserToAddRows = false;
            this.dgvVigentes.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dgvVigentes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvVigentes.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dgvVigentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVigentes.Location = new System.Drawing.Point(23, 100);
            this.dgvVigentes.Name = "dgvVigentes";
            this.dgvVigentes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVigentes.Size = new System.Drawing.Size(924, 174);
            this.dgvVigentes.TabIndex = 0;
            this.dgvVigentes.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVigentes_CellContentDoubleClick);
            // 
            // btnReactivar
            // 
            this.btnReactivar.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnReactivar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReactivar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReactivar.ForeColor = System.Drawing.SystemColors.Control;
            this.btnReactivar.Location = new System.Drawing.Point(731, 11);
            this.btnReactivar.Name = "btnReactivar";
            this.btnReactivar.Size = new System.Drawing.Size(105, 25);
            this.btnReactivar.TabIndex = 3;
            this.btnReactivar.Text = "REACTIVAR";
            this.btnReactivar.UseVisualStyleBackColor = false;
            this.btnReactivar.Click += new System.EventHandler(this.btnReactivar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(19, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(228, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "USUARIOS NO VIGENTES";
            // 
            // dgvNoVigentes
            // 
            this.dgvNoVigentes.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dgvNoVigentes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvNoVigentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNoVigentes.Location = new System.Drawing.Point(23, 39);
            this.dgvNoVigentes.Name = "dgvNoVigentes";
            this.dgvNoVigentes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNoVigentes.Size = new System.Drawing.Size(924, 86);
            this.dgvNoVigentes.TabIndex = 1;
            // 
            // ABMUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(972, 519);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ABMUsuarios";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MODIFICACION USUARIOS";
            this.Activated += new System.EventHandler(this.ABMUsuarios_Activated);
            this.Load += new System.EventHandler(this.ABMUsuarios_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVigentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNoVigentes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvVigentes;
        private System.Windows.Forms.DataGridView dgvNoVigentes;
        private System.Windows.Forms.Button btnDarBaja;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnReactivar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbSalir;
    }
}