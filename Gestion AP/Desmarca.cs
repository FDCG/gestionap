﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;

using System.Windows.Forms;
using System.Data.OleDb;


namespace Gestion_AP
{
    public partial class Desmarca : Form
    {

        UsuarioDaoImpl usuarioDaoImpl;

        public Desmarca()
        {

            InitializeComponent();

            txtUsuario.CharacterCasing = CharacterCasing.Upper;

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void desmarcarUsuario(string iUsuario)
        {
            Usuario usuario = usuarioDaoImpl.getByIdUsuario(iUsuario);

            if(usuario == null)
            {
                throw new ArgumentException("Usuario inexistente");
            }

            usuario.Activo = false;
            usuarioDaoImpl.updateUsuario(usuario);
        }

        private void desbloquearUsuario(string iUsuario)
        {
            Usuario usuario = usuarioDaoImpl.getByIdUsuario(iUsuario);

            if (usuario == null)
            {
                throw new ArgumentException("Usuario inexistente");
            }

            usuario.Contrasenia = Logins.CONTRASENIA_BLANQUEADA;
            usuario.VtoClave = new DateTime(1, 1, 1);
            usuario.Bloqueado = false;
            usuario.IntentosFallidos = 0;
            usuarioDaoImpl.updateUsuario(usuario);

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {

            usuarioDaoImpl = new UsuarioDaoImpl();

            string iUsuario = txtUsuario.Text.Trim();

            try
            {
                if (Usuario.usuarioSingleton.iUsuario.Equals(iUsuario))
                {
                    throw new ArgumentException("No puede accionar sobre su propio usuario.");
                }


                if (rbtDesmarcar.Checked)
                {
                    desmarcarUsuario(iUsuario);
                    MessageBox.Show("Usuario desmarcado.");
                }
                else if (rbtBlanquear.Checked)
                {
                    desbloquearUsuario(iUsuario);
                    MessageBox.Show("Usuario desbloqueado.");
                }
                else
                {
                    MessageBox.Show("Debe seleccionar una opción.");
                }

            }
            catch (ArgumentException argException)
            {
                MessageBox.Show(argException.Message);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

            txtUsuario.Clear();


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}


