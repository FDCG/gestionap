﻿namespace Gestion_AP
{
    partial class ModuloVerde
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModuloVerde));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLimite = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTipoTJ = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtAdministradora = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCtaDeb = new System.Windows.Forms.TextBox();
            this.DNI = new System.Windows.Forms.Label();
            this.txtDni = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDenomCte = new System.Windows.Forms.TextBox();
            this.txtPersona = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCodGeo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDomicilio = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtCodOca = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtSucEntrega = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtDomEsp = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtInfAdic = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtUserAlta = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMotivo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dgwTarjetas = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgwHistorico = new System.Windows.Forms.DataGridView();
            this.pagProceso = new System.Windows.Forms.TabPage();
            this.gbHistorial = new System.Windows.Forms.GroupBox();
            this.dgvProcesos = new System.Windows.Forms.DataGridView();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.txtPrioridad = new System.Windows.Forms.TextBox();
            this.txtCargar = new System.Windows.Forms.Button();
            this.txtNroTramite = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTramite = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtAdicComent = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwTarjetas)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwHistorico)).BeginInit();
            this.pagProceso.SuspendLayout();
            this.gbHistorial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcesos)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.pagProceso);
            this.tabControl1.Location = new System.Drawing.Point(44, 66);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(658, 293);
            this.tabControl1.TabIndex = 31;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtCliente);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.DNI);
            this.tabPage1.Controls.Add(this.txtDni);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtDenomCte);
            this.tabPage1.Controls.Add(this.txtPersona);
            this.tabPage1.Controls.Add(this.txtNombre);
            this.tabPage1.Controls.Add(this.txtTelefono);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(650, 267);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "DATOS GENERALES";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(297, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 15);
            this.label6.TabIndex = 65;
            this.label6.Text = "CLIENTE BT";
            // 
            // txtCliente
            // 
            this.txtCliente.Location = new System.Drawing.Point(383, 28);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.ReadOnly = true;
            this.txtCliente.Size = new System.Drawing.Size(192, 20);
            this.txtCliente.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtLimite);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtTipoTJ);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtAdministradora);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtCtaDeb);
            this.groupBox1.Location = new System.Drawing.Point(85, 121);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 129);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ADMINISTRACION DE Tarjetas";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 15);
            this.label5.TabIndex = 62;
            this.label5.Text = "LIMITE DE COMPRA";
            // 
            // txtLimite
            // 
            this.txtLimite.Location = new System.Drawing.Point(134, 75);
            this.txtLimite.Name = "txtLimite";
            this.txtLimite.ReadOnly = true;
            this.txtLimite.Size = new System.Drawing.Size(295, 20);
            this.txtLimite.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 15);
            this.label4.TabIndex = 60;
            this.label4.Text = "TIPO DE TARJETA";
            // 
            // txtTipoTJ
            // 
            this.txtTipoTJ.Location = new System.Drawing.Point(134, 49);
            this.txtTipoTJ.Name = "txtTipoTJ";
            this.txtTipoTJ.ReadOnly = true;
            this.txtTipoTJ.Size = new System.Drawing.Size(295, 20);
            this.txtTipoTJ.TabIndex = 13;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(10, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(104, 15);
            this.label18.TabIndex = 56;
            this.label18.Text = "ADMINISTRADORA";
            // 
            // txtAdministradora
            // 
            this.txtAdministradora.Location = new System.Drawing.Point(134, 23);
            this.txtAdministradora.Name = "txtAdministradora";
            this.txtAdministradora.ReadOnly = true;
            this.txtAdministradora.Size = new System.Drawing.Size(295, 20);
            this.txtAdministradora.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(10, 103);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(90, 15);
            this.label19.TabIndex = 58;
            this.label19.Text = "CUENTA DEBITO";
            // 
            // txtCtaDeb
            // 
            this.txtCtaDeb.Location = new System.Drawing.Point(134, 101);
            this.txtCtaDeb.Name = "txtCtaDeb";
            this.txtCtaDeb.ReadOnly = true;
            this.txtCtaDeb.Size = new System.Drawing.Size(295, 20);
            this.txtCtaDeb.TabIndex = 15;
            // 
            // DNI
            // 
            this.DNI.AutoSize = true;
            this.DNI.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DNI.Location = new System.Drawing.Point(18, 58);
            this.DNI.Name = "DNI";
            this.DNI.Size = new System.Drawing.Size(26, 15);
            this.DNI.TabIndex = 54;
            this.DNI.Text = "DNI";
            // 
            // txtDni
            // 
            this.txtDni.Location = new System.Drawing.Point(99, 55);
            this.txtDni.Name = "txtDni";
            this.txtDni.ReadOnly = true;
            this.txtDni.Size = new System.Drawing.Size(192, 20);
            this.txtDni.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(297, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 15);
            this.label10.TabIndex = 50;
            this.label10.Text = "DENOM. CLTE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(297, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 15);
            this.label11.TabIndex = 49;
            this.label11.Text = "NOMBRE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 47;
            this.label2.Text = "CUIL";
            // 
            // txtDenomCte
            // 
            this.txtDenomCte.Location = new System.Drawing.Point(383, 82);
            this.txtDenomCte.Name = "txtDenomCte";
            this.txtDenomCte.ReadOnly = true;
            this.txtDenomCte.Size = new System.Drawing.Size(218, 20);
            this.txtDenomCte.TabIndex = 11;
            // 
            // txtPersona
            // 
            this.txtPersona.Location = new System.Drawing.Point(99, 28);
            this.txtPersona.Name = "txtPersona";
            this.txtPersona.ReadOnly = true;
            this.txtPersona.Size = new System.Drawing.Size(192, 20);
            this.txtPersona.TabIndex = 6;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(383, 55);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(218, 20);
            this.txtNombre.TabIndex = 9;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(99, 82);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.ReadOnly = true;
            this.txtTelefono.Size = new System.Drawing.Size(192, 20);
            this.txtTelefono.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 15);
            this.label7.TabIndex = 51;
            this.label7.Text = "TELEFONO";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.txtCodGeo);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.txtDomicilio);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.txtCodOca);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.txtSucEntrega);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.txtDomEsp);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(650, 267);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "DISTRIBUCION";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(13, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 15);
            this.label8.TabIndex = 66;
            this.label8.Text = "COD. GEOGRAFICO";
            // 
            // txtCodGeo
            // 
            this.txtCodGeo.Location = new System.Drawing.Point(144, 76);
            this.txtCodGeo.Name = "txtCodGeo";
            this.txtCodGeo.ReadOnly = true;
            this.txtCodGeo.Size = new System.Drawing.Size(459, 20);
            this.txtCodGeo.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 15);
            this.label3.TabIndex = 64;
            this.label3.Text = "DOMICILIO ACTUAL";
            // 
            // txtDomicilio
            // 
            this.txtDomicilio.Location = new System.Drawing.Point(144, 51);
            this.txtDomicilio.Name = "txtDomicilio";
            this.txtDomicilio.ReadOnly = true;
            this.txtDomicilio.Size = new System.Drawing.Size(459, 20);
            this.txtDomicilio.TabIndex = 18;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(13, 128);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(80, 15);
            this.label31.TabIndex = 62;
            this.label31.Text = "CODIGO OCA";
            // 
            // txtCodOca
            // 
            this.txtCodOca.Location = new System.Drawing.Point(144, 126);
            this.txtCodOca.Name = "txtCodOca";
            this.txtCodOca.ReadOnly = true;
            this.txtCodOca.Size = new System.Drawing.Size(459, 20);
            this.txtCodOca.TabIndex = 21;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(13, 103);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(131, 15);
            this.label30.TabIndex = 60;
            this.label30.Text = "SUCURSAL DE ENTREGA";
            // 
            // txtSucEntrega
            // 
            this.txtSucEntrega.Location = new System.Drawing.Point(144, 101);
            this.txtSucEntrega.Name = "txtSucEntrega";
            this.txtSucEntrega.ReadOnly = true;
            this.txtSucEntrega.Size = new System.Drawing.Size(459, 20);
            this.txtSucEntrega.TabIndex = 20;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(13, 28);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(125, 15);
            this.label29.TabIndex = 58;
            this.label29.Text = "¿DOMICILIO ESPECIAL?";
            // 
            // txtDomEsp
            // 
            this.txtDomEsp.Location = new System.Drawing.Point(144, 26);
            this.txtDomEsp.Name = "txtDomEsp";
            this.txtDomEsp.ReadOnly = true;
            this.txtDomEsp.Size = new System.Drawing.Size(48, 20);
            this.txtDomEsp.TabIndex = 17;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(650, 267);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "INF. ADICIONAL";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtInfAdic);
            this.groupBox5.ForeColor = System.Drawing.Color.OrangeRed;
            this.groupBox5.Location = new System.Drawing.Point(3, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(644, 94);
            this.groupBox5.TabIndex = 64;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "DATOS ADICIONALES";
            // 
            // txtInfAdic
            // 
            this.txtInfAdic.Location = new System.Drawing.Point(16, 20);
            this.txtInfAdic.Multiline = true;
            this.txtInfAdic.Name = "txtInfAdic";
            this.txtInfAdic.ReadOnly = true;
            this.txtInfAdic.Size = new System.Drawing.Size(622, 67);
            this.txtInfAdic.TabIndex = 23;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtUserAlta);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.txtObservaciones);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.txtMotivo);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.ForeColor = System.Drawing.Color.DarkViolet;
            this.groupBox2.Location = new System.Drawing.Point(3, 113);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(644, 144);
            this.groupBox2.TabIndex = 63;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DATOS TRAMITE";
            // 
            // txtUserAlta
            // 
            this.txtUserAlta.Location = new System.Drawing.Point(108, 113);
            this.txtUserAlta.Name = "txtUserAlta";
            this.txtUserAlta.ReadOnly = true;
            this.txtUserAlta.Size = new System.Drawing.Size(146, 20);
            this.txtUserAlta.TabIndex = 26;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(20, 116);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 15);
            this.label27.TabIndex = 60;
            this.label27.Text = "USUARIO ALTA";
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Location = new System.Drawing.Point(108, 59);
            this.txtObservaciones.Multiline = true;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.ReadOnly = true;
            this.txtObservaciones.Size = new System.Drawing.Size(530, 47);
            this.txtObservaciones.TabIndex = 25;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(4, 73);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(98, 15);
            this.label26.TabIndex = 58;
            this.label26.Text = "OBSERVACIONES";
            // 
            // txtMotivo
            // 
            this.txtMotivo.Location = new System.Drawing.Point(108, 32);
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.ReadOnly = true;
            this.txtMotivo.Size = new System.Drawing.Size(530, 20);
            this.txtMotivo.TabIndex = 24;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(50, 35);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(50, 15);
            this.label25.TabIndex = 56;
            this.label25.Text = "MOTIVO";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox7);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(650, 267);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Tarjetas";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dgwTarjetas);
            this.groupBox7.ForeColor = System.Drawing.Color.Red;
            this.groupBox7.Location = new System.Drawing.Point(5, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(642, 242);
            this.groupBox7.TabIndex = 66;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Tarjetas DE LA CUENTA";
            // 
            // dgwTarjetas
            // 
            this.dgwTarjetas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwTarjetas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgwTarjetas.Location = new System.Drawing.Point(3, 16);
            this.dgwTarjetas.Name = "dgwTarjetas";
            this.dgwTarjetas.Size = new System.Drawing.Size(636, 223);
            this.dgwTarjetas.TabIndex = 28;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox3);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(650, 267);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Procesos";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgwHistorico);
            this.groupBox3.ForeColor = System.Drawing.Color.ForestGreen;
            this.groupBox3.Location = new System.Drawing.Point(5, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(642, 242);
            this.groupBox3.TabIndex = 66;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "HISTORICO TRAMITE";
            // 
            // dgwHistorico
            // 
            this.dgwHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwHistorico.Location = new System.Drawing.Point(3, 16);
            this.dgwHistorico.Name = "dgwHistorico";
            this.dgwHistorico.Size = new System.Drawing.Size(633, 223);
            this.dgwHistorico.TabIndex = 30;
            // 
            // pagProceso
            // 
            this.pagProceso.Controls.Add(this.gbHistorial);
            this.pagProceso.Location = new System.Drawing.Point(4, 22);
            this.pagProceso.Name = "pagProceso";
            this.pagProceso.Padding = new System.Windows.Forms.Padding(3);
            this.pagProceso.Size = new System.Drawing.Size(650, 267);
            this.pagProceso.TabIndex = 5;
            this.pagProceso.Text = "HIST. PROCESO";
            this.pagProceso.UseVisualStyleBackColor = true;
            // 
            // gbHistorial
            // 
            this.gbHistorial.Controls.Add(this.dgvProcesos);
            this.gbHistorial.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbHistorial.ForeColor = System.Drawing.Color.MidnightBlue;
            this.gbHistorial.Location = new System.Drawing.Point(3, 3);
            this.gbHistorial.Name = "gbHistorial";
            this.gbHistorial.Size = new System.Drawing.Size(641, 261);
            this.gbHistorial.TabIndex = 43;
            this.gbHistorial.TabStop = false;
            this.gbHistorial.Text = "Historial Proceso";
            // 
            // dgvProcesos
            // 
            this.dgvProcesos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProcesos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProcesos.Location = new System.Drawing.Point(3, 17);
            this.dgvProcesos.Name = "dgvProcesos";
            this.dgvProcesos.Size = new System.Drawing.Size(635, 241);
            this.dgvProcesos.TabIndex = 32;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(597, 12);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(74, 21);
            this.btnGuardar.TabIndex = 35;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(299, 15);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(65, 15);
            this.label32.TabIndex = 69;
            this.label32.Text = "PRIORIDAD";
            // 
            // txtPrioridad
            // 
            this.txtPrioridad.Location = new System.Drawing.Point(373, 12);
            this.txtPrioridad.Name = "txtPrioridad";
            this.txtPrioridad.Size = new System.Drawing.Size(114, 20);
            this.txtPrioridad.TabIndex = 3;
            // 
            // txtCargar
            // 
            this.txtCargar.Location = new System.Drawing.Point(518, 12);
            this.txtCargar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCargar.Name = "txtCargar";
            this.txtCargar.Size = new System.Drawing.Size(74, 21);
            this.txtCargar.TabIndex = 34;
            this.txtCargar.Text = "CARGAR";
            this.txtCargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.txtCargar.UseVisualStyleBackColor = true;
            this.txtCargar.Click += new System.EventHandler(this.txtCargar_Click);
            // 
            // txtNroTramite
            // 
            this.txtNroTramite.Location = new System.Drawing.Point(150, 12);
            this.txtNroTramite.Name = "txtNroTramite";
            this.txtNroTramite.ReadOnly = true;
            this.txtNroTramite.Size = new System.Drawing.Size(143, 20);
            this.txtNroTramite.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.TabIndex = 67;
            this.label1.Text = "TRAMITE";
            // 
            // txtTramite
            // 
            this.txtTramite.Location = new System.Drawing.Point(101, 12);
            this.txtTramite.Name = "txtTramite";
            this.txtTramite.ReadOnly = true;
            this.txtTramite.Size = new System.Drawing.Size(43, 20);
            this.txtTramite.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(41, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 15);
            this.label12.TabIndex = 48;
            this.label12.Text = "DESCRIPCION";
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(129, 38);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.ReadOnly = true;
            this.txtDesc.Size = new System.Drawing.Size(358, 20);
            this.txtDesc.TabIndex = 4;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtAdicComent);
            this.groupBox4.ForeColor = System.Drawing.Color.AliceBlue;
            this.groupBox4.Location = new System.Drawing.Point(44, 370);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(658, 79);
            this.groupBox4.TabIndex = 71;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "COMENTARIO ADICIONAL";
            // 
            // txtAdicComent
            // 
            this.txtAdicComent.Location = new System.Drawing.Point(12, 20);
            this.txtAdicComent.Multiline = true;
            this.txtAdicComent.Name = "txtAdicComent";
            this.txtAdicComent.Size = new System.Drawing.Size(635, 20);
            this.txtAdicComent.TabIndex = 33;
            // 
            // ModuloVerde
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.SeaGreen;
            this.ClientSize = new System.Drawing.Size(747, 461);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.txtPrioridad);
            this.Controls.Add(this.txtCargar);
            this.Controls.Add(this.txtNroTramite);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTramite);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtDesc);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ModuloVerde";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Módulo Verde";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgwTarjetas)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgwHistorico)).EndInit();
            this.pagProceso.ResumeLayout(false);
            this.gbHistorial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcesos)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtPrioridad;
        private System.Windows.Forms.Button txtCargar;
        private System.Windows.Forms.TextBox txtNroTramite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTramite;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtAdministradora;
        private System.Windows.Forms.Label DNI;
        private System.Windows.Forms.TextBox txtDni;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtCtaDeb;
        private System.Windows.Forms.TextBox txtDenomCte;
        private System.Windows.Forms.TextBox txtPersona;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDomicilio;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtCodOca;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtSucEntrega;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtDomEsp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLimite;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTipoTJ;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtInfAdic;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtUserAlta;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtMotivo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dgwTarjetas;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgwHistorico;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCodGeo;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtAdicComent;
        private System.Windows.Forms.TabPage pagProceso;
        private System.Windows.Forms.GroupBox gbHistorial;
        private System.Windows.Forms.DataGridView dgvProcesos;
    }
}