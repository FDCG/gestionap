﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;

using System.Windows.Forms;
using System.Data.OleDb;


namespace Gestion_AP
{
    public partial class Activos : Form
    {
        List<Usuario> usuarios;
        UsuarioDaoImpl usuarioDaoImpl;
        DataSet dsUsuarios;

        public Activos()
        {
            InitializeComponent();

            usuarioDaoImpl = new UsuarioDaoImpl();

            dsUsuarios = new DataSet("Usuarios Activos");

            try
            {
                usuarios = usuarioDaoImpl.getUsuarios();

                //dsUsuarios = getDataSetFromList(usuarios);

                //DataGridView dgvUsuarioActivos = new DataGridView();
                //dgvUsuarioActivos.DataSource = usuarios;


            }catch(Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

            



        }

        //private DataSet getDataSetFromList(List<Usuario> usuarios)
        //{
        //    DataSet ds = new DataSet();



        //}

        private void Activos_Load(object sender, EventArgs e)
        {
            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnUsuario;

            cn.Open();
            try
            {

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM Usuarios WHERE ACTIVO=" + true + "";
                DataTable dt = new DataTable();
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);

                gridviewActivos.DataSource = dt;
                gridviewActivos.Columns["Id"].Visible = false;
                gridviewActivos.Columns["Contraseña"].Visible = false;
                gridviewActivos.Columns["Fecha_Alta"].Visible = false;
                gridviewActivos.Columns["Fecha_Baja"].Visible = false;
                gridviewActivos.Columns["Nivel"].Visible = false;
                gridviewActivos.Columns["ACTIVO"].Visible = false;
                gridviewActivos.Columns["VTO_CLAVE"].Visible = false;
                gridviewActivos.Columns["BLOQUEADO"].Visible = false;
                gridviewActivos.Columns["INTENTOS"].Visible = false;
                gridviewActivos.Columns["DOMINIO"].Visible = false;
                gridviewActivos.Columns["Ultimo_Ingreso"].HeaderText = "Último Ingreso";
                gridviewActivos.Columns["Ultimo_Egreso"].Visible = false;
                gridviewActivos.ReadOnly = true;
                AspectoTabla.AspectoDGV(gridviewActivos, Color.White);



                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);


            }
            finally { cn.Close(); }









        }

        private void gridviewActivos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pbVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
