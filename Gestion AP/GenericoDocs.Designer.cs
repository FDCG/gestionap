﻿namespace Gestion_AP
{
    partial class GenericoDocs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnReprocesar = new System.Windows.Forms.Button();
            this.btnControlado = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvHistorico = new System.Windows.Forms.DataGridView();
            this.txtPrioridad = new System.Windows.Forms.TextBox();
            this.txtAdm = new System.Windows.Forms.TextBox();
            this.txtCanal = new System.Windows.Forms.TextBox();
            this.txtNovedad = new System.Windows.Forms.TextBox();
            this.txtTramite = new System.Windows.Forms.TextBox();
            this.txtNroTram = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.wbMail = new System.Windows.Forms.WebBrowser();
            this.Secciones = new System.Windows.Forms.TabControl();
            this.pagMail = new System.Windows.Forms.TabPage();
            this.pagImagen = new System.Windows.Forms.TabPage();
            this.pbImagen = new System.Windows.Forms.PictureBox();
            this.pagAdjuntos = new System.Windows.Forms.TabPage();
            this.dgvAdjuntos = new System.Windows.Forms.DataGridView();
            this.pagComent = new System.Windows.Forms.TabPage();
            this.txtComAdic = new System.Windows.Forms.TextBox();
            this.pagBatch = new System.Windows.Forms.TabPage();
            this.txtRechazo = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorico)).BeginInit();
            this.Secciones.SuspendLayout();
            this.pagMail.SuspendLayout();
            this.pagImagen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).BeginInit();
            this.pagAdjuntos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdjuntos)).BeginInit();
            this.pagComent.SuspendLayout();
            this.pagBatch.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnReprocesar);
            this.groupBox1.Controls.Add(this.btnControlado);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.txtPrioridad);
            this.groupBox1.Controls.Add(this.txtAdm);
            this.groupBox1.Controls.Add(this.txtCanal);
            this.groupBox1.Controls.Add(this.txtNovedad);
            this.groupBox1.Controls.Add(this.txtTramite);
            this.groupBox1.Controls.Add(this.txtNroTram);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.ForestGreen;
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(908, 208);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Trámite";
            // 
            // btnReprocesar
            // 
            this.btnReprocesar.ForeColor = System.Drawing.Color.Firebrick;
            this.btnReprocesar.Location = new System.Drawing.Point(788, 19);
            this.btnReprocesar.Name = "btnReprocesar";
            this.btnReprocesar.Size = new System.Drawing.Size(107, 23);
            this.btnReprocesar.TabIndex = 8;
            this.btnReprocesar.Text = "Reprocesar";
            this.btnReprocesar.UseVisualStyleBackColor = true;
            // 
            // btnControlado
            // 
            this.btnControlado.Location = new System.Drawing.Point(788, 48);
            this.btnControlado.Name = "btnControlado";
            this.btnControlado.Size = new System.Drawing.Size(107, 23);
            this.btnControlado.TabIndex = 9;
            this.btnControlado.Text = "Controlado";
            this.btnControlado.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvHistorico);
            this.groupBox2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBox2.Location = new System.Drawing.Point(6, 77);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(892, 125);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Historial de Proceso";
            // 
            // dgvHistorico
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHistorico.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistorico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvHistorico.Location = new System.Drawing.Point(3, 19);
            this.dgvHistorico.Name = "dgvHistorico";
            this.dgvHistorico.Size = new System.Drawing.Size(886, 103);
            this.dgvHistorico.TabIndex = 7;
            this.dgvHistorico.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHistorico_CellContentDoubleClick);
            // 
            // txtPrioridad
            // 
            this.txtPrioridad.Location = new System.Drawing.Point(100, 48);
            this.txtPrioridad.Name = "txtPrioridad";
            this.txtPrioridad.ReadOnly = true;
            this.txtPrioridad.Size = new System.Drawing.Size(147, 23);
            this.txtPrioridad.TabIndex = 4;
            // 
            // txtAdm
            // 
            this.txtAdm.Location = new System.Drawing.Point(380, 48);
            this.txtAdm.Name = "txtAdm";
            this.txtAdm.ReadOnly = true;
            this.txtAdm.Size = new System.Drawing.Size(134, 23);
            this.txtAdm.TabIndex = 5;
            // 
            // txtCanal
            // 
            this.txtCanal.Location = new System.Drawing.Point(647, 48);
            this.txtCanal.Name = "txtCanal";
            this.txtCanal.ReadOnly = true;
            this.txtCanal.Size = new System.Drawing.Size(119, 23);
            this.txtCanal.TabIndex = 6;
            // 
            // txtNovedad
            // 
            this.txtNovedad.Location = new System.Drawing.Point(708, 20);
            this.txtNovedad.Name = "txtNovedad";
            this.txtNovedad.ReadOnly = true;
            this.txtNovedad.Size = new System.Drawing.Size(58, 23);
            this.txtNovedad.TabIndex = 3;
            // 
            // txtTramite
            // 
            this.txtTramite.Location = new System.Drawing.Point(315, 19);
            this.txtTramite.Name = "txtTramite";
            this.txtTramite.ReadOnly = true;
            this.txtTramite.Size = new System.Drawing.Size(313, 23);
            this.txtTramite.TabIndex = 2;
            // 
            // txtNroTram
            // 
            this.txtNroTram.Location = new System.Drawing.Point(100, 19);
            this.txtNroTram.Name = "txtNroTram";
            this.txtNroTram.ReadOnly = true;
            this.txtNroTram.Size = new System.Drawing.Size(147, 23);
            this.txtNroTram.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(6, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Prioridad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(253, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Administradora";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(520, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Canal de Ingreso";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(634, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Novedad";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(253, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Trámite";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nro. Trámite";
            // 
            // wbMail
            // 
            this.wbMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbMail.Location = new System.Drawing.Point(3, 3);
            this.wbMail.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbMail.Name = "wbMail";
            this.wbMail.Size = new System.Drawing.Size(895, 397);
            this.wbMail.TabIndex = 1;
            // 
            // Secciones
            // 
            this.Secciones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Secciones.Controls.Add(this.pagMail);
            this.Secciones.Controls.Add(this.pagImagen);
            this.Secciones.Controls.Add(this.pagAdjuntos);
            this.Secciones.Controls.Add(this.pagBatch);
            this.Secciones.Controls.Add(this.pagComent);
            this.Secciones.Location = new System.Drawing.Point(13, 224);
            this.Secciones.Name = "Secciones";
            this.Secciones.SelectedIndex = 0;
            this.Secciones.Size = new System.Drawing.Size(909, 432);
            this.Secciones.TabIndex = 2;
            // 
            // pagMail
            // 
            this.pagMail.Controls.Add(this.wbMail);
            this.pagMail.Location = new System.Drawing.Point(4, 25);
            this.pagMail.Name = "pagMail";
            this.pagMail.Padding = new System.Windows.Forms.Padding(3);
            this.pagMail.Size = new System.Drawing.Size(901, 403);
            this.pagMail.TabIndex = 0;
            this.pagMail.Text = "MAIL";
            this.pagMail.UseVisualStyleBackColor = true;
            // 
            // pagImagen
            // 
            this.pagImagen.Controls.Add(this.pbImagen);
            this.pagImagen.Location = new System.Drawing.Point(4, 25);
            this.pagImagen.Name = "pagImagen";
            this.pagImagen.Padding = new System.Windows.Forms.Padding(3);
            this.pagImagen.Size = new System.Drawing.Size(901, 403);
            this.pagImagen.TabIndex = 1;
            this.pagImagen.Text = "IMAGEN";
            this.pagImagen.UseVisualStyleBackColor = true;
            // 
            // pbImagen
            // 
            this.pbImagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbImagen.Location = new System.Drawing.Point(3, 3);
            this.pbImagen.Name = "pbImagen";
            this.pbImagen.Size = new System.Drawing.Size(895, 397);
            this.pbImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagen.TabIndex = 0;
            this.pbImagen.TabStop = false;
            this.pbImagen.DoubleClick += new System.EventHandler(this.pbImagen_DoubleClick);
            // 
            // pagAdjuntos
            // 
            this.pagAdjuntos.Controls.Add(this.dgvAdjuntos);
            this.pagAdjuntos.Location = new System.Drawing.Point(4, 25);
            this.pagAdjuntos.Name = "pagAdjuntos";
            this.pagAdjuntos.Padding = new System.Windows.Forms.Padding(3);
            this.pagAdjuntos.Size = new System.Drawing.Size(901, 403);
            this.pagAdjuntos.TabIndex = 2;
            this.pagAdjuntos.Text = "ADJUNTOS";
            this.pagAdjuntos.UseVisualStyleBackColor = true;
            // 
            // dgvAdjuntos
            // 
            this.dgvAdjuntos.AllowUserToAddRows = false;
            this.dgvAdjuntos.AllowUserToDeleteRows = false;
            this.dgvAdjuntos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAdjuntos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAdjuntos.Location = new System.Drawing.Point(3, 3);
            this.dgvAdjuntos.Name = "dgvAdjuntos";
            this.dgvAdjuntos.ReadOnly = true;
            this.dgvAdjuntos.Size = new System.Drawing.Size(895, 397);
            this.dgvAdjuntos.TabIndex = 0;
            this.dgvAdjuntos.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAdjuntos_CellContentDoubleClick);
            // 
            // pagComent
            // 
            this.pagComent.BackColor = System.Drawing.Color.NavajoWhite;
            this.pagComent.Controls.Add(this.txtComAdic);
            this.pagComent.ForeColor = System.Drawing.Color.Red;
            this.pagComent.Location = new System.Drawing.Point(4, 25);
            this.pagComent.Name = "pagComent";
            this.pagComent.Padding = new System.Windows.Forms.Padding(3);
            this.pagComent.Size = new System.Drawing.Size(901, 403);
            this.pagComent.TabIndex = 3;
            this.pagComent.Text = "COMENTARIO ADICIONAL";
            // 
            // txtComAdic
            // 
            this.txtComAdic.Location = new System.Drawing.Point(7, 7);
            this.txtComAdic.Multiline = true;
            this.txtComAdic.Name = "txtComAdic";
            this.txtComAdic.Size = new System.Drawing.Size(888, 167);
            this.txtComAdic.TabIndex = 0;
            // 
            // pagBatch
            // 
            this.pagBatch.Controls.Add(this.txtRechazo);
            this.pagBatch.Location = new System.Drawing.Point(4, 25);
            this.pagBatch.Name = "pagBatch";
            this.pagBatch.Padding = new System.Windows.Forms.Padding(3);
            this.pagBatch.Size = new System.Drawing.Size(901, 403);
            this.pagBatch.TabIndex = 4;
            this.pagBatch.Text = "RECHAZO BATCH";
            this.pagBatch.UseVisualStyleBackColor = true;
            // 
            // txtRechazo
            // 
            this.txtRechazo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRechazo.Location = new System.Drawing.Point(3, 3);
            this.txtRechazo.Name = "txtRechazo";
            this.txtRechazo.ReadOnly = true;
            this.txtRechazo.Size = new System.Drawing.Size(895, 397);
            this.txtRechazo.TabIndex = 0;
            this.txtRechazo.Text = "";
            // 
            // GenericoDocs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 668);
            this.Controls.Add(this.Secciones);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "GenericoDocs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de Procesos";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorico)).EndInit();
            this.Secciones.ResumeLayout(false);
            this.pagMail.ResumeLayout(false);
            this.pagImagen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).EndInit();
            this.pagAdjuntos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdjuntos)).EndInit();
            this.pagComent.ResumeLayout(false);
            this.pagComent.PerformLayout();
            this.pagBatch.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvHistorico;
        private System.Windows.Forms.TextBox txtPrioridad;
        private System.Windows.Forms.TextBox txtAdm;
        private System.Windows.Forms.TextBox txtCanal;
        private System.Windows.Forms.TextBox txtNovedad;
        private System.Windows.Forms.TextBox txtTramite;
        private System.Windows.Forms.TextBox txtNroTram;
        private System.Windows.Forms.WebBrowser wbMail;
        private System.Windows.Forms.Button btnReprocesar;
        private System.Windows.Forms.Button btnControlado;
        private System.Windows.Forms.TabControl Secciones;
        private System.Windows.Forms.TabPage pagMail;
        private System.Windows.Forms.TabPage pagImagen;
        private System.Windows.Forms.PictureBox pbImagen;
        private System.Windows.Forms.TabPage pagAdjuntos;
        private System.Windows.Forms.DataGridView dgvAdjuntos;
        private System.Windows.Forms.TabPage pagComent;
        private System.Windows.Forms.TextBox txtComAdic;
        private System.Windows.Forms.TabPage pagBatch;
        private System.Windows.Forms.RichTextBox txtRechazo;
    }
}