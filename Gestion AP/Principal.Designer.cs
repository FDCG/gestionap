﻿namespace Gestion_AP
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblstripUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstripDiaHora = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.pbUsuarios = new System.Windows.Forms.PictureBox();
            this.pbTramites = new System.Windows.Forms.PictureBox();
            this.pbAjustes = new System.Windows.Forms.PictureBox();
            this.tblLayoutUsuarios = new System.Windows.Forms.TableLayoutPanel();
            this.btnDesmarcar = new System.Windows.Forms.Button();
            this.btnCambio = new System.Windows.Forms.Button();
            this.btnUsuariosActivos = new System.Windows.Forms.Button();
            this.btnAdmUsuarios = new System.Windows.Forms.Button();
            this.btnAgregarUsuario = new System.Windows.Forms.Button();
            this.tblLayoutTramites = new System.Windows.Forms.TableLayoutPanel();
            this.btnRech = new System.Windows.Forms.Button();
            this.btnConsultas = new System.Windows.Forms.Button();
            this.btnGestiones = new System.Windows.Forms.Button();
            this.btnCambioEstado = new System.Windows.Forms.Button();
            this.btnControl = new System.Windows.Forms.Button();
            this.btnRechBatch = new System.Windows.Forms.Button();
            this.tblLayoutAjustes = new System.Windows.Forms.TableLayoutPanel();
            this.btnNormalizacion = new System.Windows.Forms.Button();
            this.btnMantenimiento = new System.Windows.Forms.Button();
            this.btnEstadistic = new System.Windows.Forms.Button();
            this.btnAcerca = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.PictureBox();
            this.btnContador = new System.Windows.Forms.Button();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTramites)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAjustes)).BeginInit();
            this.tblLayoutUsuarios.SuspendLayout();
            this.tblLayoutTramites.SuspendLayout();
            this.tblLayoutAjustes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalir)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.Color.White;
            this.statusStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.statusStrip.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstripUsuario,
            this.lblstripDiaHora});
            this.statusStrip.Location = new System.Drawing.Point(0, 497);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(972, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 0;
            // 
            // lblstripUsuario
            // 
            this.lblstripUsuario.Name = "lblstripUsuario";
            this.lblstripUsuario.Size = new System.Drawing.Size(0, 17);
            // 
            // lblstripDiaHora
            // 
            this.lblstripDiaHora.Name = "lblstripDiaHora";
            this.lblstripDiaHora.Size = new System.Drawing.Size(0, 17);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pbUsuarios
            // 
            this.pbUsuarios.BackColor = System.Drawing.SystemColors.Menu;
            this.pbUsuarios.Image = global::Gestion_AP.Properties.Resources.Iconic_e062_0__128;
            this.pbUsuarios.Location = new System.Drawing.Point(13, 15);
            this.pbUsuarios.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbUsuarios.Name = "pbUsuarios";
            this.pbUsuarios.Size = new System.Drawing.Size(146, 150);
            this.pbUsuarios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUsuarios.TabIndex = 16;
            this.pbUsuarios.TabStop = false;
            this.pbUsuarios.Click += new System.EventHandler(this.pbUsuarios_Click);
            // 
            // pbTramites
            // 
            this.pbTramites.BackColor = System.Drawing.SystemColors.Menu;
            this.pbTramites.Image = global::Gestion_AP.Properties.Resources.Material_Icons_e85d_0__128;
            this.pbTramites.Location = new System.Drawing.Point(13, 173);
            this.pbTramites.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbTramites.Name = "pbTramites";
            this.pbTramites.Size = new System.Drawing.Size(146, 150);
            this.pbTramites.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbTramites.TabIndex = 17;
            this.pbTramites.TabStop = false;
            this.pbTramites.Click += new System.EventHandler(this.pbTramites_Click);
            // 
            // pbAjustes
            // 
            this.pbAjustes.BackColor = System.Drawing.SystemColors.Menu;
            this.pbAjustes.Image = global::Gestion_AP.Properties.Resources.Material_Icons_e869_0__128;
            this.pbAjustes.Location = new System.Drawing.Point(13, 329);
            this.pbAjustes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbAjustes.Name = "pbAjustes";
            this.pbAjustes.Size = new System.Drawing.Size(146, 150);
            this.pbAjustes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAjustes.TabIndex = 18;
            this.pbAjustes.TabStop = false;
            this.pbAjustes.Click += new System.EventHandler(this.pbAjustes_Click);
            // 
            // tblLayoutUsuarios
            // 
            this.tblLayoutUsuarios.ColumnCount = 5;
            this.tblLayoutUsuarios.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutUsuarios.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutUsuarios.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutUsuarios.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutUsuarios.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutUsuarios.Controls.Add(this.btnDesmarcar, 4, 0);
            this.tblLayoutUsuarios.Controls.Add(this.btnCambio, 3, 0);
            this.tblLayoutUsuarios.Controls.Add(this.btnUsuariosActivos, 2, 0);
            this.tblLayoutUsuarios.Controls.Add(this.btnAdmUsuarios, 1, 0);
            this.tblLayoutUsuarios.Controls.Add(this.btnAgregarUsuario, 0, 0);
            this.tblLayoutUsuarios.Location = new System.Drawing.Point(168, 54);
            this.tblLayoutUsuarios.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tblLayoutUsuarios.Name = "tblLayoutUsuarios";
            this.tblLayoutUsuarios.RowCount = 1;
            this.tblLayoutUsuarios.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutUsuarios.Size = new System.Drawing.Size(714, 75);
            this.tblLayoutUsuarios.TabIndex = 19;
            this.tblLayoutUsuarios.Visible = false;
            // 
            // btnDesmarcar
            // 
            this.btnDesmarcar.BackColor = System.Drawing.SystemColors.Menu;
            this.btnDesmarcar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnDesmarcar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnDesmarcar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesmarcar.Location = new System.Drawing.Point(571, 4);
            this.btnDesmarcar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDesmarcar.Name = "btnDesmarcar";
            this.btnDesmarcar.Size = new System.Drawing.Size(139, 67);
            this.btnDesmarcar.TabIndex = 5;
            this.btnDesmarcar.Text = "Desbloquear Usuarios";
            this.btnDesmarcar.UseVisualStyleBackColor = false;
            this.btnDesmarcar.Click += new System.EventHandler(this.btnDesmarcar_Click);
            // 
            // btnCambio
            // 
            this.btnCambio.BackColor = System.Drawing.SystemColors.Menu;
            this.btnCambio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCambio.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCambio.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCambio.Location = new System.Drawing.Point(429, 4);
            this.btnCambio.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCambio.Name = "btnCambio";
            this.btnCambio.Size = new System.Drawing.Size(136, 67);
            this.btnCambio.TabIndex = 4;
            this.btnCambio.Text = "Cambiar Contraseña";
            this.btnCambio.UseVisualStyleBackColor = false;
            this.btnCambio.Click += new System.EventHandler(this.btnCambio_Click);
            // 
            // btnUsuariosActivos
            // 
            this.btnUsuariosActivos.BackColor = System.Drawing.SystemColors.Menu;
            this.btnUsuariosActivos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnUsuariosActivos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnUsuariosActivos.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsuariosActivos.Location = new System.Drawing.Point(287, 4);
            this.btnUsuariosActivos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnUsuariosActivos.Name = "btnUsuariosActivos";
            this.btnUsuariosActivos.Size = new System.Drawing.Size(136, 67);
            this.btnUsuariosActivos.TabIndex = 3;
            this.btnUsuariosActivos.Text = "Usuarios Activos";
            this.btnUsuariosActivos.UseVisualStyleBackColor = false;
            this.btnUsuariosActivos.Click += new System.EventHandler(this.btnUsuariosActivos_Click);
            // 
            // btnAdmUsuarios
            // 
            this.btnAdmUsuarios.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAdmUsuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAdmUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAdmUsuarios.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdmUsuarios.Location = new System.Drawing.Point(145, 4);
            this.btnAdmUsuarios.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAdmUsuarios.Name = "btnAdmUsuarios";
            this.btnAdmUsuarios.Size = new System.Drawing.Size(136, 67);
            this.btnAdmUsuarios.TabIndex = 2;
            this.btnAdmUsuarios.Text = "Modificar Usuarios";
            this.btnAdmUsuarios.UseVisualStyleBackColor = false;
            this.btnAdmUsuarios.Click += new System.EventHandler(this.btnAdmUsuarios_Click);
            // 
            // btnAgregarUsuario
            // 
            this.btnAgregarUsuario.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAgregarUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAgregarUsuario.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAgregarUsuario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarUsuario.Location = new System.Drawing.Point(3, 4);
            this.btnAgregarUsuario.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAgregarUsuario.Name = "btnAgregarUsuario";
            this.btnAgregarUsuario.Size = new System.Drawing.Size(136, 67);
            this.btnAgregarUsuario.TabIndex = 1;
            this.btnAgregarUsuario.Text = "Nuevo Usuario";
            this.btnAgregarUsuario.UseVisualStyleBackColor = false;
            this.btnAgregarUsuario.Click += new System.EventHandler(this.btnAgregarUsuario_Click);
            // 
            // tblLayoutTramites
            // 
            this.tblLayoutTramites.ColumnCount = 5;
            this.tblLayoutTramites.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutTramites.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutTramites.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutTramites.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutTramites.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutTramites.Controls.Add(this.btnRechBatch, 2, 0);
            this.tblLayoutTramites.Controls.Add(this.btnConsultas, 2, 1);
            this.tblLayoutTramites.Controls.Add(this.btnGestiones, 0, 0);
            this.tblLayoutTramites.Controls.Add(this.btnCambioEstado, 0, 1);
            this.tblLayoutTramites.Controls.Add(this.btnControl, 4, 0);
            this.tblLayoutTramites.Controls.Add(this.btnRech, 4, 1);
            this.tblLayoutTramites.Location = new System.Drawing.Point(168, 173);
            this.tblLayoutTramites.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tblLayoutTramites.Name = "tblLayoutTramites";
            this.tblLayoutTramites.RowCount = 2;
            this.tblLayoutTramites.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutTramites.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutTramites.Size = new System.Drawing.Size(714, 150);
            this.tblLayoutTramites.TabIndex = 20;
            // 
            // btnRech
            // 
            this.btnRech.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnRech.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRech.Location = new System.Drawing.Point(571, 79);
            this.btnRech.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRech.Name = "btnRech";
            this.btnRech.Size = new System.Drawing.Size(136, 67);
            this.btnRech.TabIndex = 11;
            this.btnRech.Text = "Bandeja de Rechazos";
            this.btnRech.UseVisualStyleBackColor = true;
            this.btnRech.Click += new System.EventHandler(this.btnRech_Click);
            // 
            // btnConsultas
            // 
            this.btnConsultas.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnConsultas.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultas.Location = new System.Drawing.Point(287, 79);
            this.btnConsultas.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnConsultas.Name = "btnConsultas";
            this.btnConsultas.Size = new System.Drawing.Size(136, 67);
            this.btnConsultas.TabIndex = 10;
            this.btnConsultas.Text = "Buscar Trámite";
            this.btnConsultas.UseVisualStyleBackColor = true;
            this.btnConsultas.Click += new System.EventHandler(this.btnConsultas_Click);
            // 
            // btnGestiones
            // 
            this.btnGestiones.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnGestiones.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGestiones.Location = new System.Drawing.Point(3, 4);
            this.btnGestiones.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnGestiones.Name = "btnGestiones";
            this.btnGestiones.Size = new System.Drawing.Size(136, 67);
            this.btnGestiones.TabIndex = 6;
            this.btnGestiones.Text = "Nuevo Trámite";
            this.btnGestiones.UseVisualStyleBackColor = true;
            this.btnGestiones.Click += new System.EventHandler(this.btnGestiones_Click);
            // 
            // btnCambioEstado
            // 
            this.btnCambioEstado.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCambioEstado.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCambioEstado.Location = new System.Drawing.Point(3, 79);
            this.btnCambioEstado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCambioEstado.Name = "btnCambioEstado";
            this.btnCambioEstado.Size = new System.Drawing.Size(136, 67);
            this.btnCambioEstado.TabIndex = 9;
            this.btnCambioEstado.Text = "Modificar Trámite";
            this.btnCambioEstado.UseVisualStyleBackColor = true;
            this.btnCambioEstado.Click += new System.EventHandler(this.btnCambioEstado_Click);
            // 
            // btnControl
            // 
            this.btnControl.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnControl.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnControl.Location = new System.Drawing.Point(571, 4);
            this.btnControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnControl.Name = "btnControl";
            this.btnControl.Size = new System.Drawing.Size(136, 67);
            this.btnControl.TabIndex = 8;
            this.btnControl.Text = "Bandeja de Control";
            this.btnControl.UseVisualStyleBackColor = true;
            this.btnControl.Click += new System.EventHandler(this.btnControl_Click);
            // 
            // btnRechBatch
            // 
            this.btnRechBatch.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnRechBatch.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRechBatch.Location = new System.Drawing.Point(287, 4);
            this.btnRechBatch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRechBatch.Name = "btnRechBatch";
            this.btnRechBatch.Size = new System.Drawing.Size(136, 67);
            this.btnRechBatch.TabIndex = 7;
            this.btnRechBatch.Text = "Nuevo Rechazo";
            this.btnRechBatch.UseVisualStyleBackColor = true;
            this.btnRechBatch.Click += new System.EventHandler(this.btnRechBatch_Click);
            // 
            // tblLayoutAjustes
            // 
            this.tblLayoutAjustes.ColumnCount = 5;
            this.tblLayoutAjustes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutAjustes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutAjustes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutAjustes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutAjustes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblLayoutAjustes.Controls.Add(this.btnEstadistic, 2, 0);
            this.tblLayoutAjustes.Controls.Add(this.btnAcerca, 4, 0);
            this.tblLayoutAjustes.Controls.Add(this.btnContador, 3, 0);
            this.tblLayoutAjustes.Controls.Add(this.btnNormalizacion, 1, 0);
            this.tblLayoutAjustes.Controls.Add(this.btnMantenimiento, 0, 0);
            this.tblLayoutAjustes.Location = new System.Drawing.Point(168, 369);
            this.tblLayoutAjustes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tblLayoutAjustes.Name = "tblLayoutAjustes";
            this.tblLayoutAjustes.RowCount = 1;
            this.tblLayoutAjustes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutAjustes.Size = new System.Drawing.Size(714, 75);
            this.tblLayoutAjustes.TabIndex = 23;
            this.tblLayoutAjustes.Visible = false;
            // 
            // btnNormalizacion
            // 
            this.btnNormalizacion.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNormalizacion.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNormalizacion.Location = new System.Drawing.Point(145, 4);
            this.btnNormalizacion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNormalizacion.Name = "btnNormalizacion";
            this.btnNormalizacion.Size = new System.Drawing.Size(136, 67);
            this.btnNormalizacion.TabIndex = 12;
            this.btnNormalizacion.Text = "Normalización";
            this.btnNormalizacion.UseVisualStyleBackColor = true;
            this.btnNormalizacion.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnMantenimiento
            // 
            this.btnMantenimiento.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnMantenimiento.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantenimiento.Location = new System.Drawing.Point(3, 4);
            this.btnMantenimiento.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMantenimiento.Name = "btnMantenimiento";
            this.btnMantenimiento.Size = new System.Drawing.Size(136, 67);
            this.btnMantenimiento.TabIndex = 13;
            this.btnMantenimiento.Text = "Configuraciones";
            this.btnMantenimiento.UseVisualStyleBackColor = true;
            this.btnMantenimiento.Click += new System.EventHandler(this.btnMantenimiento_Click);
            // 
            // btnEstadistic
            // 
            this.btnEstadistic.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnEstadistic.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstadistic.Location = new System.Drawing.Point(287, 4);
            this.btnEstadistic.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEstadistic.Name = "btnEstadistic";
            this.btnEstadistic.Size = new System.Drawing.Size(136, 67);
            this.btnEstadistic.TabIndex = 14;
            this.btnEstadistic.Text = "Estadísticas";
            this.btnEstadistic.UseVisualStyleBackColor = true;
            this.btnEstadistic.Click += new System.EventHandler(this.btnEstadistic_Click);
            // 
            // btnAcerca
            // 
            this.btnAcerca.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAcerca.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAcerca.Location = new System.Drawing.Point(571, 4);
            this.btnAcerca.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAcerca.Name = "btnAcerca";
            this.btnAcerca.Size = new System.Drawing.Size(139, 67);
            this.btnAcerca.TabIndex = 16;
            this.btnAcerca.Text = "Acerca de...";
            this.btnAcerca.UseVisualStyleBackColor = true;
            this.btnAcerca.Click += new System.EventHandler(this.btnAcerca_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = global::Gestion_AP.Properties.Resources.Material_Icons_e8c6_0__128;
            this.btnSalir.Location = new System.Drawing.Point(888, 373);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 69);
            this.btnSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnSalir.TabIndex = 24;
            this.btnSalir.TabStop = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnContador
            // 
            this.btnContador.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnContador.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContador.Location = new System.Drawing.Point(429, 4);
            this.btnContador.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnContador.Name = "btnContador";
            this.btnContador.Size = new System.Drawing.Size(136, 67);
            this.btnContador.TabIndex = 17;
            this.btnContador.Text = "Contador";
            this.btnContador.UseVisualStyleBackColor = true;
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(972, 519);
            this.ControlBox = false;
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.tblLayoutAjustes);
            this.Controls.Add(this.tblLayoutTramites);
            this.Controls.Add(this.tblLayoutUsuarios);
            this.Controls.Add(this.pbAjustes);
            this.Controls.Add(this.pbTramites);
            this.Controls.Add(this.pbUsuarios);
            this.Controls.Add(this.statusStrip);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "..::GESTIONAP::..";
            this.Load += new System.EventHandler(this.Principal_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTramites)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAjustes)).EndInit();
            this.tblLayoutUsuarios.ResumeLayout(false);
            this.tblLayoutTramites.ResumeLayout(false);
            this.tblLayoutAjustes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSalir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblstripUsuario;
        private System.Windows.Forms.ToolStripStatusLabel lblstripDiaHora;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.PictureBox pbUsuarios;
        private System.Windows.Forms.PictureBox pbTramites;
        private System.Windows.Forms.PictureBox pbAjustes;
        private System.Windows.Forms.TableLayoutPanel tblLayoutUsuarios;
        private System.Windows.Forms.Button btnDesmarcar;
        private System.Windows.Forms.Button btnCambio;
        private System.Windows.Forms.Button btnUsuariosActivos;
        private System.Windows.Forms.Button btnAdmUsuarios;
        private System.Windows.Forms.Button btnAgregarUsuario;
        private System.Windows.Forms.TableLayoutPanel tblLayoutTramites;
        private System.Windows.Forms.Button btnRech;
        private System.Windows.Forms.Button btnConsultas;
        private System.Windows.Forms.Button btnGestiones;
        private System.Windows.Forms.Button btnCambioEstado;
        private System.Windows.Forms.Button btnControl;
        private System.Windows.Forms.Button btnRechBatch;
        private System.Windows.Forms.TableLayoutPanel tblLayoutAjustes;
        private System.Windows.Forms.Button btnNormalizacion;
        private System.Windows.Forms.Button btnMantenimiento;
        private System.Windows.Forms.Button btnEstadistic;
        private System.Windows.Forms.Button btnAcerca;
        private System.Windows.Forms.PictureBox btnSalir;
        private System.Windows.Forms.Button btnContador;
    }
}