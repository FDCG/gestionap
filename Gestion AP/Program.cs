﻿using System;
using System.Collections.Generic;


using System.Windows.Forms;

namespace Gestion_AP
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Ingreso frm = new Ingreso();
            frm.Show();
            Application.Run();
        }
    }
}

