﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;

using System.Windows.Forms;
using System.Data.OleDb;


namespace Gestion_AP
{
    class AspectoTabla
    {

        public static void AspectoDGV(DataGridView dgv, Color dgvColor)

        {
            dgv.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
            
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgv.RowsDefaultCellStyle.BackColor = dgvColor;
            dgv.RowHeadersVisible = false;
            dgv.AllowUserToAddRows = false;

        }

        public static void AspectoComent(DataGridView dgv)
        {
            dgv.AllowUserToAddRows = false;
            dgv.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
            dgv.Columns[0].Visible = false;
            dgv.Columns[1].Visible = false;
            DataGridViewColumn fecha = dgv.Columns[2];
            dgv.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgv.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgv.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.Sort(fecha, ListSortDirection.Ascending);
            dgv.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgv.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgv.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgv.RowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
            dgv.ReadOnly = true;
            dgv.RowHeadersVisible = false;
            dgv.AllowUserToAddRows = false;


        }

    }
}
