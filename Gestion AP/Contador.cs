﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Gestion_AP
{
    public partial class Contador : Form
    {
        public Contador()
        {
            InitializeComponent();
        }



        private void Contador_Load(object sender, EventArgs e)
        {

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            DataTable DT = new DataTable();
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            cmd.CommandText = "SELECT TRAMITE,WF FROM TRAMITES";
            da.Fill(DT);
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                cmbTram1.Items.Add(Convert.ToString(DT.Rows[i]["TRAMITE"]));
                cmbTram2.Items.Add(Convert.ToString(DT.Rows[i]["TRAMITE"]));
                cmbTram3.Items.Add(Convert.ToString(DT.Rows[i]["TRAMITE"]));
                cmbTram4.Items.Add(Convert.ToString(DT.Rows[i]["TRAMITE"]));
                cmbTram5.Items.Add(Convert.ToString(DT.Rows[i]["TRAMITE"]));
            }



        }

        public void Novedad(ComboBox cmb, TextBox txt)

        {
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT WF FROM TRAMITES WHERE TRAMITE='" + Convert.ToString(cmb.SelectedItem) + "'";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                txt.Text = Convert.ToString(dt.Rows[0][0]);

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);
            }

        }

        private void cmbTram1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Novedad(cmbTram1, txtNov1);


        }

        private void cmbTram2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTram1.SelectedItem == null)
            {
                cmbTram2.SelectedItem = null;


            }
            else
            {
                Novedad(cmbTram2, txtNov2);
            }


        }

        private void cmbTram3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTram2.SelectedItem == null)
            {
                cmbTram3.SelectedItem = null;


            }
            else
            {

                Novedad(cmbTram3, txtNov3);
            }
        }

        private void cmbTram4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTram3.SelectedItem == null)
            {
                cmbTram4.SelectedItem = null;


            }
            else
            {
                Novedad(cmbTram4, txtNov4);
            }
        }

        private void cmbTram5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTram4.SelectedItem == null)
            {
                cmbTram5.SelectedItem = null;
              
                
            }
            else
            {
                Novedad(cmbTram5, txtNov5);
            }
        }
        public void filasupe()

        {
            MessageBox.Show("Utilice el seleccionador superior antes de continuar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void BorrarTodo()


        {
            try
            {

                cmbTram1.SelectedIndexChanged -= new EventHandler(cmbTram1_SelectedIndexChanged);
                cmbTram2.SelectedIndexChanged -= new EventHandler(cmbTram2_SelectedIndexChanged);
                cmbTram3.SelectedIndexChanged -= new EventHandler(cmbTram3_SelectedIndexChanged);
                cmbTram4.SelectedIndexChanged -= new EventHandler(cmbTram4_SelectedIndexChanged);
                cmbTram5.SelectedIndexChanged -= new EventHandler(cmbTram5_SelectedIndexChanged);
                cmbTram1.SelectedItem = null;
                cmbTram2.SelectedItem = null;
                cmbTram3.SelectedItem = null;
                cmbTram4.SelectedItem = null;
                cmbTram5.SelectedItem = null;
                txtNov1.Text = "";
                txtNov2.Text = "";
                txtNov3.Text = "";
                txtNov4.Text = "";
                txtNov5.Text = "";
                nudCant1.Value = 0;
                nudCant2.Value = 0;
                nudCant3.Value = 0;
                nudCant4.Value = 0;
                nudCant5.Value = 0;
                cmbTram1.SelectedIndexChanged += new EventHandler(cmbTram1_SelectedIndexChanged);
                cmbTram2.SelectedIndexChanged += new EventHandler(cmbTram2_SelectedIndexChanged);
                cmbTram3.SelectedIndexChanged += new EventHandler(cmbTram3_SelectedIndexChanged);
                cmbTram4.SelectedIndexChanged += new EventHandler(cmbTram4_SelectedIndexChanged);
                cmbTram5.SelectedIndexChanged += new EventHandler(cmbTram5_SelectedIndexChanged);

            }
            catch (Exception)
            { }

        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            BorrarTodo();
            
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {


            MessageBox.Show("Este formulario te permitirá ir sumando y registrando \ntrámites que vas realizando. Serán imputados en tu producción del día \ny podrás verificarlas en la sección Estadísticas. Si hay alguna \ngestión que cargaste por error podés dejarla en cero o bien \nborrar todo y comenzar nuevamente. \nEn caso de completar todas, registralas y comenzará nuevamente el contador.","Ayuda",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (ErrorValidacion() == true)
            {
                MessageBox.Show("Se registraron cantidades mayores a 0 sin un trámite asignado.\nRevise nuevamente antes de registrar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                OleDbConnection cn = new OleDbConnection(Conexion.cnProceso);
                OleDbCommand cmd = new OleDbCommand();
                OleDbCommand cmde = new OleDbCommand();
                try
                {
                    int Grabado = 0;

                    cmd.Connection = cn;
                    cmde.Connection = cn;
                    cmd.CommandText = "INSERT INTO GESTIONES(US_INICIO,FECHA_INICIO,TRAMITE,WF,NRO_TRAMITE,CANAL_INGRESO,ESTADO) VALUES(@US,@FE,@TRAM,@WF,@NRO,@CAN,@EST)";
                    cmde.CommandText = "INSERT INTO ESTADISTICAS(USUARIO,FECHA,ACCION) VALUES(@USER,@FECH,@ACC)";

                    cn.Open();

                    if (nudCant1.Value > 0)
                    {
                        for (int i = 1; i <= nudCant1.Value; i++)
                        {
                            cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                            cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@TRAM", cmbTram1.SelectedItem.ToString());
                            cmd.Parameters.AddWithValue("@WF", txtNov1.Text);
                            cmd.Parameters.AddWithValue("@NRO", "JL-" + Ingreso.UsuarioLogueado.Substring(0, 3) + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + i);
                            cmd.Parameters.AddWithValue("@CAN", "INGRESO MANUAL");
                            cmd.Parameters.AddWithValue("@EST", "CERRADO");
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            cmde.Parameters.AddWithValue("@USER", Ingreso.UsuarioLogueado);
                            cmde.Parameters.AddWithValue("@FECH", DateTime.Now.ToOADate());
                            cmde.Parameters.AddWithValue("@ACC", "PROCESO");
                            cmde.ExecuteNonQuery();
                            cmde.Parameters.Clear();
                            
                            
                        }
                        Grabado++;
                    }

                    if (nudCant2.Value > 0)
                    {
                        for (int i = 1; i <= nudCant2.Value; i++)
                        {
                            cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                            cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@TRAM", cmbTram2.SelectedItem.ToString());
                            cmd.Parameters.AddWithValue("@WF", txtNov2.Text);
                            cmd.Parameters.AddWithValue("@NRO", "PM-" +Ingreso.UsuarioLogueado.Substring(0,3) + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + i);
                            cmd.Parameters.AddWithValue("@CAN", "INGRESO MANUAL");
                            cmd.Parameters.AddWithValue("@EST", "CERRADO");
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            cmde.Parameters.AddWithValue("@USER", Ingreso.UsuarioLogueado);
                            cmde.Parameters.AddWithValue("@FECH", DateTime.Now.ToOADate());
                            cmde.Parameters.AddWithValue("@ACC", "PROCESO");
                            cmde.ExecuteNonQuery();
                            cmde.Parameters.Clear();
                        }
                        Grabado++;
                    }
                   
                    if (nudCant3.Value > 0)
                    {
                        for (int i = 1; i <= nudCant3.Value; i++)
                        {
                            cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                            cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@TRAM", cmbTram3.SelectedItem.ToString());
                            cmd.Parameters.AddWithValue("@WF", txtNov3.Text);
                            cmd.Parameters.AddWithValue("@NRO", "GH-" + Ingreso.UsuarioLogueado.Substring(0, 3) + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + i);
                            cmd.Parameters.AddWithValue("@CAN", "INGRESO MANUAL");
                            cmd.Parameters.AddWithValue("@EST", "CERRADO");
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            cmde.Parameters.AddWithValue("@USER", Ingreso.UsuarioLogueado);
                            cmde.Parameters.AddWithValue("@FECH", DateTime.Now.ToOADate());
                            cmde.Parameters.AddWithValue("@ACC", "PROCESO");
                            cmde.ExecuteNonQuery();
                            cmde.Parameters.Clear();
                        }
                        Grabado++;
                    }
                  
                    if (nudCant4.Value > 0)
                    {
                        for (int i = 1; i <= nudCant4.Value; i++)
                        {
                            cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                            cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@TRAM", cmbTram4.SelectedItem.ToString());
                            cmd.Parameters.AddWithValue("@WF", txtNov4.Text);
                            cmd.Parameters.AddWithValue("@NRO", "RS-" + Ingreso.UsuarioLogueado.Substring(0, 3) + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + i);
                            cmd.Parameters.AddWithValue("@CAN", "INGRESO MANUAL");
                            cmd.Parameters.AddWithValue("@EST", "CERRADO");
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            cmde.Parameters.AddWithValue("@USER", Ingreso.UsuarioLogueado);
                            cmde.Parameters.AddWithValue("@FECH", DateTime.Now.ToOADate());
                            cmde.Parameters.AddWithValue("@ACC", "PROCESO");
                            cmde.ExecuteNonQuery();
                            cmde.Parameters.Clear();
                        }
                        Grabado++;
                    }
                  
                    if (nudCant5.Value > 0)
                    {
                        for (int i = 1; i <= nudCant5.Value; i++)
                        {
                            cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                            cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@TRAM", cmbTram5.SelectedItem.ToString());
                            cmd.Parameters.AddWithValue("@WF", txtNov5.Text);
                            cmd.Parameters.AddWithValue("@NRO", "TB-" + Ingreso.UsuarioLogueado.Substring(0, 3)+"-" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + i);
                            cmd.Parameters.AddWithValue("@CAN", "INGRESO MANUAL");
                            cmd.Parameters.AddWithValue("@EST", "CERRADO");
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            cmde.Parameters.AddWithValue("@USER", Ingreso.UsuarioLogueado);
                            cmde.Parameters.AddWithValue("@FECH", DateTime.Now.ToOADate());
                            cmde.Parameters.AddWithValue("@ACC", "PROCESO");
                            cmde.ExecuteNonQuery();
                            cmde.Parameters.Clear();
                        }
                        Grabado++;
                    }
                    if (Grabado>0)
                    {
                        MessageBox.Show("Trámites registrados correctamente!", "Contador", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        BorrarTodo();
                        Grabado = 0;
                    }
                    else
                    {
                        MessageBox.Show("No se puede registrar campos vacíos", "Ups!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }


                }
                catch (Exception EX) {
                    MessageBox.Show("Algo salió mal. Avisale a alguien." + EX.Message);
                    Logins.LogError(EX); }


           
                cn.Close();
            }

        }

        public bool ErrorValidacion()

        {

            if (nudCant1.Value>0 && txtNov1.Text=="")
            {
                goto Salir;
            }
            if (nudCant2.Value > 0 && txtNov2.Text == "")
            {
                goto Salir;
            }
            if (nudCant3.Value > 0 && txtNov3.Text == "")
            {
                goto Salir;
            }
            if (nudCant4.Value > 0 && txtNov4.Text == "")
            {
                goto Salir;
            }
            if (nudCant5.Value > 0 && txtNov5.Text == "")
            {
                goto Salir;
            }
            return false;

        Salir: return true;

        }
    }
}
