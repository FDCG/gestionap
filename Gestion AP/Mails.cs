﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;


using System.Text.RegularExpressions;
using EAGetMail;






namespace Gestion_AP
{
    public partial class Mails : Form
    {
        public Mails()
        {
            InitializeComponent();
        }

        private OleDbConnection cn;

        private DataSet dsTramites;

        private FileSystemWatcher fsw;



        private void Mails_Load(object sender, EventArgs e)
        {

            try
            {
                // Establecer la conexión

                cn = getConexion(cn);

                // Crear DataSet de trámites

                dsTramites = getTramites();

                // Carga ComboBox con DataSet

                for (int i = 0; i < dsTramites.Tables[0].Rows.Count; i++)
                {
                    cmbGestion.Items.Add(Convert.ToString(dsTramites.Tables[0].Rows[i]["TRAMITE"]));
                }

                // Establecer FSW para cambios en carpeta GestiónAP

                fsw = setFileSystemWatch();



            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                Logins.LogError(ex);
            }



        }

        private FileSystemWatcher setFileSystemWatch()
        {
            FileSystemWatcher auxWatcher;
            try
            {
                auxWatcher = new FileSystemWatcher(@"C:\GestionAP");

                auxWatcher.IncludeSubdirectories = true;

                auxWatcher.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.FileName;

                auxWatcher.Filter = "*.eml";

                auxWatcher.Created += Cambio;

                auxWatcher.EnableRaisingEvents = true;

            }
            catch
            {
                throw new Exception("Asegurese de tener la carpera GestionAP en directorio C:");
            }

            return auxWatcher;

        }

        private DataSet getTramites()
        {


            OleDbCommand cmdTramites = new OleDbCommand();
            cmdTramites.Connection = cn;
            cmdTramites.CommandText = "SELECT * FROM TRAMITES";
            OleDbDataAdapter da = new OleDbDataAdapter(cmdTramites);
            DataSet ds = new DataSet();
            da.Fill(ds);

            if (ds == null || ds.Tables[0].Rows.Count == 0)
            {
                throw new Exception("Error al obtener datos");
            }

            return ds;
        }

        private string getNombreTramite(DataSet ds, int index)
        {
            string nombreTram = null;

            nombreTram = ds.Tables[0].Rows[index]["WF"].ToString();

            return nombreTram;
        }

        private OleDbConnection getConexion(OleDbConnection cn)
        {

            if (cn == null)
            {

                cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;

            }

            return cn;
        }

        public static string strNuevoURL;
        public static bool NuevoURL = false;

        public static void Cambio(object source, FileSystemEventArgs e)

        {

            strNuevoURL = e.FullPath;
            NuevoURL = true;

        }

        private void cmbGestion_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {

                lblWF_Asociado.Text = getNombreTramite(dsTramites, cmbGestion.SelectedIndex);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);
            }
        }


        string strPath = "";


        public void CargarMail()
        {


            if (NuevoURL == true)
            {
                webBrowser.ScriptErrorsSuppressed = true;

                webBrowser.AllowWebBrowserDrop = false;

                txtPath.Text = strNuevoURL;
                NuevoURL = false;
                ConvertMailToHtml(txtPath.Text);

                webBrowser.Navigate(strPathMail);

            }
            else
            {
                try
                {


                    OpenFileDialog fd = new OpenFileDialog();



                    fd.InitialDirectory = @"C:\GestionAP";
                    fd.Filter = "Archivos Lotus (*.*eml)|*.eml";
                    fd.FilterIndex = 1;
                    fd.RestoreDirectory = true;


                    if (fd.ShowDialog() == DialogResult.OK)
                    {

                        strPath = fd.FileName;

                        webBrowser.ScriptErrorsSuppressed = true;

                        webBrowser.AllowWebBrowserDrop = false;
                        //  webBrowser.Url = new Uri(strPath);
                        txtPath.Text = strPath;

                        ConvertMailToHtml(txtPath.Text);

                        webBrowser.Navigate(strPathMail);

                    }
                }
                catch (Exception ex)
                {
                    Logins.LogError(ex);
                }

            }
        }


        private void btnCargar_Click(object sender, EventArgs e)
        {
            try
            {

                CargarMail();

                txtIdControl.Text = strIdInterno;

            }
            catch (Exception EX)
            {

                Logins.LogError(EX);

            }

        }





        private void btnGrabar_Click(object sender, EventArgs e)
        {
            cn = getConexion(cn);

            OleDbTransaction transaction = null;


            try
            {
                esFormValido();

                

                cn.Open();

                OleDbCommand cmdTramite = buildTramite(cn);

                OleDbCommand cmdComentarios = buildComentarios(cn);



                transaction = cn.BeginTransaction();

                cmdTramite.Transaction = transaction;
                cmdComentarios.Transaction = transaction;

                cmdTramite.ExecuteNonQuery();
                
                cmdComentarios.ExecuteNonQuery();

                transaction.Commit();

                MessageBox.Show("Trámite " + strIdInterno + " generado correctamente");
                Logins.Estadistica("PROCESO", txtIdControl.Text.ToString());
                this.Close();

            }
            catch (ArgumentException argE)
            {
                MessageBox.Show(argE.Message);
            }
            catch (Exception ex)
            {
                if(transaction != null)
                {
                    transaction.Rollback();
                }
               
                MessageBox.Show("No se pudo grabar el Trámite.\nError: " + ex.Message);
                Logins.LogError(ex);
            }
            finally
            {
                cn.Close();
            }

        }





        private OleDbCommand buildComentarios(OleDbConnection cn)
        {


            OleDbCommand cmdInsertarComentarios = new OleDbCommand();

            cmdInsertarComentarios.Connection = cn;

            cmdInsertarComentarios.CommandText = "INSERT INTO Procesos(NroTramite,FECHA" +
                ",USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
            cmdInsertarComentarios.Parameters.AddWithValue("@ID", txtIdControl.Text.ToString());
            cmdInsertarComentarios.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
            cmdInsertarComentarios.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
            cmdInsertarComentarios.Parameters.AddWithValue("@COM", txtComAdic.Text.Trim());

            return cmdInsertarComentarios;


        }


        /// <summary>
        /// Metodo que genera query para inserción en base de datos 
        /// </summary>
        private OleDbCommand buildTramite(OleDbConnection cn)
        {

            OleDbCommand cmdInsertarRegistro = new OleDbCommand();
            cmdInsertarRegistro.Connection = cn;

            string prioridad = "NORMAL";

            cmdInsertarRegistro.CommandType = CommandType.Text;
            cmdInsertarRegistro.CommandText = "INSERT INTO GESTIONES(US_INICIO,FECHA_INICIO,EMP,TRAMITE,WF," +
                "NRO_TRAMITE,PRIORIDAD,CANAL_INGRESO,DOC_ASOCIADO,ESTADO,A_CONTROLAR)" +
                " VALUES(@US_INICIO,@FECHA_INICIO,@EMP,@TRAMITE,@WF,@NRO_TRAMITE," +
                "@PRIORIDAD,@CANAL_INGRESO,@DOC_ASOCIADO,@ESTADO,@ACONTROL)";


            cmdInsertarRegistro.Parameters.AddWithValue("@US_INICIO", Ingreso.UsuarioLogueado.ToUpper());
            cmdInsertarRegistro.Parameters.AddWithValue("@FECHA_INICIO", DateTime.Now.ToOADate());
            cmdInsertarRegistro.Parameters.AddWithValue("@EMP", cmbAdm.SelectedItem.ToString());
            cmdInsertarRegistro.Parameters.AddWithValue("@TRAMITE", cmbGestion.Text.ToString());
            cmdInsertarRegistro.Parameters.AddWithValue("@WF", lblWF_Asociado.Text.ToString());
            cmdInsertarRegistro.Parameters.AddWithValue("@NRO_TRAMITE", txtIdControl.Text.ToString());

            if (chbUrgente.Checked)
            {
                prioridad = "URGENTE";
            }
            else if (chbLiberar.Checked)
            {
                prioridad = "LIBERACION";

            }

            cmdInsertarRegistro.Parameters.AddWithValue("@PRIORIDAD", prioridad);
            cmdInsertarRegistro.Parameters.AddWithValue("@CANAL_INGRESO", "MAIL");
            cmdInsertarRegistro.Parameters.AddWithValue("@DOC_ASOCIADO", strDocRelacionado);

            cmdInsertarRegistro.Parameters.AddWithValue("@ESTADO", "ABIERTO");
            cmdInsertarRegistro.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today).ToOADate());


            return cmdInsertarRegistro;
        }

        /// <summary>
        /// Metodo booleando que valida la completitud y validez de los campos
        /// necesarios para registrar el trámite
        /// </summary>
        /// <returns></returns>
        private bool esFormValido()
        {
            bool valido = false;

            if (txtIdControl.Text == null || txtIdControl.Text.Trim() == "")
            {
                throw new ArgumentException("No indica numero de gestión");
            }
            else if (lblWF_Asociado.Text == "")
            {
                throw new ArgumentException("No seleccionó trámite");

            }
            else if (cmbAdm.SelectedItem == null)
            {
                throw new ArgumentException("No seleccionó administradora");

            }
            else if (validarVinculacion(txtIdVincular.Text.Trim()) == false)
            {

                throw new ArgumentException("Error con trámite vinculado");
            }

            valido = true;

            return valido;

        }

        public static void _GenerateHtmlForEmail(string htmlName, string emlFile,
                    string tempFolder)
        {



            Mail oMail = new Mail("TEAM BEAN 2014-00666-a8fa1baf7e92e8e38c737232cd1a81ff");

            //  Mail oMail = new Mail("TryIt");
            oMail.Load(emlFile, false);

            if (oMail.IsEncrypted)
            {
                try
                {
                    // This email is encrypted, we decrypt it by user default certificate.
                    oMail = oMail.Decrypt(null);
                }
                catch (Exception ep)
                {
                    Console.WriteLine(ep.Message);
                    oMail.Load(emlFile, false);
                    Logins.LogError(ep);
                }
            }

            if (oMail.IsSigned)
            {
                try
                {
                    // This email is digital signed.
                    EAGetMail.Certificate cert = oMail.VerifySignature();
                    Console.WriteLine("El mail tiene una firma digital.");
                }
                catch (Exception ep)
                {
                    Console.WriteLine(ep.Message);
                    Logins.LogError(ep);
                }
            }

            // Parse html body
            string html = oMail.HtmlBody;
            StringBuilder hdr = new StringBuilder();

            // Parse sender
            hdr.Append("<font face=\"Courier New,Arial\" size=2>");
            hdr.Append("<b>Enviado por:</b> " + _FormatHtmlTag(oMail.From.ToString()) + "<br>");

            // Parse to
            MailAddress[] addrs = oMail.To;
            int count = addrs.Length;
            if (count > 0)
            {
                hdr.Append("<b>Para:</b> ");
                for (int i = 0; i < count; i++)
                {
                    hdr.Append(_FormatHtmlTag(addrs[i].ToString()));
                    if (i < count - 1)
                    {
                        hdr.Append(";");
                    }
                }
                hdr.Append("<br>");
            }

            // Parse cc
            addrs = oMail.Cc;

            count = addrs.Length;
            if (count > 0)
            {
                hdr.Append("<b>En copia:</b> ");
                for (int i = 0; i < count; i++)
                {
                    hdr.Append(_FormatHtmlTag(addrs[i].ToString()));
                    if (i < count - 1)
                    {
                        hdr.Append(";");
                    }
                }
                hdr.Append("<br>");
            }

            hdr.Append(String.Format("<b>Asunto:</b>{0}<br>\r\n",
                _FormatHtmlTag(oMail.Subject)));

            // Parse attachments and save to local folder
            Attachment[] atts = oMail.Attachments;
            count = atts.Length;
            if (count > 0)
            {
                if (!Directory.Exists(tempFolder))
                    Directory.CreateDirectory(tempFolder);

                hdr.Append("<b>Adjuntos:</b>");
                for (int i = 0; i < count; i++)
                {
                    Attachment att = atts[i];
                    if (!att.Name.Contains("graycol"))
                    {
                        // this attachment is in OUTLOOK RTF format, decode it here.
                        if (String.Compare(att.Name, "winmail.dat") == 0 && att.Name != "graycol.gif")
                        {
                            Attachment[] tatts = null;
                            try
                            {
                                tatts = Mail.ParseTNEF(att.Content, true);
                            }
                            catch (Exception ep)
                            {
                                Console.WriteLine(ep.Message);
                                continue;
                            }

                            int y = tatts.Length;
                            for (int x = 0; x < y; x++)
                            {
                                Attachment tatt = tatts[x];
                                string tattname = String.Format("{0}\\{1}", tempFolder, tatt.Name);
                                tatt.SaveAs(tattname, true);
                                hdr.Append(
                                String.Format("<a href=\"{0}\" target=\"_blank\">{1}</a> ",
                                    tattname, tatt.Name));
                            }
                            continue;
                        }

                        string attname = String.Format("{0}\\{1}", tempFolder, att.Name);
                        att.SaveAs(attname, true);
                        hdr.Append(String.Format("<a href=\"{0}\" target=\"_blank\">{1}</a> ",
                                attname, att.Name));

                        if (att.ContentID.Length > 0)
                        {

                            // Show embedded images.
                            html = html.Replace("cid:" + att.ContentID, attname);
                        }
                        else if (String.Compare(att.ContentType, 0, "image/", 0,
                                    "image/".Length, true) == 0)
                        {

                            // show attached images.
                            html = html + String.Format("<hr><img src=\"{0}\">", attname);
                        }


                    }


                }
            }

            Regex reg = new Regex("(<meta[^>]*charset[ \t]*=[ \t\"]*)([^<> \r\n\"]*)",
                RegexOptions.Multiline | RegexOptions.IgnoreCase);
            html = reg.Replace(html, "$1utf-8");
            if (!reg.IsMatch(html))
            {
                hdr.Insert(0,
                    "<meta HTTP-EQUIV=\"Content-Type\" Content=\"text-html; charset=utf-8\">");
            }

            // write html to file
            html = hdr.ToString() + "<hr>" + html;
            FileStream fs = new FileStream(htmlName, FileMode.Create,
                FileAccess.Write, FileShare.None);

            byte[] data = System.Text.UTF8Encoding.UTF8.GetBytes(html);
            fs.Write(data, 0, data.Length);
            fs.Close();
            oMail.Clear();
        }
        private static string _FormatHtmlTag(string src)
        {
            src = src.Replace(">", "&gt;");
            src = src.Replace("<", "&lt;");
            return src;
        }
        public static string strPathMail = "";
        public static string strIdInterno = null;
        public static string strDocRelacionado = null;
        private static void ConvertMailToHtml(string fileName)
        {
            try
            {
                //int pos = fileName.LastIndexOf(".");
                //string mainName = fileName.Substring(0, pos);
                //string htmlName = mainName + ".htm";
                string id = "LN" + DateTime.Now.ToString("yyyyMMddHHmmss");
                string mainName = UNC.PathUNC(Directory.GetCurrentDirectory().ToString()) + @"\01\" + id;
                string htmlName = mainName + ".htm";
                strDocRelacionado = htmlName;

                string tempFolder = mainName;
                if (!File.Exists(htmlName))
                {

                    _GenerateHtmlForEmail(htmlName, fileName, tempFolder);
                    strPathMail = htmlName;
                }
                strIdInterno = id;
                File.Delete(fileName);


            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message);
                Logins.LogError(ep);
            }
        }

        private void btnValidarVinc_Click(object sender, EventArgs e)
        {
            if (validarVinculacion(txtIdVincular.Text.Trim()))
            {
                MessageBox.Show("Vinculación válida.");
            }
            else
            {
                MessageBox.Show("Trámite no encontrado. \nRevise el número de trámite e intentelo nuevamente o déjelo en blanco");
            }
        }

        private bool validarVinculacion(string idTramite)
        {
            bool valido = false;

            cn = getConexion(cn);

            OleDbCommand cmd = new OleDbCommand();

            try

            {


                cmd.Connection = cn;
                cmd.CommandText = "SELECT NRO_TRAMITE FROM GESTIONES WHERE NRO_TRAMITE='" + idTramite + "'";

                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataSet ds = new DataSet();

                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0 || idTramite == "")
                {
                    valido = true;
                }

            }
            catch (Exception e)
            {
                Logins.LogError(e);
            }

            return valido;
        }



        private void chbUrgente_CheckedChanged(object sender, EventArgs e)
        {
            if (chbUrgente.Checked)
            {
                chbLiberar.Checked = false;
            }
        }

        private void chbLiberar_CheckedChanged(object sender, EventArgs e)
        {
            if (chbLiberar.Checked)
            {
                chbUrgente.Checked = false;
            }
        }
    }
}
