﻿namespace Gestion_AP
{
    partial class ModuloPurpura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModuloPurpura));
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.txtPrioridad = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCtaDeb = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtFpago = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtCodPaq = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtConvPS = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtModLiq = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtLimAut = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtLim = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTipoTJ = new System.Windows.Forms.TextBox();
            this.txtTitTJ = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAdministradora = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCuentaTJ = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmbozado = new System.Windows.Forms.TextBox();
            this.txtTarjeta = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DNI = new System.Windows.Forms.Label();
            this.txtDni = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDenomCte = new System.Windows.Forms.TextBox();
            this.txtPersona = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCargar = new System.Windows.Forms.Button();
            this.txtNroTramite = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTramite = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtInfAdic = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMotivo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpGenerales = new System.Windows.Forms.TabPage();
            this.tpDistribucion = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtHorEnt = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDomEnt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDescGeo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCodGeo = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtDomicilio = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtCodOca = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtSucEntrega = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtDomEsp = new System.Windows.Forms.TextBox();
            this.tpAdicionales = new System.Windows.Forms.TabPage();
            this.tpTarjetas = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dgwTarjetas = new System.Windows.Forms.DataGridView();
            this.tpHistoricos = new System.Windows.Forms.TabPage();
            this.gbHistorial = new System.Windows.Forms.GroupBox();
            this.dgvProcesos = new System.Windows.Forms.DataGridView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dgwHistorico = new System.Windows.Forms.DataGridView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtAdicComent = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.chbLiberacion = new System.Windows.Forms.CheckBox();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpGenerales.SuspendLayout();
            this.tpDistribucion.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tpAdicionales.SuspendLayout();
            this.tpTarjetas.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwTarjetas)).BeginInit();
            this.tpHistoricos.SuspendLayout();
            this.gbHistorial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcesos)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwHistorico)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(629, 9);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(93, 47);
            this.btnGuardar.TabIndex = 73;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Silver;
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label32.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label32.Location = new System.Drawing.Point(340, 11);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(67, 17);
            this.label32.TabIndex = 69;
            this.label32.Text = "PRIORIDAD";
            // 
            // txtPrioridad
            // 
            this.txtPrioridad.Location = new System.Drawing.Point(420, 9);
            this.txtPrioridad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPrioridad.Name = "txtPrioridad";
            this.txtPrioridad.Size = new System.Drawing.Size(94, 21);
            this.txtPrioridad.TabIndex = 68;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.CadetBlue;
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.txtCtaDeb);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.txtFpago);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.txtCodPaq);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.txtConvPS);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtModLiq);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txtLimAut);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.txtLim);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtTipoTJ);
            this.groupBox3.Controls.Add(this.txtTitTJ);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtAdministradora);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtCuentaTJ);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtEmbozado);
            this.groupBox3.Controls.Add(this.txtTarjeta);
            this.groupBox3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(5, 111);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Size = new System.Drawing.Size(709, 209);
            this.groupBox3.TabIndex = 71;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "DATOS CUENTA TARJETA";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Gestion_AP.Properties.Resources.About;
            this.pictureBox1.Location = new System.Drawing.Point(411, 84);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 42;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(331, 174);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(90, 15);
            this.label33.TabIndex = 41;
            this.label33.Text = "CUENTA DEBITO";
            // 
            // txtCtaDeb
            // 
            this.txtCtaDeb.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCtaDeb.Location = new System.Drawing.Point(442, 171);
            this.txtCtaDeb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCtaDeb.Name = "txtCtaDeb";
            this.txtCtaDeb.ReadOnly = true;
            this.txtCtaDeb.Size = new System.Drawing.Size(254, 21);
            this.txtCtaDeb.TabIndex = 40;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 174);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(49, 15);
            this.label28.TabIndex = 39;
            this.label28.Text = "F.PAGO";
            // 
            // txtFpago
            // 
            this.txtFpago.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFpago.Location = new System.Drawing.Point(84, 171);
            this.txtFpago.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFpago.Name = "txtFpago";
            this.txtFpago.ReadOnly = true;
            this.txtFpago.Size = new System.Drawing.Size(238, 21);
            this.txtFpago.TabIndex = 38;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(522, 145);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 15);
            this.label24.TabIndex = 37;
            this.label24.Text = "COD.PAQUETE";
            // 
            // txtCodPaq
            // 
            this.txtCodPaq.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodPaq.Location = new System.Drawing.Point(623, 142);
            this.txtCodPaq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodPaq.Name = "txtCodPaq";
            this.txtCodPaq.ReadOnly = true;
            this.txtCodPaq.Size = new System.Drawing.Size(74, 21);
            this.txtCodPaq.TabIndex = 36;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(330, 145);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(83, 15);
            this.label23.TabIndex = 35;
            this.label23.Text = "CONVENIO PS";
            // 
            // txtConvPS
            // 
            this.txtConvPS.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConvPS.Location = new System.Drawing.Point(442, 142);
            this.txtConvPS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtConvPS.Name = "txtConvPS";
            this.txtConvPS.ReadOnly = true;
            this.txtConvPS.Size = new System.Drawing.Size(74, 21);
            this.txtConvPS.TabIndex = 34;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 145);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 15);
            this.label16.TabIndex = 33;
            this.label16.Text = "MOD.LIQ.";
            // 
            // txtModLiq
            // 
            this.txtModLiq.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModLiq.Location = new System.Drawing.Point(84, 142);
            this.txtModLiq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtModLiq.Name = "txtModLiq";
            this.txtModLiq.ReadOnly = true;
            this.txtModLiq.Size = new System.Drawing.Size(238, 21);
            this.txtModLiq.TabIndex = 32;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(329, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(99, 15);
            this.label15.TabIndex = 31;
            this.label15.Text = "LIM. AUTORIZADO";
            // 
            // txtLimAut
            // 
            this.txtLimAut.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLimAut.Location = new System.Drawing.Point(442, 114);
            this.txtLimAut.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLimAut.Name = "txtLimAut";
            this.txtLimAut.ReadOnly = true;
            this.txtLimAut.Size = new System.Drawing.Size(255, 21);
            this.txtLimAut.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 116);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 15);
            this.label14.TabIndex = 29;
            this.label14.Text = "LIMITE";
            // 
            // txtLim
            // 
            this.txtLim.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLim.Location = new System.Drawing.Point(84, 113);
            this.txtLim.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLim.Name = "txtLim";
            this.txtLim.ReadOnly = true;
            this.txtLim.Size = new System.Drawing.Size(238, 21);
            this.txtLim.TabIndex = 28;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(331, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 15);
            this.label13.TabIndex = 27;
            this.label13.Text = "TIPO TJ";
            // 
            // txtTipoTJ
            // 
            this.txtTipoTJ.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTipoTJ.Location = new System.Drawing.Point(442, 28);
            this.txtTipoTJ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTipoTJ.Name = "txtTipoTJ";
            this.txtTipoTJ.ReadOnly = true;
            this.txtTipoTJ.Size = new System.Drawing.Size(254, 21);
            this.txtTipoTJ.TabIndex = 26;
            // 
            // txtTitTJ
            // 
            this.txtTitTJ.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitTJ.Location = new System.Drawing.Point(442, 56);
            this.txtTitTJ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTitTJ.Name = "txtTitTJ";
            this.txtTitTJ.ReadOnly = true;
            this.txtTitTJ.Size = new System.Drawing.Size(254, 21);
            this.txtTitTJ.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(331, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 15);
            this.label8.TabIndex = 23;
            this.label8.Text = "TIT CTA TJ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(329, 87);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 15);
            this.label9.TabIndex = 22;
            this.label9.Text = "EMBOZADO";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 15);
            this.label6.TabIndex = 18;
            this.label6.Text = "ADMINST";
            // 
            // txtAdministradora
            // 
            this.txtAdministradora.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdministradora.Location = new System.Drawing.Point(84, 28);
            this.txtAdministradora.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAdministradora.Name = "txtAdministradora";
            this.txtAdministradora.ReadOnly = true;
            this.txtAdministradora.Size = new System.Drawing.Size(238, 21);
            this.txtAdministradora.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 15);
            this.label5.TabIndex = 16;
            this.label5.Text = "CUENTA TJ";
            // 
            // txtCuentaTJ
            // 
            this.txtCuentaTJ.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCuentaTJ.Location = new System.Drawing.Point(84, 56);
            this.txtCuentaTJ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCuentaTJ.Name = "txtCuentaTJ";
            this.txtCuentaTJ.ReadOnly = true;
            this.txtCuentaTJ.Size = new System.Drawing.Size(238, 21);
            this.txtCuentaTJ.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 15);
            this.label4.TabIndex = 14;
            this.label4.Text = "TARJETA";
            // 
            // txtEmbozado
            // 
            this.txtEmbozado.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmbozado.Location = new System.Drawing.Point(442, 85);
            this.txtEmbozado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtEmbozado.Name = "txtEmbozado";
            this.txtEmbozado.Size = new System.Drawing.Size(254, 21);
            this.txtEmbozado.TabIndex = 10;
            // 
            // txtTarjeta
            // 
            this.txtTarjeta.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTarjeta.Location = new System.Drawing.Point(84, 84);
            this.txtTarjeta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTarjeta.Name = "txtTarjeta";
            this.txtTarjeta.ReadOnly = true;
            this.txtTarjeta.Size = new System.Drawing.Size(238, 21);
            this.txtTarjeta.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 15);
            this.label3.TabIndex = 13;
            this.label3.Text = "CLIENTE";
            // 
            // txtCliente
            // 
            this.txtCliente.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCliente.Location = new System.Drawing.Point(60, 57);
            this.txtCliente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.ReadOnly = true;
            this.txtCliente.Size = new System.Drawing.Size(262, 21);
            this.txtCliente.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Teal;
            this.groupBox2.Controls.Add(this.DNI);
            this.groupBox2.Controls.Add(this.txtDni);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtDenomCte);
            this.groupBox2.Controls.Add(this.txtPersona);
            this.groupBox2.Controls.Add(this.txtNombre);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtCliente);
            this.groupBox2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Lavender;
            this.groupBox2.Location = new System.Drawing.Point(6, 7);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(709, 96);
            this.groupBox2.TabIndex = 70;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DATOS FILIATORIOS";
            // 
            // DNI
            // 
            this.DNI.AutoSize = true;
            this.DNI.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DNI.Location = new System.Drawing.Point(196, 30);
            this.DNI.Name = "DNI";
            this.DNI.Size = new System.Drawing.Size(26, 15);
            this.DNI.TabIndex = 28;
            this.DNI.Text = "DNI";
            // 
            // txtDni
            // 
            this.txtDni.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDni.Location = new System.Drawing.Point(228, 26);
            this.txtDni.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDni.Name = "txtDni";
            this.txtDni.ReadOnly = true;
            this.txtDni.Size = new System.Drawing.Size(94, 21);
            this.txtDni.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(330, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 15);
            this.label10.TabIndex = 21;
            this.label10.Text = "DENOM. CLTE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(331, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 15);
            this.label11.TabIndex = 20;
            this.label11.Text = "NOMBRE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 12;
            this.label2.Text = "CUIL";
            // 
            // txtDenomCte
            // 
            this.txtDenomCte.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDenomCte.Location = new System.Drawing.Point(443, 57);
            this.txtDenomCte.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDenomCte.Name = "txtDenomCte";
            this.txtDenomCte.ReadOnly = true;
            this.txtDenomCte.Size = new System.Drawing.Size(254, 21);
            this.txtDenomCte.TabIndex = 8;
            // 
            // txtPersona
            // 
            this.txtPersona.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersona.Location = new System.Drawing.Point(60, 26);
            this.txtPersona.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPersona.Name = "txtPersona";
            this.txtPersona.ReadOnly = true;
            this.txtPersona.Size = new System.Drawing.Size(130, 21);
            this.txtPersona.TabIndex = 6;
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(443, 27);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(254, 21);
            this.txtNombre.TabIndex = 5;
            // 
            // txtCargar
            // 
            this.txtCargar.Location = new System.Drawing.Point(520, 9);
            this.txtCargar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtCargar.Name = "txtCargar";
            this.txtCargar.Size = new System.Drawing.Size(97, 47);
            this.txtCargar.TabIndex = 64;
            this.txtCargar.Text = "CARGAR";
            this.txtCargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.txtCargar.UseVisualStyleBackColor = true;
            this.txtCargar.Click += new System.EventHandler(this.txtCargar_Click);
            // 
            // txtNroTramite
            // 
            this.txtNroTramite.Location = new System.Drawing.Point(168, 8);
            this.txtNroTramite.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNroTramite.Name = "txtNroTramite";
            this.txtNroTramite.ReadOnly = true;
            this.txtNroTramite.Size = new System.Drawing.Size(166, 21);
            this.txtNroTramite.TabIndex = 66;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(39, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 67;
            this.label1.Text = "TRAMITE";
            // 
            // txtTramite
            // 
            this.txtTramite.Location = new System.Drawing.Point(113, 8);
            this.txtTramite.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTramite.Name = "txtTramite";
            this.txtTramite.ReadOnly = true;
            this.txtTramite.Size = new System.Drawing.Size(49, 21);
            this.txtTramite.TabIndex = 65;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Gray;
            this.groupBox5.Controls.Add(this.txtInfAdic);
            this.groupBox5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.Black;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(715, 159);
            this.groupBox5.TabIndex = 75;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "DATOS ADICIONALES";
            // 
            // txtInfAdic
            // 
            this.txtInfAdic.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfAdic.Location = new System.Drawing.Point(6, 20);
            this.txtInfAdic.Multiline = true;
            this.txtInfAdic.Name = "txtInfAdic";
            this.txtInfAdic.ReadOnly = true;
            this.txtInfAdic.Size = new System.Drawing.Size(705, 133);
            this.txtInfAdic.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkGray;
            this.groupBox1.Controls.Add(this.txtObservaciones);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.txtMotivo);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(2, 168);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(715, 162);
            this.groupBox1.TabIndex = 74;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DATOS TRAMITE";
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObservaciones.Location = new System.Drawing.Point(108, 59);
            this.txtObservaciones.Multiline = true;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.ReadOnly = true;
            this.txtObservaciones.Size = new System.Drawing.Size(603, 66);
            this.txtObservaciones.TabIndex = 57;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(6, 83);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(98, 15);
            this.label26.TabIndex = 58;
            this.label26.Text = "OBSERVACIONES";
            // 
            // txtMotivo
            // 
            this.txtMotivo.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivo.Location = new System.Drawing.Point(108, 32);
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.ReadOnly = true;
            this.txtMotivo.Size = new System.Drawing.Size(603, 21);
            this.txtMotivo.TabIndex = 55;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(50, 35);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(50, 15);
            this.label25.TabIndex = 56;
            this.label25.Text = "MOTIVO";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpGenerales);
            this.tabControl1.Controls.Add(this.tpDistribucion);
            this.tabControl1.Controls.Add(this.tpAdicionales);
            this.tabControl1.Controls.Add(this.tpTarjetas);
            this.tabControl1.Controls.Add(this.tpHistoricos);
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.Location = new System.Drawing.Point(3, 64);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(728, 359);
            this.tabControl1.TabIndex = 76;
            // 
            // tpGenerales
            // 
            this.tpGenerales.BackColor = System.Drawing.Color.MidnightBlue;
            this.tpGenerales.Controls.Add(this.groupBox2);
            this.tpGenerales.Controls.Add(this.groupBox3);
            this.tpGenerales.ImageIndex = 4;
            this.tpGenerales.Location = new System.Drawing.Point(4, 31);
            this.tpGenerales.Name = "tpGenerales";
            this.tpGenerales.Padding = new System.Windows.Forms.Padding(3);
            this.tpGenerales.Size = new System.Drawing.Size(720, 324);
            this.tpGenerales.TabIndex = 0;
            this.tpGenerales.Text = "DATOS GENERALES";
            // 
            // tpDistribucion
            // 
            this.tpDistribucion.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.tpDistribucion.Controls.Add(this.groupBox9);
            this.tpDistribucion.ImageIndex = 3;
            this.tpDistribucion.Location = new System.Drawing.Point(4, 31);
            this.tpDistribucion.Name = "tpDistribucion";
            this.tpDistribucion.Padding = new System.Windows.Forms.Padding(3);
            this.tpDistribucion.Size = new System.Drawing.Size(720, 324);
            this.tpDistribucion.TabIndex = 3;
            this.tpDistribucion.Text = "DISTRIBUCION";
            this.tpDistribucion.Click += new System.EventHandler(this.tpDistribucion_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.txtTelefono);
            this.groupBox9.Controls.Add(this.label7);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.txtHorEnt);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Controls.Add(this.txtDomEnt);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.txtDescGeo);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.txtCodGeo);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.txtDomicilio);
            this.groupBox9.Controls.Add(this.label31);
            this.groupBox9.Controls.Add(this.txtCodOca);
            this.groupBox9.Controls.Add(this.label30);
            this.groupBox9.Controls.Add(this.txtSucEntrega);
            this.groupBox9.Controls.Add(this.label29);
            this.groupBox9.Controls.Add(this.txtDomEsp);
            this.groupBox9.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.Color.Crimson;
            this.groupBox9.Location = new System.Drawing.Point(3, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(714, 228);
            this.groupBox9.TabIndex = 87;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "DATOS DE DISTRIBUCION";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(316, 26);
            this.txtTelefono.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.ReadOnly = true;
            this.txtTelefono.Size = new System.Drawing.Size(374, 21);
            this.txtTelefono.TabIndex = 86;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.OrangeRed;
            this.label7.Location = new System.Drawing.Point(238, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 15);
            this.label7.TabIndex = 85;
            this.label7.Text = "TELEFONO";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.OrangeRed;
            this.label22.Location = new System.Drawing.Point(39, 139);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(110, 15);
            this.label22.TabIndex = 84;
            this.label22.Text = "HORARIO ENTREGA";
            // 
            // txtHorEnt
            // 
            this.txtHorEnt.Location = new System.Drawing.Point(183, 136);
            this.txtHorEnt.Name = "txtHorEnt";
            this.txtHorEnt.ReadOnly = true;
            this.txtHorEnt.Size = new System.Drawing.Size(507, 21);
            this.txtHorEnt.TabIndex = 83;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.OrangeRed;
            this.label21.Location = new System.Drawing.Point(36, 112);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(115, 15);
            this.label21.TabIndex = 82;
            this.label21.Text = "DOMICILIO ENTREGA";
            // 
            // txtDomEnt
            // 
            this.txtDomEnt.Location = new System.Drawing.Point(183, 109);
            this.txtDomEnt.Name = "txtDomEnt";
            this.txtDomEnt.ReadOnly = true;
            this.txtDomEnt.Size = new System.Drawing.Size(507, 21);
            this.txtDomEnt.TabIndex = 81;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.OrangeRed;
            this.label19.Location = new System.Drawing.Point(379, 56);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(90, 15);
            this.label19.TabIndex = 78;
            this.label19.Text = "DESC.COD.GEO";
            // 
            // txtDescGeo
            // 
            this.txtDescGeo.Location = new System.Drawing.Point(500, 53);
            this.txtDescGeo.Name = "txtDescGeo";
            this.txtDescGeo.ReadOnly = true;
            this.txtDescGeo.Size = new System.Drawing.Size(190, 21);
            this.txtDescGeo.TabIndex = 77;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.OrangeRed;
            this.label17.Location = new System.Drawing.Point(38, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(111, 15);
            this.label17.TabIndex = 76;
            this.label17.Text = "COD. GEOGRAFICO";
            // 
            // txtCodGeo
            // 
            this.txtCodGeo.Location = new System.Drawing.Point(183, 53);
            this.txtCodGeo.Name = "txtCodGeo";
            this.txtCodGeo.ReadOnly = true;
            this.txtCodGeo.Size = new System.Drawing.Size(190, 21);
            this.txtCodGeo.TabIndex = 69;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.OrangeRed;
            this.label18.Location = new System.Drawing.Point(43, 84);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 15);
            this.label18.TabIndex = 75;
            this.label18.Text = "DOMICILIO ACTUAL";
            // 
            // txtDomicilio
            // 
            this.txtDomicilio.Location = new System.Drawing.Point(183, 81);
            this.txtDomicilio.Name = "txtDomicilio";
            this.txtDomicilio.ReadOnly = true;
            this.txtDomicilio.Size = new System.Drawing.Size(507, 21);
            this.txtDomicilio.TabIndex = 68;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.OrangeRed;
            this.label31.Location = new System.Drawing.Point(69, 193);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(80, 15);
            this.label31.TabIndex = 74;
            this.label31.Text = "CODIGO OCA";
            // 
            // txtCodOca
            // 
            this.txtCodOca.Location = new System.Drawing.Point(183, 190);
            this.txtCodOca.Name = "txtCodOca";
            this.txtCodOca.ReadOnly = true;
            this.txtCodOca.Size = new System.Drawing.Size(507, 21);
            this.txtCodOca.TabIndex = 71;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.OrangeRed;
            this.label30.Location = new System.Drawing.Point(18, 166);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(131, 15);
            this.label30.TabIndex = 73;
            this.label30.Text = "SUCURSAL DE ENTREGA";
            // 
            // txtSucEntrega
            // 
            this.txtSucEntrega.Location = new System.Drawing.Point(183, 163);
            this.txtSucEntrega.Name = "txtSucEntrega";
            this.txtSucEntrega.ReadOnly = true;
            this.txtSucEntrega.Size = new System.Drawing.Size(507, 21);
            this.txtSucEntrega.TabIndex = 70;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.OrangeRed;
            this.label29.Location = new System.Drawing.Point(28, 29);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(125, 15);
            this.label29.TabIndex = 72;
            this.label29.Text = "¿DOMICILIO ESPECIAL?";
            // 
            // txtDomEsp
            // 
            this.txtDomEsp.Location = new System.Drawing.Point(183, 26);
            this.txtDomEsp.Name = "txtDomEsp";
            this.txtDomEsp.ReadOnly = true;
            this.txtDomEsp.Size = new System.Drawing.Size(48, 21);
            this.txtDomEsp.TabIndex = 67;
            // 
            // tpAdicionales
            // 
            this.tpAdicionales.BackColor = System.Drawing.Color.DimGray;
            this.tpAdicionales.Controls.Add(this.groupBox5);
            this.tpAdicionales.Controls.Add(this.groupBox1);
            this.tpAdicionales.ImageIndex = 1;
            this.tpAdicionales.Location = new System.Drawing.Point(4, 31);
            this.tpAdicionales.Name = "tpAdicionales";
            this.tpAdicionales.Padding = new System.Windows.Forms.Padding(3);
            this.tpAdicionales.Size = new System.Drawing.Size(720, 324);
            this.tpAdicionales.TabIndex = 1;
            this.tpAdicionales.Text = "DATOS ADICIONALES";
            this.tpAdicionales.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // tpTarjetas
            // 
            this.tpTarjetas.BackColor = System.Drawing.Color.OrangeRed;
            this.tpTarjetas.Controls.Add(this.groupBox8);
            this.tpTarjetas.Controls.Add(this.groupBox6);
            this.tpTarjetas.ImageIndex = 0;
            this.tpTarjetas.Location = new System.Drawing.Point(4, 31);
            this.tpTarjetas.Name = "tpTarjetas";
            this.tpTarjetas.Padding = new System.Windows.Forms.Padding(3);
            this.tpTarjetas.Size = new System.Drawing.Size(720, 324);
            this.tpTarjetas.TabIndex = 4;
            this.tpTarjetas.Text = "Tarjetas";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Controls.Add(this.label35);
            this.groupBox8.Controls.Add(this.label36);
            this.groupBox8.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(2, 238);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(146, 86);
            this.groupBox8.TabIndex = 73;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "REFERENCIAS ESTADO";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(6, 65);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(92, 13);
            this.label34.TabIndex = 69;
            this.label34.Text = "3 = BOLETINADA (B)";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(6, 33);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(103, 13);
            this.label37.TabIndex = 72;
            this.label37.Text = "1 = DADA DE BAJA (X)";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(6, 17);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(61, 13);
            this.label35.TabIndex = 70;
            this.label35.Text = "0 = NORMAL";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(6, 49);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(96, 13);
            this.label36.TabIndex = 71;
            this.label36.Text = "2 = INHABILITADA (I)";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dgwTarjetas);
            this.groupBox6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.GhostWhite;
            this.groupBox6.Location = new System.Drawing.Point(1, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(716, 232);
            this.groupBox6.TabIndex = 67;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Tarjetas DE LA CUENTA";
            // 
            // dgwTarjetas
            // 
            this.dgwTarjetas.AllowUserToAddRows = false;
            this.dgwTarjetas.AllowUserToDeleteRows = false;
            this.dgwTarjetas.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgwTarjetas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgwTarjetas.BackgroundColor = System.Drawing.Color.White;
            this.dgwTarjetas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgwTarjetas.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgwTarjetas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgwTarjetas.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.dgwTarjetas.Location = new System.Drawing.Point(3, 17);
            this.dgwTarjetas.Name = "dgwTarjetas";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ScrollBar;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgwTarjetas.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.MediumAquamarine;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dgwTarjetas.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgwTarjetas.Size = new System.Drawing.Size(710, 212);
            this.dgwTarjetas.TabIndex = 28;
            this.dgwTarjetas.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgwTarjetas_CellMouseDown);
            // 
            // tpHistoricos
            // 
            this.tpHistoricos.BackColor = System.Drawing.Color.Indigo;
            this.tpHistoricos.Controls.Add(this.gbHistorial);
            this.tpHistoricos.Controls.Add(this.groupBox7);
            this.tpHistoricos.ImageIndex = 2;
            this.tpHistoricos.Location = new System.Drawing.Point(4, 31);
            this.tpHistoricos.Name = "tpHistoricos";
            this.tpHistoricos.Padding = new System.Windows.Forms.Padding(3);
            this.tpHistoricos.Size = new System.Drawing.Size(720, 324);
            this.tpHistoricos.TabIndex = 2;
            this.tpHistoricos.Text = "HISTORICOS";
            // 
            // gbHistorial
            // 
            this.gbHistorial.Controls.Add(this.dgvProcesos);
            this.gbHistorial.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbHistorial.ForeColor = System.Drawing.SystemColors.Control;
            this.gbHistorial.Location = new System.Drawing.Point(3, 175);
            this.gbHistorial.Name = "gbHistorial";
            this.gbHistorial.Size = new System.Drawing.Size(711, 149);
            this.gbHistorial.TabIndex = 68;
            this.gbHistorial.TabStop = false;
            this.gbHistorial.Text = "HISTORICO PROCESO";
            // 
            // dgvProcesos
            // 
            this.dgvProcesos.AllowUserToAddRows = false;
            this.dgvProcesos.AllowUserToDeleteRows = false;
            this.dgvProcesos.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvProcesos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvProcesos.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvProcesos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Goldenrod;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.DarkGoldenrod;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProcesos.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvProcesos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProcesos.Location = new System.Drawing.Point(3, 17);
            this.dgvProcesos.Name = "dgvProcesos";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Olive;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Olive;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvProcesos.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvProcesos.Size = new System.Drawing.Size(705, 129);
            this.dgvProcesos.TabIndex = 0;
            this.dgvProcesos.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProcesos_CellContentDoubleClick);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dgwHistorico);
            this.groupBox7.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.ForeColor = System.Drawing.Color.GhostWhite;
            this.groupBox7.Location = new System.Drawing.Point(3, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(711, 166);
            this.groupBox7.TabIndex = 66;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "HISTORICO TRAMITE";
            // 
            // dgwHistorico
            // 
            this.dgwHistorico.AllowUserToAddRows = false;
            this.dgwHistorico.AllowUserToDeleteRows = false;
            this.dgwHistorico.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgwHistorico.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgwHistorico.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgwHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Salmon;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Salmon;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgwHistorico.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgwHistorico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgwHistorico.Location = new System.Drawing.Point(3, 17);
            this.dgwHistorico.Name = "dgwHistorico";
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.dgwHistorico.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgwHistorico.Size = new System.Drawing.Size(705, 146);
            this.dgwHistorico.TabIndex = 0;
            this.dgwHistorico.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwHistorico_CellContentDoubleClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Entypo_d83d(0)_48.png");
            this.imageList1.Images.SetKeyName(1, "FontAwesome_f08d(0)_48.png");
            this.imageList1.Images.SetKeyName(2, "linecons_e018(0)_48.png");
            this.imageList1.Images.SetKeyName(3, "linecons_e019(0)_48.png");
            this.imageList1.Images.SetKeyName(4, "linecons_e021(0)_48.png");
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Black;
            this.groupBox4.Controls.Add(this.txtAdicComent);
            this.groupBox4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.AliceBlue;
            this.groupBox4.Location = new System.Drawing.Point(3, 429);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(724, 137);
            this.groupBox4.TabIndex = 77;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "COMENTARIO ADICIONAL";
            // 
            // txtAdicComent
            // 
            this.txtAdicComent.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdicComent.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdicComent.Location = new System.Drawing.Point(4, 20);
            this.txtAdicComent.Multiline = true;
            this.txtAdicComent.Name = "txtAdicComent";
            this.txtAdicComent.Size = new System.Drawing.Size(718, 110);
            this.txtAdicComent.TabIndex = 33;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Silver;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(10, 38);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 17);
            this.label12.TabIndex = 79;
            this.label12.Text = "DESCRIPCION";
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(113, 35);
            this.txtDesc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.ReadOnly = true;
            this.txtDesc.Size = new System.Drawing.Size(280, 21);
            this.txtDesc.TabIndex = 78;
            // 
            // chbLiberacion
            // 
            this.chbLiberacion.AutoSize = true;
            this.chbLiberacion.ForeColor = System.Drawing.Color.Red;
            this.chbLiberacion.Location = new System.Drawing.Point(399, 36);
            this.chbLiberacion.Name = "chbLiberacion";
            this.chbLiberacion.Size = new System.Drawing.Size(121, 20);
            this.chbLiberacion.TabIndex = 80;
            this.chbLiberacion.Text = "CON LIBERACIÓN";
            this.chbLiberacion.UseVisualStyleBackColor = true;
            // 
            // ModuloPurpura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(733, 571);
            this.Controls.Add(this.chbLiberacion);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.txtPrioridad);
            this.Controls.Add(this.txtCargar);
            this.Controls.Add(this.txtNroTramite);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTramite);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "ModuloPurpura";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administración de Tarjetas: WorkFlow";
            this.Load += new System.EventHandler(this.ModuloPurpura_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpGenerales.ResumeLayout(false);
            this.tpDistribucion.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tpAdicionales.ResumeLayout(false);
            this.tpTarjetas.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgwTarjetas)).EndInit();
            this.tpHistoricos.ResumeLayout(false);
            this.gbHistorial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcesos)).EndInit();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgwHistorico)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtPrioridad;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtTitTJ;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAdministradora;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCuentaTJ;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmbozado;
        private System.Windows.Forms.TextBox txtTarjeta;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label DNI;
        private System.Windows.Forms.TextBox txtDni;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDenomCte;
        private System.Windows.Forms.TextBox txtPersona;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button txtCargar;
        private System.Windows.Forms.TextBox txtNroTramite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTramite;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTipoTJ;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtModLiq;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtLimAut;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtLim;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtInfAdic;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtMotivo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpGenerales;
        private System.Windows.Forms.TabPage tpAdicionales;
        private System.Windows.Forms.TabPage tpHistoricos;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dgwHistorico;
        private System.Windows.Forms.GroupBox gbHistorial;
        private System.Windows.Forms.DataGridView dgvProcesos;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtAdicComent;
        private System.Windows.Forms.TabPage tpDistribucion;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCodGeo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtDomicilio;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtCodOca;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtSucEntrega;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtDomEsp;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtCtaDeb;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtFpago;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtCodPaq;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtConvPS;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtHorEnt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtDomEnt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDescGeo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.TabPage tpTarjetas;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dgwTarjetas;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox chbLiberacion;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}