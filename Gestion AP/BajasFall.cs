﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Text.RegularExpressions;


namespace Gestion_AP
{
    public partial class BajasFall : Form
    {
        public BajasFall()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdInterno"></param>
        /// <param name="ComentarioI"></param>
        /// <param name="ComentarioR"></param>
        /// <param name="Estado"></param>
        /// 
        public string ID;
        public BajasFall(string Control, string Estado)

        {
            InitializeComponent();
            


            try
            {
                txtGrabar.Click -= new System.EventHandler(this.txtGrabar_Click);
                btnBorrar.Click -= new System.EventHandler(this.btnBorrar_Click);
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM BAJAS WHERE NroTramite='" + Control + "'";
                DataTable dt = new DataTable();
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count == 1)
                {
                    txtAdm.Text = dt.Rows[0]["ADM"].ToString();
                    txtDenominacion.Text = dt.Rows[0]["DENOMINACION"].ToString();
                    txtCuenta.Text = dt.Rows[0]["NRO_CUENTA"].ToString();
                    txtSucursal.Text = dt.Rows[0]["SUCURSAL"].ToString();
                    txtTarjeta.Text = dt.Rows[0]["TARJETA"].ToString();

                    txtAdm.ReadOnly = true;
                    txtDenominacion.ReadOnly = true;
                    txtCuenta.ReadOnly = true;
                    txtSucursal.ReadOnly = true;
                    txtTarjeta.ReadOnly = true;


                }
                dt.Dispose();
                da.Dispose();

                if (Estado == "RECHAZADO")
                {

                    txtGrabar.Visible = false;
                    btnBorrar.Click += new EventHandler(Corregido);
                    this.Text = "Rechazo: " + Control;
                    this.ShowIcon = false;
                    this.Icon = Gestion_AP.Properties.Resources.ntfPendientes;
                }
                if (Estado == "ABIERTO")
                {
                    txtGrabar.Text = "PROCESADO OK";
                    txtGrabar.Click += new System.EventHandler(this.Controlado);
                    btnBorrar.Click += new EventHandler(Reprocesar);
                    this.Text = "Control: " + Control;
                    this.ShowIcon = false;
                    this.Icon = Gestion_AP.Properties.Resources.Icono;

                }


                btnBorrar.Text = "REPROCESAR";
                chbNoRen.Visible = false;
                btnCaptura.Visible = false;
                cmd.CommandText = "SELECT * FROM Procesos WHERE NroTramite='" + Control + "'";
                OleDbDataAdapter dah = new OleDbDataAdapter(cmd);
                DataTable dth = new DataTable();
                dah.Fill(dth);



                dgvHistorial.DataSource = dth;
                AspectoTabla.AspectoComent(dgvHistorial);




            }
            catch (Exception ex)
            {

                Logins.LogError(ex);
            }


            this.Size = new Size(697, 415);

            gbHistorial.Visible = true;


            ID = Control;






        }

        private void Corregido(object sender, EventArgs e)

        {

            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + ID + "'";
                cmd.Parameters.AddWithValue("@EST", "ABIERTO");
                cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today));
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", ID);
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", txtComAdic.Text.Trim());
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                MessageBox.Show("Trámite Reprocesado.", "Reproceso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Logins.Estadistica("REPROCESO", ID);
                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Rechazos").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Rechazos.EliminarRegistro(ID);
                        Rechazos.TotRech--;
                        Rechazos.TotUs--;
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }
                this.Close();
                cn.Close();
            }
            catch (Exception ex) { Logins.LogError(ex); }
        }
    

        private void Reprocesar(object sender, EventArgs e)

        {
            if (txtComAdic.Text == "")
            {
                MessageBox.Show("Debe ingresar un comentario para rechazar trámite.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    OleDbConnection cn = new OleDbConnection();
                    cn.ConnectionString = Conexion.cnProceso;
                    OleDbCommand cmd = new OleDbCommand();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + ID + "'";
                    cmd.Parameters.AddWithValue("@EST", "RECHAZADO");
                    cmd.Parameters.AddWithValue("@ACONTROL", DateTime.Today);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                    cmd.Parameters.AddWithValue("@ID", ID);
                    cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                    cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                    cmd.Parameters.AddWithValue("@COM", txtComAdic.Text.Trim());
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    MessageBox.Show("Trámite enviado a reproceso.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Logins.Estadistica("CONTROL", ID);

                    try
                    {
                        Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                        if (existe != null)
                        {
                            Control.EliminarRegistro(ID);
                        }
                    }
                    catch (Exception ex) { Logins.LogError(ex); }
                    this.Close();
                    cn.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    Logins.LogError(ex);

                }

            }
        }

        private void Controlado(object sender, EventArgs e)

        {
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST WHERE NRO_TRAMITE='" + ID + "'";
                cmd.Parameters.AddWithValue("@EST", "CERRADO");

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", ID);
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", "TRÁMITE FINALIZADO CORRECTAMENTE");
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("CONTROL",ID);

                MessageBox.Show("Trámite finalizado correctamente.", "Aceptado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Control.EliminarRegistro(ID);
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }
                this.Close();
                cn.Close();



            }
            catch (Exception ex) { MessageBox.Show(ex.Message); Logins.LogError(ex); }

        }



        private void btnCaptura_Click(object sender, EventArgs e)
        {
            string Datos = Clipboard.GetText();

            try
            {
                if (Datos.Contains("Banco/Casa :  634") || Datos.Contains("Bco/Casa: 634"))
                {
                    if (Datos.Contains("Tarjeta:"))
                    {

                        // BAJA ADICIONAL
                        int inicio = Datos.IndexOf("Tarjeta:");
                        txtTarjeta.Text = Datos.Substring(inicio + 10, 16).Trim();

                        int fin = Datos.IndexOf("Tipo");
                        txtDenominacion.Text = Datos.Substring(inicio + 30, fin - inicio - 32).Trim();
                        inicio = Datos.IndexOf("Cuenta:");
                        txtCuenta.Text = Datos.Substring(inicio + 10, 11).Trim();
                        inicio = Datos.IndexOf("Banco/Casa :");
                        txtSucursal.Text = Datos.Substring(inicio + 18, 4).Trim();



                    }
                    else
                    {

                        // BAJA CUENTA
                        int inicio = Datos.IndexOf("Cuenta:");
                        txtCuenta.Text = Datos.Substring(inicio + 7, 11).Trim();
                        int fin = Datos.IndexOf("Bco/");
                        txtDenominacion.Text = Datos.Substring(inicio + 21, fin - 21).Trim();
                        inicio = Datos.IndexOf("Casa");
                        txtSucursal.Text = Datos.Substring(inicio + 9, 4).Trim();
                        txtTarjeta.Text = "BAJA DE CUENTA - TODAS LAS Tarjetas";

                    }
                    txtAdm.Text = "AMERICAN EXPRESS";

                }
                if (Datos.Contains("Banco/Casa :  034") || Datos.Contains("Bco/Casa: 034"))
                {
                    if (Datos.Contains("Tarjeta:"))
                    {

                        // BAJA ADICIONAL
                        int inicio = Datos.IndexOf("Tarjeta:");
                        txtTarjeta.Text = Datos.Substring(inicio + 10, 16).Trim();

                        int fin = Datos.IndexOf("Tipo");
                        txtDenominacion.Text = Datos.Substring(inicio + 30, fin - inicio - 32).Trim();
                        inicio = Datos.IndexOf("Cuenta:");
                        txtCuenta.Text = Datos.Substring(inicio + 10, 11).Trim();
                        inicio = Datos.IndexOf("Banco/Casa :");
                        txtSucursal.Text = Datos.Substring(inicio + 18, 4).Trim();
                        txtDenominacion.Text = txtDenominacion.Text.Replace("/", " ");



                    }
                    else
                    {

                        // BAJA CUENTA
                        int inicio = Datos.IndexOf("Cuenta:");
                        txtCuenta.Text = Datos.Substring(inicio + 7, 11).Trim();
                        int fin = Datos.IndexOf("Bco/");
                        txtDenominacion.Text = Datos.Substring(inicio + 21, fin - 21).Trim();
                        inicio = Datos.IndexOf("Casa");
                        txtSucursal.Text = Datos.Substring(inicio + 9, 4).Trim();
                        txtTarjeta.Text = "BAJA DE CUENTA - TODAS LAS Tarjetas";

                    }
                    txtAdm.Text = "VISA";

                }
                if (Datos.Contains("MASTERCARD"))
                {

                    int inicio = Datos.IndexOf("NUMERO DE CUENTA :");

                    string cuenta = Regex.Replace(Datos.Substring(inicio + 19, 19).Trim(), @"[^\d]", "");
                    txtCuenta.Text = cuenta;
                    int fin = Datos.IndexOf("TIPO DE SOCIO");
                    string nombre = Regex.Replace(Datos.Substring(0, fin), @"[0-9\p{P}\d]", "");

                    nombre = nombre.Replace("NUMERO DE CUENTA", "").Trim();

                    txtDenominacion.Text = nombre;

                    inicio = Datos.IndexOf("NRO. DE TARJETA");
                    string tarjeta = Regex.Replace(Datos.Substring(inicio + 12, 25), @"[^\d]", "");
                    txtTarjeta.Text = tarjeta;
                    txtAdm.Text = "MASTERCARD";

                    inicio = Datos.IndexOf("SUCURSAL:");
                    string sucursal = Regex.Replace(Datos.Substring(inicio).Trim(), @"[^\d]", "");
                    txtSucursal.Text = sucursal.Substring(0, 3);


                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error al capturar los datos: " + ex.Message);
                Logins.LogError(ex);
            }




        }
        public void Limpiar()

        {
            txtTarjeta.Clear();
            txtAdm.Clear();
            txtDenominacion.Clear();
            txtCuenta.Clear();
            txtSucursal.Clear();


        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            Limpiar();

        }

        public bool Validacion()
        {

            if (txtAdm.Text == "" && txtCuenta.Text == "" && txtDenominacion.Text == "" && txtSucursal.Text == "" && txtTarjeta.Text == "")
            {
                return false;
            }
            else
            {
                return true;

            }


        }

        private void txtGrabar_Click(object sender, EventArgs e)
        {

            if (Validacion() == true)
            {
                try
                {

                    OleDbConnection cn = new OleDbConnection();
                    cn.ConnectionString = Conexion.cnProceso;
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO GESTIONES(US_INICIO,FECHA_INICIO,EMP,TRAMITE,WF,NRO_TRAMITE,PRIORIDAD,CANAL_INGRESO,ESTADO,A_CONTROLAR) VALUES(@US_INICIO,@FECHA_INICIO,@EMP,@TRAMITE,@WF,@NRO_TRAMITE,@PRIORIDAD,@CANAL_INGRESO,@ESTADO,@ACONTROL)";

                    cn.Open();

                    string Id_Interno = "TJ" + DateTime.Now.ToString("yyyyMMddHHmmss");

                    cmd.Parameters.AddWithValue("@US_INICIO", Ingreso.UsuarioLogueado);
                    cmd.Parameters.AddWithValue("@FECHA_INICIO", DateTime.Now.ToOADate());
                    cmd.Parameters.AddWithValue("@EMP", txtAdm.Text.Trim());
                    if (chbNoRen.Checked)
                    {
                        cmd.Parameters.AddWithValue("@TRAMITE", "MARCA NO RENOVACION");
                        cmd.Parameters.AddWithValue("@WF", "MNOR");
                        cmd.Parameters.AddWithValue("@NRO_TRAMITE", Id_Interno);
                        cmd.Parameters.AddWithValue("@PRIORIDAD", "NORMAL");
                        cmd.Parameters.AddWithValue("@CANAL_INGRESO", "SEGURIDAD");

                        cmd.Parameters.AddWithValue("@ESTADO", "ABIERTO");
                        cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today).ToOADate());

                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@TRAMITE", "BAJAS");
                        cmd.Parameters.AddWithValue("@WF", "BAJAS");
                        cmd.Parameters.AddWithValue("@NRO_TRAMITE", Id_Interno);
                        cmd.Parameters.AddWithValue("@PRIORIDAD", "NORMAL");
                        cmd.Parameters.AddWithValue("@CANAL_INGRESO", "CALLCENTER/FALLECIDOS");

                        cmd.Parameters.AddWithValue("@ESTADO", "ABIERTO");
                        cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today).ToOADate());

                    }



                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    cmd.CommandText = "INSERT INTO BAJAS(NroTramite,ADM,DENOMINACION,NRO_CUENTA,SUCURSAL,TARJETA) VALUES (@ID,@ADM,@DENOM,@CTA,@SUC,@TJ)";
                    cmd.Parameters.AddWithValue("@ID", Id_Interno);
                    cmd.Parameters.AddWithValue("@ADM", txtAdm.Text.Trim());
                    cmd.Parameters.AddWithValue("@DENOM", txtDenominacion.Text.Trim());
                    cmd.Parameters.AddWithValue("@CTA", txtCuenta.Text.Trim());
                    cmd.Parameters.AddWithValue("@SUC", txtSucursal.Text.Trim());
                    cmd.Parameters.AddWithValue("@TJ", txtTarjeta.Text.Trim());

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                    cmd.Parameters.AddWithValue("@ID", Id_Interno);
                    cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                    cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                    cmd.Parameters.AddWithValue("@COM", txtComAdic.Text.Trim());
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    cn.Close();
                    Limpiar();
                    txtComAdic.Text = "";
                    Logins.Estadistica("PROCESO", Id_Interno);
                    MessageBox.Show("Baja grabada correctamente.", "Tramite Baja", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }


                catch (Exception EX)
                {

                    MessageBox.Show("Error al grabar. Verifique los datos copiados. " + EX.Message);
                    Logins.LogError(EX);

                }

            }
            else
            {
                MessageBox.Show("Faltan datos en el formulario");

            }
        }
    }
}
