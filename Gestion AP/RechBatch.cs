﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Gestion_AP
{
    public partial class RechBatch : Form
    {
        public RechBatch()
        {
            InitializeComponent();
        
        }

        private void RechBatch_Load(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnUsuario;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT Usuario, Nombre, Apellido FROM Usuarios WHERE Fecha_Baja is NULL";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cmbOperador.Items.Add(
                        Convert.ToString(dt.Rows[i][0]) + " *** " +
                        Convert.ToString(dt.Rows[i][1]) + " " +
                        Convert.ToString(dt.Rows[i][2])
                        );                       
                }

            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }
            

        }

        

        

        private void RechBatch_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {

                switch (e.KeyCode)
                {

                    case Keys.F9:

                        if (cmbOperador.SelectedIndex >= 0)
                        {

                            if (txtRechazo.Text.Length > 30)
                            {


                                OleDbConnection cn = new OleDbConnection();
                                cn.ConnectionString = Conexion.cnProceso;
                                OleDbCommand cmd = new OleDbCommand();
                                cmd.Connection = cn;
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = "INSERT INTO GESTIONES(US_INICIO,FECHA_INICIO,EMP,TRAMITE,WF,NRO_TRAMITE,PRIORIDAD,CANAL_INGRESO,ESTADO,A_CONTROLAR) VALUES(@US_INICIO,@FECHA_INICIO,@EMP,@TRAMITE,@WF,@NRO_TRAMITE,@PRIORIDAD,@CANAL_INGRESO,@ESTADO,@ACONTROL)";

                                cn.Open();

                                string ID_Tramite = "RECH" + DateTime.Now.ToString("yyyyMMddHHmmss");
                                string usuario = cmbOperador.SelectedItem.ToString().Split('*')[0].ToString();
                                cmd.Parameters.AddWithValue("@US_INICIO", usuario);
                                cmd.Parameters.AddWithValue("@FECHA_INICIO", DateTime.Now.ToOADate());
                                if (rbtVisa.Checked)
                                {
                                    cmd.Parameters.AddWithValue("@EMP", "VISA");
                                    cmd.Parameters.AddWithValue("@TRAMITE", "RECHAZOS VISA");
                                    cmd.Parameters.AddWithValue("@WF", "RECHV");
                                }
                                if (rbtAmex.Checked)
                                {
                                    cmd.Parameters.AddWithValue("@EMP", "AMERICAN EXPRESS");
                                    cmd.Parameters.AddWithValue("@TRAMITE", "RECHAZOS AMERICAN EXPRESS");
                                    cmd.Parameters.AddWithValue("@WF", "RECHX");
                                }
                                if (rbtMaster.Checked)
                                {
                                    cmd.Parameters.AddWithValue("@EMP", "MASTERCARD");
                                    cmd.Parameters.AddWithValue("@TRAMITE", "RECHAZOS MASTERCARD");
                                    cmd.Parameters.AddWithValue("@WF", "RECHM");
                                }


                                cmd.Parameters.AddWithValue("@NRO_TRAMITE", ID_Tramite);

                                cmd.Parameters.AddWithValue("@PRIORIDAD", "NORMAL");


                                cmd.Parameters.AddWithValue("@CANAL_INGRESO", "BATCH");

                                cmd.Parameters.AddWithValue("@ESTADO", "RECHAZADO");
                                cmd.Parameters.AddWithValue("@ACONTROL", DateTime.Today.ToOADate());

                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();

                                cmd.CommandText = "INSERT INTO RECHAZOS(ID_TRAMITE,RECHAZO) VALUES (@ID_TRAMITE,@RECHAZO)";
                                cmd.Parameters.AddWithValue("@ID_TRAMITE", ID_Tramite);
                                cmd.Parameters.AddWithValue("@RECHAZO", txtRechazo.Text);

                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                Logins.Estadistica("PROCESO", ID_Tramite);


                                MessageBox.Show("Rechazo grabado exitosamente.\nNro. de control interno: " + ID_Tramite);
                                txtRechazo.Text = "";
                                cmbOperador.SelectedIndex = -1;
                                




                            }
                            else
                            {
                                MessageBox.Show("La longitud del texto es demasiado corta o no ha introducido ningún rechazo. \nRevise por favor.", "Error en grabado", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);


                            }


                        }
                        else
                        {
                            MessageBox.Show("No seleccionó Operador. Es necesario indique un operador a derivar el RechazoBatch. \nRevise por favor.", "Error en grabado", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                        }
                        break;

                }

                





            }
            catch (Exception ex)
            {

                Logins.LogError(ex);
                MessageBox.Show("Ups!, algo salió mal. Contactar al administrador");
            }



        }

        private void RechBatch_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9: e.IsInputKey = true;
                
                    break;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            rbtVisa.Checked = true;

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            rbtMaster.Checked = true;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            rbtAmex.Checked = true;
        }
    }
    }

