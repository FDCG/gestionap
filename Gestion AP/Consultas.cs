﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;


namespace Gestion_AP
{
    
    public partial class Consultas : Form
    {
        public DataTable dtg;
        public DataTable dtv;
        public DataTable dtb;
        public DataTable dtp;
        public DataTable dth;

        public Consultas()
        {
            InitializeComponent();

            cmbGestiones.SelectedIndex = 0;
            cmbVarios.SelectedIndex = 0;
            cmbBajas.SelectedIndex = 0;
            cmbProcesos.SelectedIndex = 0;
            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;

            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;

            // CONSULTA DE GESTIONES

            DataTable dtGestiones = new DataTable();
            OleDbDataAdapter daGestiones = new OleDbDataAdapter(cmd);
            dtg = dtGestiones;
            cmd.CommandText = "SELECT * FROM Cons_Gestiones";
            daGestiones.Fill(dtGestiones);
            dgvGestiones.DataSource = dtGestiones;

            cmd.Parameters.Clear();
            // CONSULTA TJ VARIOS

            DataTable dtVarios = new DataTable();
            OleDbDataAdapter daVarios = new OleDbDataAdapter(cmd);
            dtv = dtVarios;

            cmd.CommandText = "SELECT * FROM TJ_Varios";

            daVarios.Fill(dtVarios);
            dgvVarios.DataSource = dtVarios;
            cmd.Parameters.Clear();

            // CONSULTA BAJAS

            DataTable dtBajas = new DataTable();
            OleDbDataAdapter daBajas = new OleDbDataAdapter(cmd);
            dtb = dtBajas;

            cmd.CommandText = "SELECT * FROM Cons_Bajas";

            daBajas.Fill(dtBajas);
            dgvBajas.DataSource = dtBajas;
            cmd.Parameters.Clear();

            // CONSULTA Procesos

            DataTable dtProcesos = new DataTable();
            OleDbDataAdapter daProcesos = new OleDbDataAdapter(cmd);
            dtp = dtProcesos;

            cmd.CommandText = "SELECT * FROM Procesos";

            daProcesos.Fill(dtProcesos);
            dgvProcesos.DataSource = dtProcesos;
            cmd.Parameters.Clear();

            // CONSULTA Historico Proceso

            DataTable dtHistorico = new DataTable();
            OleDbDataAdapter daHistorico = new OleDbDataAdapter(cmd);
            dth = dtHistorico;

            cmd.CommandText = "SELECT * FROM Cons_Historico";

            daHistorico.Fill(dtHistorico);
            dgvHistorico.DataSource = dtHistorico;
            cmd.Parameters.Clear();



        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAyuda_A_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Para consultar por Gestiones considerar estos parametros para determinadas búsquedas:\n•Prioridad: Normal/Urgente/Liberación\n•Canal Ingreso: Workflow/Mail/Seguridad(No Renovaciones)/Prevalid/Adjuntos/Varios\n•Estado: Abierto/Cerrado/Rechazado", "Parametros de Busqueda", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public int IndiceGestion()
        {
            int ind = 2;
            if (cmbGestiones.SelectedIndex==0)
            {
                ind = 0;
            }
            if (cmbGestiones.SelectedIndex == 1)
            {
                ind = 2;
            }
            if (cmbGestiones.SelectedIndex == 2)
            {
                ind = 3;
            }
            if (cmbGestiones.SelectedIndex == 3)
            {
                ind = 4;
            }
            if (cmbGestiones.SelectedIndex == 4)
            {
                ind = 5;
            }
            if (cmbGestiones.SelectedIndex == 5)
            {
                ind = 6;
            }
            if (cmbGestiones.SelectedIndex == 6)
            {
                ind = 7;
            }
           
            return ind;
        }
        public void TextoGestion(int columna,DataTable dtg)
        {

            string fieldName = string.Concat("[", dtg.Columns[columna].ColumnName, "]");
            dtg.DefaultView.Sort = fieldName;
            DataView view = dtg.DefaultView;
            view.RowFilter = string.Empty;
            if (txtGestiones.Text != string.Empty)
                view.RowFilter = fieldName + " LIKE '%" + txtGestiones.Text + "%'";
            dgvGestiones.DataSource = view;

        }
        private void txtGestiones_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextoGestion(IndiceGestion(),dtg);
            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }

        }

        private void dgvProcesos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            string ID = dgvProcesos.CurrentRow.Cells[0].Value.ToString();
            string fieldName = string.Concat("[", dth.Columns[0].ColumnName, "]");
            DataView view = dth.DefaultView;
            view.RowFilter = string.Empty;
            if (ID != string.Empty)
                view.RowFilter = fieldName + " LIKE '%" + ID + "%'";
            dgvHistorico.DataSource = view;
        }

        public int IndiceVarios()
        {
            int ind = 2;
            if (cmbVarios.SelectedIndex == 0)
            {
                ind = 0;
            }
            if (cmbVarios.SelectedIndex == 1)
            {
                ind = 1;
            }
            if (cmbVarios.SelectedIndex == 2)
            {
                ind = 2;
            }
            if (cmbVarios.SelectedIndex == 3)
            {
                ind = 3;
            }
            if (cmbVarios.SelectedIndex == 4)
            {
                ind = 4;
            }
            if (cmbVarios.SelectedIndex == 5)
            {
                ind = 5;
            }
            if (cmbVarios.SelectedIndex == 6)
            {
                ind = 6;
            }
            if (cmbVarios.SelectedIndex == 7)
            {
                ind = 7;
            }
            if (cmbVarios.SelectedIndex == 8)
            {
                ind = 8;
            }

            return ind;
        }
        public void TextoVarios(int columna, DataTable dtv)
        {

            string fieldName = string.Concat("[", dtv.Columns[columna].ColumnName, "]");
            dtv.DefaultView.Sort = fieldName;
            DataView view = dtv.DefaultView;
            view.RowFilter = string.Empty;
            if (txtVarios.Text != string.Empty)
                view.RowFilter = fieldName + " LIKE '%" + txtVarios.Text + "%'";
            dgvVarios.DataSource = view;

        }
        private void txtVarios_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextoVarios(IndiceVarios(), dtv);
            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }

        }
        public int IndiceBajas()
        {
            int ind = 2;
            if (cmbBajas.SelectedIndex == 0)
            {
                ind = 0;
            }
            if (cmbBajas.SelectedIndex == 1)
            {
                ind = 1;
            }
            if (cmbBajas.SelectedIndex == 2)
            {
                ind = 2;
            }
            if (cmbBajas.SelectedIndex == 3)
            {
                ind = 3;
            }
            if (cmbBajas.SelectedIndex == 4)
            {
                ind = 4;
            }
            if (cmbBajas.SelectedIndex == 5)
            {
                ind = 5;
            }


            return ind;
        }
        public void TextoBajas(int columna, DataTable dtv)
        {

            string fieldName = string.Concat("[", dtv.Columns[columna].ColumnName, "]");
            dtv.DefaultView.Sort = fieldName;
            DataView view = dtv.DefaultView;
            view.RowFilter = string.Empty;
            if (txtBajas.Text != string.Empty)
                view.RowFilter = fieldName + " LIKE '%" + txtBajas.Text + "%'";
            dgvBajas.DataSource = view;

        }
        private void txtBajas_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextoBajas(IndiceBajas(), dtb);
            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }

        }
        public int IndiceProcesos()
        {
            int ind = 0;
            if (cmbProcesos.SelectedIndex == 0)
            {
                ind = 0;
            }
            if (cmbProcesos.SelectedIndex == 1)
            {
                ind = 1;
            }
            if (cmbProcesos.SelectedIndex == 2)
            {
                ind = 2;
            }
            if (cmbProcesos.SelectedIndex == 3)
            {
                ind = 4;
            }
            if (cmbProcesos.SelectedIndex == 4)
            {
                ind = 5;
            }
            if (cmbProcesos.SelectedIndex == 5)
            {
                ind = 6;
            }
            if (cmbProcesos.SelectedIndex == 6)
            {
                ind = 7;
            }


            return ind;
        }
        public void TextoProcesos(int columna, DataTable dtp)
        {

            string fieldName = string.Concat("[", dtp.Columns[columna].ColumnName, "]");
            dtp.DefaultView.Sort = fieldName;
            DataView view = dtp.DefaultView;
            view.RowFilter = string.Empty;
            if (txtProcesos.Text != string.Empty)
                view.RowFilter = fieldName + " LIKE '%" + txtProcesos.Text + "%'";
            dgvProcesos.DataSource = view;

        }
        private void txtProcesos_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextoProcesos(IndiceProcesos(), dtp);
            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }

        }

        private void dgvHistorico_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            VisorTexto frm = new VisorTexto(dgvHistorico.CurrentRow.Cells[3].Value.ToString());
            frm.Show();
        }

        private void dgvVarios_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            VisorTexto frm = new VisorTexto(dgvVarios.CurrentRow.Cells[8].Value.ToString());
            frm.Show();
        }
       
        private void btnBusqMail_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Trámite");
           
            string Texto;
            string Ruta = Directory.GetCurrentDirectory() + @"\01\";
            string[] Archivos = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\01\","*.htm");
            try
            {
                if (txtBusqMail.Text.Trim() == "")
                {
                    MessageBox.Show("El texto a buscar no puede estar vacío.", "Error búsqueda", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else
                {

                    foreach (string Archivo in Archivos)
                    {
                        //Encriptado.Desencriptar(Archivo, Encriptado.ClaveEncript);

                        StreamReader sr = new StreamReader(Archivo);
                        Texto = sr.ReadToEnd();
                        
                        if (Texto.ToLower().Contains(txtBusqMail.Text.Trim().ToLower()))
                        {
                            int inicio = Archivo.ToString().LastIndexOf(@"\");

                            dt.Rows.Add(Archivo.ToString().Substring(inicio+1).Replace(".htm",""));
                        }
                       // Encriptado.Encriptar(Archivo, Encriptado.ClaveEncript);

                    }
                    dgvMails.DataSource = dt;
                    dt = null;



                }


            }
            catch (Exception ex )
            {

                Logins.LogError(ex);
            }


        }

        private void dgvMails_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {

                DataTable dt = new DataTable();
                
                OleDbConnection cn = new OleDbConnection(Conexion.cnProceso);
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM Cons_Mails WHERE [ID INTERNO]='" + dgvMails.CurrentCell.Value.ToString().Trim() + "'";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count>=1)
                {
                    dgvDetalle.DataSource = dt;
                }
                //Encriptado.Desencriptar(Directory.GetCurrentDirectory() + @"\01\" + dgvMails.CurrentCell.Value.ToString().Trim() + ".htm", Encriptado.ClaveEncript);

                wbPrevisualiza.Navigate(Directory.GetCurrentDirectory() + @"\01\" + dgvMails.CurrentCell.Value.ToString().Trim()+".htm");
                //Encriptado.Encriptar(Directory.GetCurrentDirectory() + @"\01\" + dgvMails.CurrentCell.Value.ToString().Trim() + ".htm", Encriptado.ClaveEncript);

            }
            catch (Exception ex)
            {

                Logins.LogError(ex);
            }


        }

        private void txtBusqMail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                btnBusqMail_Click(null, null);
            }

            
        }

        private void btnBusqRech_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();

            OleDbConnection cn = new OleDbConnection(Conexion.cnProceso);
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM RECHAZOS",cn);
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataTable dt_rech = new DataTable();
            da.Fill(dt_rech);
            


            dt.Columns.Add("Rechazo");

            
           
            
            try
            {
                if (txtBusqRech.Text.Trim() == "")
                {
                    MessageBox.Show("El texto a buscar no puede estar vacío.", "Error búsqueda", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else
                {

                    foreach (DataRow fila in dt_rech.Rows)
                    {
                        //Encriptado.Desencriptar(Archivo, Encriptado.ClaveEncript);

                        

                        if (fila[2].ToString().ToLower().Contains(txtBusqRech.Text.Trim().ToLower()))
                        {
                           

                            dt.Rows.Add(fila[1].ToString());
                        }
                        // Encriptado.Encriptar(Archivo, Encriptado.ClaveEncript);

                    }
                    dgvRechazos.DataSource = dt;
                    dt = null;



                }


            }
            catch (Exception ex)
            {

                Logins.LogError(ex);
            }





        }

        private void dgvMails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvRechazos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                DataTable dt = new DataTable();

                OleDbConnection cn = new OleDbConnection(Conexion.cnProceso);
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM Cons_Rech WHERE [ID INTERNO]='" + dgvRechazos.CurrentCell.Value.ToString().Trim() + "'";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count >= 1)
                {
                    dgvDetalleRech.DataSource = dt;
                }

                cn.Open();
                cmd.CommandText = "SELECT RECHAZO FROM RECHAZOS WHERE ID_TRAMITE='" + dgvRechazos.CurrentCell.Value.ToString().Trim() + "'";
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    txtRechazo.Text = dr.GetString(0);
                }
               
                cn.Close();

            }
            catch (Exception ex)
            {

                Logins.LogError(ex);
            }
        }

        private void txtBusqRech_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                btnBusqRech_Click(null, null);
            }
        }
    }

}

