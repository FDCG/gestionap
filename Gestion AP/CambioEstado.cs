﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;


namespace Gestion_AP
{
    public partial class CambioEstado : Form
    {
        public CambioEstado()
        {
            InitializeComponent();
        }
        public static string Origen;

        public CambioEstado(string Delegar)
        {
            InitializeComponent();
            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            txtTramDelega.Enabled = false;
            
            txtTramDelega.ForeColor = Color.Red;
            Origen = Delegar;
            txtTramDelega.Text = Delegar;


        }
        public void LimpiarEst(string ID, string Estado)
        {
            txtMotivoEstado.Text = "";
            txtTramEstado.Text = "";
            MessageBox.Show("Se ha modificado el estado del trámite nro.: " + ID + " a " + Estado + " correctamente.", "Cambio de Estado", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }
        public void LimpiarPrio(string ID, string Prioridad)
        {
            txtMotivoPrioridad.Text = "";
            txtTramPrioridad.Text = "";
            MessageBox.Show("Se ha modificado la prioridad del trámite nro.: " + ID + " a " + Prioridad + " correctamente.", "Cambio de Estado", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void btnAplicarEst_Click(object sender, EventArgs e)
        {

            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM Cons_Gestiones WHERE [NRO TRAMITE]='" + txtTramEstado.Text.Trim() + "'";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cn.Open();

                if (dt.Rows.Count==1)
                {


                    cmd.Parameters.Clear();
                    cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@ESTADO WHERE NRO_TRAMITE='" + txtTramEstado.Text.Trim() + "'";

                    //if (Ingreso.NivelUsuario == "00")
                    //{
                    //    MessageBox.Show("Su usuario carece de autorización para utilizar esta opción. Contacte al administrador.", "Autorización", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //}
                    //else
                    //{



                    //    switch (rbtAbierto.Checked)
                    //    {
                    //        case true:
                    //            if (txtMotivoEstado.Text == "")
                    //            {
                    //                MessageBox.Show("Debe ingresar un Motivo para cambiar el estado");
                    //            }
                    //            else
                    //            {
                    //                cmd.Parameters.AddWithValue("@ESTADO", "ABIERTO");
                    //                cmd.ExecuteNonQuery();
                    //                cmd.Parameters.Clear();
                    //                cmd.CommandText = "INSERT INTO Procesos (NroTramite,FECHA,USUARIO,COMENTARIO) VALUES(@ID,@FE,@US,@COM)";
                    //                cmd.Parameters.AddWithValue("@ID", txtTramEstado.Text);
                    //                cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                    //                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                    //                cmd.Parameters.AddWithValue("@COM", "Cambio de Estado a: ABIERTO - Motivo: " + txtMotivoEstado.Text.Trim().ToUpper());
                    //                cmd.ExecuteNonQuery();
                    //                cmd.Parameters.Clear();
                    //                LimpiarEst(txtTramEstado.Text, "Abierto");
                    //            }
                    //            break;
                    //        default:
                    //            break;
                    //    }
                    //    switch (rbtRechazado.Checked)
                    //    {
                    //        case true:
                    //            if (txtMotivoEstado.Text == "")
                    //            {
                    //                MessageBox.Show("Debe ingresar un Motivo para cambiar el estado");
                    //            }
                    //            else
                    //            {
                    //                cmd.Parameters.AddWithValue("@ESTADO", "RECHAZADO");
                    //                cmd.ExecuteNonQuery();
                    //                cmd.Parameters.Clear();
                    //                cmd.CommandText = "INSERT INTO Procesos (NroTramite,FECHA,USUARIO,COMENTARIO) VALUES(@ID,@FE,@US,@COM)";
                    //                cmd.Parameters.AddWithValue("@ID", txtTramEstado.Text);
                    //                cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                    //                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                    //                cmd.Parameters.AddWithValue("@COM", "Cambio de Estado a: RECHAZADO - Motivo: " + txtMotivoEstado.Text.Trim().ToUpper());
                    //                cmd.ExecuteNonQuery();
                    //                cmd.Parameters.Clear();
                    //                LimpiarEst(txtTramEstado.Text, "Rechazado");
                    //            }
                    //            break;
                    //        default:
                    //            break;
                    //    }
                    //    switch (rbtCerrado.Checked)
                    //    {
                    //        case true:
                    //            if (txtMotivoEstado.Text == "")
                    //            {
                    //                MessageBox.Show("Debe ingresar un Motivo para cambiar el estado");
                    //            }
                    //            else
                    //            {
                    //                cmd.Parameters.AddWithValue("@ESTADO", "CERRADO");
                    //                cmd.ExecuteNonQuery();
                    //                cmd.Parameters.Clear();
                    //                cmd.CommandText = "INSERT INTO Procesos (NroTramite,FECHA,USUARIO,COMENTARIO) VALUES(@ID,@FE,@US,@COM)";
                    //                cmd.Parameters.AddWithValue("@ID", txtTramEstado.Text);
                    //                cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                    //                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                    //                cmd.Parameters.AddWithValue("@COM", "Cambio de Estado a: CERRADO - Motivo: " + txtMotivoEstado.Text.Trim().ToUpper());
                    //                cmd.ExecuteNonQuery();
                    //                cmd.Parameters.Clear();
                    //                LimpiarEst(txtTramEstado.Text, "Cerrado");
                    //            }
                    //            break;
                    //        default:
                    //            break;
                    //    }


                    //}

                }
                else
                {
                    MessageBox.Show("El Nro. de Trámite ingresado no existe o es erróneo. Revíselo.\nTrámites WorkFlow: 9 posiciones","Trámite no encontrado",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);

                }
                cn.Close();

                dt.Clear();
                da = null;

            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }


        }

        private void btnAplicarPrio_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM Cons_Gestiones WHERE [NRO TRAMITE]='" + txtTramPrioridad.Text.Trim() + "'";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cn.Open();

                if (dt.Rows.Count == 1)
                {


                    cmd.Parameters.Clear();
                    cmd.CommandText = "UPDATE GESTIONES SET PRIORIDAD=@PRIO WHERE NRO_TRAMITE='" + txtTramPrioridad.Text.Trim() + "'";

                    switch (rbtNormal.Checked)
                    {
                        case true:
                            if (txtTramPrioridad.Text == "")
                            {
                                MessageBox.Show("Debe ingresar un Motivo para cambiar la prioridad");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@PRIO", "NORMAL");
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                cmd.CommandText = "INSERT INTO Procesos (NroTramite,FECHA,USUARIO,COMENTARIO) VALUES(@ID,@FE,@US,@COM)";
                                cmd.Parameters.AddWithValue("@ID", txtTramPrioridad.Text);
                                cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                                cmd.Parameters.AddWithValue("@COM", "Cambio de Prioridad a: NORMAL - Motivo: " + txtMotivoPrioridad.Text.Trim().ToUpper());
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                LimpiarPrio(txtTramPrioridad.Text.Trim(), "Normal");
                            }
                            break;
                        default:
                            break;
                    }
                    switch (rbtUrgente.Checked)
                    {
                        case true:
                            if (txtTramPrioridad.Text == "")
                            {
                                MessageBox.Show("Debe ingresar un Motivo para cambiar la prioridad");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@PRIO", "URGENTE");
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                cmd.CommandText = "INSERT INTO Procesos (NroTramite,FECHA,USUARIO,COMENTARIO) VALUES(@ID,@FE,@US,@COM)";
                                cmd.Parameters.AddWithValue("@ID", txtTramPrioridad.Text);
                                cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                                cmd.Parameters.AddWithValue("@COM", "Cambio de Prioridad a: URGENTE - Motivo: " + txtMotivoPrioridad.Text.Trim().ToUpper());
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                LimpiarPrio(txtTramPrioridad.Text, "Urgente");
                            }
                            break;
                        default:
                            break;
                    }
                    switch (rbtLiberacion.Checked)
                    {
                        case true:
                            if (txtTramPrioridad.Text == "")
                            {
                                MessageBox.Show("Debe ingresar un Motivo para cambiar la prioridad");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@PRIO", "NORMAL+LIBERACION");
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                cmd.CommandText = "INSERT INTO Procesos (NroTramite,FECHA,USUARIO,COMENTARIO) VALUES(@ID,@FE,@US,@COM)";
                                cmd.Parameters.AddWithValue("@ID", txtTramPrioridad.Text);
                                cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                                cmd.Parameters.AddWithValue("@COM", "Cambio de Prioridad a: NORMAL+LIBERACION - Motivo: " + txtMotivoPrioridad.Text.Trim().ToUpper());
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                LimpiarPrio(txtTramPrioridad.Text, "Normal con Liberación");
                            }
                            break;
                        default:
                            break;
                    }


                }
                else
                {
                    MessageBox.Show("El Nro.de Trámite ingresado no existe o es erróneo. Revíselo.\nTrámites WorkFlow: 9 posiciones","Trámite no encontrado",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);

                }
                cn.Close();
                dt.Clear();
                da = null;

            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void CambioEstado_Load(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnUsuario;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT Usuario FROM Usuarios Where Fecha_Baja is NULL";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cmbUsuarios.Items.Add(Convert.ToString(dt.Rows[i][0]));
                }

            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }
        }

        private void cmbUsuarios_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnUsuario;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT Nombre,Apellido FROM Usuarios WHERE Usuario='" + Convert.ToString(cmbUsuarios.SelectedItem) + "'";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                try
                {
                    txtOperador.Text = Convert.ToString(dt.Rows[0][0] + " " + dt.Rows[0][1]);
                }
                catch (Exception)
                {

                    txtOperador.Text = "";
                }
                

            }
            catch (Exception ex)
            {

                Logins.LogError(ex);
            }
        }
        public void LimpiarDele(string ID, string Usuario)
        {
            txtMotivoDelega.Text = "";
            txtTramDelega.Text = "";
            txtOperador.Text = "";
            cmbUsuarios.SelectedIndex = -1;
            MessageBox.Show("Se delegó el trámite nro.: " + ID + " a " + Usuario + " correctamente.", "Cambio de Estado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            

        }
        private void btnDelegacion_Click(object sender, EventArgs e)
        {


            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM Cons_Gestiones WHERE [NRO TRAMITE]='" + txtTramDelega.Text.Trim() + "'";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cn.Open();

                //if (dt.Rows.Count == 1)
                //{

                //    if (Ingreso.NivelUsuario == "00")
                //    {
                //        MessageBox.Show("Su usuario carece de autorización para utilizar esta opción. Contacte al administrador.", "Autorización", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    }
                //    else
                //    {
                //        cmd.Parameters.Clear();
                //        cmd.CommandText = "UPDATE GESTIONES SET US_INICIO=@US WHERE NRO_TRAMITE='" + txtTramDelega.Text.Trim() + "'";


                //        if (txtMotivoDelega.Text == "")
                //        {
                //            MessageBox.Show("Debe ingresar un Motivo para delegar el trámite");
                //        }
                //        else
                //        if (dt.Rows[0][7].ToString() != "CERRADO")
                //        {
                //            cmd.Parameters.AddWithValue("@US", cmbUsuarios.SelectedItem.ToString());
                //            cmd.ExecuteNonQuery();
                //            cmd.Parameters.Clear();
                //            cmd.CommandText = "INSERT INTO Procesos (NroTramite,FECHA,USUARIO,COMENTARIO) VALUES(@ID,@FE,@US,@COM)";
                //            cmd.Parameters.AddWithValue("@ID", txtTramDelega.Text);
                //            cmd.Parameters.AddWithValue("@FE", DateTime.Now.ToOADate());
                //            cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                //            cmd.Parameters.AddWithValue("@COM", "Trámite delegado a: " + cmbUsuarios.SelectedItem.ToString() + " - Motivo: " + txtMotivoDelega.Text.Trim().ToUpper());
                //            cmd.ExecuteNonQuery();
                //            cmd.Parameters.Clear();
                //            LimpiarDele(txtTramDelega.Text.Trim(), cmbUsuarios.SelectedItem.ToString());
                //            if (Origen != "")
                //            {
                //                this.Close();
                //                MessageBox.Show("Cierre la ventana 'Rechazos' y vuelva a ejecutarla para ver los cambios actualizados");

                //            }
                //        }
                //        else
                //        {
                //            MessageBox.Show("Trámite en estado CERRADO, no se puede delegar.");
                //        }



                //    }
                //}
                //else
                //{
                //    MessageBox.Show("El Nro.de Trámite ingresado no existe o es erróneo. Revíselo.\nTrámites WorkFlow: 9 posiciones", "Trámite no encontrado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                //}
                //    cn.Close();
                //    dt.Clear();
                //    da = null;
               
                

                }
            
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }

        }
    }
}
