﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;


namespace Gestion_AP
{
    public partial class Control : Form
    {
        public Control()
        {
            InitializeComponent();


        }

        public DataTable tabla;
        private void Control_Load(object sender, EventArgs e)
        {

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;

            cn.Open();

            try
            {


                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM GESTIONES WHERE ESTADO='ABIERTO'";
                DataTable dt = new DataTable();
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);
                tabla = dt;
                if (dt.Rows.Count != 0)
                {
                    dgvControl.DataSource = dt;
                    dgvControl.ReadOnly = true;
                    dgvControl.Columns["Id"].Visible = false;
                    dgvControl.Columns["US_CONTROL"].Visible = false;
                    dgvControl.Columns["FECHA_CONTROL"].Visible = false;
                    dgvControl.Columns["US_REPROCESO"].Visible = false;
                    dgvControl.Columns["FECHA_REPROCESO"].Visible = false;
                    dgvControl.Columns["DOC_ASOCIADO"].Visible = false;

                    dgvControl.Columns["ESTADO"].Visible = false;
                    dgvControl.Columns["A_CONTROLAR"].Visible = true;
                    dgvControl.Columns["A_CONTROLAR"].HeaderText = "FECHA CONTROL";
                    dgvControl.Columns["CANAL_INGRESO"].HeaderText = "CANAL INGRESO";

                    dgvControl.Columns["NRO_TRAMITE"].HeaderText = "NRO.TRAMITE";
                    dgvControl.Columns["US_INICIO"].HeaderText = "USUARIO PROCESO";
                    dgvControl.Columns["FECHA_INICIO"].HeaderText = "FECHA TRAMITE";
                    dgvControl.Columns["WF"].HeaderText = "TIPO";
                    AspectoTabla.AspectoDGV(dgvControl, Color.Silver);
                    dgvControl.ScrollBars = ScrollBars.Both;
                    cmd.ExecuteNonQuery();
                    string fieldName = string.Concat("[", tabla.Columns[15].ColumnName, "]");
                    tabla.DefaultView.Sort = fieldName;
                    DataView view = tabla.DefaultView;
                    view.RowFilter = string.Empty;

                    view.RowFilter = fieldName + " <= '#" + DateTime.Today + "#'";
                    dgvControl.DataSource = view;


                }
                else
                {
                    MessageBox.Show("No hay tramites pendientes de Control ☺", "Control", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }



            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);

            }
            finally
            {
                cn.Close();
            }

        }



        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextoFiltro(Index());
            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }

        }


        public void TextoFiltro(int columna)
        {

            string fieldName = string.Concat("[", tabla.Columns[columna].ColumnName, "]");
            tabla.DefaultView.Sort = fieldName;
            DataView view = tabla.DefaultView;
            view.RowFilter = string.Empty;
            if (txtFiltro.Text != string.Empty)
                view.RowFilter = fieldName + " LIKE '%" + txtFiltro.Text + "%'";
            dgvControl.DataSource = view;

        }

        public int Index()
        {
            int ind = 99;
            if (rbtPrio.Checked)
            {
                ind = 12;
            }

            if (rbtCanal.Checked)
            {
                ind = 13;

            }
            if (rbtAdm.Checked)
            {
                ind = 7;
            }
            if (rbtTram.Checked)
            {
                ind = 8;
            }
            if (rbtWf.Checked)
            {
                ind = 9;

            }
            if (rbtUser.Checked)
            {
                ind = 1;
            }


            if (rbtNro.Checked)
            {
                ind = 10;
            }


            return ind;


        }

        private void dtpDiaProceso_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string fieldName = string.Concat("[", tabla.Columns[2].ColumnName, "]");
                tabla.DefaultView.Sort = fieldName;
                DataView view = tabla.DefaultView;
                view.RowFilter = string.Empty;
                DateTime fechacorta = Convert.ToDateTime(dtpDiaProceso.Value.Day + "/" + dtpDiaProceso.Value.Month + "/" + dtpDiaProceso.Value.Year);
                view.RowFilter = fieldName + " >= '#" + fechacorta + "#' AND " + fieldName + " < '#" + fechacorta.AddDays(1) + "#'";
                dgvControl.DataSource = view;
            }
            catch (Exception ex)
            {
                Logins.LogError(ex);
            }


        }

        private void btnVertodo_Click(object sender, EventArgs e)
        {
            DataView view = tabla.DefaultView;
            view.RowFilter = string.Empty;
            dgvControl.DataSource = view;
        }

        private void dgvControl_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                dgv = dgvControl;
                string CanalIngreso = Convert.ToString(dgvControl.CurrentRow.Cells["CANAL_INGRESO"].Value);
                string IdTramite = Convert.ToString(dgvControl.CurrentRow.Cells["NRO_TRAMITE"].Value);
                string Estado = Convert.ToString(dgvControl.CurrentRow.Cells["ESTADO"].Value);
                string Prioridad = Convert.ToString(dgvControl.CurrentRow.Cells["PRIORIDAD"].Value);

                if (CanalIngreso == "SEGURIDAD" || CanalIngreso == "CALLCENTER/FALLECIDOS")
                {
                    BajasFall frm = new BajasFall(IdTramite, Estado);
                    frm.Show();

                }

                if (CanalIngreso == "MAIL" || CanalIngreso == "VARIOS" || CanalIngreso == "ADJUNTOS" || CanalIngreso == "BATCH")
                {
                    GenericoDocs frm = new GenericoDocs(IdTramite, Estado);
                    frm.Show();
                }

                if (CanalIngreso == "WORKFLOW")
                {

                    ModuloPurpura frm = new ModuloPurpura(IdTramite, Estado, Prioridad);
                    frm.Show();
                }
                if (CanalIngreso == "ERR-PREVALID")
                {

                    ModuloPrevalid frm = new ModuloPrevalid(IdTramite, Estado, Prioridad);
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo falló al abrir trámite. Contactá al administrador.\n" + ex.Message);
                Logins.LogError(ex);
            }
        }





        public static DataGridView dgv;
        public static void EliminarRegistro(string ID)

        {

            foreach (DataGridViewRow fila in dgv.Rows)
            {
                if (fila.Cells[10].Value.ToString() == ID)
                {
                    dgv.Rows.Remove(fila);
                }

            }


        }

        private void dgvControl_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {

                dgvControl_CellContentDoubleClick(null, null);

            }
        }

        private void Control_FormClosed(object sender, FormClosedEventArgs e)
        {
            Principal frm = new Principal();
            frm.Show();

        }

        private void txtHoy_Click(object sender, EventArgs e)
        {

            string fieldName = string.Concat("[", tabla.Columns[15].ColumnName, "]");
            tabla.DefaultView.Sort = fieldName;
            DataView view = tabla.DefaultView;
            view.RowFilter = string.Empty;

            view.RowFilter = fieldName + " = '#" + DateTime.Today + "#'";
            dgvControl.DataSource = view;

        }
    }
}
