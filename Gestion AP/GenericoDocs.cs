﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Gestion_AP
{
    public partial class GenericoDocs : Form
    {
        public GenericoDocs()
        {
            InitializeComponent();
        }

        public string EnlaceIM;
        public GenericoDocs(string Control, string Estado)
        {



            InitializeComponent();


            if (Estado == "ABIERTO")
            {
                this.Text = "Control: " + Control;
                this.Icon = Gestion_AP.Properties.Resources.Icono;
                btnReprocesar.Click += new EventHandler(Reprocesar);
                btnControlado.Click += new EventHandler(Controlado);

            }

            if (Estado == "RECHAZADO")
            {
                this.Text = "Rechazo: " + Control;
                this.Icon = Gestion_AP.Properties.Resources.ntfPendientes;
                btnReprocesar.Click += new EventHandler(Corregido);
                btnControlado.Visible = false;
            }

           
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM GESTIONES WHERE NRO_TRAMITE='" + Control + "'";
                DataTable dt = new DataTable();
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count == 1)
                {
                    txtNroTram.Text = Control;
                    txtTramite.Text = dt.Rows[0]["TRAMITE"].ToString();
                    txtAdm.Text = dt.Rows[0]["EMP"].ToString();
                    txtCanal.Text = dt.Rows[0]["CANAL_INGRESO"].ToString();
                    txtPrioridad.Text = dt.Rows[0]["PRIORIDAD"].ToString();
                    txtNovedad.Text = dt.Rows[0]["WF"].ToString();
                    string NombreArch = Path.GetFileName(dt.Rows[0]["DOC_ASOCIADO"].ToString());

                    if (dt.Rows[0]["CANAL_INGRESO"].ToString()=="MAIL")
                    {
                        
                        string Archivo = Directory.GetCurrentDirectory() + @"\01\" + Control + ".htm";
                       // Encriptado.Desencriptar(Archivo, Encriptado.ClaveEncript);
                        wbMail.Navigate(Archivo);
                        //Encriptado.Encriptar(Archivo, Encriptado.ClaveEncript);

                        Secciones.TabPages.Remove(pagImagen);
                        Secciones.TabPages.Remove(pagAdjuntos);
                        Secciones.TabPages.Remove(pagBatch);
                    }
                    if (dt.Rows[0]["CANAL_INGRESO"].ToString() == "VARIOS")
                    {
                        //Encriptado.Desencriptar(dt.Rows[0]["DOC_ASOCIADO"].ToString(), Encriptado.ClaveEncript);
                        pbImagen.Image = Image.FromFile(dt.Rows[0]["DOC_ASOCIADO"].ToString());
                        EnlaceIM = dt.Rows[0]["DOC_ASOCIADO"].ToString();
                        Secciones.TabPages.Remove(pagMail);
                        Secciones.TabPages.Remove(pagAdjuntos);
                        Secciones.TabPages.Remove(pagBatch);
                    }
                    if (dt.Rows[0]["CANAL_INGRESO"].ToString() == "BATCH")
                    {
                        OleDbConnection cn_batch = new OleDbConnection();
                        cn_batch.ConnectionString = Conexion.cnProceso;
                        OleDbCommand cmd_batch = new OleDbCommand();
                        cmd_batch.Connection = cn_batch;
                        cmd_batch.CommandText = "SELECT RECHAZO FROM RECHAZOS WHERE ID_TRAMITE='" + Control + "'";
                        DataTable dt_batch = new DataTable();
                        OleDbDataAdapter da_batch = new OleDbDataAdapter(cmd_batch);
                        da_batch.Fill(dt_batch);
                        txtRechazo.Text = dt_batch.Rows[0]["RECHAZO"].ToString();
                        Secciones.TabPages.Remove(pagMail);
                        Secciones.TabPages.Remove(pagAdjuntos);
                        Secciones.TabPages.Remove(pagImagen);
                    }

                    if (dt.Rows[0]["CANAL_INGRESO"].ToString() == "ADJUNTOS")
                    {

                        OleDbCommand adj = new OleDbCommand();
                        adj.Connection = cn;
                        DataTable dtadj = new DataTable();
                        OleDbDataAdapter da_adj = new OleDbDataAdapter(adj);
                        adj.CommandText= "SELECT ADJUNTO FROM ADJUNTOS WHERE NroTramite='" + Control + "'";
                        da_adj.Fill(dtadj);
                        dgvAdjuntos.DataSource = dtadj;
                        
                        foreach (DataGridViewRow registro in dgvAdjuntos.Rows)
                        {
                            string Archivo = registro.Cells[0].Value.ToString();
                            registro.Cells[0].Value = Path.GetFileName(Archivo).ToString();
                            Archivo = string.Empty;
                        }
                        Secciones.TabPages.Remove(pagMail);
                        Secciones.TabPages.Remove(pagImagen);
                        Secciones.TabPages.Remove(pagBatch);
                        dgvAdjuntos.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        da_adj.Dispose();
                        dtadj.Dispose();
                    }
                    da.Dispose();
                    dt.Dispose();
                }

                cmd.CommandText = "SELECT * FROM Procesos WHERE NroTramite='" + Control + "'";
                DataTable dth = new DataTable();
                OleDbDataAdapter dah = new OleDbDataAdapter(cmd);
                dah.Fill(dth);
                dgvHistorico.DataSource = dth;
                AspectoTabla.AspectoComent(dgvHistorico);

               


            }
            catch (Exception ex) { MessageBox.Show(ex.Message); Logins.LogError(ex); }
        }

        private void Corregido(object sender, EventArgs e)
        {

            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + txtNroTram.Text.Trim() + "'";
                cmd.Parameters.AddWithValue("@EST", "ABIERTO");
                cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today));
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", txtNroTram.Text.Trim());
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", txtComAdic.Text.Trim());
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("REPROCESO",txtNroTram.Text);
                MessageBox.Show("Trámite Reprocesado.", "Reproceso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Rechazos").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Rechazos.EliminarRegistro(txtNroTram.Text.Trim());
                        Rechazos.TotRech--;
                        Rechazos.TotUs--;
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }

                this.Close();
                cn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Logins.LogError(ex);

            }

        }

        private void Reprocesar(object sender, EventArgs e)

        {

            if (txtComAdic.Text == "")
            {
                MessageBox.Show("Debe ingresar un comentario para rechazar trámite.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    OleDbConnection cn = new OleDbConnection();
                    cn.ConnectionString = Conexion.cnProceso;
                    OleDbCommand cmd = new OleDbCommand();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + txtNroTram.Text.Trim() + "'";
                    cmd.Parameters.AddWithValue("@EST", "RECHAZADO");
                    cmd.Parameters.AddWithValue("@ACONTROL", DateTime.Today);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                    cmd.Parameters.AddWithValue("@ID", txtNroTram.Text.Trim());
                    cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                    cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                    cmd.Parameters.AddWithValue("@COM", txtComAdic.Text.Trim());
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    Logins.Estadistica("CONTROL",txtNroTram.Text);
                    MessageBox.Show("Trámite enviado a reproceso.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    try
                    {
                        Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                        if (existe != null)
                        {
                            Control.EliminarRegistro(txtNroTram.Text.Trim());
                        }
                    }
                    catch (Exception ex) { Logins.LogError(ex); }

                    this.Close();
                    cn.Close();
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    Logins.LogError(ex);

                }

            }



        }

        private void Controlado(object sender, EventArgs e)

        {
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST WHERE NRO_TRAMITE='" + txtNroTram.Text.Trim() + "'";
                cmd.Parameters.AddWithValue("@EST", "CERRADO");

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", txtNroTram.Text.Trim());
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", "TRÁMITE FINALIZADO CORRECTAMENTE");
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("CONTROL",txtNroTram.Text);
                MessageBox.Show("Trámite finalizado correctamente.", "Aceptado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Control.EliminarRegistro(txtNroTram.Text.Trim());
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }
                this.Close();
                cn.Close();
               


            }
            catch (Exception ex) { MessageBox.Show(ex.Message); Logins.LogError(ex); }


        }

        private void pbImagen_DoubleClick(object sender, EventArgs e)
        {
            VisorImagen frm = new VisorImagen(EnlaceIM);
            frm.Show();

           
        }

        private void dgvAdjuntos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string NombreArchivo = Convert.ToString(dgvAdjuntos.CurrentRow.Cells[0].Value);
            string Path = UNC.PathUNC(Directory.GetCurrentDirectory().ToString()) + @"\03\" + NombreArchivo;

            Process.Start(Path);
        }

       

        private void dgvHistorico_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            VisorTexto frm = new VisorTexto(dgvHistorico.CurrentCell.Value.ToString());
            frm.Show();
        }

    
    }
}
