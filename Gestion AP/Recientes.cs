﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Gestion_AP
{
    public partial class Recientes : Form
    {
        public Recientes()
        {
            InitializeComponent();
        }
        public string Doc;
        public Recientes(string DNI)
        {
            InitializeComponent();
            try
            {
                Doc = DNI;
                OleDbConnection cn = new OleDbConnection(Conexion.cnProceso);
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM Recientes WHERE DOCUMENTO='" + DNI + "'";
                DataTable dt = new DataTable();
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);
                dgvAntecedentes.DataSource = dt;
            }
            catch (Exception ex)
            {

                Logins.LogError(ex);
            }
       

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Logins.Login(Ingreso.UsuarioLogueado, "", DateTime.Now,  "El usuario registrado inicio un tramite duplicado para el documento: " + Doc + " y confirmo su condición de duplicidad. Confirma su verificación y notificación de antecendentes");
            this.Close();
        }
    }
}
