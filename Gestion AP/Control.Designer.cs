﻿namespace Gestion_AP
{
    partial class Control
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Control));
            this.dgvControl = new System.Windows.Forms.DataGridView();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.rbtPrio = new System.Windows.Forms.RadioButton();
            this.rbtUser = new System.Windows.Forms.RadioButton();
            this.rbtTram = new System.Windows.Forms.RadioButton();
            this.rbtWf = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtNro = new System.Windows.Forms.RadioButton();
            this.rbtAdm = new System.Windows.Forms.RadioButton();
            this.rbtCanal = new System.Windows.Forms.RadioButton();
            this.dtpDiaProceso = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtHoy = new System.Windows.Forms.Button();
            this.btnVertodo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvControl)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvControl
            // 
            this.dgvControl.AllowUserToOrderColumns = true;
            this.dgvControl.AllowUserToResizeRows = false;
            this.dgvControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvControl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvControl.GridColor = System.Drawing.Color.OrangeRed;
            this.dgvControl.Location = new System.Drawing.Point(12, 130);
            this.dgvControl.Name = "dgvControl";
            this.dgvControl.Size = new System.Drawing.Size(695, 439);
            this.dgvControl.TabIndex = 11;
            this.dgvControl.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvControl_CellContentDoubleClick);
            this.dgvControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvControl_KeyDown);
            // 
            // txtFiltro
            // 
            this.txtFiltro.Location = new System.Drawing.Point(107, 72);
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(317, 21);
            this.txtFiltro.TabIndex = 8;
            this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
            // 
            // rbtPrio
            // 
            this.rbtPrio.AutoSize = true;
            this.rbtPrio.ForeColor = System.Drawing.Color.Red;
            this.rbtPrio.Location = new System.Drawing.Point(10, 20);
            this.rbtPrio.Name = "rbtPrio";
            this.rbtPrio.Size = new System.Drawing.Size(74, 20);
            this.rbtPrio.TabIndex = 1;
            this.rbtPrio.TabStop = true;
            this.rbtPrio.Text = "&Prioridad";
            this.rbtPrio.UseVisualStyleBackColor = true;
            // 
            // rbtUser
            // 
            this.rbtUser.AutoSize = true;
            this.rbtUser.ForeColor = System.Drawing.Color.Blue;
            this.rbtUser.Location = new System.Drawing.Point(107, 20);
            this.rbtUser.Name = "rbtUser";
            this.rbtUser.Size = new System.Drawing.Size(65, 20);
            this.rbtUser.TabIndex = 2;
            this.rbtUser.TabStop = true;
            this.rbtUser.Text = "&Usuario";
            this.rbtUser.UseVisualStyleBackColor = true;
            // 
            // rbtTram
            // 
            this.rbtTram.AutoSize = true;
            this.rbtTram.Location = new System.Drawing.Point(208, 20);
            this.rbtTram.Name = "rbtTram";
            this.rbtTram.Size = new System.Drawing.Size(65, 20);
            this.rbtTram.TabIndex = 3;
            this.rbtTram.TabStop = true;
            this.rbtTram.Text = "&Tramite";
            this.rbtTram.UseVisualStyleBackColor = true;
            // 
            // rbtWf
            // 
            this.rbtWf.AutoSize = true;
            this.rbtWf.Location = new System.Drawing.Point(10, 46);
            this.rbtWf.Name = "rbtWf";
            this.rbtWf.Size = new System.Drawing.Size(74, 20);
            this.rbtWf.TabIndex = 5;
            this.rbtWf.TabStop = true;
            this.rbtWf.Text = "&Workflow";
            this.rbtWf.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Silver;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtFiltro);
            this.groupBox1.Controls.Add(this.rbtNro);
            this.groupBox1.Controls.Add(this.rbtAdm);
            this.groupBox1.Controls.Add(this.rbtCanal);
            this.groupBox1.Controls.Add(this.rbtPrio);
            this.groupBox1.Controls.Add(this.rbtWf);
            this.groupBox1.Controls.Add(this.rbtUser);
            this.groupBox1.Controls.Add(this.rbtTram);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(431, 100);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Buscar por";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Texto a buscar:";
            // 
            // rbtNro
            // 
            this.rbtNro.AutoSize = true;
            this.rbtNro.Location = new System.Drawing.Point(316, 20);
            this.rbtNro.Name = "rbtNro";
            this.rbtNro.Size = new System.Drawing.Size(86, 20);
            this.rbtNro.TabIndex = 4;
            this.rbtNro.TabStop = true;
            this.rbtNro.Text = "&Nro.Trámite";
            this.rbtNro.UseVisualStyleBackColor = true;
            // 
            // rbtAdm
            // 
            this.rbtAdm.AutoSize = true;
            this.rbtAdm.Location = new System.Drawing.Point(208, 46);
            this.rbtAdm.Name = "rbtAdm";
            this.rbtAdm.Size = new System.Drawing.Size(107, 20);
            this.rbtAdm.TabIndex = 7;
            this.rbtAdm.TabStop = true;
            this.rbtAdm.Text = "&Administradora";
            this.rbtAdm.UseVisualStyleBackColor = true;
            // 
            // rbtCanal
            // 
            this.rbtCanal.AutoSize = true;
            this.rbtCanal.Location = new System.Drawing.Point(107, 46);
            this.rbtCanal.Name = "rbtCanal";
            this.rbtCanal.Size = new System.Drawing.Size(101, 20);
            this.rbtCanal.TabIndex = 6;
            this.rbtCanal.TabStop = true;
            this.rbtCanal.Text = "&Canal Ingreso";
            this.rbtCanal.UseVisualStyleBackColor = true;
            // 
            // dtpDiaProceso
            // 
            this.dtpDiaProceso.Location = new System.Drawing.Point(6, 20);
            this.dtpDiaProceso.Name = "dtpDiaProceso";
            this.dtpDiaProceso.Size = new System.Drawing.Size(242, 21);
            this.dtpDiaProceso.TabIndex = 9;
            this.dtpDiaProceso.ValueChanged += new System.EventHandler(this.dtpDiaProceso_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Gray;
            this.groupBox2.Controls.Add(this.txtHoy);
            this.groupBox2.Controls.Add(this.btnVertodo);
            this.groupBox2.Controls.Add(this.dtpDiaProceso);
            this.groupBox2.Location = new System.Drawing.Point(449, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 100);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Por Fecha de Proceso";
            // 
            // txtHoy
            // 
            this.txtHoy.Location = new System.Drawing.Point(6, 70);
            this.txtHoy.Name = "txtHoy";
            this.txtHoy.Size = new System.Drawing.Size(118, 23);
            this.txtHoy.TabIndex = 11;
            this.txtHoy.Text = "HOY";
            this.txtHoy.UseVisualStyleBackColor = true;
            this.txtHoy.Click += new System.EventHandler(this.txtHoy_Click);
            // 
            // btnVertodo
            // 
            this.btnVertodo.Location = new System.Drawing.Point(130, 70);
            this.btnVertodo.Name = "btnVertodo";
            this.btnVertodo.Size = new System.Drawing.Size(118, 23);
            this.btnVertodo.TabIndex = 10;
            this.btnVertodo.Text = "VER TODOS";
            this.btnVertodo.UseVisualStyleBackColor = true;
            this.btnVertodo.Click += new System.EventHandler(this.btnVertodo_Click);
            // 
            // Control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(719, 581);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvControl);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Control";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Control_FormClosed);
            this.Load += new System.EventHandler(this.Control_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvControl)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.RadioButton rbtPrio;
        private System.Windows.Forms.RadioButton rbtUser;
        private System.Windows.Forms.RadioButton rbtTram;
        private System.Windows.Forms.RadioButton rbtWf;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtAdm;
        private System.Windows.Forms.RadioButton rbtCanal;
        private System.Windows.Forms.RadioButton rbtNro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpDiaProceso;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnVertodo;
        public System.Windows.Forms.DataGridView dgvControl;
        private System.Windows.Forms.Button txtHoy;
    }
}