﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Mail;
using System.Net;
using System.Text;

using System.Windows.Forms;
using System.Data.OleDb;

namespace Gestion_AP
{
    public partial class Principal : Form
    {
      

        //public static int Pendientes;
        public Principal()
        {
            InitializeComponent();
            timer1.Enabled = true;
            timer2.Enabled = true;
            lblstripUsuario.Text = Usuario.usuarioSingleton.ToString();
            //  Pendientes = Ingreso.Actualizacion();

        }



        private void Principal_Load(object sender, EventArgs e)
        {

        }


        //public void ntfPendientes(int Pendientes, bool Mostrar)
            

        //{
           
            
        //    notifyIcon1.Visible = Mostrar;
        //    notifyIcon1.BalloonTipTitle = "Rechazos";
        //    notifyIcon1.BalloonTipText = "Tiene un nuevo trámite en su Bandeja";
        //    notifyIcon1.ShowBalloonTip(10000);
            

        //}
       

       

        private void btnAcerca_Click(object sender, EventArgs e)
        {


 

            Acercade frm = new Acercade();
            frm.Show();


        }

        private void btnAgregarUsuario_Click(object sender, EventArgs e)
        {

            AltaUsuario frm = new AltaUsuario();
            frm.Show();


        }

        private void btnDesmarcar_Click(object sender, EventArgs e)
        {
            Desmarca frm = new Desmarca();
            frm.Show();

        }

        private void btnUsuariosActivos_Click(object sender, EventArgs e)
        {
            Activos frm = new Activos();
            frm.Show();

        }

        private void btnAdmUsuarios_Click(object sender, EventArgs e)
        {
            ABMUsuarios frm = new ABMUsuarios();
            frm.Show();

        }

        private void logoutUsuario()
        {
            try
            {
                UsuarioDaoImpl daoUsuario = new UsuarioDaoImpl();

                Usuario usuario = Usuario.usuarioSingleton;

                setUpdateLogout(usuario);

                daoUsuario.updateUsuario(usuario);
            }
            catch
            {
                throw new Exception();
            }

        }

        private Usuario setUpdateLogout(Usuario usuario)
        {

            usuario.Activo = false;
            usuario.UltimoEgreso = DateTime.Now;

            return usuario;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {

            DialogResult strRespuesta = MessageBox.Show("¿Realmente desea salir de la aplicación?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            try
            {
                if (strRespuesta == DialogResult.Yes)
                {
                    logoutUsuario();
                    killProcess();
                }

            }catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }


        }

        private void killProcess()
        {

            this.Close();
            Application.Exit();
            Application.ExitThread();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        private void btnGestiones_Click(object sender, EventArgs e)
        {
            this.Hide();
            AltaGestiones frm = new AltaGestiones();
            frm.Show();

        }

      
        

        private void button1_Click(object sender, EventArgs e)
        {
            
            //Normalizacion frm = new Normalizacion();
            //frm.Show();

        }

        private void btnControl_Click(object sender, EventArgs e)
        {
            Control frm = new Control();
            frm.Show();
            this.Hide();

        }

       
        private void btnCambio_Click(object sender, EventArgs e)
        {
            CbioPass frm = new CbioPass();
            frm.Show();


        }

        //private void timer2_Tick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        OleDbConnection CN = new OleDbConnection(Conexiones.cnProcesos);
        //        OleDbCommand CMD = new OleDbCommand(null, CN);
        //        CMD.CommandText = "SELECT COUNT(*) FROM GESTIONES WHERE US_INICIO='" + Ingreso.UsuarioLogueado + "' AND ESTADO='RECHAZADO'";
        //        CN.Open();
        //        if (Convert.ToInt32(CMD.ExecuteScalar()) > Pendientes)
        //        {
        //            Pendientes++;
        //            ntfPendientes(Pendientes, true);
        //            notifyIcon1.BalloonTipText = "Tiene un nuevo tramite rechazado";
        //            notifyIcon1.ShowBalloonTip(10000);
                   
        //        }
        //        CN.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logins.LogError(ex);
                
        //    }
           



        //}

        private void btnRech_Click(object sender, EventArgs e)
        {
            Rechazos frm = new Rechazos();
            frm.Show();
            this.Hide();

        }

        private void btnEstadistic_Click(object sender, EventArgs e)
        {
            Estadisticas frm = new Estadisticas();
            frm.Show();

        }

        private void btnContador_Click(object sender, EventArgs e)
        {
            Contador frm = new Contador();
            frm.Show();

        }

        private void btnMantenimiento_Click(object sender, EventArgs e)
        {
            //EnvioBantotal.enviarPrueba();
           // BPMPrisma frm = new BPMPrisma();
            //frm.Show();
            MessageBox.Show("En construcción!", "Mantenimiento de Software", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void btnConsultas_Click(object sender, EventArgs e)
        {
            Consultas frm = new Consultas();
            frm.Show();

        }

        private void btnCambioEstado_Click(object sender, EventArgs e)
        {
            CambioEstado frm = new CambioEstado();
            frm.Show();

        }

        private void btnRechBatch_Click(object sender, EventArgs e)
        {
            RechBatch frm = new RechBatch();
            frm.Show();

        }

        private void pbUsuarios_Click(object sender, EventArgs e)
        {
            if (tblLayoutUsuarios.Visible)
            {
                tblLayoutUsuarios.Visible = false;
            }
            else
            {
                tblLayoutUsuarios.Visible = true;
            }
        }

        private void pbTramites_Click(object sender, EventArgs e)
        {
            if (tblLayoutTramites.Visible)
            {
                tblLayoutTramites.Visible = false;
            }
            else
            {
                tblLayoutTramites.Visible = true;
            }
        }

        private void pbAjustes_Click(object sender, EventArgs e)
        {
            if (tblLayoutAjustes.Visible)
            {
                tblLayoutAjustes.Visible = false;
            }
            else
            {
                tblLayoutAjustes.Visible = true;
            }
        }
    }
}
