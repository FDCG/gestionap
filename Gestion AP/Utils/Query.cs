﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    class Query
    {

        /// <summary>
        /// Constante para Gestion
        /// </summary>
        /// 
    
        // GESTION QUERY

        public const string INSERT_GESTION_FIELDS = "(NroTramite, CodTramite, NomTramite, Adm, UsuarioInicio, " +
                "FechaInicio, FechaControl, Prioridad, CanalIngreso, Estado)";

        public const string INSERT_GESTION_PARAMETERS = "(@NroTramite, @CodTramite, @NomTramite, @Adm, @UsuarioInicio, " +
                "@FechaInicio, @FechaControl, @Prioridad, @CanalIngreso, @Estado)";

        public const string UPDATE_GESTION_QUERY = "(NroTramite=@NroTramite, CodTramite=@CodTramite, " +
            "NomTramite=@NomTramite, Adm=@Adm, UsuarioInicio=@UsuarioInicio, " +
        "FechaInicio=@FechaInicio, FechaControl=@FechaControl, " +
            "Prioridad=@Prioridad, CanalIngreso=@CanalIngreso, Estado=@Estado)";

        // PROCESO QUERY

        public const string INSERT_PROCESO_FIELDS = "(NroTramite, Fecha, Usuario, Accion, Detalle)";

        public const string INSERT_PROCESO_PARAMETERS = "(@NroTramite, @Fecha, @Usuario, @Accion, @Detalle)";

        public const string UPDATE_PROCESO_QUERY = "(NroTramite=@NroTramite, Fecha=@Fecha, " +
            "Usuario=@Usuario, Accion=@Accion, Detalle=@Detalle)";


 
        // USUARIO QUERY

        public const string INSERT_USUARIO_FIELDS = "(Usuario, Contrasena, Nombre, Apellido, Nivel, " +
        "FechaAlta, FechaBaja, UltimoIngreso, UltimoEgreso, Ntdom, " +
        "Equipo, Dominio, Activo, VtoClave, Intentos, Bloqueado)";

        public const string INSERT_USUARIO_PARAMETERS = "(@Usuario, @Contrasena, @Nombre, @Apellido, @Nivel, " +
            "@FechaAlta, @FechaBaja, @UltimoIngreso, @UltimoEgreso, @Ntdom, " +
            "@Equipo, @Dominio, @Activo, @VtoClave, @Intentos, @Bloqueado)";

        public const string UPDATE_USUARIO_QUERY = "Usuario=@Usuario,Contrasena=@Contrasena," +
            "Nombre=@Nombre,Apellido=@Apellido," +
            "Nivel=@Nivel,FechaAlta=@FechaAlta," +
            "FechaBaja=@FechaBaja,UltimoIngreso=@UltimoIngreso," +
            "UltimoEgreso=@UltimoEgreso,Ntdom=@Ntdom, " +
            "Equipo=@Equipo,Dominio=@Dominio,Activo=@Activo," +
            "VtoClave=@VtoClave,Intentos=@Intentos,Bloqueado=@Bloqueado";

        // WORKFLOW QUERY

        public const string INSERT_WORKFLOW_FIELDS = "(NroTramite, CuilPersona, NomPersona, NroCliente, " +
            "NomCliente, NroCtaTJ, TitCtaTJ, NroTJ, EmbozadoTJ, TipoTJ, CtaDebito, " +
            "FormaPago, LimiteTJ, LimiteAut, DomEspecial, SucEntrega, DatosAdic, MotivoTramite, ObsTramite)";

        public const string INSERT_WORKFLOW_PARAMETERS = "(@NroTramite, @CuilPersona, @NomPersona, @NroCliente, " +
    "@NomCliente, @NroCtaTJ, @TitCtaTJ, @NroTJ, @EmbozadoTJ, @TipoTJ, @CtaDebito, " +
    "@FormaPago, @LimiteTJ, @LimiteAut, @DomEspecial, @SucEntrega, @DatosAdic, @MotivoTramite, @ObsTramite)";


        // TARJETA QUERY

        public const string INSERT_TARJETA_FIELDS = "(NroTramite, NroTarjeta, Categoria, " +
            "NombreTJ, TipoDoc, NroDoc, Vigencia, Estado)";


        public const string INSERT_TARJETA_PARAMETERS = "(@NroTramite, @NroTarjeta, @Categoria, " +
            "@NombreTJ, @TipoDoc, @NroDoc, @Vigencia, @Estado)";
    }
}
