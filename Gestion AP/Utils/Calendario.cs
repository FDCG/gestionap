﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;


namespace Gestion_AP
{
    class Calendario
    {

        private static readonly List<DateTime> Feriados = new List<DateTime>();

        private static bool EsFeriado(DateTime fecha)
        {
            return Feriados.Contains(fecha);
        }
        private static bool EsFindeSemana(DateTime fecha)
        {
            return fecha.DayOfWeek == DayOfWeek.Saturday || fecha.DayOfWeek == DayOfWeek.Sunday;
        }
        public static DateTime DiaLabAnterior(DateTime fecha)
        {

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnUsuario;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select * FROM Feriados";
            DataTable dt = new DataTable();
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Feriados.Add(Convert.ToDateTime(dt.Rows[i][1].ToString()));
            }

            do
            {
                fecha = fecha.AddDays(-1);
            } while (EsFindeSemana(fecha) || EsFeriado(fecha));

            return fecha;





        }
        public static DateTime DiaLabPosterior(DateTime fecha)
        {

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnUsuario;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select * FROM Feriados";
            DataTable dt = new DataTable();
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Feriados.Add(Convert.ToDateTime(dt.Rows[i][1].ToString()));
            }

            do
            {
                fecha = fecha.AddDays(1);
            } while (EsFindeSemana(fecha) || EsFeriado(fecha));

            return fecha;





        }


    }
}

