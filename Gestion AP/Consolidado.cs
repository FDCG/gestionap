﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Gestion_AP
{
    public partial class Consolidado : Form
    {
        public Consolidado()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            DateTime dtFechaDesde = Convert.ToDateTime(dtpFechaDesde.Value.Day + "/" + dtpFechaDesde.Value.Month + "/" + dtpFechaDesde.Value.Year);
            DateTime dtFechaHasta = Convert.ToDateTime(dtpFechaHasta.Value.Day + "/" + dtpFechaHasta.Value.Month + "/" + dtpFechaHasta.Value.Year);

            try
            {
                //----------------------------------------
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "TRANSFORM Count(ACCION) AS CANTIDADES " +
                "SELECT USUARIO FROM ESTADISTICAS WHERE FECHA > @F1 AND FECHA > @F2 " +
                "GROUP BY USUARIO " +
                "PIVOT ACCION ";
                cmd.Parameters.AddWithValue("@F1", dtFechaDesde);
                cmd.Parameters.AddWithValue("@F2", dtFechaHasta.AddDays(1));
                cn.Open();
                cmd.ExecuteReader();
                DataTable dt_1 = new DataTable();
                OleDbDataAdapter da_1 = new OleDbDataAdapter(cmd);
                da_1.Fill(dt_1);

                dgvAcc.DataSource = dt_1;


                cn.Close();

            }
            catch (Exception ex) { Logins.LogError(ex); }
        }
    }
}
