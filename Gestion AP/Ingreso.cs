﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Data.OleDb;

using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
//using EAGetMail;




namespace Gestion_AP
{
    public partial class Ingreso : Form
    {

        Usuario usuario;
        UsuarioDaoImpl daoUsuario;

    
        public static string UsuarioLogueado = "";


        public Ingreso()
        {




            string path = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\prueba.mdf";

            string cnp = string.Format(@"AttachDbFilename={0};Database={1};Trusted_Connection=Yes;", path, "prueba");

            SqlConnection cnn = new SqlConnection();

            cnn.ConnectionString = cnp;

            SqlCommand cmd = new SqlCommand("insert into pruebasql (id, nombre, apellido) values ('12691914', 'facundo', 'cervin')", cnn);

            cnn.Open();

            cmd.ExecuteNonQuery();



            //ProcesoDaoImpl procesoDaoImpl = new ProcesoDaoImpl();

            //Proceso proceso = new Proceso(DateTime.Now, "CEF95432", AccionProceso.Ingreso, "Prueba");

            //procesoDaoImpl.createProceso(proceso, "Bajas111");


            //TarjetaDaoImpl tarjetaDaoImpl = new TarjetaDaoImpl();

            //Tarjeta tarjeta = new Tarjeta("4509940006317890", 'T', "CERVIN GOMEZ/F", "DNI", "36295432", "201909", 2);

            //tarjetaDaoImpl.createTarjeta(tarjeta, "Bajas111");






            InitializeComponent();

            // Instanciacion de Dao Usuarios

            daoUsuario = new UsuarioDaoImpl();

            // Version del producto para Label.
            txtVersion.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            // Usuario mayuscula
            txtUsuario.CharacterCasing = CharacterCasing.Upper;

            // Sugerencia de usuario logueado en Entorno SO
            txtUsuario.Text = Environment.UserName.ToUpper();
            
 
        }


        private void loginUsuario(string iUsuario, string contrasenia)
        {

            try
            {
                usuario = daoUsuario.getByIdUsuario(iUsuario);

                if(usuario == null)
                {
                    throw new Exception("Usuario inexistente.");
                }

                if (usuario.Bloqueado)
                {
                    MessageBox.Show("Usuario bloqueado. Solicite desbloqueo.");
                }else if(usuario.Contrasenia == Logins.CONTRASENIA_BLANQUEADA)
                {
                    changePassword(usuario);
                }
                else
                {

                    if (usuario.Contrasenia.Equals(contrasenia))
                    {

                        if (usuario.Activo)
                        {

                            MessageBox.Show("Usuario logueado. Solicite desmarque.");

                        }
                        else if (usuario.FechaBaja != new DateTime(1, 1, 1))
                        {

                            MessageBox.Show("Acceso denegado. Usuario inhabilitado para operar.");

                        }
                        else if (usuario.VtoClave <= DateTime.Today)
                        {

                            changePassword(usuario);

                        }
                        else
                        {
                            // Realizar logueo ok e ingresar

                            daoUsuario.updateUsuario(setUpdateLogin(usuario));

                            Usuario.usuarioSingleton = usuario;

                            usuario = null;

                            goToForm(new Principal());

                            
                        }
                    }
                    else
                    {
                        // Control y edición de intentos fallidos

                        // Se adiciona un intento fallido
                        usuario.IntentosFallidos++;
                        string messageLog = "Usuario y/o contraseña inválido.";
                        

                        // Si cumple con regla de intentos fallidos, se procede a bloqueo
                        if (usuario.IntentosFallidos == Usuario.MAX_INTENTOS)
                        {
                            // Si llega a el maximo, se resetea contador y se procede a bloquear
                            usuario.IntentosFallidos = 0;
                            usuario.Bloqueado = true;
                            messageLog += "\nUsuario bloqueado. Solicite desbloqueo.";

                        }
                        MessageBox.Show(messageLog);
                        // Actualización en DB
                        daoUsuario.updateUsuario(usuario);
                        

                    }
                }


            }
            catch (ArgumentException argException)
            {
                MessageBox.Show(argException.Message);

            }catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }


        }

        private void goToForm(Form form)
        {

            this.Hide();
            form.Show();

        }

        private void changePassword (Usuario usuario)
        {

            MessageBox.Show("Su contraseña ha expirado.");

            setUpdateLogin(setUpdateLogin(usuario));

            goToForm(new NuevaClave(usuario));

            this.usuario = null;

        }

        private Usuario setUpdateLogin(Usuario usuario)
        {

            usuario.Activo = true;
            usuario.UltimoIngreso = DateTime.Now;
            usuario.IntentosFallidos = 0;
            usuario.Dominio = Environment.UserDomainName.ToUpper();
            usuario.Ntdom = Environment.UserName.ToUpper();
            usuario.Equipo = Environment.MachineName.ToUpper();

            return usuario;

        }

        public void cleanControls()
        {

            txtUsuario.Clear();
            txtContrasenia.Clear();
            txtUsuario.Focus();
                                 
        }


        private void btnSalir_Click(object sender, EventArgs e)
        {
            killProcess();
        }

        private void killProcess()
        {
            this.Close();
            Application.Exit();
            Application.ExitThread();
        }
      

        /*****************************************************************/
        //      PARA REVISAR

        private void tActualizacion_Tick(object sender, EventArgs e)
        {
            if (Actualizacion() != 0)
            {
                Notificacion(Actualizacion());
                ntfPendientes.Visible = true;
            }



        }



        public void Notificacion(int Pendientes)

        {
            if (Pendientes>0)
            {
                ntfPendientes.BalloonTipText = "Tiene " + Pendientes + " trámites pendientes";
                ntfPendientes.Visible = true;
                ntfPendientes.ShowBalloonTip(60000);
            }
         
            

        }

        public static int Actualizacion()

        {
            int pendientes;
           
                OleDbConnection cnp = new OleDbConnection();
                cnp.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmdp = new OleDbCommand();
                cmdp.Connection = cnp;
                cnp.Open();
            //    cmdp.CommandText = "SELECT COUNT(*) FROM GESTIONES WHERE US_INICIO='" + Ingreso.UsuarioLogueado + "' AND ESTADO='RECHAZADO'";
                pendientes = Convert.ToInt32(cmdp.ExecuteScalar());
                cnp.Close();
                return pendientes;

        }

        private void ntfPendientes_MouseClick(object sender, MouseEventArgs e)
        {
            if (Application.OpenForms["Rechazos"]==null)
            {
                Rechazos frm = new Rechazos();
                frm.Show();
                ntfPendientes.Visible = false;
            }
        }


        private void btnIngresar_Click(object sender, EventArgs e)
        {
            string usuario, contrasenia;

            usuario = txtUsuario.Text.Trim();
            contrasenia = txtContrasenia.Text.Trim();

            cleanControls();

            try
            {
                if(usuario == "" && contrasenia == "")
                {
                    throw new ArgumentException("Usuario y contraseña deben ser informados");
                }

                loginUsuario(usuario, contrasenia);

            }catch(ArgumentException argException)
            {
                MessageBox.Show(argException.Message);
            }catch(Exception exception){

                MessageBox.Show(exception.Message);
            }

        }
        //**************
        /*****************************************************************/

        //--- Control de mouse Mostrar/Ocultar contraseña

        public void showHideContrasenia()
        {
            txtContrasenia.UseSystemPasswordChar = !txtContrasenia.UseSystemPasswordChar;
        }

        private void pbMostrarContrasenia_MouseUp(object sender, MouseEventArgs e)
        {
            showHideContrasenia();
        }

        private void pbMostrarContrasenia_MouseDown(object sender, MouseEventArgs e)
        {
            showHideContrasenia();
        }

        //---------
    }
}
