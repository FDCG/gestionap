﻿namespace Gestion_AP
{
    partial class Consultas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Consultas));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnAyuda_A = new System.Windows.Forms.Button();
            this.txtGestiones = new System.Windows.Forms.TextBox();
            this.cmbGestiones = new System.Windows.Forms.ComboBox();
            this.dgvGestiones = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtVarios = new System.Windows.Forms.TextBox();
            this.cmbVarios = new System.Windows.Forms.ComboBox();
            this.dgvVarios = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtBajas = new System.Windows.Forms.TextBox();
            this.cmbBajas = new System.Windows.Forms.ComboBox();
            this.dgvBajas = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgvHistorico = new System.Windows.Forms.DataGridView();
            this.txtProcesos = new System.Windows.Forms.TextBox();
            this.cmbProcesos = new System.Windows.Forms.ComboBox();
            this.dgvProcesos = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.wbPrevisualiza = new System.Windows.Forms.WebBrowser();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvDetalle = new System.Windows.Forms.DataGridView();
            this.dgvMails = new System.Windows.Forms.DataGridView();
            this.btnBusqMail = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBusqMail = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dgvLogComentarios = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtRechazo = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dgvDetalleRech = new System.Windows.Forms.DataGridView();
            this.dgvRechazos = new System.Windows.Forms.DataGridView();
            this.btnBusqRech = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBusqRech = new System.Windows.Forms.TextBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGestiones)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVarios)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBajas)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcesos)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMails)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLogComentarios)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleRech)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRechazos)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.Location = new System.Drawing.Point(27, 48);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1157, 617);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnAyuda_A);
            this.tabPage1.Controls.Add(this.txtGestiones);
            this.tabPage1.Controls.Add(this.cmbGestiones);
            this.tabPage1.Controls.Add(this.dgvGestiones);
            this.tabPage1.ForeColor = System.Drawing.Color.Black;
            this.tabPage1.ImageIndex = 1;
            this.tabPage1.Location = new System.Drawing.Point(4, 39);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Size = new System.Drawing.Size(1149, 574);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Historico Gestiones";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnAyuda_A
            // 
            this.btnAyuda_A.Location = new System.Drawing.Point(603, 7);
            this.btnAyuda_A.Name = "btnAyuda_A";
            this.btnAyuda_A.Size = new System.Drawing.Size(167, 24);
            this.btnAyuda_A.TabIndex = 3;
            this.btnAyuda_A.Text = "¿Cómo Consultar?";
            this.btnAyuda_A.UseVisualStyleBackColor = true;
            this.btnAyuda_A.Click += new System.EventHandler(this.btnAyuda_A_Click);
            // 
            // txtGestiones
            // 
            this.txtGestiones.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGestiones.Location = new System.Drawing.Point(297, 8);
            this.txtGestiones.Name = "txtGestiones";
            this.txtGestiones.Size = new System.Drawing.Size(299, 23);
            this.txtGestiones.TabIndex = 2;
            this.txtGestiones.TextChanged += new System.EventHandler(this.txtGestiones_TextChanged);
            // 
            // cmbGestiones
            // 
            this.cmbGestiones.BackColor = System.Drawing.Color.Blue;
            this.cmbGestiones.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGestiones.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbGestiones.ForeColor = System.Drawing.SystemColors.Info;
            this.cmbGestiones.FormattingEnabled = true;
            this.cmbGestiones.Items.AddRange(new object[] {
            "Usuario",
            "Nombre Trámite",
            "Novedad",
            "Nro. Trámite",
            "Prioridad",
            "Canal Ingreso",
            "Estado"});
            this.cmbGestiones.Location = new System.Drawing.Point(7, 8);
            this.cmbGestiones.Name = "cmbGestiones";
            this.cmbGestiones.Size = new System.Drawing.Size(284, 23);
            this.cmbGestiones.TabIndex = 1;
            // 
            // dgvGestiones
            // 
            this.dgvGestiones.AllowUserToAddRows = false;
            this.dgvGestiones.AllowUserToDeleteRows = false;
            this.dgvGestiones.AllowUserToResizeRows = false;
            this.dgvGestiones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvGestiones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGestiones.BackgroundColor = System.Drawing.Color.White;
            this.dgvGestiones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvGestiones.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvGestiones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGestiones.Location = new System.Drawing.Point(7, 42);
            this.dgvGestiones.Name = "dgvGestiones";
            this.dgvGestiones.ReadOnly = true;
            this.dgvGestiones.RowHeadersVisible = false;
            this.dgvGestiones.Size = new System.Drawing.Size(1136, 540);
            this.dgvGestiones.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtVarios);
            this.tabPage2.Controls.Add(this.cmbVarios);
            this.tabPage2.Controls.Add(this.dgvVarios);
            this.tabPage2.ForeColor = System.Drawing.Color.Black;
            this.tabPage2.ImageIndex = 3;
            this.tabPage2.Location = new System.Drawing.Point(4, 39);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Size = new System.Drawing.Size(1149, 574);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "TJ Varios";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtVarios
            // 
            this.txtVarios.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVarios.Location = new System.Drawing.Point(296, 8);
            this.txtVarios.Name = "txtVarios";
            this.txtVarios.Size = new System.Drawing.Size(299, 23);
            this.txtVarios.TabIndex = 4;
            this.txtVarios.TextChanged += new System.EventHandler(this.txtVarios_TextChanged);
            // 
            // cmbVarios
            // 
            this.cmbVarios.BackColor = System.Drawing.Color.OrangeRed;
            this.cmbVarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbVarios.ForeColor = System.Drawing.SystemColors.Menu;
            this.cmbVarios.FormattingEnabled = true;
            this.cmbVarios.Items.AddRange(new object[] {
            "Novedad",
            "Nro. Trámite",
            "Descripcion",
            "Documento",
            "Nombre Persona",
            "Cliente Bantotal",
            "Tarjeta",
            "Cuenta Tarjeta",
            "Datos Adicionales"});
            this.cmbVarios.Location = new System.Drawing.Point(6, 8);
            this.cmbVarios.Name = "cmbVarios";
            this.cmbVarios.Size = new System.Drawing.Size(284, 23);
            this.cmbVarios.TabIndex = 3;
            // 
            // dgvVarios
            // 
            this.dgvVarios.AllowUserToAddRows = false;
            this.dgvVarios.AllowUserToDeleteRows = false;
            this.dgvVarios.AllowUserToResizeRows = false;
            this.dgvVarios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVarios.BackgroundColor = System.Drawing.Color.White;
            this.dgvVarios.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvVarios.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvVarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVarios.Location = new System.Drawing.Point(6, 43);
            this.dgvVarios.Name = "dgvVarios";
            this.dgvVarios.ReadOnly = true;
            this.dgvVarios.RowHeadersVisible = false;
            this.dgvVarios.Size = new System.Drawing.Size(1136, 525);
            this.dgvVarios.TabIndex = 1;
            this.dgvVarios.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVarios_CellContentDoubleClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtBajas);
            this.tabPage3.Controls.Add(this.cmbBajas);
            this.tabPage3.Controls.Add(this.dgvBajas);
            this.tabPage3.ForeColor = System.Drawing.Color.Black;
            this.tabPage3.ImageIndex = 2;
            this.tabPage3.Location = new System.Drawing.Point(4, 39);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1149, 574);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Bajas";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtBajas
            // 
            this.txtBajas.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBajas.Location = new System.Drawing.Point(296, 8);
            this.txtBajas.Name = "txtBajas";
            this.txtBajas.Size = new System.Drawing.Size(299, 23);
            this.txtBajas.TabIndex = 4;
            this.txtBajas.TextChanged += new System.EventHandler(this.txtBajas_TextChanged);
            // 
            // cmbBajas
            // 
            this.cmbBajas.BackColor = System.Drawing.SystemColors.MenuText;
            this.cmbBajas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBajas.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbBajas.ForeColor = System.Drawing.SystemColors.Window;
            this.cmbBajas.FormattingEnabled = true;
            this.cmbBajas.Items.AddRange(new object[] {
            "Nro Trámite",
            "Administradora",
            "Nombre/Apellido",
            "Cuenta Tarjeta",
            "Tarjeta",
            "Sucursal"});
            this.cmbBajas.Location = new System.Drawing.Point(6, 8);
            this.cmbBajas.Name = "cmbBajas";
            this.cmbBajas.Size = new System.Drawing.Size(284, 23);
            this.cmbBajas.TabIndex = 3;
            this.cmbBajas.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // dgvBajas
            // 
            this.dgvBajas.AllowUserToAddRows = false;
            this.dgvBajas.AllowUserToDeleteRows = false;
            this.dgvBajas.AllowUserToResizeRows = false;
            this.dgvBajas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBajas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBajas.BackgroundColor = System.Drawing.Color.White;
            this.dgvBajas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBajas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBajas.Location = new System.Drawing.Point(6, 43);
            this.dgvBajas.Name = "dgvBajas";
            this.dgvBajas.ReadOnly = true;
            this.dgvBajas.RowHeadersVisible = false;
            this.dgvBajas.Size = new System.Drawing.Size(1136, 525);
            this.dgvBajas.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgvHistorico);
            this.tabPage4.Controls.Add(this.txtProcesos);
            this.tabPage4.Controls.Add(this.cmbProcesos);
            this.tabPage4.Controls.Add(this.dgvProcesos);
            this.tabPage4.ForeColor = System.Drawing.Color.Black;
            this.tabPage4.ImageIndex = 0;
            this.tabPage4.Location = new System.Drawing.Point(4, 39);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1149, 574);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Control de Procesos";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgvHistorico
            // 
            this.dgvHistorico.AllowUserToAddRows = false;
            this.dgvHistorico.AllowUserToDeleteRows = false;
            this.dgvHistorico.AllowUserToResizeRows = false;
            this.dgvHistorico.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvHistorico.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvHistorico.BackgroundColor = System.Drawing.Color.White;
            this.dgvHistorico.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistorico.Location = new System.Drawing.Point(6, 423);
            this.dgvHistorico.Name = "dgvHistorico";
            this.dgvHistorico.ReadOnly = true;
            this.dgvHistorico.RowHeadersVisible = false;
            this.dgvHistorico.Size = new System.Drawing.Size(1136, 138);
            this.dgvHistorico.TabIndex = 5;
            this.dgvHistorico.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHistorico_CellContentDoubleClick);
            // 
            // txtProcesos
            // 
            this.txtProcesos.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProcesos.Location = new System.Drawing.Point(296, 8);
            this.txtProcesos.Name = "txtProcesos";
            this.txtProcesos.Size = new System.Drawing.Size(299, 23);
            this.txtProcesos.TabIndex = 4;
            this.txtProcesos.TextChanged += new System.EventHandler(this.txtProcesos_TextChanged);
            // 
            // cmbProcesos
            // 
            this.cmbProcesos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cmbProcesos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProcesos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbProcesos.ForeColor = System.Drawing.SystemColors.Window;
            this.cmbProcesos.FormattingEnabled = true;
            this.cmbProcesos.Items.AddRange(new object[] {
            "Nro Trámite",
            "Usuario",
            "Acción",
            "Tramite",
            "Novedad",
            "Prioridad",
            "Estado"});
            this.cmbProcesos.Location = new System.Drawing.Point(6, 8);
            this.cmbProcesos.Name = "cmbProcesos";
            this.cmbProcesos.Size = new System.Drawing.Size(284, 23);
            this.cmbProcesos.TabIndex = 3;
            // 
            // dgvProcesos
            // 
            this.dgvProcesos.AllowUserToAddRows = false;
            this.dgvProcesos.AllowUserToDeleteRows = false;
            this.dgvProcesos.AllowUserToResizeRows = false;
            this.dgvProcesos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProcesos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProcesos.BackgroundColor = System.Drawing.Color.White;
            this.dgvProcesos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvProcesos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProcesos.Location = new System.Drawing.Point(6, 42);
            this.dgvProcesos.Name = "dgvProcesos";
            this.dgvProcesos.ReadOnly = true;
            this.dgvProcesos.RowHeadersVisible = false;
            this.dgvProcesos.Size = new System.Drawing.Size(1136, 360);
            this.dgvProcesos.TabIndex = 1;
            this.dgvProcesos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProcesos_CellContentClick);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Controls.Add(this.groupBox1);
            this.tabPage5.Controls.Add(this.btnBusqMail);
            this.tabPage5.Controls.Add(this.label3);
            this.tabPage5.Controls.Add(this.label2);
            this.tabPage5.Controls.Add(this.txtBusqMail);
            this.tabPage5.ImageIndex = 4;
            this.tabPage5.Location = new System.Drawing.Point(4, 39);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1149, 574);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Busqueda por Mail";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(547, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "*** Doble Click sobre el resultado para obtener detalle del trámite y previsualiz" +
    "ación del mail ***";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(721, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "** Case Semi-Sensitive: El texto a buscar NO distingue mayúsculas y minúsculas pe" +
    "ro si tildes y cualquier signo de puntuación **";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.dgvMails);
            this.groupBox1.Location = new System.Drawing.Point(25, 101);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1105, 467);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resultados de la búsqueda";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.wbPrevisualiza);
            this.groupBox3.Location = new System.Drawing.Point(150, 85);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(946, 376);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Previsualización del Mail";
            // 
            // wbPrevisualiza
            // 
            this.wbPrevisualiza.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbPrevisualiza.Location = new System.Drawing.Point(3, 17);
            this.wbPrevisualiza.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbPrevisualiza.Name = "wbPrevisualiza";
            this.wbPrevisualiza.Size = new System.Drawing.Size(940, 356);
            this.wbPrevisualiza.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dgvDetalle);
            this.groupBox2.Location = new System.Drawing.Point(150, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(949, 67);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detalle del Trámite";
            // 
            // dgvDetalle
            // 
            this.dgvDetalle.AllowUserToAddRows = false;
            this.dgvDetalle.AllowUserToDeleteRows = false;
            this.dgvDetalle.AllowUserToResizeColumns = false;
            this.dgvDetalle.AllowUserToResizeRows = false;
            this.dgvDetalle.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDetalle.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvDetalle.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetalle.Location = new System.Drawing.Point(3, 17);
            this.dgvDetalle.Name = "dgvDetalle";
            this.dgvDetalle.ReadOnly = true;
            this.dgvDetalle.RowHeadersVisible = false;
            this.dgvDetalle.Size = new System.Drawing.Size(943, 47);
            this.dgvDetalle.TabIndex = 1;
            // 
            // dgvMails
            // 
            this.dgvMails.AllowUserToAddRows = false;
            this.dgvMails.AllowUserToDeleteRows = false;
            this.dgvMails.AllowUserToResizeColumns = false;
            this.dgvMails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMails.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvMails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvMails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMails.Location = new System.Drawing.Point(6, 20);
            this.dgvMails.Name = "dgvMails";
            this.dgvMails.ReadOnly = true;
            this.dgvMails.RowHeadersVisible = false;
            this.dgvMails.Size = new System.Drawing.Size(138, 441);
            this.dgvMails.TabIndex = 0;
            this.dgvMails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMails_CellContentClick);
            this.dgvMails.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMails_CellContentDoubleClick);
            // 
            // btnBusqMail
            // 
            this.btnBusqMail.Location = new System.Drawing.Point(450, 6);
            this.btnBusqMail.Name = "btnBusqMail";
            this.btnBusqMail.Size = new System.Drawing.Size(165, 23);
            this.btnBusqMail.TabIndex = 8;
            this.btnBusqMail.Text = "Aplicar Búsqueda";
            this.btnBusqMail.UseVisualStyleBackColor = true;
            this.btnBusqMail.Click += new System.EventHandler(this.btnBusqMail_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(29, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(629, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "* Las coincidencias aplicarán sobre remitentes, destinatarios, asuntos, nombre de" +
    " adjuntos y cuerpo del mail *";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Palabra a Buscar:";
            // 
            // txtBusqMail
            // 
            this.txtBusqMail.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBusqMail.Location = new System.Drawing.Point(139, 6);
            this.txtBusqMail.Name = "txtBusqMail";
            this.txtBusqMail.Size = new System.Drawing.Size(299, 23);
            this.txtBusqMail.TabIndex = 5;
            this.txtBusqMail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBusqMail_KeyPress);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.label6);
            this.tabPage6.Controls.Add(this.groupBox4);
            this.tabPage6.Controls.Add(this.btnBusqRech);
            this.tabPage6.Controls.Add(this.label8);
            this.tabPage6.Controls.Add(this.label9);
            this.tabPage6.Controls.Add(this.txtBusqRech);
            this.tabPage6.ImageIndex = 5;
            this.tabPage6.Location = new System.Drawing.Point(4, 39);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1149, 574);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Rechazos";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(547, 15);
            this.label6.TabIndex = 18;
            this.label6.Text = "*** Doble Click sobre el resultado para obtener detalle del trámite y previsualiz" +
    "ación del mail ***";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.groupBox7);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.dgvRechazos);
            this.groupBox4.Location = new System.Drawing.Point(23, 71);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1105, 497);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Resultados de la búsqueda";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.dgvLogComentarios);
            this.groupBox7.Location = new System.Drawing.Point(564, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(532, 99);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Log de Comentarios";
            // 
            // dgvLogComentarios
            // 
            this.dgvLogComentarios.AllowUserToAddRows = false;
            this.dgvLogComentarios.AllowUserToDeleteRows = false;
            this.dgvLogComentarios.AllowUserToResizeColumns = false;
            this.dgvLogComentarios.AllowUserToResizeRows = false;
            this.dgvLogComentarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLogComentarios.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvLogComentarios.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvLogComentarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLogComentarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLogComentarios.Location = new System.Drawing.Point(3, 17);
            this.dgvLogComentarios.Name = "dgvLogComentarios";
            this.dgvLogComentarios.ReadOnly = true;
            this.dgvLogComentarios.RowHeadersVisible = false;
            this.dgvLogComentarios.Size = new System.Drawing.Size(526, 79);
            this.dgvLogComentarios.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.txtRechazo);
            this.groupBox5.Location = new System.Drawing.Point(150, 117);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(946, 374);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Previsualización del Rechazo";
            // 
            // txtRechazo
            // 
            this.txtRechazo.Location = new System.Drawing.Point(3, 21);
            this.txtRechazo.Multiline = true;
            this.txtRechazo.Name = "txtRechazo";
            this.txtRechazo.ReadOnly = true;
            this.txtRechazo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRechazo.Size = new System.Drawing.Size(937, 353);
            this.txtRechazo.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.dgvDetalleRech);
            this.groupBox6.Location = new System.Drawing.Point(150, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(408, 99);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Detalle del Rechazo";
            // 
            // dgvDetalleRech
            // 
            this.dgvDetalleRech.AllowUserToAddRows = false;
            this.dgvDetalleRech.AllowUserToDeleteRows = false;
            this.dgvDetalleRech.AllowUserToResizeColumns = false;
            this.dgvDetalleRech.AllowUserToResizeRows = false;
            this.dgvDetalleRech.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDetalleRech.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvDetalleRech.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvDetalleRech.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalleRech.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetalleRech.Location = new System.Drawing.Point(3, 17);
            this.dgvDetalleRech.Name = "dgvDetalleRech";
            this.dgvDetalleRech.ReadOnly = true;
            this.dgvDetalleRech.RowHeadersVisible = false;
            this.dgvDetalleRech.Size = new System.Drawing.Size(402, 79);
            this.dgvDetalleRech.TabIndex = 1;
            // 
            // dgvRechazos
            // 
            this.dgvRechazos.AllowUserToAddRows = false;
            this.dgvRechazos.AllowUserToDeleteRows = false;
            this.dgvRechazos.AllowUserToResizeColumns = false;
            this.dgvRechazos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRechazos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvRechazos.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvRechazos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRechazos.Location = new System.Drawing.Point(6, 20);
            this.dgvRechazos.Name = "dgvRechazos";
            this.dgvRechazos.ReadOnly = true;
            this.dgvRechazos.RowHeadersVisible = false;
            this.dgvRechazos.Size = new System.Drawing.Size(138, 471);
            this.dgvRechazos.TabIndex = 0;
            this.dgvRechazos.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRechazos_CellContentDoubleClick);
            // 
            // btnBusqRech
            // 
            this.btnBusqRech.Location = new System.Drawing.Point(448, 6);
            this.btnBusqRech.Name = "btnBusqRech";
            this.btnBusqRech.Size = new System.Drawing.Size(165, 23);
            this.btnBusqRech.TabIndex = 15;
            this.btnBusqRech.Text = "Aplicar Búsqueda";
            this.btnBusqRech.UseVisualStyleBackColor = true;
            this.btnBusqRech.Click += new System.EventHandler(this.btnBusqRech_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(27, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(359, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "* Las coincidencias aplicarán sobre todo el texto del rechazo *";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 15);
            this.label9.TabIndex = 13;
            this.label9.Text = "Palabra a Buscar:";
            // 
            // txtBusqRech
            // 
            this.txtBusqRech.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBusqRech.Location = new System.Drawing.Point(137, 6);
            this.txtBusqRech.Name = "txtBusqRech";
            this.txtBusqRech.Size = new System.Drawing.Size(299, 23);
            this.txtBusqRech.TabIndex = 12;
            this.txtBusqRech.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBusqRech_KeyPress);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "FontAwesome_f0ac(3)_64.png");
            this.imageList1.Images.SetKeyName(1, "FontAwesome_f0fd(2)_64.png");
            this.imageList1.Images.SetKeyName(2, "FontAwesome_f15a(1)_64.png");
            this.imageList1.Images.SetKeyName(3, "FontAwesome_f18b(0)_64.png");
            this.imageList1.Images.SetKeyName(4, "Entypo_2709(0)_32.png");
            this.imageList1.Images.SetKeyName(5, "Icono.ico");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(24, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(343, 38);
            this.label1.TabIndex = 1;
            this.label1.Text = "Módulo de Consultas";
            // 
            // Consultas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.ClientSize = new System.Drawing.Size(1209, 669);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Consultas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultas";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGestiones)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVarios)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBajas)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcesos)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMails)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLogComentarios)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleRech)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRechazos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtGestiones;
        private System.Windows.Forms.ComboBox cmbGestiones;
        private System.Windows.Forms.DataGridView dgvGestiones;
        private System.Windows.Forms.TextBox txtVarios;
        private System.Windows.Forms.ComboBox cmbVarios;
        private System.Windows.Forms.DataGridView dgvVarios;
        private System.Windows.Forms.TextBox txtBajas;
        private System.Windows.Forms.ComboBox cmbBajas;
        private System.Windows.Forms.DataGridView dgvBajas;
        private System.Windows.Forms.DataGridView dgvProcesos;
        private System.Windows.Forms.TextBox txtProcesos;
        private System.Windows.Forms.ComboBox cmbProcesos;
        private System.Windows.Forms.Button btnAyuda_A;
        private System.Windows.Forms.DataGridView dgvHistorico;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvMails;
        private System.Windows.Forms.Button btnBusqMail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBusqMail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvDetalle;
        private System.Windows.Forms.WebBrowser wbPrevisualiza;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dgvLogComentarios;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtRechazo;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dgvDetalleRech;
        private System.Windows.Forms.DataGridView dgvRechazos;
        private System.Windows.Forms.Button btnBusqRech;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBusqRech;
    }
}