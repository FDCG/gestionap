﻿namespace Gestion_AP
{
    partial class Estadisticas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Estadisticas));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTotFecha = new System.Windows.Forms.TextBox();
            this.txtTotRep = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTotCon = new System.Windows.Forms.TextBox();
            this.txtTotIng = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTotUs = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUsRep = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUsCon = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUsIng = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvTotTram = new System.Windows.Forms.DataGridView();
            this.btnConsulta = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgvUsTram = new System.Windows.Forms.DataGridView();
            this.btnConsolidado = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotTram)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsTram)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.MidnightBlue;
            this.groupBox1.Controls.Add(this.txtTotFecha);
            this.groupBox1.Controls.Add(this.txtTotRep);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtTotCon);
            this.groupBox1.Controls.Add(this.txtTotIng);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(20, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(274, 157);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Totales por Fecha";
            // 
            // txtTotFecha
            // 
            this.txtTotFecha.Location = new System.Drawing.Point(175, 120);
            this.txtTotFecha.Name = "txtTotFecha";
            this.txtTotFecha.ReadOnly = true;
            this.txtTotFecha.Size = new System.Drawing.Size(73, 24);
            this.txtTotFecha.TabIndex = 15;
            // 
            // txtTotRep
            // 
            this.txtTotRep.Location = new System.Drawing.Point(175, 91);
            this.txtTotRep.Name = "txtTotRep";
            this.txtTotRep.ReadOnly = true;
            this.txtTotRep.Size = new System.Drawing.Size(73, 24);
            this.txtTotRep.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Gray;
            this.label8.Location = new System.Drawing.Point(44, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 18);
            this.label8.TabIndex = 14;
            this.label8.Text = "TOTAL FECHA:";
            // 
            // txtTotCon
            // 
            this.txtTotCon.Location = new System.Drawing.Point(175, 62);
            this.txtTotCon.Name = "txtTotCon";
            this.txtTotCon.ReadOnly = true;
            this.txtTotCon.Size = new System.Drawing.Size(73, 24);
            this.txtTotCon.TabIndex = 4;
            // 
            // txtTotIng
            // 
            this.txtTotIng.Location = new System.Drawing.Point(175, 32);
            this.txtTotIng.Name = "txtTotIng";
            this.txtTotIng.ReadOnly = true;
            this.txtTotIng.Size = new System.Drawing.Size(73, 24);
            this.txtTotIng.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(17, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "REPROCESADOS:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Gold;
            this.label2.Location = new System.Drawing.Point(29, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "CONTROLADOS:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.ForestGreen;
            this.label1.Location = new System.Drawing.Point(47, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "INGRESADOS:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTotUs);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtUsRep);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtUsCon);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtUsIng);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(300, 48);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(267, 157);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Estadisticas del Usuario";
            // 
            // txtTotUs
            // 
            this.txtTotUs.Location = new System.Drawing.Point(172, 120);
            this.txtTotUs.Name = "txtTotUs";
            this.txtTotUs.ReadOnly = true;
            this.txtTotUs.Size = new System.Drawing.Size(73, 24);
            this.txtTotUs.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(22, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(144, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "TOTAL USUARIO:";
            // 
            // txtUsRep
            // 
            this.txtUsRep.Location = new System.Drawing.Point(172, 90);
            this.txtUsRep.Name = "txtUsRep";
            this.txtUsRep.ReadOnly = true;
            this.txtUsRep.Size = new System.Drawing.Size(73, 24);
            this.txtUsRep.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.ForestGreen;
            this.label6.Location = new System.Drawing.Point(44, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "INGRESADOS:";
            // 
            // txtUsCon
            // 
            this.txtUsCon.Location = new System.Drawing.Point(172, 60);
            this.txtUsCon.Name = "txtUsCon";
            this.txtUsCon.ReadOnly = true;
            this.txtUsCon.Size = new System.Drawing.Size(73, 24);
            this.txtUsCon.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Gold;
            this.label5.Location = new System.Drawing.Point(26, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 18);
            this.label5.TabIndex = 7;
            this.label5.Text = "CONTROLADOS:";
            // 
            // txtUsIng
            // 
            this.txtUsIng.Location = new System.Drawing.Point(172, 30);
            this.txtUsIng.Name = "txtUsIng";
            this.txtUsIng.ReadOnly = true;
            this.txtUsIng.Size = new System.Drawing.Size(73, 24);
            this.txtUsIng.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(14, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "REPROCESADOS:";
            // 
            // dtpFecha
            // 
            this.dtpFecha.CalendarTitleBackColor = System.Drawing.Color.Red;
            this.dtpFecha.CalendarTitleForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dtpFecha.CalendarTrailingForeColor = System.Drawing.Color.Gold;
            this.dtpFecha.Location = new System.Drawing.Point(20, 13);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(325, 20);
            this.dtpFecha.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.MidnightBlue;
            this.groupBox3.Controls.Add(this.dgvTotTram);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Location = new System.Drawing.Point(20, 211);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(547, 157);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Trámites Ingresados";
            // 
            // dgvTotTram
            // 
            this.dgvTotTram.AllowUserToAddRows = false;
            this.dgvTotTram.AllowUserToDeleteRows = false;
            this.dgvTotTram.AllowUserToResizeColumns = false;
            this.dgvTotTram.AllowUserToResizeRows = false;
            this.dgvTotTram.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTotTram.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTotTram.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTotTram.Location = new System.Drawing.Point(3, 20);
            this.dgvTotTram.Name = "dgvTotTram";
            this.dgvTotTram.ReadOnly = true;
            this.dgvTotTram.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvTotTram.RowHeadersVisible = false;
            this.dgvTotTram.Size = new System.Drawing.Size(541, 134);
            this.dgvTotTram.TabIndex = 0;
            // 
            // btnConsulta
            // 
            this.btnConsulta.Location = new System.Drawing.Point(367, 13);
            this.btnConsulta.Name = "btnConsulta";
            this.btnConsulta.Size = new System.Drawing.Size(94, 20);
            this.btnConsulta.TabIndex = 4;
            this.btnConsulta.Text = "Consultar";
            this.btnConsulta.UseVisualStyleBackColor = true;
            this.btnConsulta.Click += new System.EventHandler(this.btnConsulta_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.MidnightBlue;
            this.groupBox4.Controls.Add(this.dgvUsTram);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox4.Location = new System.Drawing.Point(20, 374);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(547, 157);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Trámites del Usuario";
            // 
            // dgvUsTram
            // 
            this.dgvUsTram.AllowUserToAddRows = false;
            this.dgvUsTram.AllowUserToDeleteRows = false;
            this.dgvUsTram.AllowUserToResizeColumns = false;
            this.dgvUsTram.AllowUserToResizeRows = false;
            this.dgvUsTram.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightCoral;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DarkSalmon;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvUsTram.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvUsTram.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUsTram.Location = new System.Drawing.Point(3, 20);
            this.dgvUsTram.Name = "dgvUsTram";
            this.dgvUsTram.ReadOnly = true;
            this.dgvUsTram.RowHeadersVisible = false;
            this.dgvUsTram.Size = new System.Drawing.Size(541, 134);
            this.dgvUsTram.TabIndex = 1;
            // 
            // btnConsolidado
            // 
            this.btnConsolidado.Location = new System.Drawing.Point(472, 13);
            this.btnConsolidado.Name = "btnConsolidado";
            this.btnConsolidado.Size = new System.Drawing.Size(94, 20);
            this.btnConsolidado.TabIndex = 5;
            this.btnConsolidado.Text = "Consolidado";
            this.btnConsolidado.UseVisualStyleBackColor = true;
            this.btnConsolidado.Click += new System.EventHandler(this.btnConsolidado_Click);
            // 
            // Estadisticas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(582, 538);
            this.Controls.Add(this.btnConsolidado);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnConsulta);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Estadisticas";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estadisticas";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotTram)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsTram)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtTotRep;
        private System.Windows.Forms.TextBox txtTotCon;
        private System.Windows.Forms.TextBox txtTotIng;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtUsRep;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUsCon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUsIng;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgvTotTram;
        private System.Windows.Forms.Button btnConsulta;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgvUsTram;
        private System.Windows.Forms.TextBox txtTotFecha;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTotUs;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnConsolidado;
    }
}