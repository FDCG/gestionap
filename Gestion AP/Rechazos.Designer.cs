﻿namespace Gestion_AP
{
    partial class Rechazos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Rechazos));
            this.btnVertodo = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnWorklist = new System.Windows.Forms.Button();
            this.btnDelegar = new System.Windows.Forms.Button();
            this.txtTotUs = new System.Windows.Forms.TextBox();
            this.txtTotRech = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.rbtNro = new System.Windows.Forms.RadioButton();
            this.rbtAdm = new System.Windows.Forms.RadioButton();
            this.rbtCanal = new System.Windows.Forms.RadioButton();
            this.rbtPrio = new System.Windows.Forms.RadioButton();
            this.rbtWf = new System.Windows.Forms.RadioButton();
            this.rbtUser = new System.Windows.Forms.RadioButton();
            this.rbtTram = new System.Windows.Forms.RadioButton();
            this.dgvRechazos = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRechazos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVertodo
            // 
            this.btnVertodo.Location = new System.Drawing.Point(429, 70);
            this.btnVertodo.Name = "btnVertodo";
            this.btnVertodo.Size = new System.Drawing.Size(154, 23);
            this.btnVertodo.TabIndex = 10;
            this.btnVertodo.Text = "VISUALIZAR TODOS";
            this.btnVertodo.UseVisualStyleBackColor = true;
            this.btnVertodo.Click += new System.EventHandler(this.btnVertodo_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.MediumAquamarine;
            this.groupBox1.Controls.Add(this.btnWorklist);
            this.groupBox1.Controls.Add(this.btnDelegar);
            this.groupBox1.Controls.Add(this.txtTotUs);
            this.groupBox1.Controls.Add(this.txtTotRech);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnVertodo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtFiltro);
            this.groupBox1.Controls.Add(this.rbtNro);
            this.groupBox1.Controls.Add(this.rbtAdm);
            this.groupBox1.Controls.Add(this.rbtCanal);
            this.groupBox1.Controls.Add(this.rbtPrio);
            this.groupBox1.Controls.Add(this.rbtWf);
            this.groupBox1.Controls.Add(this.rbtUser);
            this.groupBox1.Controls.Add(this.rbtTram);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(696, 100);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Buscar por";
            // 
            // btnWorklist
            // 
            this.btnWorklist.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnWorklist.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWorklist.ForeColor = System.Drawing.SystemColors.Control;
            this.btnWorklist.Location = new System.Drawing.Point(595, 14);
            this.btnWorklist.Name = "btnWorklist";
            this.btnWorklist.Size = new System.Drawing.Size(90, 38);
            this.btnWorklist.TabIndex = 18;
            this.btnWorklist.Text = "RECHLIST";
            this.btnWorklist.UseVisualStyleBackColor = false;
            this.btnWorklist.Click += new System.EventHandler(this.btnWorklist_Click);
            // 
            // btnDelegar
            // 
            this.btnDelegar.BackColor = System.Drawing.Color.Red;
            this.btnDelegar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelegar.ForeColor = System.Drawing.SystemColors.Control;
            this.btnDelegar.Location = new System.Drawing.Point(595, 58);
            this.btnDelegar.Name = "btnDelegar";
            this.btnDelegar.Size = new System.Drawing.Size(90, 36);
            this.btnDelegar.TabIndex = 17;
            this.btnDelegar.Text = "DELEGAR";
            this.btnDelegar.UseVisualStyleBackColor = false;
            this.btnDelegar.Click += new System.EventHandler(this.btnDelegar_Click);
            // 
            // txtTotUs
            // 
            this.txtTotUs.Location = new System.Drawing.Point(535, 43);
            this.txtTotUs.Name = "txtTotUs";
            this.txtTotUs.ReadOnly = true;
            this.txtTotUs.Size = new System.Drawing.Size(48, 20);
            this.txtTotUs.TabIndex = 16;
            // 
            // txtTotRech
            // 
            this.txtTotRech.Location = new System.Drawing.Point(535, 17);
            this.txtTotRech.Name = "txtTotRech";
            this.txtTotRech.ReadOnly = true;
            this.txtTotRech.Size = new System.Drawing.Size(48, 20);
            this.txtTotRech.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(441, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Total Usuario:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(429, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Total Rechazos:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Texto a buscar:";
            // 
            // txtFiltro
            // 
            this.txtFiltro.Location = new System.Drawing.Point(107, 72);
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(317, 20);
            this.txtFiltro.TabIndex = 8;
            this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
            // 
            // rbtNro
            // 
            this.rbtNro.AutoSize = true;
            this.rbtNro.Location = new System.Drawing.Point(316, 20);
            this.rbtNro.Name = "rbtNro";
            this.rbtNro.Size = new System.Drawing.Size(80, 17);
            this.rbtNro.TabIndex = 4;
            this.rbtNro.TabStop = true;
            this.rbtNro.Text = "&Nro.Trámite";
            this.rbtNro.UseVisualStyleBackColor = true;
            // 
            // rbtAdm
            // 
            this.rbtAdm.AutoSize = true;
            this.rbtAdm.Location = new System.Drawing.Point(208, 46);
            this.rbtAdm.Name = "rbtAdm";
            this.rbtAdm.Size = new System.Drawing.Size(94, 17);
            this.rbtAdm.TabIndex = 7;
            this.rbtAdm.TabStop = true;
            this.rbtAdm.Text = "&Administradora";
            this.rbtAdm.UseVisualStyleBackColor = true;
            // 
            // rbtCanal
            // 
            this.rbtCanal.AutoSize = true;
            this.rbtCanal.Location = new System.Drawing.Point(107, 46);
            this.rbtCanal.Name = "rbtCanal";
            this.rbtCanal.Size = new System.Drawing.Size(90, 17);
            this.rbtCanal.TabIndex = 6;
            this.rbtCanal.TabStop = true;
            this.rbtCanal.Text = "&Canal Ingreso";
            this.rbtCanal.UseVisualStyleBackColor = true;
            // 
            // rbtPrio
            // 
            this.rbtPrio.AutoSize = true;
            this.rbtPrio.ForeColor = System.Drawing.Color.Red;
            this.rbtPrio.Location = new System.Drawing.Point(10, 20);
            this.rbtPrio.Name = "rbtPrio";
            this.rbtPrio.Size = new System.Drawing.Size(66, 17);
            this.rbtPrio.TabIndex = 1;
            this.rbtPrio.TabStop = true;
            this.rbtPrio.Text = "&Prioridad";
            this.rbtPrio.UseVisualStyleBackColor = true;
            // 
            // rbtWf
            // 
            this.rbtWf.AutoSize = true;
            this.rbtWf.Location = new System.Drawing.Point(10, 46);
            this.rbtWf.Name = "rbtWf";
            this.rbtWf.Size = new System.Drawing.Size(70, 17);
            this.rbtWf.TabIndex = 5;
            this.rbtWf.TabStop = true;
            this.rbtWf.Text = "&Workflow";
            this.rbtWf.UseVisualStyleBackColor = true;
            // 
            // rbtUser
            // 
            this.rbtUser.AutoSize = true;
            this.rbtUser.ForeColor = System.Drawing.Color.Blue;
            this.rbtUser.Location = new System.Drawing.Point(107, 20);
            this.rbtUser.Name = "rbtUser";
            this.rbtUser.Size = new System.Drawing.Size(61, 17);
            this.rbtUser.TabIndex = 2;
            this.rbtUser.TabStop = true;
            this.rbtUser.Text = "&Usuario";
            this.rbtUser.UseVisualStyleBackColor = true;
            // 
            // rbtTram
            // 
            this.rbtTram.AutoSize = true;
            this.rbtTram.Location = new System.Drawing.Point(208, 20);
            this.rbtTram.Name = "rbtTram";
            this.rbtTram.Size = new System.Drawing.Size(60, 17);
            this.rbtTram.TabIndex = 3;
            this.rbtTram.TabStop = true;
            this.rbtTram.Text = "&Tramite";
            this.rbtTram.UseVisualStyleBackColor = true;
            // 
            // dgvRechazos
            // 
            this.dgvRechazos.AllowUserToOrderColumns = true;
            this.dgvRechazos.AllowUserToResizeRows = false;
            this.dgvRechazos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRechazos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRechazos.GridColor = System.Drawing.Color.OrangeRed;
            this.dgvRechazos.Location = new System.Drawing.Point(12, 119);
            this.dgvRechazos.Name = "dgvRechazos";
            this.dgvRechazos.Size = new System.Drawing.Size(696, 229);
            this.dgvRechazos.TabIndex = 14;
            this.dgvRechazos.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRechazos_CellContentDoubleClick);
            this.dgvRechazos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvRechazos_KeyDown);
            // 
            // Rechazos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(718, 353);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvRechazos);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Rechazos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rechazos";
            this.Activated += new System.EventHandler(this.Rechazos_Enter);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Rechazos_FormClosed);
            this.Load += new System.EventHandler(this.Rechazos_Load);
            this.Enter += new System.EventHandler(this.Rechazos_Enter);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRechazos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnVertodo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.RadioButton rbtNro;
        private System.Windows.Forms.RadioButton rbtAdm;
        private System.Windows.Forms.RadioButton rbtCanal;
        private System.Windows.Forms.RadioButton rbtPrio;
        private System.Windows.Forms.RadioButton rbtWf;
        private System.Windows.Forms.RadioButton rbtUser;
        private System.Windows.Forms.RadioButton rbtTram;
        public System.Windows.Forms.DataGridView dgvRechazos;
        private System.Windows.Forms.TextBox txtTotUs;
        private System.Windows.Forms.TextBox txtTotRech;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnDelegar;
        private System.Windows.Forms.Button btnWorklist;
    }
}