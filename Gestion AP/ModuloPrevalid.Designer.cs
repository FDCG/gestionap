﻿namespace Gestion_AP
{
    partial class ModuloPrevalid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModuloPrevalid));
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPrioridad = new System.Windows.Forms.TextBox();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.btnCargar = new System.Windows.Forms.Button();
            this.txtNroTramite = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTramite = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpDatosTitular = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtFRta = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtFEnvio = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.txtNroSolTj = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.txtNroSolCta = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtDescGeo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtCodGeo = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtProv = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtLocal = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCodPos = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDpto = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPiso = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPuerta = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNroCamp = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtPreCa = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.txtSucVta = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFNac = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtConv = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEstCivil = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtSeg = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCodPaq = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCte_Paq = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNacion = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTipDoc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPais = new System.Windows.Forms.TextBox();
            this.DNI = new System.Windows.Forms.Label();
            this.txtDni = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tpParametros = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label62 = new System.Windows.Forms.Label();
            this.txtCodDist = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.txtOcupacion = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.txtC_Ser_M = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.txtC_Ser_Bo = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txtGtia = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.txtMod_Au = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.txtC_Ser_Cu = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtBonifCred = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtCoefAde = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.txtPromo = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.txtBonifEmi = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtCantBonif = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.txtProd = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtVigencia = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtTipVig = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtCodAgrup = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtHabATM = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtCargoBco = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtCatBco = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtIva = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtCtaDebit = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtLimCred = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtCodLim = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtLimCom = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTipoCta = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtTipoTJ = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtFPago = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtCCLim = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtLimOto = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtEmbozado = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label67 = new System.Windows.Forms.Label();
            this.txtGaffTit = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.txtTarjeta = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.txtCuentaTJ = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtS_Mod_ta = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txt4taLinea = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtCartera = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSucAdm = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtGaff = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtModLiq = new System.Windows.Forms.TextBox();
            this.txtS_Mod_ca = new System.Windows.Forms.TextBox();
            this.tpAdicionales = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtAdicInf = new System.Windows.Forms.RichTextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dgvAdicionales = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgvErrPrevalid = new System.Windows.Forms.DataGridView();
            this.tpHistorico = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.dgvProceso = new System.Windows.Forms.DataGridView();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dgvComentarios = new System.Windows.Forms.DataGridView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtAdicComent = new System.Windows.Forms.RichTextBox();
            this.chbLiberacion = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tpDatosTitular.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpParametros.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tpAdicionales.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdicionales)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvErrPrevalid)).BeginInit();
            this.tpHistorico.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProceso)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComentarios)).BeginInit();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(681, 10);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(111, 58);
            this.btnGuardar.TabIndex = 72;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(315, 14);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(65, 15);
            this.label32.TabIndex = 71;
            this.label32.Text = "PRIORIDAD";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(14, 46);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 15);
            this.label12.TabIndex = 69;
            this.label12.Text = "DESCRIPCION";
            // 
            // txtPrioridad
            // 
            this.txtPrioridad.Location = new System.Drawing.Point(401, 10);
            this.txtPrioridad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPrioridad.Name = "txtPrioridad";
            this.txtPrioridad.Size = new System.Drawing.Size(132, 21);
            this.txtPrioridad.TabIndex = 70;
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(113, 43);
            this.txtDesc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.ReadOnly = true;
            this.txtDesc.Size = new System.Drawing.Size(289, 21);
            this.txtDesc.TabIndex = 67;
            // 
            // btnCargar
            // 
            this.btnCargar.Location = new System.Drawing.Point(554, 10);
            this.btnCargar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(111, 58);
            this.btnCargar.TabIndex = 64;
            this.btnCargar.Text = "CARGAR";
            this.btnCargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCargar.UseVisualStyleBackColor = true;
            this.btnCargar.Click += new System.EventHandler(this.btnCargar_Click);
            // 
            // txtNroTramite
            // 
            this.txtNroTramite.Location = new System.Drawing.Point(141, 10);
            this.txtNroTramite.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNroTramite.Name = "txtNroTramite";
            this.txtNroTramite.ReadOnly = true;
            this.txtNroTramite.Size = new System.Drawing.Size(166, 21);
            this.txtNroTramite.TabIndex = 66;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.TabIndex = 68;
            this.label1.Text = "TRAMITE";
            // 
            // txtTramite
            // 
            this.txtTramite.Location = new System.Drawing.Point(84, 10);
            this.txtTramite.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTramite.Name = "txtTramite";
            this.txtTramite.ReadOnly = true;
            this.txtTramite.Size = new System.Drawing.Size(49, 21);
            this.txtTramite.TabIndex = 65;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpDatosTitular);
            this.tabControl1.Controls.Add(this.tpParametros);
            this.tabControl1.Controls.Add(this.tpAdicionales);
            this.tabControl1.Controls.Add(this.tpHistorico);
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.Location = new System.Drawing.Point(17, 76);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(775, 478);
            this.tabControl1.TabIndex = 73;
            // 
            // tpDatosTitular
            // 
            this.tpDatosTitular.Controls.Add(this.groupBox8);
            this.tpDatosTitular.Controls.Add(this.groupBox2);
            this.tpDatosTitular.Controls.Add(this.groupBox1);
            this.tpDatosTitular.ImageIndex = 2;
            this.tpDatosTitular.Location = new System.Drawing.Point(4, 31);
            this.tpDatosTitular.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tpDatosTitular.Name = "tpDatosTitular";
            this.tpDatosTitular.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tpDatosTitular.Size = new System.Drawing.Size(767, 443);
            this.tpDatosTitular.TabIndex = 0;
            this.tpDatosTitular.Text = "DATOS DEL TITULAR";
            this.tpDatosTitular.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.groupBox8.Controls.Add(this.label68);
            this.groupBox8.Controls.Add(this.txtFRta);
            this.groupBox8.Controls.Add(this.label69);
            this.groupBox8.Controls.Add(this.txtFEnvio);
            this.groupBox8.Controls.Add(this.label72);
            this.groupBox8.Controls.Add(this.txtNroSolTj);
            this.groupBox8.Controls.Add(this.label73);
            this.groupBox8.Controls.Add(this.txtNroSolCta);
            this.groupBox8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox8.ForeColor = System.Drawing.Color.Red;
            this.groupBox8.Location = new System.Drawing.Point(3, 354);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox8.Size = new System.Drawing.Size(758, 87);
            this.groupBox8.TabIndex = 85;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Datos de la Solicitud";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(343, 58);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(116, 15);
            this.label68.TabIndex = 70;
            this.label68.Text = "Fecha de Respuesta";
            // 
            // txtFRta
            // 
            this.txtFRta.Location = new System.Drawing.Point(481, 55);
            this.txtFRta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFRta.Name = "txtFRta";
            this.txtFRta.ReadOnly = true;
            this.txtFRta.Size = new System.Drawing.Size(188, 21);
            this.txtFRta.TabIndex = 69;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(13, 57);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(128, 15);
            this.label69.TabIndex = 68;
            this.label69.Text = "Fecha Envio Administr.";
            // 
            // txtFEnvio
            // 
            this.txtFEnvio.Location = new System.Drawing.Point(154, 54);
            this.txtFEnvio.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFEnvio.Name = "txtFEnvio";
            this.txtFEnvio.ReadOnly = true;
            this.txtFEnvio.Size = new System.Drawing.Size(175, 21);
            this.txtFEnvio.TabIndex = 67;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(343, 26);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(132, 15);
            this.label72.TabIndex = 62;
            this.label72.Text = "Nro.Solicitud de Tarjeta";
            // 
            // txtNroSolTj
            // 
            this.txtNroSolTj.Location = new System.Drawing.Point(481, 23);
            this.txtNroSolTj.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNroSolTj.Name = "txtNroSolTj";
            this.txtNroSolTj.ReadOnly = true;
            this.txtNroSolTj.Size = new System.Drawing.Size(188, 21);
            this.txtNroSolTj.TabIndex = 61;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(13, 26);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(135, 15);
            this.label73.TabIndex = 60;
            this.label73.Text = "Nro.Solicitud de Cuenta";
            // 
            // txtNroSolCta
            // 
            this.txtNroSolCta.Location = new System.Drawing.Point(154, 23);
            this.txtNroSolCta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNroSolCta.Name = "txtNroSolCta";
            this.txtNroSolCta.ReadOnly = true;
            this.txtNroSolCta.Size = new System.Drawing.Size(175, 21);
            this.txtNroSolCta.TabIndex = 59;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.txtDescGeo);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.txtCodGeo);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.txtProv);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.txtLocal);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.txtCodPos);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtDpto);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtPiso);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtPuerta);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtCalle);
            this.groupBox2.Location = new System.Drawing.Point(3, 236);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(758, 116);
            this.groupBox2.TabIndex = 84;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Domicilio";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(176, 86);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 15);
            this.label24.TabIndex = 76;
            this.label24.Text = "DESCRIPCION";
            // 
            // txtDescGeo
            // 
            this.txtDescGeo.Location = new System.Drawing.Point(275, 83);
            this.txtDescGeo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDescGeo.Name = "txtDescGeo";
            this.txtDescGeo.ReadOnly = true;
            this.txtDescGeo.Size = new System.Drawing.Size(188, 21);
            this.txtDescGeo.TabIndex = 75;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(13, 87);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 15);
            this.label23.TabIndex = 74;
            this.label23.Text = "COD.GEOGR.";
            // 
            // txtCodGeo
            // 
            this.txtCodGeo.Location = new System.Drawing.Point(113, 83);
            this.txtCodGeo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodGeo.Name = "txtCodGeo";
            this.txtCodGeo.ReadOnly = true;
            this.txtCodGeo.Size = new System.Drawing.Size(58, 21);
            this.txtCodGeo.TabIndex = 73;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(471, 57);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 15);
            this.label22.TabIndex = 72;
            this.label22.Text = "PROVINCIA";
            // 
            // txtProv
            // 
            this.txtProv.Location = new System.Drawing.Point(553, 54);
            this.txtProv.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtProv.Name = "txtProv";
            this.txtProv.ReadOnly = true;
            this.txtProv.Size = new System.Drawing.Size(172, 21);
            this.txtProv.TabIndex = 71;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(189, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 15);
            this.label21.TabIndex = 70;
            this.label21.Text = "LOCALIDAD";
            // 
            // txtLocal
            // 
            this.txtLocal.Location = new System.Drawing.Point(275, 54);
            this.txtLocal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLocal.Name = "txtLocal";
            this.txtLocal.ReadOnly = true;
            this.txtLocal.Size = new System.Drawing.Size(188, 21);
            this.txtLocal.TabIndex = 69;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(13, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(75, 15);
            this.label20.TabIndex = 68;
            this.label20.Text = "COD.POSTAL";
            // 
            // txtCodPos
            // 
            this.txtCodPos.Location = new System.Drawing.Point(113, 54);
            this.txtCodPos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodPos.Name = "txtCodPos";
            this.txtCodPos.ReadOnly = true;
            this.txtCodPos.Size = new System.Drawing.Size(58, 21);
            this.txtCodPos.TabIndex = 67;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(618, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 15);
            this.label19.TabIndex = 66;
            this.label19.Text = "DPTO";
            // 
            // txtDpto
            // 
            this.txtDpto.Location = new System.Drawing.Point(666, 23);
            this.txtDpto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDpto.Name = "txtDpto";
            this.txtDpto.ReadOnly = true;
            this.txtDpto.Size = new System.Drawing.Size(58, 21);
            this.txtDpto.TabIndex = 65;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(505, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 15);
            this.label18.TabIndex = 64;
            this.label18.Text = "PISO";
            // 
            // txtPiso
            // 
            this.txtPiso.Location = new System.Drawing.Point(553, 23);
            this.txtPiso.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPiso.Name = "txtPiso";
            this.txtPiso.ReadOnly = true;
            this.txtPiso.Size = new System.Drawing.Size(58, 21);
            this.txtPiso.TabIndex = 63;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(343, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 15);
            this.label17.TabIndex = 62;
            this.label17.Text = "NRO.";
            // 
            // txtPuerta
            // 
            this.txtPuerta.Location = new System.Drawing.Point(391, 23);
            this.txtPuerta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPuerta.Name = "txtPuerta";
            this.txtPuerta.ReadOnly = true;
            this.txtPuerta.Size = new System.Drawing.Size(101, 21);
            this.txtPuerta.TabIndex = 61;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 15);
            this.label9.TabIndex = 60;
            this.label9.Text = "CALLE";
            // 
            // txtCalle
            // 
            this.txtCalle.Location = new System.Drawing.Point(75, 23);
            this.txtCalle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.ReadOnly = true;
            this.txtCalle.Size = new System.Drawing.Size(254, 21);
            this.txtCalle.TabIndex = 59;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNroCamp);
            this.groupBox1.Controls.Add(this.label64);
            this.groupBox1.Controls.Add(this.txtPreCa);
            this.groupBox1.Controls.Add(this.label63);
            this.groupBox1.Controls.Add(this.txtSucVta);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtFNac);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtConv);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtEstCivil);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtSeg);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtCodPaq);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtCte_Paq);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtNacion);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtTipDoc);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtPais);
            this.groupBox1.Controls.Add(this.DNI);
            this.groupBox1.Controls.Add(this.txtDni);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtCliente);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.txtTelefono);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(3, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(758, 230);
            this.groupBox1.TabIndex = 83;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filiatorios";
            // 
            // txtNroCamp
            // 
            this.txtNroCamp.Location = new System.Drawing.Point(540, 198);
            this.txtNroCamp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNroCamp.Name = "txtNroCamp";
            this.txtNroCamp.ReadOnly = true;
            this.txtNroCamp.Size = new System.Drawing.Size(187, 21);
            this.txtNroCamp.TabIndex = 86;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(441, 201);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(93, 15);
            this.label64.TabIndex = 85;
            this.label64.Text = "NRO CAMPAÑA";
            // 
            // txtPreCa
            // 
            this.txtPreCa.Location = new System.Drawing.Point(126, 198);
            this.txtPreCa.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPreCa.Name = "txtPreCa";
            this.txtPreCa.ReadOnly = true;
            this.txtPreCa.Size = new System.Drawing.Size(288, 21);
            this.txtPreCa.TabIndex = 84;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(16, 200);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(102, 15);
            this.label63.TabIndex = 83;
            this.label63.Text = "PRECALIFICACION";
            // 
            // txtSucVta
            // 
            this.txtSucVta.Location = new System.Drawing.Point(540, 166);
            this.txtSucVta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSucVta.Name = "txtSucVta";
            this.txtSucVta.ReadOnly = true;
            this.txtSucVta.Size = new System.Drawing.Size(137, 21);
            this.txtSucVta.TabIndex = 82;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(464, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 15);
            this.label8.TabIndex = 81;
            this.label8.Text = "SUC. VENTA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 15);
            this.label6.TabIndex = 80;
            this.label6.Text = "FEC.NACIMIENTO";
            // 
            // txtFNac
            // 
            this.txtFNac.Location = new System.Drawing.Point(126, 136);
            this.txtFNac.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFNac.Name = "txtFNac";
            this.txtFNac.ReadOnly = true;
            this.txtFNac.Size = new System.Drawing.Size(288, 21);
            this.txtFNac.TabIndex = 79;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(430, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 15);
            this.label5.TabIndex = 78;
            this.label5.Text = "CONVENIO / ENTE";
            // 
            // txtConv
            // 
            this.txtConv.Location = new System.Drawing.Point(540, 104);
            this.txtConv.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtConv.Name = "txtConv";
            this.txtConv.ReadOnly = true;
            this.txtConv.Size = new System.Drawing.Size(187, 21);
            this.txtConv.TabIndex = 77;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(482, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 76;
            this.label4.Text = "EST.CIVIL";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtEstCivil
            // 
            this.txtEstCivil.Location = new System.Drawing.Point(540, 73);
            this.txtEstCivil.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtEstCivil.Name = "txtEstCivil";
            this.txtEstCivil.ReadOnly = true;
            this.txtEstCivil.Size = new System.Drawing.Size(137, 21);
            this.txtEstCivil.TabIndex = 75;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(42, 107);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 15);
            this.label16.TabIndex = 74;
            this.label16.Text = "SEGMENTO";
            // 
            // txtSeg
            // 
            this.txtSeg.Location = new System.Drawing.Point(126, 105);
            this.txtSeg.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSeg.Name = "txtSeg";
            this.txtSeg.ReadOnly = true;
            this.txtSeg.Size = new System.Drawing.Size(288, 21);
            this.txtSeg.TabIndex = 73;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(156, 77);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 15);
            this.label15.TabIndex = 72;
            this.label15.Text = "COD.PAQ.";
            // 
            // txtCodPaq
            // 
            this.txtCodPaq.Location = new System.Drawing.Point(224, 74);
            this.txtCodPaq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodPaq.Name = "txtCodPaq";
            this.txtCodPaq.ReadOnly = true;
            this.txtCodPaq.Size = new System.Drawing.Size(190, 21);
            this.txtCodPaq.TabIndex = 71;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(19, 77);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 15);
            this.label10.TabIndex = 70;
            this.label10.Text = "CTE C/PAQUETE?";
            // 
            // txtCte_Paq
            // 
            this.txtCte_Paq.Location = new System.Drawing.Point(126, 74);
            this.txtCte_Paq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCte_Paq.Name = "txtCte_Paq";
            this.txtCte_Paq.ReadOnly = true;
            this.txtCte_Paq.Size = new System.Drawing.Size(25, 21);
            this.txtCte_Paq.TabIndex = 69;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(443, 138);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 15);
            this.label14.TabIndex = 68;
            this.label14.Text = "NACIONALIDAD";
            // 
            // txtNacion
            // 
            this.txtNacion.Location = new System.Drawing.Point(540, 135);
            this.txtNacion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNacion.Name = "txtNacion";
            this.txtNacion.ReadOnly = true;
            this.txtNacion.Size = new System.Drawing.Size(187, 21);
            this.txtNacion.TabIndex = 67;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(171, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 15);
            this.label13.TabIndex = 66;
            this.label13.Text = "TIPO";
            // 
            // txtTipDoc
            // 
            this.txtTipDoc.Location = new System.Drawing.Point(213, 45);
            this.txtTipDoc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTipDoc.Name = "txtTipDoc";
            this.txtTipDoc.ReadOnly = true;
            this.txtTipDoc.Size = new System.Drawing.Size(201, 21);
            this.txtTipDoc.TabIndex = 65;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(79, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 15);
            this.label2.TabIndex = 64;
            this.label2.Text = "PAÍS";
            // 
            // txtPais
            // 
            this.txtPais.Location = new System.Drawing.Point(126, 45);
            this.txtPais.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPais.Name = "txtPais";
            this.txtPais.ReadOnly = true;
            this.txtPais.Size = new System.Drawing.Size(38, 21);
            this.txtPais.TabIndex = 63;
            // 
            // DNI
            // 
            this.DNI.AutoSize = true;
            this.DNI.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DNI.Location = new System.Drawing.Point(456, 46);
            this.DNI.Name = "DNI";
            this.DNI.Size = new System.Drawing.Size(78, 15);
            this.DNI.TabIndex = 62;
            this.DNI.Text = "DOCUMENTO";
            // 
            // txtDni
            // 
            this.txtDni.Location = new System.Drawing.Point(540, 44);
            this.txtDni.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDni.Name = "txtDni";
            this.txtDni.ReadOnly = true;
            this.txtDni.Size = new System.Drawing.Size(137, 21);
            this.txtDni.TabIndex = 61;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(486, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 15);
            this.label3.TabIndex = 57;
            this.label3.Text = "CLIENTE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(54, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 15);
            this.label11.TabIndex = 58;
            this.label11.Text = "NOMBRE";
            // 
            // txtCliente
            // 
            this.txtCliente.Location = new System.Drawing.Point(540, 16);
            this.txtCliente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.ReadOnly = true;
            this.txtCliente.Size = new System.Drawing.Size(137, 21);
            this.txtCliente.TabIndex = 56;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(126, 17);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(288, 21);
            this.txtNombre.TabIndex = 55;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(126, 167);
            this.txtTelefono.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.ReadOnly = true;
            this.txtTelefono.Size = new System.Drawing.Size(288, 21);
            this.txtTelefono.TabIndex = 60;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(47, 169);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 15);
            this.label7.TabIndex = 59;
            this.label7.Text = "TELEFONO";
            // 
            // tpParametros
            // 
            this.tpParametros.Controls.Add(this.groupBox4);
            this.tpParametros.Controls.Add(this.groupBox3);
            this.tpParametros.ImageIndex = 0;
            this.tpParametros.Location = new System.Drawing.Point(4, 31);
            this.tpParametros.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tpParametros.Name = "tpParametros";
            this.tpParametros.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tpParametros.Size = new System.Drawing.Size(767, 443);
            this.tpParametros.TabIndex = 1;
            this.tpParametros.Text = "PARÁMETROS";
            this.tpParametros.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label62);
            this.groupBox4.Controls.Add(this.txtCodDist);
            this.groupBox4.Controls.Add(this.label61);
            this.groupBox4.Controls.Add(this.txtOcupacion);
            this.groupBox4.Controls.Add(this.label60);
            this.groupBox4.Controls.Add(this.txtC_Ser_M);
            this.groupBox4.Controls.Add(this.label59);
            this.groupBox4.Controls.Add(this.txtC_Ser_Bo);
            this.groupBox4.Controls.Add(this.label58);
            this.groupBox4.Controls.Add(this.txtGtia);
            this.groupBox4.Controls.Add(this.label56);
            this.groupBox4.Controls.Add(this.txtMod_Au);
            this.groupBox4.Controls.Add(this.label57);
            this.groupBox4.Controls.Add(this.txtC_Ser_Cu);
            this.groupBox4.Controls.Add(this.label53);
            this.groupBox4.Controls.Add(this.txtBonifCred);
            this.groupBox4.Controls.Add(this.label54);
            this.groupBox4.Controls.Add(this.txtCoefAde);
            this.groupBox4.Controls.Add(this.label55);
            this.groupBox4.Controls.Add(this.txtPromo);
            this.groupBox4.Controls.Add(this.label51);
            this.groupBox4.Controls.Add(this.txtBonifEmi);
            this.groupBox4.Controls.Add(this.label52);
            this.groupBox4.Controls.Add(this.txtCantBonif);
            this.groupBox4.Controls.Add(this.label50);
            this.groupBox4.Controls.Add(this.txtProd);
            this.groupBox4.Controls.Add(this.label49);
            this.groupBox4.Controls.Add(this.txtVigencia);
            this.groupBox4.Controls.Add(this.label48);
            this.groupBox4.Controls.Add(this.txtTipVig);
            this.groupBox4.Controls.Add(this.label47);
            this.groupBox4.Controls.Add(this.txtCodAgrup);
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Controls.Add(this.txtHabATM);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.txtCargoBco);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.txtCatBco);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.txtIva);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.txtCtaDebit);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.txtLimCred);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.txtCodLim);
            this.groupBox4.Controls.Add(this.label37);
            this.groupBox4.Controls.Add(this.txtLimCom);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.txtTipoCta);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.txtTipoTJ);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.txtFPago);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.txtCCLim);
            this.groupBox4.Controls.Add(this.label38);
            this.groupBox4.Controls.Add(this.txtLimOto);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.txtEmbozado);
            this.groupBox4.Location = new System.Drawing.Point(3, 121);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(761, 321);
            this.groupBox4.TabIndex = 99;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Particularidad de los Productos";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(17, 297);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(107, 15);
            this.label62.TabIndex = 146;
            this.label62.Text = "COD.DISTRIBUCION";
            // 
            // txtCodDist
            // 
            this.txtCodDist.Location = new System.Drawing.Point(133, 294);
            this.txtCodDist.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodDist.Name = "txtCodDist";
            this.txtCodDist.ReadOnly = true;
            this.txtCodDist.Size = new System.Drawing.Size(89, 21);
            this.txtCodDist.TabIndex = 145;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(228, 269);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(105, 15);
            this.label61.TabIndex = 144;
            this.label61.Text = "COD. OCUPACION";
            // 
            // txtOcupacion
            // 
            this.txtOcupacion.Location = new System.Drawing.Point(334, 266);
            this.txtOcupacion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtOcupacion.Name = "txtOcupacion";
            this.txtOcupacion.ReadOnly = true;
            this.txtOcupacion.Size = new System.Drawing.Size(230, 21);
            this.txtOcupacion.TabIndex = 143;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(512, 212);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(50, 15);
            this.label60.TabIndex = 142;
            this.label60.Text = "C.SER.M";
            // 
            // txtC_Ser_M
            // 
            this.txtC_Ser_M.Location = new System.Drawing.Point(562, 210);
            this.txtC_Ser_M.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtC_Ser_M.Name = "txtC_Ser_M";
            this.txtC_Ser_M.ReadOnly = true;
            this.txtC_Ser_M.Size = new System.Drawing.Size(43, 21);
            this.txtC_Ser_M.TabIndex = 141;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(392, 213);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(56, 15);
            this.label59.TabIndex = 140;
            this.label59.Text = "C.SER.BO";
            // 
            // txtC_Ser_Bo
            // 
            this.txtC_Ser_Bo.Location = new System.Drawing.Point(448, 210);
            this.txtC_Ser_Bo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtC_Ser_Bo.Name = "txtC_Ser_Bo";
            this.txtC_Ser_Bo.ReadOnly = true;
            this.txtC_Ser_Bo.Size = new System.Drawing.Size(58, 21);
            this.txtC_Ser_Bo.TabIndex = 139;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(443, 241);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(63, 15);
            this.label58.TabIndex = 138;
            this.label58.Text = "GARANTÍA";
            // 
            // txtGtia
            // 
            this.txtGtia.Location = new System.Drawing.Point(562, 238);
            this.txtGtia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGtia.Name = "txtGtia";
            this.txtGtia.ReadOnly = true;
            this.txtGtia.Size = new System.Drawing.Size(89, 21);
            this.txtGtia.TabIndex = 137;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(228, 241);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(52, 15);
            this.label56.TabIndex = 136;
            this.label56.Text = "MOD.AU";
            // 
            // txtMod_Au
            // 
            this.txtMod_Au.Location = new System.Drawing.Point(334, 238);
            this.txtMod_Au.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMod_Au.Name = "txtMod_Au";
            this.txtMod_Au.ReadOnly = true;
            this.txtMod_Au.Size = new System.Drawing.Size(89, 21);
            this.txtMod_Au.TabIndex = 135;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(228, 213);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(55, 15);
            this.label57.TabIndex = 134;
            this.label57.Text = "C.SER.CU";
            // 
            // txtC_Ser_Cu
            // 
            this.txtC_Ser_Cu.Location = new System.Drawing.Point(334, 210);
            this.txtC_Ser_Cu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtC_Ser_Cu.Name = "txtC_Ser_Cu";
            this.txtC_Ser_Cu.ReadOnly = true;
            this.txtC_Ser_Cu.Size = new System.Drawing.Size(52, 21);
            this.txtC_Ser_Cu.TabIndex = 133;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(49, 269);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(75, 15);
            this.label53.TabIndex = 132;
            this.label53.Text = "BONIF.CRED.";
            // 
            // txtBonifCred
            // 
            this.txtBonifCred.Location = new System.Drawing.Point(133, 266);
            this.txtBonifCred.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBonifCred.Name = "txtBonifCred";
            this.txtBonifCred.ReadOnly = true;
            this.txtBonifCred.Size = new System.Drawing.Size(89, 21);
            this.txtBonifCred.TabIndex = 131;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(30, 241);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(97, 15);
            this.label54.TabIndex = 130;
            this.label54.Text = "COEF.ADELANTO";
            // 
            // txtCoefAde
            // 
            this.txtCoefAde.Location = new System.Drawing.Point(133, 238);
            this.txtCoefAde.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCoefAde.Name = "txtCoefAde";
            this.txtCoefAde.ReadOnly = true;
            this.txtCoefAde.Size = new System.Drawing.Size(89, 21);
            this.txtCoefAde.TabIndex = 129;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(54, 213);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(70, 15);
            this.label55.TabIndex = 128;
            this.label55.Text = "PROMOTOR";
            // 
            // txtPromo
            // 
            this.txtPromo.Location = new System.Drawing.Point(133, 210);
            this.txtPromo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPromo.Name = "txtPromo";
            this.txtPromo.ReadOnly = true;
            this.txtPromo.Size = new System.Drawing.Size(89, 21);
            this.txtPromo.TabIndex = 127;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(611, 187);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(87, 15);
            this.label51.TabIndex = 126;
            this.label51.Text = "BONIF.EMISION";
            // 
            // txtBonifEmi
            // 
            this.txtBonifEmi.Location = new System.Drawing.Point(707, 184);
            this.txtBonifEmi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBonifEmi.Name = "txtBonifEmi";
            this.txtBonifEmi.ReadOnly = true;
            this.txtBonifEmi.Size = new System.Drawing.Size(42, 21);
            this.txtBonifEmi.TabIndex = 125;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(434, 187);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(122, 15);
            this.label52.TabIndex = 124;
            this.label52.Text = "CANT.CUOTAS BONIF.";
            // 
            // txtCantBonif
            // 
            this.txtCantBonif.Location = new System.Drawing.Point(562, 184);
            this.txtCantBonif.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCantBonif.Name = "txtCantBonif";
            this.txtCantBonif.ReadOnly = true;
            this.txtCantBonif.Size = new System.Drawing.Size(43, 21);
            this.txtCantBonif.TabIndex = 123;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(228, 184);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(67, 15);
            this.label50.TabIndex = 122;
            this.label50.Text = "PRODUCTO";
            // 
            // txtProd
            // 
            this.txtProd.Location = new System.Drawing.Point(334, 181);
            this.txtProd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtProd.Name = "txtProd";
            this.txtProd.ReadOnly = true;
            this.txtProd.Size = new System.Drawing.Size(89, 21);
            this.txtProd.TabIndex = 121;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(55, 184);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(72, 15);
            this.label49.TabIndex = 120;
            this.label49.Text = "VIGENCIA TJ";
            // 
            // txtVigencia
            // 
            this.txtVigencia.Location = new System.Drawing.Point(133, 181);
            this.txtVigencia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVigencia.Name = "txtVigencia";
            this.txtVigencia.ReadOnly = true;
            this.txtVigencia.Size = new System.Drawing.Size(89, 21);
            this.txtVigencia.TabIndex = 119;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(434, 156);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(85, 15);
            this.label48.TabIndex = 118;
            this.label48.Text = "TIPO VIGENCIA";
            // 
            // txtTipVig
            // 
            this.txtTipVig.Location = new System.Drawing.Point(540, 153);
            this.txtTipVig.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTipVig.Name = "txtTipVig";
            this.txtTipVig.ReadOnly = true;
            this.txtTipVig.Size = new System.Drawing.Size(89, 21);
            this.txtTipVig.TabIndex = 117;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(228, 156);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(76, 15);
            this.label47.TabIndex = 116;
            this.label47.Text = "COD.AGRUP.";
            // 
            // txtCodAgrup
            // 
            this.txtCodAgrup.Location = new System.Drawing.Point(334, 153);
            this.txtCodAgrup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodAgrup.Name = "txtCodAgrup";
            this.txtCodAgrup.ReadOnly = true;
            this.txtCodAgrup.Size = new System.Drawing.Size(89, 21);
            this.txtCodAgrup.TabIndex = 115;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(21, 156);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(106, 15);
            this.label46.TabIndex = 114;
            this.label46.Text = "HABILITACION ATM";
            // 
            // txtHabATM
            // 
            this.txtHabATM.Location = new System.Drawing.Point(133, 153);
            this.txtHabATM.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtHabATM.Name = "txtHabATM";
            this.txtHabATM.ReadOnly = true;
            this.txtHabATM.Size = new System.Drawing.Size(89, 21);
            this.txtHabATM.TabIndex = 113;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(434, 128);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(103, 15);
            this.label44.TabIndex = 112;
            this.label44.Text = "CARGO BANELCO";
            // 
            // txtCargoBco
            // 
            this.txtCargoBco.Location = new System.Drawing.Point(540, 125);
            this.txtCargoBco.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCargoBco.Name = "txtCargoBco";
            this.txtCargoBco.ReadOnly = true;
            this.txtCargoBco.Size = new System.Drawing.Size(89, 21);
            this.txtCargoBco.TabIndex = 111;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(228, 128);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(83, 15);
            this.label45.TabIndex = 110;
            this.label45.Text = "CAT.BANELCO";
            // 
            // txtCatBco
            // 
            this.txtCatBco.Location = new System.Drawing.Point(334, 125);
            this.txtCatBco.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCatBco.Name = "txtCatBco";
            this.txtCatBco.ReadOnly = true;
            this.txtCatBco.Size = new System.Drawing.Size(89, 21);
            this.txtCatBco.TabIndex = 109;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(68, 128);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(59, 15);
            this.label43.TabIndex = 108;
            this.label43.Text = "CONT.IVA";
            // 
            // txtIva
            // 
            this.txtIva.Location = new System.Drawing.Point(133, 125);
            this.txtIva.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIva.Name = "txtIva";
            this.txtIva.ReadOnly = true;
            this.txtIva.Size = new System.Drawing.Size(89, 21);
            this.txtIva.TabIndex = 107;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(228, 100);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(90, 15);
            this.label42.TabIndex = 106;
            this.label42.Text = "CUENTA DEBITO";
            // 
            // txtCtaDebit
            // 
            this.txtCtaDebit.Location = new System.Drawing.Point(334, 97);
            this.txtCtaDebit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCtaDebit.Name = "txtCtaDebit";
            this.txtCtaDebit.ReadOnly = true;
            this.txtCtaDebit.Size = new System.Drawing.Size(203, 21);
            this.txtCtaDebit.TabIndex = 105;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(614, 71);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(47, 15);
            this.label41.TabIndex = 104;
            this.label41.Text = "L.CRED.";
            // 
            // txtLimCred
            // 
            this.txtLimCred.Location = new System.Drawing.Point(661, 68);
            this.txtLimCred.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLimCred.Name = "txtLimCred";
            this.txtLimCred.ReadOnly = true;
            this.txtLimCred.Size = new System.Drawing.Size(88, 21);
            this.txtLimCred.TabIndex = 103;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(364, 71);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(52, 15);
            this.label40.TabIndex = 102;
            this.label40.Text = "COD.LIM";
            // 
            // txtCodLim
            // 
            this.txtCodLim.Location = new System.Drawing.Point(422, 68);
            this.txtCodLim.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodLim.Name = "txtCodLim";
            this.txtCodLim.ReadOnly = true;
            this.txtCodLim.Size = new System.Drawing.Size(40, 21);
            this.txtCodLim.TabIndex = 101;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(228, 71);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(57, 15);
            this.label37.TabIndex = 100;
            this.label37.Text = "LIM.COM.";
            // 
            // txtLimCom
            // 
            this.txtLimCom.Location = new System.Drawing.Point(291, 68);
            this.txtLimCom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLimCom.Name = "txtLimCom";
            this.txtLimCom.ReadOnly = true;
            this.txtLimCom.Size = new System.Drawing.Size(68, 21);
            this.txtLimCom.TabIndex = 99;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(554, 43);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(76, 15);
            this.label25.TabIndex = 98;
            this.label25.Text = "TIPO CUENTA";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // txtTipoCta
            // 
            this.txtTipoCta.Location = new System.Drawing.Point(636, 39);
            this.txtTipoCta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTipoCta.Name = "txtTipoCta";
            this.txtTipoCta.ReadOnly = true;
            this.txtTipoCta.Size = new System.Drawing.Size(113, 21);
            this.txtTipoCta.TabIndex = 97;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(437, 43);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 15);
            this.label26.TabIndex = 96;
            this.label26.Text = "TIPO TJ";
            this.label26.Click += new System.EventHandler(this.label26_Click);
            // 
            // txtTipoTJ
            // 
            this.txtTipoTJ.Location = new System.Drawing.Point(486, 40);
            this.txtTipoTJ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTipoTJ.Name = "txtTipoTJ";
            this.txtTipoTJ.ReadOnly = true;
            this.txtTipoTJ.Size = new System.Drawing.Size(62, 21);
            this.txtTipoTJ.TabIndex = 95;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(27, 100);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(100, 15);
            this.label29.TabIndex = 90;
            this.label29.Text = "FORMA DE PAGO";
            this.label29.Click += new System.EventHandler(this.label29_Click);
            // 
            // txtFPago
            // 
            this.txtFPago.Location = new System.Drawing.Point(133, 97);
            this.txtFPago.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFPago.Name = "txtFPago";
            this.txtFPago.ReadOnly = true;
            this.txtFPago.Size = new System.Drawing.Size(89, 21);
            this.txtFPago.TabIndex = 89;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(469, 71);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(54, 15);
            this.label30.TabIndex = 88;
            this.label30.Text = "CC.LIM.C";
            // 
            // txtCCLim
            // 
            this.txtCCLim.Location = new System.Drawing.Point(531, 68);
            this.txtCCLim.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCCLim.Name = "txtCCLim";
            this.txtCCLim.ReadOnly = true;
            this.txtCCLim.Size = new System.Drawing.Size(78, 21);
            this.txtCCLim.TabIndex = 87;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(36, 71);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(91, 15);
            this.label38.TabIndex = 80;
            this.label38.Text = "LIM.OTORGADO";
            // 
            // txtLimOto
            // 
            this.txtLimOto.Location = new System.Drawing.Point(133, 68);
            this.txtLimOto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLimOto.Name = "txtLimOto";
            this.txtLimOto.ReadOnly = true;
            this.txtLimOto.Size = new System.Drawing.Size(89, 21);
            this.txtLimOto.TabIndex = 79;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(6, 43);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(123, 15);
            this.label39.TabIndex = 78;
            this.label39.Text = "EMBOZADO PLÁSTICO";
            // 
            // txtEmbozado
            // 
            this.txtEmbozado.Location = new System.Drawing.Point(133, 39);
            this.txtEmbozado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtEmbozado.Name = "txtEmbozado";
            this.txtEmbozado.ReadOnly = true;
            this.txtEmbozado.Size = new System.Drawing.Size(294, 21);
            this.txtEmbozado.TabIndex = 77;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label67);
            this.groupBox3.Controls.Add(this.txtGaffTit);
            this.groupBox3.Controls.Add(this.label65);
            this.groupBox3.Controls.Add(this.txtTarjeta);
            this.groupBox3.Controls.Add(this.label66);
            this.groupBox3.Controls.Add(this.txtCuentaTJ);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.txtS_Mod_ta);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.txt4taLinea);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.txtCartera);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.txtSucAdm);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.txtGaff);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.txtModLiq);
            this.groupBox3.Controls.Add(this.txtS_Mod_ca);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(761, 117);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Parametrías de la Cuenta";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(217, 60);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(78, 15);
            this.label67.TabIndex = 104;
            this.label67.Text = "GAFF TITULAR";
            // 
            // txtGaffTit
            // 
            this.txtGaffTit.Location = new System.Drawing.Point(306, 57);
            this.txtGaffTit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGaffTit.Name = "txtGaffTit";
            this.txtGaffTit.ReadOnly = true;
            this.txtGaffTit.Size = new System.Drawing.Size(68, 21);
            this.txtGaffTit.TabIndex = 103;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(529, 59);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(51, 15);
            this.label65.TabIndex = 102;
            this.label65.Text = "TARJETA";
            // 
            // txtTarjeta
            // 
            this.txtTarjeta.Location = new System.Drawing.Point(586, 56);
            this.txtTarjeta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTarjeta.Name = "txtTarjeta";
            this.txtTarjeta.ReadOnly = true;
            this.txtTarjeta.Size = new System.Drawing.Size(156, 21);
            this.txtTarjeta.TabIndex = 101;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(460, 30);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(63, 15);
            this.label66.TabIndex = 100;
            this.label66.Text = "CUENTA TJ";
            // 
            // txtCuentaTJ
            // 
            this.txtCuentaTJ.Location = new System.Drawing.Point(529, 28);
            this.txtCuentaTJ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCuentaTJ.Name = "txtCuentaTJ";
            this.txtCuentaTJ.ReadOnly = true;
            this.txtCuentaTJ.Size = new System.Drawing.Size(100, 21);
            this.txtCuentaTJ.TabIndex = 99;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(352, 31);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(62, 15);
            this.label36.TabIndex = 98;
            this.label36.Text = "S.MOD.CA";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(249, 32);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(59, 15);
            this.label35.TabIndex = 96;
            this.label35.Text = "S.MOD.TA";
            // 
            // txtS_Mod_ta
            // 
            this.txtS_Mod_ta.Location = new System.Drawing.Point(314, 28);
            this.txtS_Mod_ta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtS_Mod_ta.Name = "txtS_Mod_ta";
            this.txtS_Mod_ta.ReadOnly = true;
            this.txtS_Mod_ta.Size = new System.Drawing.Size(29, 21);
            this.txtS_Mod_ta.TabIndex = 95;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(44, 89);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(83, 15);
            this.label27.TabIndex = 90;
            this.label27.Text = "CUARTA LINEA";
            // 
            // txt4taLinea
            // 
            this.txt4taLinea.Location = new System.Drawing.Point(133, 86);
            this.txt4taLinea.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt4taLinea.Name = "txt4taLinea";
            this.txt4taLinea.ReadOnly = true;
            this.txt4taLinea.Size = new System.Drawing.Size(431, 21);
            this.txt4taLinea.TabIndex = 89;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(633, 32);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(56, 15);
            this.label28.TabIndex = 88;
            this.label28.Text = "CARTERA";
            // 
            // txtCartera
            // 
            this.txtCartera.Location = new System.Drawing.Point(695, 29);
            this.txtCartera.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCartera.Name = "txtCartera";
            this.txtCartera.ReadOnly = true;
            this.txtCartera.Size = new System.Drawing.Size(47, 21);
            this.txtCartera.TabIndex = 87;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(380, 59);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(93, 15);
            this.label31.TabIndex = 82;
            this.label31.Text = "SUCURSAL ADM.";
            // 
            // txtSucAdm
            // 
            this.txtSucAdm.Location = new System.Drawing.Point(479, 56);
            this.txtSucAdm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSucAdm.Name = "txtSucAdm";
            this.txtSucAdm.ReadOnly = true;
            this.txtSucAdm.Size = new System.Drawing.Size(44, 21);
            this.txtSucAdm.TabIndex = 81;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(86, 62);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(36, 15);
            this.label33.TabIndex = 80;
            this.label33.Text = "GAFF";
            // 
            // txtGaff
            // 
            this.txtGaff.Location = new System.Drawing.Point(133, 57);
            this.txtGaff.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGaff.Name = "txtGaff";
            this.txtGaff.ReadOnly = true;
            this.txtGaff.Size = new System.Drawing.Size(68, 21);
            this.txtGaff.TabIndex = 79;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(21, 32);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(106, 15);
            this.label34.TabIndex = 78;
            this.label34.Text = "MOD.LIQUIDACION";
            // 
            // txtModLiq
            // 
            this.txtModLiq.Location = new System.Drawing.Point(133, 28);
            this.txtModLiq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtModLiq.Name = "txtModLiq";
            this.txtModLiq.ReadOnly = true;
            this.txtModLiq.Size = new System.Drawing.Size(107, 21);
            this.txtModLiq.TabIndex = 77;
            // 
            // txtS_Mod_ca
            // 
            this.txtS_Mod_ca.Location = new System.Drawing.Point(420, 28);
            this.txtS_Mod_ca.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtS_Mod_ca.Name = "txtS_Mod_ca";
            this.txtS_Mod_ca.ReadOnly = true;
            this.txtS_Mod_ca.Size = new System.Drawing.Size(30, 21);
            this.txtS_Mod_ca.TabIndex = 97;
            // 
            // tpAdicionales
            // 
            this.tpAdicionales.Controls.Add(this.groupBox7);
            this.tpAdicionales.Controls.Add(this.groupBox6);
            this.tpAdicionales.Controls.Add(this.groupBox5);
            this.tpAdicionales.ImageIndex = 3;
            this.tpAdicionales.Location = new System.Drawing.Point(4, 31);
            this.tpAdicionales.Name = "tpAdicionales";
            this.tpAdicionales.Padding = new System.Windows.Forms.Padding(3);
            this.tpAdicionales.Size = new System.Drawing.Size(767, 443);
            this.tpAdicionales.TabIndex = 2;
            this.tpAdicionales.Text = "DATOS ADICIONALES";
            this.tpAdicionales.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtAdicInf);
            this.groupBox7.Location = new System.Drawing.Point(3, 303);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(757, 119);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Otros Datos";
            // 
            // txtAdicInf
            // 
            this.txtAdicInf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAdicInf.Location = new System.Drawing.Point(3, 17);
            this.txtAdicInf.Name = "txtAdicInf";
            this.txtAdicInf.Size = new System.Drawing.Size(751, 99);
            this.txtAdicInf.TabIndex = 0;
            this.txtAdicInf.Text = "";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dgvAdicionales);
            this.groupBox6.Location = new System.Drawing.Point(3, 148);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(757, 149);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Datos de Adicionales";
            // 
            // dgvAdicionales
            // 
            this.dgvAdicionales.AllowUserToAddRows = false;
            this.dgvAdicionales.AllowUserToDeleteRows = false;
            this.dgvAdicionales.AllowUserToResizeRows = false;
            this.dgvAdicionales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAdicionales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAdicionales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAdicionales.Location = new System.Drawing.Point(3, 17);
            this.dgvAdicionales.Name = "dgvAdicionales";
            this.dgvAdicionales.RowHeadersVisible = false;
            this.dgvAdicionales.Size = new System.Drawing.Size(751, 129);
            this.dgvAdicionales.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgvErrPrevalid);
            this.groupBox5.Location = new System.Drawing.Point(4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(757, 138);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Errores de Prevalidación";
            // 
            // dgvErrPrevalid
            // 
            this.dgvErrPrevalid.AllowUserToAddRows = false;
            this.dgvErrPrevalid.AllowUserToDeleteRows = false;
            this.dgvErrPrevalid.AllowUserToResizeRows = false;
            this.dgvErrPrevalid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvErrPrevalid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvErrPrevalid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvErrPrevalid.Location = new System.Drawing.Point(3, 17);
            this.dgvErrPrevalid.Name = "dgvErrPrevalid";
            this.dgvErrPrevalid.RowHeadersVisible = false;
            this.dgvErrPrevalid.Size = new System.Drawing.Size(751, 118);
            this.dgvErrPrevalid.TabIndex = 0;
            this.dgvErrPrevalid.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvErrPrevalid_CellContentDoubleClick);
            // 
            // tpHistorico
            // 
            this.tpHistorico.Controls.Add(this.groupBox10);
            this.tpHistorico.Controls.Add(this.groupBox9);
            this.tpHistorico.ImageIndex = 4;
            this.tpHistorico.Location = new System.Drawing.Point(4, 31);
            this.tpHistorico.Name = "tpHistorico";
            this.tpHistorico.Padding = new System.Windows.Forms.Padding(3);
            this.tpHistorico.Size = new System.Drawing.Size(767, 443);
            this.tpHistorico.TabIndex = 3;
            this.tpHistorico.Text = "HISTORICO";
            this.tpHistorico.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.dgvProceso);
            this.groupBox10.Location = new System.Drawing.Point(3, 231);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(763, 206);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "HISTORICO DE PROCESO ABM";
            // 
            // dgvProceso
            // 
            this.dgvProceso.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvProceso.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProceso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProceso.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvProceso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProceso.GridColor = System.Drawing.SystemColors.Control;
            this.dgvProceso.Location = new System.Drawing.Point(3, 17);
            this.dgvProceso.Name = "dgvProceso";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Peru;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProceso.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvProceso.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvProceso.Size = new System.Drawing.Size(757, 186);
            this.dgvProceso.TabIndex = 0;
            this.dgvProceso.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProceso_CellContentDoubleClick);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.dgvComentarios);
            this.groupBox9.Location = new System.Drawing.Point(2, 7);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(763, 218);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Procesos DEL TRAMITE";
            // 
            // dgvComentarios
            // 
            this.dgvComentarios.AllowUserToAddRows = false;
            this.dgvComentarios.AllowUserToDeleteRows = false;
            this.dgvComentarios.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvComentarios.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvComentarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvComentarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.PaleTurquoise;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.MediumAquamarine;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvComentarios.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvComentarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvComentarios.GridColor = System.Drawing.SystemColors.Control;
            this.dgvComentarios.Location = new System.Drawing.Point(3, 17);
            this.dgvComentarios.Name = "dgvComentarios";
            this.dgvComentarios.ReadOnly = true;
            this.dgvComentarios.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Wheat;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Olive;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvComentarios.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvComentarios.Size = new System.Drawing.Size(757, 198);
            this.dgvComentarios.TabIndex = 0;
            this.dgvComentarios.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvComentarios_CellContentDoubleClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Entypo_2692(0)_48.png");
            this.imageList1.Images.SetKeyName(1, "Entypo_d83d(0)_48.png");
            this.imageList1.Images.SetKeyName(2, "Entypo_e700(0)_48.png");
            this.imageList1.Images.SetKeyName(3, "FontAwesome_f08d(0)_48.png");
            this.imageList1.Images.SetKeyName(4, "linecons_e018(0)_48.png");
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.txtAdicComent);
            this.groupBox11.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBox11.Location = new System.Drawing.Point(19, 557);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(772, 135);
            this.groupBox11.TabIndex = 74;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Procesos ADICIONALES";
            // 
            // txtAdicComent
            // 
            this.txtAdicComent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAdicComent.Location = new System.Drawing.Point(3, 18);
            this.txtAdicComent.Name = "txtAdicComent";
            this.txtAdicComent.Size = new System.Drawing.Size(766, 114);
            this.txtAdicComent.TabIndex = 0;
            this.txtAdicComent.Text = "";
            // 
            // chbLiberacion
            // 
            this.chbLiberacion.AutoSize = true;
            this.chbLiberacion.ForeColor = System.Drawing.Color.Red;
            this.chbLiberacion.Location = new System.Drawing.Point(408, 45);
            this.chbLiberacion.Name = "chbLiberacion";
            this.chbLiberacion.Size = new System.Drawing.Size(121, 20);
            this.chbLiberacion.TabIndex = 75;
            this.chbLiberacion.Text = "CON LIBERACION";
            this.chbLiberacion.UseVisualStyleBackColor = true;
            // 
            // ModuloPrevalid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(811, 704);
            this.Controls.Add(this.chbLiberacion);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtPrioridad);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.btnCargar);
            this.Controls.Add(this.txtNroTramite);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTramite);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "ModuloPrevalid";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administración de Tarjetas: Errores de Prevalidacion";
            this.tabControl1.ResumeLayout(false);
            this.tpDatosTitular.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpParametros.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tpAdicionales.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdicionales)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvErrPrevalid)).EndInit();
            this.tpHistorico.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProceso)).EndInit();
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvComentarios)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPrioridad;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Button btnCargar;
        private System.Windows.Forms.TextBox txtNroTramite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTramite;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpDatosTitular;
        private System.Windows.Forms.TabPage tpParametros;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtSeg;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCodPaq;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCte_Paq;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNacion;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTipDoc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPais;
        private System.Windows.Forms.Label DNI;
        private System.Windows.Forms.TextBox txtDni;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtDescGeo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtCodGeo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtProv;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtLocal;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtCodPos;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDpto;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPiso;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPuerta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSucVta;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFNac;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtConv;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEstCivil;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txtMod_Au;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtC_Ser_Cu;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtBonifCred;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtCoefAde;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox txtPromo;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txtBonifEmi;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtCantBonif;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtProd;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txtVigencia;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtTipVig;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtCodAgrup;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtHabATM;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtCargoBco;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtCatBco;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtIva;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtCtaDebit;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtLimCred;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtCodLim;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtLimCom;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtTipoCta;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtTipoTJ;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtFPago;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtCCLim;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtLimOto;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtEmbozado;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtS_Mod_ca;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtS_Mod_ta;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txt4taLinea;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtCartera;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtSucAdm;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtGaff;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtModLiq;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtCodDist;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txtOcupacion;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox txtC_Ser_M;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtC_Ser_Bo;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txtGtia;
        private System.Windows.Forms.TabPage tpAdicionales;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RichTextBox txtAdicInf;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dgvAdicionales;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dgvErrPrevalid;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtFRta;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox txtFEnvio;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox txtNroSolTj;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox txtNroSolCta;
        private System.Windows.Forms.TextBox txtNroCamp;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox txtPreCa;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtTarjeta;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txtCuentaTJ;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txtGaffTit;
        private System.Windows.Forms.TabPage tpHistorico;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DataGridView dgvProceso;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView dgvComentarios;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RichTextBox txtAdicComent;
        private System.Windows.Forms.CheckBox chbLiberacion;
    }
}