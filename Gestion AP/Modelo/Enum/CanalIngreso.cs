﻿namespace Gestion_AP
{
    public enum CanalIngreso
    {
        MAIL = 1,
        WORKFLOW = 2,
        PREVALIDACION = 3,
        SEGURIDAD = 4,
        ADJUNTO = 5,
        IMAGEN = 6,
        BATCH = 7
        
    }
}