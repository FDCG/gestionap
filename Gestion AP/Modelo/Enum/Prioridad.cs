﻿namespace Gestion_AP
{
    public enum Prioridad
    {
        NORMAL = 1,
        LIBERAR = 2,
        URGENTE = 3
    }
}