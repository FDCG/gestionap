﻿namespace Gestion_AP
{
    public enum AccionProceso
    {
        Ingreso = 1,
        Control = 2,
        Rechazo = 3,
        Reproceso = 4,
        Delegación = 5,
        Reapertura = 6,
        Forzado = 7,
        Cancelación = 8,
        Workflow = 99

    }
}