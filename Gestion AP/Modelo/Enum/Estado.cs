﻿namespace Gestion_AP
{
    public enum Estado
    {
        ABIERTO = 1,
        RECHAZADO = 2,
        CERRADO = 3
    }
}