﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    public enum Administrador
    {
        [Description("VISA")] VISA = 1,
        [Description("MASTERCARD")] MASTERCARD = 2,
        [Description("AMERICAN EXPRESS")] AMERICANEXPRESS = 3,
        [Description("BANELCO")] BANELCO = 4

    }
}
