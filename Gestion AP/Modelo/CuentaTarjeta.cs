﻿namespace Gestion_AP
{
    public class CuentaTarjeta
    {

        private string nroCuentaTarjeta;
        private string titularCuenta;
        private string tarjeta;
        private string embozadoTarjeta;
        private string tipoTarjeta;
        private string cuentaDebito;
        private string formaDePago;
        private string limiteTarjeta;
        private string limiteAutorizado;

        public CuentaTarjeta()
        {
        }

        public CuentaTarjeta(string nroCuentaTarjeta, string titularCuenta, string tarjeta, string embozadoTarjeta, string tipoTarjeta, string cuentaDebito, string formaDePago, string limiteTarjeta, string limiteAutorizado)
        {
            NroCuentaTarjeta = nroCuentaTarjeta;
            TitularCuenta = titularCuenta;
            Tarjeta = tarjeta;
            EmbozadoTarjeta = embozadoTarjeta;
            TipoTarjeta = tipoTarjeta;
            CuentaDebito = cuentaDebito;
            FormaDePago = formaDePago;
            LimiteTarjeta = limiteTarjeta;
            LimiteAutorizado = limiteAutorizado;
        }

        public string NroCuentaTarjeta { get => nroCuentaTarjeta; set => nroCuentaTarjeta = value; }
        public string TitularCuenta { get => titularCuenta; set => titularCuenta = value; }
        public string Tarjeta { get => tarjeta; set => tarjeta = value; }
        public string EmbozadoTarjeta { get => embozadoTarjeta; set => embozadoTarjeta = value; }
        public string TipoTarjeta { get => tipoTarjeta; set => tipoTarjeta = value; }
        public string CuentaDebito { get => cuentaDebito; set => cuentaDebito = value; }
        public string FormaDePago { get => formaDePago; set => formaDePago = value; }
        public string LimiteTarjeta { get => limiteTarjeta; set => limiteTarjeta = value; }
        public string LimiteAutorizado { get => limiteAutorizado; set => limiteAutorizado = value; }
    }
}