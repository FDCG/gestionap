﻿namespace Gestion_AP
{
    public class Filiatorio
    {

        private string cuil;
        private string nombre;
        private string denominacionCliente;
        private string cliente;

        public Filiatorio() { }

        public Filiatorio(string cuil, string nombre, string denominacionCliente, string cliente)
        {
            Cuil = cuil;
            Nombre = nombre;
            DenominacionCliente = denominacionCliente;
            Cliente = cliente;
        }

        public string Cuil { get => cuil; set => cuil = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string DenominacionCliente { get => denominacionCliente; set => denominacionCliente = value; }
        public string Cliente { get => cliente; set => cliente = value; }
    }
}