﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    public class Tramite
    {

        private int id;
        private string usuario;
        private DateTime fechaInicio;
        private Administrador administrador;
        private TipoTramite tipoTramite;
        private string nroTramite;
        private Prioridad prioridad;
        private CanalIngreso canalIngreso;
        private Estado estado;
        private DateTime fechaControl;
        private List<Proceso> procesos;


        public Tramite() {

            TipoTramite = new TipoTramite();
            Procesos = new List<Proceso>();

        }



        public Tramite(int id, string usuario, DateTime fechaInicio, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos)
        {
            Id = id;
            Usuario = usuario;
            FechaInicio = fechaInicio;
            NroTramite = nroTramite;
            Prioridad = prioridad;
            CanalIngreso = canalIngreso;
            Estado = estado;
            FechaControl = fechaControl;
            Administrador = administrador;
            TipoTramite = tipoTramite;
            Procesos = procesos;
        }

        public Tramite(string usuario, DateTime fechaInicio, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos)
        {
            Usuario = usuario;
            FechaInicio = fechaInicio;
            NroTramite = nroTramite;
            Prioridad = prioridad;
            CanalIngreso = canalIngreso;
            Estado = estado;
            FechaControl = fechaControl;
            Administrador = administrador;
            TipoTramite = tipoTramite;
            Procesos = procesos;
        }



        public int Id { get => id; set => id = value; }
        public string Usuario { get => usuario; set => usuario = value; }
        public DateTime FechaInicio { get => fechaInicio; set => fechaInicio = value; }
        public string NroTramite { get => nroTramite; set => nroTramite = value; }
        public Prioridad Prioridad { get => prioridad; set => prioridad = value; }
        public CanalIngreso CanalIngreso { get => canalIngreso; set => canalIngreso = value; }
        public Estado Estado { get => estado; set => estado = value; }
        public DateTime FechaControl { get => fechaControl; set => fechaControl = value; }
        public Administrador Administrador { get => administrador; set => administrador = value; }
        public TipoTramite TipoTramite { get => tipoTramite; set => tipoTramite = value; }
        public List<Proceso> Procesos { get => procesos; set => procesos = value; }

        internal static Prioridad getPrioridadByString(string prioridad)
        {
            if (prioridad.Equals("urgente",StringComparison.OrdinalIgnoreCase))
            {
                return Prioridad.URGENTE;

            }
            else if (prioridad.Equals("liberacion", StringComparison.OrdinalIgnoreCase))
            {
                return Prioridad.LIBERAR;

            }
            else
            {
                return Prioridad.NORMAL;

            }

        }

        internal static CanalIngreso getCanalIngresoByString(string canalIngreso)
        {
            if (canalIngreso.Equals("mail", StringComparison.OrdinalIgnoreCase))
            {
                return CanalIngreso.MAIL;
            }
            else if (canalIngreso.Equals("workflow", StringComparison.OrdinalIgnoreCase))
            {
                return CanalIngreso.WORKFLOW;
            }
            else if (canalIngreso.Equals("batch", StringComparison.OrdinalIgnoreCase))
            {
                return CanalIngreso.BATCH;
            }
            else
            {
                return CanalIngreso.SEGURIDAD;
            }
        }



        internal static Administrador getAdministradorByString(string administrador)
        {
            if (administrador.Equals("visa"))
            {
                return Administrador.VISA;

            }else if (administrador.Equals("mastercard"))
            {
                return Administrador.MASTERCARD;

            }else if (administrador.Equals("american express"))
            {
                return Administrador.AMERICANEXPRESS;
            }
            else
            {
                return Administrador.BANELCO;

            }
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
