﻿namespace Gestion_AP
{
    public class Tarjeta
    {

        private string nroTarjeta;
        private char categoriaTarjeta;
        private string denominacionTarjeta;
        private string tipoDocumento;
        private string nroDocumento;
        private string vigenciaTarjeta;
        private int estadoTarjeta;

        public Tarjeta()
        {
        }

        public Tarjeta(string nroTarjeta, char categoriaTarjeta, string denominacionTarjeta, string tipoDocumento, string nroDocumento, string vigenciaTarjeta, int estadoTarjeta)
        {
            NroTarjeta = nroTarjeta;
            CategoriaTarjeta = categoriaTarjeta;
            DenominacionTarjeta = denominacionTarjeta;
            TipoDocumento = tipoDocumento;
            NroDocumento = nroDocumento;
            VigenciaTarjeta = vigenciaTarjeta;
            EstadoTarjeta = estadoTarjeta;
        }

        public string NroTarjeta { get => nroTarjeta; set => nroTarjeta = value; }
        public char CategoriaTarjeta { get => categoriaTarjeta; set => categoriaTarjeta = value; }
        public string DenominacionTarjeta { get => denominacionTarjeta; set => denominacionTarjeta = value; }
        public string TipoDocumento { get => tipoDocumento; set => tipoDocumento = value; }
        public string VigenciaTarjeta { get => vigenciaTarjeta; set => vigenciaTarjeta = value; }
        public int EstadoTarjeta { get => estadoTarjeta; set => estadoTarjeta = value; }
        public string NroDocumento { get => nroDocumento; set => nroDocumento = value; }
    }
}