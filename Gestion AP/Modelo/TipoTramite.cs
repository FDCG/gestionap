﻿namespace Gestion_AP
{
    public class TipoTramite
    {

        private int id;
        private string nombreTramite;
        private string codigoTramite;
        private bool postdatado;

        public TipoTramite()
        {
        }

        public TipoTramite(string nombreTramite, string codigoTramite, bool postdatado)
        {
            NombreTramite = nombreTramite;
            CodigoTramite = codigoTramite;
            Postdatado = postdatado;
        }

        public TipoTramite(int id, string nombreTramite, string codigoTramite, bool postdatado)
        {
            Id = id;
            NombreTramite = nombreTramite;
            CodigoTramite = codigoTramite;
            Postdatado = postdatado;
        }

        public int Id { get => id; set => id = value; }
        public string NombreTramite { get => nombreTramite; set => nombreTramite = value; }
        public string CodigoTramite { get => codigoTramite; set => codigoTramite = value; }
        public bool Postdatado { get => postdatado; set => postdatado = value; }
    }
}