﻿using System;

namespace Gestion_AP
{
    public class Proceso
    {

        private DateTime fecha;
        private string usuario;
        private AccionProceso accionProceso;
        private string detalleProceso;

        public Proceso()
        {
        }

        public Proceso(DateTime fecha, string usuario, AccionProceso accionProceso, string detalleProceso)
        {
            Fecha = fecha;
            Usuario = usuario;
            AccionProceso = accionProceso;
            DetalleProceso = detalleProceso;
        }

        public DateTime Fecha { get => fecha; set => fecha = value; }
        public string Usuario { get => usuario; set => usuario = value; }
        public AccionProceso AccionProceso { get => accionProceso; set => accionProceso = value; }
        public string DetalleProceso { get => detalleProceso; set => detalleProceso = value; }
    }
}