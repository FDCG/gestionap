﻿namespace Gestion_AP
{
    public class Distribucion
    {
        private string domicilioEspecial;
        private string sucursalEntrega;

        public Distribucion()
        {
        }

        public Distribucion(string domicilioEspecial, string sucursalEntrega)
        {
            DomicilioEspecial = domicilioEspecial;
            SucursalEntrega = sucursalEntrega;
        }

        public string DomicilioEspecial { get => domicilioEspecial; set => domicilioEspecial = value; }
        public string SucursalEntrega { get => sucursalEntrega; set => sucursalEntrega = value; }
    }
}