﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    public class Usuario
    {

        public static Usuario usuarioSingleton;


        private int id;
        private string usuario;
        private string contrasenia;
        private string nombre;
        private string apellido;
        private string nivel;
        private DateTime fechaAlta;
        private DateTime fechaBaja;
        private DateTime ultimoIngreso;
        private DateTime ultimoEgreso;
        private string ntdom;
        private string equipo;
        private string dominio;
        private DateTime vtoClave;
        private int intentosFallidos;
        private bool bloqueado;


        private const int LONG_USUARIO = 8;
        private const int MAX_LONG = 30;
        private const int MIN_LONG = 3;
        private const string NIVEL_ADMINISTRADOR = "00";
        private const string NIVEL_ANALISTA = "99";
        public const int MAX_INTENTOS = 3;


        public Usuario()
        {
        }


        
        public Usuario(string usuario, string contrasenia, string nombre,
            string apellido, string nivel, DateTime fechaAlta)
        {

            this.iUsuario = usuario;
            this.Contrasenia = contrasenia;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Nivel = nivel;
            this.FechaAlta = fechaAlta;

            // Seteado para nuevos usuarios
     
            this.Ntdom = ntdom;
            this.Dominio = dominio;
            this.Equipo = equipo;
            

        }

        public Usuario(string usuario, string contrasenia, string nombre, string apellido, 
            string nivel, DateTime fechaAlta, DateTime fechaBaja, DateTime ultimoIngreso, 
            DateTime ultimoEgreso, string ntdom, string equipo, string dominio, DateTime vtoClave, 
            int intentosFallidos, bool bloqueado)
        {
            this.iUsuario = usuario;
            this.Contrasenia = contrasenia;
            this.Nombre= nombre;
            this.Apellido = apellido;
            this.Nivel = nivel;
            this.FechaAlta = fechaAlta;
            this.FechaBaja = fechaBaja;
            this.UltimoIngreso = ultimoIngreso;
            this.UltimoEgreso = ultimoEgreso;
            this.Ntdom = ntdom;
            this.Equipo = equipo;
            this.Dominio = dominio;
            this.VtoClave = vtoClave;
            this.IntentosFallidos = intentosFallidos;
            this.Bloqueado = bloqueado;
        }

        public Usuario(int id, string usuario, string contrasenia, string nombre, string apellido, 
            string nivel, DateTime fechaAlta, DateTime fechaBaja, DateTime ultimoIngreso, 
            DateTime ultimoEgreso, string ntdom, string equipo, string dominio, 
            DateTime vtoClave, int intentosFallidos, bool bloqueado)
        {
            this.id = id;
            this.iUsuario = usuario;
            this.Contrasenia = contrasenia;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Nivel = nivel;
            this.FechaAlta = fechaAlta;
            this.FechaBaja = fechaBaja;
            this.UltimoIngreso = ultimoIngreso;
            this.UltimoEgreso = ultimoEgreso;
            this.Ntdom = ntdom;
            this.Equipo = equipo;
            this.Dominio = dominio;
            this.VtoClave = vtoClave;
            this.IntentosFallidos = intentosFallidos;
            this.Bloqueado = bloqueado;
        }

        public int Id { get => id; set => id = value; }
        public string iUsuario {
            get => usuario;
            set
            {
                if(value.Length != LONG_USUARIO)
                {
                    throw new ArgumentException(string.Format("El usuario no puede superar los {0} caracteres", LONG_USUARIO));
                }
                usuario = value;
            }
        }
        public string Contrasenia
        {
            get => contrasenia;
            set
            {
                if (value == null || value == "")
                {
                    throw new ArgumentException(string.Format("La contraseña no puede estar vacía"));

                }
                else if (value.Length < MIN_LONG || value.Length > MAX_LONG)
                {
                    throw new ArgumentException(string.Format("La contraseña debe tener un minimo de {0} y un maximo de {1} caracteres", MIN_LONG,MAX_LONG));
                }
                contrasenia = value;
            }
        }

        public string Nombre { get => nombre;
            set {
                if (value == null || value == "")
                {
                    throw new ArgumentException("Nombre no puede estar vacío");

                }
                else if (value.Length < MIN_LONG || value.Length > MAX_LONG)
                {
                    throw new ArgumentException(string.Format("El nombre debe tener un minimo de {0} y un maximo de {1} caracteres", MIN_LONG, MAX_LONG));
                }
                nombre = value;

            } }
        public string Apellido { get => apellido;
            set
            {
                if (value == null || value == "")
                {
                    throw new ArgumentException("Apellido no puede estar vacío");

                }
                else if (value.Length < MIN_LONG || value.Length > MAX_LONG)
                {
                    throw new ArgumentException(string.Format("El apellido debe tener un minimo de {0} y un maximo de {1} caracteres",MIN_LONG, MAX_LONG));
                }
                apellido = value;

            }
        }
        public string Nivel { get => nivel; set => nivel = value; }
        public DateTime FechaAlta { get => fechaAlta; set => fechaAlta = value; }
        public DateTime FechaBaja { get => fechaBaja; set => fechaBaja = value; }
        public DateTime UltimoIngreso { get => ultimoIngreso; set => ultimoIngreso = value; }
        public DateTime UltimoEgreso { get => ultimoEgreso; set => ultimoEgreso = value; }
        public string Ntdom { get => ntdom; set => ntdom = (value == null) ? ntdom = "" : ntdom = value.ToUpper(); }
        public string Equipo { get => equipo; set => equipo = (value == null) ? equipo = "" : equipo = value.ToUpper(); }
        public string Dominio { get => dominio; set => dominio = (value == null) ? dominio = "" : dominio = value.ToUpper(); }
        public bool Activo { get; set; }
        public DateTime VtoClave { get => vtoClave; set => vtoClave = value; }
        public int IntentosFallidos { get => intentosFallidos; set => intentosFallidos = value; }
        public bool Bloqueado { get => bloqueado; set => bloqueado = value; }

        public override string ToString()
        {
            return string.Format("{1} {2} | Usuario: {0} | Ult.Acceso: {3} | Nivel de Acceso: {4} | Ntdom: {5} | Vto.Contraseña: {6}",
                            iUsuario, Nombre, Apellido, UltimoIngreso, Nivel, Ntdom, VtoClave.ToShortDateString());
        }

    }
}
