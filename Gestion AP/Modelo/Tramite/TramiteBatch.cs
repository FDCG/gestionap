﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    class TramiteBatch : Tramite
    {
        private string detalleRechazo;

        public TramiteBatch()
        {
        }

        public TramiteBatch(string detalleRechazo)
        {
            DetalleRechazo = detalleRechazo;
        }

        public TramiteBatch(string usuario, DateTime fechaInicio, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos, string detalleRechazos) : base(usuario, fechaInicio, nroTramite, prioridad, canalIngreso, estado, fechaControl, administrador, tipoTramite, procesos)
        {
            DetalleRechazo = detalleRechazo;
        }

        public TramiteBatch(int id, string usuario, DateTime fechaInicio, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos, string detalleRechazos) : base(id, usuario, fechaInicio, nroTramite, prioridad, canalIngreso, estado, fechaControl, administrador, tipoTramite, procesos)
        {
            DetalleRechazo = detalleRechazo;
        }

        public string DetalleRechazo { get => detalleRechazo;

            set
            {
                if (value.Length < 30) throw new ArgumentException("El detalle no puede ser menor a 30 caracteres");

                detalleRechazo = value;
            }
        }
    }
}
