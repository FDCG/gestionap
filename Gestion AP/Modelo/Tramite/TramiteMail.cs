﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using EAGetMail;


namespace Gestion_AP
{
    public class TramiteMail : Tramite
    {
        private EAGetMail.Mail archivo;
        
        public TramiteMail() : base() {
            Archivo = new Mail("TEAM BEAN 2014-00666-a8fa1baf7e92e8e38c737232cd1a81ff");
        }

        public TramiteMail(Mail archivo)
        {
            Archivo = archivo;

        }

        public TramiteMail(string usuario, DateTime fechaInicio, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos, Mail archivo) : base(usuario, fechaInicio, nroTramite, prioridad, canalIngreso, estado, fechaControl, administrador, tipoTramite, procesos)
        {
            Archivo = archivo;
        }

        public TramiteMail(int id, string usuario, DateTime fechaInicio, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos, Mail archivo) : base(id, usuario, fechaInicio, nroTramite, prioridad, canalIngreso, estado, fechaControl, administrador, tipoTramite, procesos)
        {
            Archivo = archivo;
        }



        public Mail Archivo { get => archivo; set => archivo = value; }
    }
}
