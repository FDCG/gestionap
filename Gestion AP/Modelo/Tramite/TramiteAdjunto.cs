﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace Gestion_AP
{
    class TramiteAdjunto : Tramite
    {

        private string[] adjuntos;

        public TramiteAdjunto() : base()
        {
        }

        public TramiteAdjunto(string[] adjuntos)
        {
            Adjuntos = adjuntos;
        }

        public TramiteAdjunto(string usuario, DateTime fechaInicio, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos, string [] adjuntos) : base(usuario, fechaInicio, nroTramite, prioridad, canalIngreso, estado, fechaControl, administrador, tipoTramite, procesos)
        {
            Adjuntos = adjuntos;
        }

        public TramiteAdjunto(int id, string usuario, DateTime fechaInicio, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos, string [] adjuntos) : base(id, usuario, fechaInicio, nroTramite, prioridad, canalIngreso, estado, fechaControl, administrador, tipoTramite, procesos)
        {
            Adjuntos = adjuntos;
        }


        public string[] Adjuntos { get => adjuntos; set => adjuntos = value; }

    }
}
