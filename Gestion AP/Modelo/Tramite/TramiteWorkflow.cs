﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    class TramiteWorkflow : Tramite
    {

        private Filiatorio filiatorio;
        private CuentaTarjeta cuentaTarjeta;
        private List<Tarjeta> tarjetas;
        private Distribucion distribucion;
        private string datosAdicionales;
        private string motivoTramite;
        private string observacionesTramite;



        public TramiteWorkflow()
        {
            filiatorio = new Filiatorio();
            cuentaTarjeta = new CuentaTarjeta();
            tarjetas = new List<Tarjeta>();
            distribucion = new Distribucion();

        }

        public TramiteWorkflow(Filiatorio filiatorio, CuentaTarjeta cuentaTarjeta, List<Tarjeta> tarjetas, Distribucion distribucion, string datosAdicionales, string motivoTramite, string observacionesTramite)
        {
            Filiatorio = filiatorio;
            CuentaTarjeta = cuentaTarjeta;
            Tarjetas = tarjetas;
            Distribucion = distribucion;
            DatosAdicionales = datosAdicionales;
            MotivoTramite = motivoTramite;
            ObservacionesTramite = observacionesTramite;
        }


        public TramiteWorkflow(string usuario, DateTime fechaInicio, string nombreTramite, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos, Filiatorio filiatorio, CuentaTarjeta cuentaTarjeta, List<Tarjeta> tarjetas, Distribucion distribucion, string datosAdicionales, string motivoTramite, string observacionesTramite) : base(usuario, fechaInicio, nroTramite, prioridad, canalIngreso, estado, fechaControl, administrador, tipoTramite, procesos)
        {
            Filiatorio = filiatorio;
            CuentaTarjeta = cuentaTarjeta;
            Tarjetas = tarjetas;
            Distribucion = distribucion;
            DatosAdicionales = datosAdicionales;
            MotivoTramite = motivoTramite;
            ObservacionesTramite = observacionesTramite;
        }

        public TramiteWorkflow(int id, string usuario, DateTime fechaInicio, string nombreTramite, string nroTramite, Prioridad prioridad, CanalIngreso canalIngreso, Estado estado, DateTime fechaControl, Administrador administrador, TipoTramite tipoTramite, List<Proceso> procesos, Filiatorio filiatorio, CuentaTarjeta cuentaTarjeta, List<Tarjeta> tarjetas, Distribucion distribucion, string datosAdicionales, string motivoTramite, string observacionesTramite) : base(id, usuario, fechaInicio, nroTramite, prioridad, canalIngreso, estado, fechaControl, administrador, tipoTramite, procesos)
        {
            Filiatorio = filiatorio;
            CuentaTarjeta = cuentaTarjeta;
            Tarjetas = tarjetas;
            Distribucion = distribucion;
            DatosAdicionales = datosAdicionales;
            MotivoTramite = motivoTramite;
            ObservacionesTramite = observacionesTramite;

        }

        public Filiatorio Filiatorio { get => filiatorio; set => filiatorio = value; }
        public CuentaTarjeta CuentaTarjeta { get => cuentaTarjeta; set => cuentaTarjeta = value; }
        public List<Tarjeta> Tarjetas { get => tarjetas; set => tarjetas = value; }
        public Distribucion Distribucion { get => distribucion; set => distribucion = value; }
        public string DatosAdicionales { get => datosAdicionales; set => datosAdicionales = value; }
        public string MotivoTramite { get => motivoTramite; set => motivoTramite = value; }
        public string ObservacionesTramite { get => observacionesTramite; set => observacionesTramite = value; }
    }
}
