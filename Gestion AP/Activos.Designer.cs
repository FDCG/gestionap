﻿namespace Gestion_AP
{
    partial class Activos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Activos));
            this.gridviewActivos = new System.Windows.Forms.DataGridView();
            this.pbVolver = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewActivos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbVolver)).BeginInit();
            this.SuspendLayout();
            // 
            // gridviewActivos
            // 
            this.gridviewActivos.AllowUserToAddRows = false;
            this.gridviewActivos.AllowUserToDeleteRows = false;
            this.gridviewActivos.AllowUserToResizeColumns = false;
            this.gridviewActivos.AllowUserToResizeRows = false;
            this.gridviewActivos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gridviewActivos.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.gridviewActivos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridviewActivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridviewActivos.GridColor = System.Drawing.SystemColors.Menu;
            this.gridviewActivos.Location = new System.Drawing.Point(12, 69);
            this.gridviewActivos.Name = "gridviewActivos";
            this.gridviewActivos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridviewActivos.Size = new System.Drawing.Size(948, 438);
            this.gridviewActivos.TabIndex = 0;
            this.gridviewActivos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridviewActivos_CellContentClick);
            // 
            // pbVolver
            // 
            this.pbVolver.BackgroundImage = global::Gestion_AP.Properties.Resources.Entypo_e759_0__128;
            this.pbVolver.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbVolver.Location = new System.Drawing.Point(12, 12);
            this.pbVolver.Name = "pbVolver";
            this.pbVolver.Size = new System.Drawing.Size(59, 50);
            this.pbVolver.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbVolver.TabIndex = 1;
            this.pbVolver.TabStop = false;
            this.pbVolver.Click += new System.EventHandler(this.pbVolver_Click);
            // 
            // Activos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(972, 519);
            this.Controls.Add(this.pbVolver);
            this.Controls.Add(this.gridviewActivos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Activos";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Usuarios Activos";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Activos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridviewActivos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbVolver)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridviewActivos;
        private System.Windows.Forms.PictureBox pbVolver;
    }
}