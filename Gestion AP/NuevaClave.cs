﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;

using System.Windows.Forms;
using System.Data.OleDb;


namespace Gestion_AP
{
    public partial class NuevaClave : Form
    {
        private Usuario usuario;
        private UsuarioDaoImpl daoUsuario;
    

        public NuevaClave()
        {
            InitializeComponent();
        }

        public NuevaClave(Usuario usuario) : this()
        {
           

            this.usuario = usuario;

            // Instanciacion de Dao Usuarios

            daoUsuario = new UsuarioDaoImpl();

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

            killProcess();

        }

        private void killProcess()
        {
            this.Close();
            Application.Exit();
            Application.ExitThread();
        }



        public void cleanControls()
        {

            txtNueva.Clear();
            txtRepetir.Clear();
            txtNueva.Focus();

        }

        private void processChangePassword(Usuario usuario, string contrasenia, string repContrasenia)
        {

            if(contrasenia == "" || repContrasenia == "")
            {
                throw new ArgumentException("Todos los campos deben completarse.");
            }
            else if (!contrasenia.Equals(repContrasenia))
            {
                throw new ArgumentException("Las contraseñas ingresadas no coinciden.");
            }
            else if (usuario.Contrasenia.Equals(contrasenia))
            {
                throw new ArgumentException("La nueva contraseña debe ser distinta a la anterior.");
            }
            else
            {

                daoUsuario.updateUsuario(setUpdateLogin(usuario, contrasenia));

                Usuario.usuarioSingleton = usuario;

                this.usuario = null;

                goToForm(new Principal());

            }


        }

        private void goToForm(Principal principal)
        {
            principal.Show();
            this.Close();
        }

        private Usuario setUpdateLogin(Usuario usuario, string nuevaContrasenia)
        {

            usuario.Activo = true;
            usuario.UltimoIngreso = DateTime.Now;
            usuario.IntentosFallidos = 0;
            usuario.Dominio = Environment.UserDomainName.ToUpper();
            usuario.Ntdom = Environment.UserName.ToUpper();
            usuario.Equipo = Environment.MachineName.ToUpper();
            usuario.Contrasenia = nuevaContrasenia;
            usuario.VtoClave = DateTime.Today.AddDays(Logins.DiasVencimiento);

            return usuario;

        }


        private void btnConfirmar_Click(object sender, EventArgs e)
        {

            string contrasenia, repContrasenia;

            contrasenia = txtNueva.Text.Trim();
            repContrasenia = txtRepetir.Text.Trim();

            cleanControls();

            try
            {

                processChangePassword(usuario, contrasenia, repContrasenia);

            }catch(ArgumentException argException)
            {
                MessageBox.Show(argException.Message);
            }catch(Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

        }

    }
}
