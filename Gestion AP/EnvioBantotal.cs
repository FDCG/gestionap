﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Gestion_AP
{
    class EnvioBantotal
    {
        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        private static extern IntPtr FindWindow(string lp1, string lp2);

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void enviarPrueba()
        {
            // find window handle of Notepad
            IntPtr handle = FindWindow("Notepad", "pepo: Bloc de notas");
            if (!handle.Equals(IntPtr.Zero))
            {
                // activate Notepad window
                if (SetForegroundWindow(handle))
                {
                    // send "Hello World!"
                    SendKeys.Send("Hello World!");
                    // send key "Tab"
                    SendKeys.Send("{TAB}");
                    // send key "Enter"
                    SendKeys.Send("{ENTER}");
                }
            }
        }




    }
}
