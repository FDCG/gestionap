﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Data.OleDb;




namespace Gestion_AP
{
    public partial class GestionImagen : Form
    {

        private void cmbGestion_SelectedValueChanged(object sender, EventArgs e)
        {



            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT WF FROM TRAMITES WHERE TRAMITE='" + Convert.ToString(cmbGestion.SelectedItem) + "'";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                lblWF_Asociado.Text = Convert.ToString(dt.Rows[0][0]);

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);
            }




        }
        public GestionImagen()
        {
            InitializeComponent();
        }

        Bitmap Porta_A;
        Bitmap Porta_B;

        int PortaALargo;
        int PortaBLargo;
        int PortaAAncho;
        int PortaBAncho;


        int Maximo = 0;


        private void GestionImagen_Load(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM TRAMITES";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cmbGestion.Items.Add(Convert.ToString(dt.Rows[i]["TRAMITE"]));
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);
            }

        }

        private void btnPegar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Maximo <= 1)
                {
                    Image picImagen = Clipboard.GetImage();
                    picPortaImagen.Image = picImagen;
                    Bitmap btPorta = new Bitmap(picImagen);
                    Maximo++;
                    switch (Maximo)
                    {
                        case 1:
                            Porta_A = btPorta;
                            PortaALargo = Porta_A.Height;
                            PortaAAncho = Porta_A.Width;

                            break;
                        case 2:
                            Porta_B = btPorta;
                            PortaBLargo = Porta_B.Height;
                            PortaBAncho = Porta_B.Width;
                            break;

                        default:

                            break;
                    }

                }
                else
                {
                    MessageBox.Show("No se pueden agregar más imágenes");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Nada seleccionado...");
                Logins.LogError(ex);
            }
            

           
        }

       

        public void GrabarGestion(string strDocRelacionado, string strIdInterno)



        {

            try
            {
                if (lblWF_Asociado.Text != "" && cbmAdm.SelectedItem!=null)
                {
                    string _Prioridad = null;
                    try
                    {
                        OleDbConnection cn = new OleDbConnection();
                        cn.ConnectionString = Conexion.cnProceso;
                        OleDbCommand cmd = new OleDbCommand();
                        cmd.Connection = cn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "INSERT INTO GESTIONES(US_INICIO,FECHA_INICIO,EMP,TRAMITE,WF,NRO_TRAMITE,PRIORIDAD,CANAL_INGRESO,DOC_ASOCIADO,ESTADO,A_CONTROLAR) VALUES(@US_INICIO,@FECHA_INICIO,@EMP,@TRAMITE,@WF,@NRO_TRAMITE,@PRIORIDAD,@CANAL_INGRESO,@DOC_ASOCIADO,@ESTADO,@ACONTROL)";

                        cn.Open();
                        cmd.Parameters.AddWithValue("@US_INICIO", Ingreso.UsuarioLogueado.ToUpper());
                        cmd.Parameters.AddWithValue("@FECHA_INICIO", DateTime.Now.ToOADate());
                        cmd.Parameters.AddWithValue("@EMP", cbmAdm.SelectedItem.ToString().Trim());
                        cmd.Parameters.AddWithValue("@TRAMITE", cmbGestion.Text.ToString());
                        cmd.Parameters.AddWithValue("@WF", lblWF_Asociado.Text.ToString());
                        cmd.Parameters.AddWithValue("@NRO_TRAMITE", strIdInterno);
                        if (chbUrgente.Checked)
                        {
                            _Prioridad = "URGENTE";
                           
                        }
                        else
                        {
                            _Prioridad = "NORMAL";
                          
                        }

                        switch (chbLiberar.Checked)
                        {
                            case true: cmd.Parameters.AddWithValue("@PRIORIDAD", _Prioridad+"+LIBERACION");
                                break;
                            case false: cmd.Parameters.AddWithValue("@PRIORIDAD", _Prioridad);
                                break;
                            default:
                                break;
                        }

                        cmd.Parameters.AddWithValue("@CANAL_INGRESO", "VARIOS");
                        cmd.Parameters.AddWithValue("@DOC_ASOCIADO", strDocRelacionado+ ".tiff");


                        cmd.Parameters.AddWithValue("@ESTADO", "ABIERTO");
                        cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today).ToOADate());

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                        cmd.Parameters.AddWithValue("@ID", strIdInterno);
                        cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                        cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                        cmd.Parameters.AddWithValue("@COM", txtComAdic.Text.Trim());
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        Logins.Estadistica("PROCESO",strIdInterno);

                        MessageBox.Show("Gestión grabada exitosamente.\nNro. de control interno: " + strIdInterno);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("No se pudo grabar el Trámite.\nError: " + ex.Message + "\nContacte al administrador");
                        Logins.LogError(ex);
                    }

                }
                else
                {
                    MessageBox.Show("No se puede grabar, faltan completar datos.");
                }



            }
            catch (Exception ex)
            {
                Logins.LogError(ex);

            }



        }



      

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            string path = UNC.PathUNC(Directory.GetCurrentDirectory()) + @"\02\";
            string id= "IM" + DateTime.Now.ToString("yyyyMMddHHmmss");



            if (Maximo==1)
            {
                Bitmap btInd = new Bitmap(Porta_A);
                btInd.Save(path + id + ".tiff", ImageFormat.Tiff);
               // Encriptado.Encriptar(path + id + ".tiff", Encriptado.ClaveEncript);
                GrabarGestion(path + id, id);


            }
            else if (Maximo==2)
            {
                int MaximoAncho = Math.Max(PortaAAncho, PortaBAncho);

                Bitmap btFinal = new Bitmap(MaximoAncho, PortaALargo + PortaBLargo);

                Graphics g = Graphics.FromImage(btFinal);

                g.DrawImage(Porta_A, 0, 0, PortaAAncho, PortaALargo);
                g.DrawImage(Porta_B, 0, PortaALargo);
                btFinal.Save(path + id + ".tiff", ImageFormat.Tiff);
                //Encriptado.Encriptar(path + id + ".tiff", Encriptado.ClaveEncript);
                GrabarGestion(path + id, id);

            }
            else
            {
                MessageBox.Show("No se puede guardar documento vacio");
            }





        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            Maximo = 0;
            picPortaImagen.Image = null;

        }

        private void cmbGestion_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    }

