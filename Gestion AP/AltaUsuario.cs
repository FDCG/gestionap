﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using System.Windows.Forms;
using System.Data.OleDb;


namespace Gestion_AP
{
    public partial class AltaUsuario : Form
    {

        UsuarioDaoImpl usuarioDaoImpl;

        Usuario usuario;

        public AltaUsuario()
        {

            InitializeComponent();

            txtApellido.CharacterCasing = CharacterCasing.Upper;
            txtUsuario.CharacterCasing = CharacterCasing.Upper;
            txtNombre.CharacterCasing = CharacterCasing.Upper;

        }

        private Usuario addUsuario(string iUsuario, string nombre, string apellido, string nivelAcceso)
        {
            usuarioDaoImpl = new UsuarioDaoImpl();
            Usuario usuario = usuarioDaoImpl.getByIdUsuario(iUsuario);

            if(usuario != null)
            {
                throw new ArgumentException("Usuario existente.");
            }

            try
            {
                usuario = new Usuario(iUsuario, Logins.CONTRASENIA_BLANQUEADA, nombre, apellido, nivelAcceso, DateTime.Now);

                usuarioDaoImpl.createUsuario(usuario);

                MessageBox.Show("Usuario dado de alta correctamente.");
            }
            catch (Exception e)
            {
                throw e;
            }

            return usuario;
        }





        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            string iUsuario, nombre, apellido, nivelAcceso;

            iUsuario = txtUsuario.Text.Trim();

            nombre = txtNombre.Text.Trim();

            apellido = txtApellido.Text.Trim();

            nivelAcceso = rbtAnalista.Checked ? nivelAcceso = "99" : nivelAcceso = "00";

            clearControls();

            try
            {
                usuario = addUsuario(iUsuario, nombre, apellido, nivelAcceso);
            }
            catch(ArgumentException argException)
            {
                MessageBox.Show(argException.Message);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }


        }

        private void clearControls()
        {
            txtUsuario.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtUsuario.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}



