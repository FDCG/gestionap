﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;



namespace Gestion_AP
{
    public partial class RechList : Form
    {
        public RechList(string Rechazos)
        {
            InitializeComponent();

            try
            {
                DataTable dt = new DataTable();
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM RechList";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);
                dgvRechList.DataSource=dt;
                txtRechazos.Text=Rechazos;
            }
            catch (Exception)
            {

            }

         


            
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap bm = new Bitmap(dgvRechList.Width, dgvRechList.Height);
            dgvRechList.DrawToBitmap(bm, new Rectangle(0, 0, dgvRechList.Width, dgvRechList.Height));
            e.Graphics.DrawImage(bm, 0, 0);

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
           // CapturarPantalla();
            printDocument1.Print();
        }

        Bitmap imagenMemoria;

        private void CapturarPantalla()
        {
            Graphics miGrafico = this.CreateGraphics();
            Size s = this.Size;
            imagenMemoria = new Bitmap(s.Width, s.Height, miGrafico);
            Graphics memoriaGraficos = Graphics.FromImage(imagenMemoria);
            memoriaGraficos.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
        }
    }
}
