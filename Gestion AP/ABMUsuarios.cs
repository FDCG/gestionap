﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;

using System.Windows.Forms;
using System.Data.OleDb;

namespace Gestion_AP
{
    public partial class ABMUsuarios : Form
    {
        public ABMUsuarios()
        {
            InitializeComponent();
        }
        public void NoVigentes()

        {
            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnUsuario;
            cn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM Usuarios WHERE Fecha_Baja is not null";
            DataTable dt = new DataTable();
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);

            dgvNoVigentes.DataSource = dt;

            dgvNoVigentes.ReadOnly = true;
            dgvNoVigentes.Columns["Id"].ReadOnly = true;
            dgvNoVigentes.Columns["Contraseña"].Visible = false;
            dgvNoVigentes.Columns["Fecha_Alta"].ReadOnly = true;
            dgvNoVigentes.Columns["Fecha_Alta"].HeaderText = "Fecha Alta";
            dgvNoVigentes.Columns["Fecha_Baja"].ReadOnly = true;
            dgvNoVigentes.Columns["Fecha_Baja"].HeaderText = "Fecha Baja";
            dgvNoVigentes.Columns["ACTIVO"].Visible = false;
            dgvNoVigentes.Columns["Ultimo_Ingreso"].Visible = false;
            dgvNoVigentes.Columns["Ultimo_Egreso"].Visible = false;
            dgvNoVigentes.Columns["DOMINIO"].Visible = false;
            dgvNoVigentes.Columns["EQUIPO"].Visible = false;
            dgvNoVigentes.Columns["NTDOM"].Visible = false;
            dgvNoVigentes.Columns["VTO_CLAVE"].Visible = false;
            dgvNoVigentes.Columns["INTENTOS"].Visible = false;
            dgvNoVigentes.Columns["BLOQUEADO"].Visible = false;


            AspectoTabla.AspectoDGV(dgvNoVigentes, Color.White);
            dgvNoVigentes.Columns["Id"].Width = 25;
            dgvNoVigentes.Columns["NIVEL"].Width = 45;
            dgvNoVigentes.MultiSelect = false;


            cmd.ExecuteNonQuery();
            cn.Close();


        }


        public void Vigentes()
        {
            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnUsuario;

            cn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM Usuarios WHERE Fecha_Baja is null";
            DataTable dt = new DataTable();
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);

            dgvVigentes.DataSource = dt;
            dgvVigentes.ReadOnly = true;

            dgvVigentes.Columns["Id"].ReadOnly = true;
            dgvVigentes.Columns["Contraseña"].Visible = false;
            dgvVigentes.Columns["Fecha_Alta"].ReadOnly = true;
            dgvVigentes.Columns["Fecha_Alta"].HeaderText = "Fecha Alta";
            dgvVigentes.Columns["Fecha_Baja"].Visible = false;
            dgvVigentes.Columns["ACTIVO"].Visible = false;
            dgvVigentes.Columns["Fecha_Alta"].ReadOnly = true;
            dgvVigentes.Columns["Ultimo_Ingreso"].Visible = false;
            dgvVigentes.Columns["Ultimo_Egreso"].Visible = false;
            dgvVigentes.Columns["NTDOM"].Visible = false;
            dgvVigentes.Columns["DOMINIO"].Visible = false;
            dgvVigentes.Columns["EQUIPO"].Visible = false;
            dgvVigentes.Columns["VTO_CLAVE"].Visible = false;
            dgvVigentes.Columns["INTENTOS"].Visible = false;
            dgvVigentes.Columns["BLOQUEADO"].ReadOnly = true;

            AspectoTabla.AspectoDGV(dgvVigentes, Color.White);
            dgvVigentes.Columns["Id"].Width = 25;
            dgvVigentes.Columns["NIVEL"].Width = 45;
            dgvNoVigentes.MultiSelect = false;

            cmd.ExecuteNonQuery();
            cn.Close();

        }

        private void ABMUsuarios_Load(object sender, EventArgs e)
        {

            try
            {

                // VIGENTES

                Vigentes();


            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);

            }



            try
            {

                // NO VIGENTES

                NoVigentes();


            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);

            }


        }

        private void btnReactivar_Click(object sender, EventArgs e)
        {


            try
            {

                if (dgvNoVigentes.CurrentCell.Selected == true)
                {
                    DataTable dt = new DataTable();


                    dt.Columns.Add("Usuario");
                    dt.Columns.Add("Fecha_Baja");

                    dt.Rows.Add(dgvNoVigentes.CurrentRow.Cells["Usuario"].Value, dgvNoVigentes.CurrentRow.Cells["Fecha_Baja"].Value);


                    string strUsuarioReactiv = Convert.ToString(dt.Rows[0]["Usuario"]);
                    OleDbConnection cn = new OleDbConnection();
                    cn.ConnectionString = Conexion.cnUsuario;
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE Usuarios SET Fecha_Baja=null WHERE Usuario='" + strUsuarioReactiv + "'";
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();

                    MessageBox.Show("Usuario reactivado existosamente", "Reactivación", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Vigentes();
                    NoVigentes();



                }
                else
                {
                    MessageBox.Show("No ha seleccionado ningun usuario");
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error al reactivar: " + ex.Message + "\nEs posible que no haya usuarios inactivos", "Error Reactivación");
                Logins.LogError(ex);

            }






        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnDarBaja_Click(object sender, EventArgs e)
        {


            try
            {

                if (dgvVigentes.CurrentCell.Selected == true)
                {
                    DataTable dt = new DataTable();


                    dt.Columns.Add("Usuario");


                    dt.Rows.Add(dgvVigentes.CurrentRow.Cells["Usuario"].Value);


                    string strUsuarioBaja = Convert.ToString(dt.Rows[0]["Usuario"]);
                    OleDbConnection cn = new OleDbConnection();
                    cn.ConnectionString = Conexion.cnUsuario;
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE Usuarios SET Fecha_Baja=" + DateTime.Today.ToOADate() + " WHERE Usuario='" + strUsuarioBaja + "'";
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();

                    MessageBox.Show("Usuario Dado de baja existosamente", "Baja de usuario", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Logins.Login(Ingreso.UsuarioLogueado, Environment.UserDomainName, DateTime.Now, "Baja usuario " + strUsuarioBaja);


                    Vigentes();
                    NoVigentes();



                }
                else
                {
                    MessageBox.Show("No ha seleccionado ningun usuario");


                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error al dar de baja: " + ex.Message + "\nEs posible que no haya usuarios vigente", "Error Baja");
                Logins.LogError(ex);

            }



        }

        private void dgvVigentes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {




        }

        private void dgvVigentes_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string strUsuario = Convert.ToString(dgvVigentes.CurrentRow.Cells["Usuario"].Value);
            string strNombre = Convert.ToString(dgvVigentes.CurrentRow.Cells["Nombre"].Value);
            string strApellido = Convert.ToString(dgvVigentes.CurrentRow.Cells["Apellido"].Value);
            string strNivel = Convert.ToString(dgvVigentes.CurrentRow.Cells["NIVEL"].Value);
            ModificaUsuario frm = new ModificaUsuario(strUsuario, strNombre, strApellido, strNivel);
            frm.Show();
        }

        private void ABMUsuarios_Enter(object sender, EventArgs e)
        {


        }

        private void ABMUsuarios_Activated(object sender, EventArgs e)
        {
            Vigentes();
            NoVigentes();
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pbSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
