﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    class TarjetaDaoImpl
    {

        private OleDbConnection cn;

        public TarjetaDaoImpl(OleDbConnection cn)
        {
            Cn = cn;

        }

        public TarjetaDaoImpl()
        {

            Cn = new OleDbConnection(Conexion.cnProceso);
        }

        public Tarjeta createTarjeta(Tarjeta tarjeta, string nroTramite)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("INSERT INTO Tarjetas ");

            sb.Append(Query.INSERT_TARJETA_FIELDS);

            sb.Append(" VALUES ");

            sb.Append(Query.INSERT_TARJETA_PARAMETERS);

            OleDbCommand cmd = new OleDbCommand(sb.ToString(), Cn);

            setParameterFromObject(cmd, tarjeta, nroTramite);

            try
            {
                cn.Open();

                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cn.Close();
            }

            return tarjeta;

        }

        private void setParameterFromObject(OleDbCommand cmd, Tarjeta tarjeta, string nroTramite)
        {

            try
            {

                cmd.Parameters.AddWithValue("@NroTramite", nroTramite);
                cmd.Parameters.AddWithValue("@NroTarjeta", tarjeta.NroTarjeta);
                cmd.Parameters.AddWithValue("@Categoria", tarjeta.CategoriaTarjeta);
                cmd.Parameters.AddWithValue("@NombreTJ", tarjeta.DenominacionTarjeta);
                cmd.Parameters.AddWithValue("@TipoDoc", tarjeta.TipoDocumento);
                cmd.Parameters.AddWithValue("@NroDoc", tarjeta.NroDocumento);
                cmd.Parameters.AddWithValue("@Vigencia", tarjeta.VigenciaTarjeta);
                cmd.Parameters.AddWithValue("@Estado", tarjeta.EstadoTarjeta);
            }
            catch
            {
                throw new ArgumentException("Error con inserción de parametros");
            }


        }



        public List<Tarjeta> getTarjetasById(string nroTramite)
        {
            List<Tarjeta> tarjetas = new List<Tarjeta>();

            string cmdText = "SELECT * FROM Tarjetas WHERE NroTramite=@nroTramite";

            DataSet ds = new DataSet();

            try
            {

                OleDbCommand cmd = new OleDbCommand(cmdText, Cn);

                cmd.Parameters.AddWithValue("@nroTramite", nroTramite);

                OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                da.Fill(ds);

            }
            catch (Exception e)
            {
                throw e;
            }

            if (ds.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dataRow in ds.Tables[0].Rows)
                {
                    tarjetas.Add(getTarjetaFromDataSetRow(dataRow));
                }

            }
            else
            {
                throw new Exception("No se obtuvieron Tarjetas");
            }

            return tarjetas;
        }

        private Tarjeta getTarjetaFromDataSetRow(DataRow dataRow)
        {
            Tarjeta tarjeta = new Tarjeta();

            tarjeta.NroTarjeta = dataRow["NroTarjeta"].ToString();

            tarjeta.CategoriaTarjeta = Convert.ToChar(dataRow["Categoria"].ToString());

            tarjeta.DenominacionTarjeta = dataRow["NombreTJ"].ToString();

            tarjeta.TipoDocumento = dataRow["TipoDoc"].ToString();

            tarjeta.NroTarjeta = dataRow["NroDoc"].ToString();

            tarjeta.VigenciaTarjeta = dataRow["Vigencia"].ToString();

            tarjeta.EstadoTarjeta = Convert.ToByte(dataRow["Estado"].ToString());

            return tarjeta;
        }

        public OleDbConnection Cn { get => cn; set => cn = value; }


    }
}

