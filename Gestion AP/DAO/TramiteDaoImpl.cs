﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace Gestion_AP
{
    class TramiteDaoImpl : ITramiteDAO
    {
        private OleDbConnection cn;
        public OleDbConnection Cn { get => cn; set => cn = value; }

        public TramiteDaoImpl()
        {
            Cn = new OleDbConnection(Conexion.cnProceso);
        }

        public Tramite createTramite(Tramite tramite)
        {
            if (getTramiteById(tramite.NroTramite) != null)
                throw new ArgumentException(string.Format("Trámite {0} ya existe", tramite.NroTramite));

            try
            {
                Cn.Open();
                OleDbTransaction dbTransaction;
                dbTransaction = Cn.BeginTransaction();
                try
                {


                    OleDbCommand cmdGestiones = commandQuery(Query.INSERT_GESTION_FIELDS, Query.INSERT_GESTION_PARAMETERS, tramite, "Gestiones");

                    OleDbCommand cmdProcesos = commandQuery(Query.INSERT_PROCESO_FIELDS, Query.INSERT_PROCESO_PARAMETERS, tramite, "Procesos");

                    cmdGestiones.Transaction = dbTransaction;
                    cmdProcesos.Transaction = dbTransaction;

                    cmdGestiones.ExecuteNonQuery();

                    cmdProcesos.ExecuteNonQuery();

                    dbTransaction.Commit();

                }
                catch (OleDbException dbException)
                {
                    dbTransaction.Rollback();
                    throw dbException;
                }
                finally
                {
                    Cn.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error al grabar en BBDD. " + e.Message);
            }

            return tramite;
        }


        private OleDbCommand commandQuery(string insertStatement, string parameterStatement, Tramite tramite, string table)
        {
            

            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("INSERT INTO {0} ", table));

            sb.Append(insertStatement);

            sb.Append(" VALUES ");

            sb.Append(parameterStatement);

            OleDbCommand dbCommand = new OleDbCommand(sb.ToString(), Cn);

            setParameterFromObject(dbCommand, tramite, table);

            return dbCommand;

        }



        public Tramite deleteTramite(string tramite)
        {
            throw new NotImplementedException();
        }

        public List<Tramite> getTramites()
        {
            List<Tramite> tramites = new List<Tramite>();
            DataSet dsTramites;
            try
            {
                dsTramites = getDataSetTramites("SELECT * FROM Gestiones");

                foreach (DataRow row in dsTramites.Tables[0].Rows)
                {

                    tramites.Add(getTramiteFromDataSetRow(row));

                }


            }
            catch(Exception exception)
            {
                MessageBox.Show(exception.Message);

            }



            return tramites;

        }

        public Tramite getFullTramiteById(string nroTramite)
        {
            Tramite tramite = getTramiteById(nroTramite);

            ProcesoDaoImpl procesoDaoImpl = new ProcesoDaoImpl(Cn);

            try
            {
                tramite.Procesos = procesoDaoImpl.getProcesosById(nroTramite);

            }catch(Exception e)
            {
                throw e;
            }

            return tramite;

        }


        public Tramite getTramiteById(string nroTramite)
        {
            Tramite tramite = null;

            string cmdText = "SELECT * FROM Gestiones WHERE NroTramite=@nroTramite";

            DataSet ds = new DataSet();

            try
            {

                OleDbCommand cmd = new OleDbCommand(cmdText, Cn);

                cmd.Parameters.AddWithValue("@nroTramite", nroTramite);

                OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                da.Fill(ds);

            }
            catch (Exception e)
            {
                throw e;
            }

            if (ds.Tables[0].Rows.Count != 0)
            {

                tramite = getTramiteFromDataSetRow(ds.Tables[0].Rows[0]);

            }
            else
            {
                throw new Exception("No se obtuvo Trámite");
            }

            return tramite;


        }

        private void setParameterFromObject(OleDbCommand cmd, Tramite tramite, string table)
        {

            try
            {

                switch (table)
                {

                    // Create

                    case "Gestiones":

                        cmd.Parameters.AddWithValue("@NroTramite", tramite.NroTramite);
                        cmd.Parameters.AddWithValue("@CodTramite", tramite.TipoTramite.CodigoTramite);
                        cmd.Parameters.AddWithValue("@NomTramite", tramite.TipoTramite.NombreTramite);
                        cmd.Parameters.AddWithValue("@Adm", tramite.Administrador);
                        cmd.Parameters.AddWithValue("@UsuarioInicio", tramite.Usuario);
                        cmd.Parameters.AddWithValue("@FechaInicio", tramite.FechaInicio);
                        cmd.Parameters.AddWithValue("@FechaControl", tramite.FechaControl);
                        cmd.Parameters.AddWithValue("@Prioridad", tramite.Prioridad);
                        cmd.Parameters.AddWithValue("@CanalIngreso", tramite.CanalIngreso);
                        cmd.Parameters.AddWithValue("@Estado", tramite.Estado);

                        break;

                    case "Procesos":


                        cmd.Parameters.AddWithValue("@NroTramite", tramite.NroTramite);
                        cmd.Parameters.AddWithValue("@Fecha", tramite.Procesos[0].Fecha.ToOADate());
                        cmd.Parameters.AddWithValue("@Usuario", tramite.Procesos[0].Usuario);
                        cmd.Parameters.AddWithValue("@Accion", tramite.Procesos[0].AccionProceso);
                        cmd.Parameters.AddWithValue("@Detalle", tramite.Procesos[0].DetalleProceso);

                        break;

                    default:
                        throw new ArgumentException("Tabla erronea");

                }

            }
            catch
            {
                throw new ArgumentException(string.Format("Error con inserción de parametros en {0}",table));
            }


        }



        protected Tramite getTramiteFromDataSetRow(DataRow dataRow)
        {
            Tramite tramite = new Tramite();

            tramite.Usuario = dataRow["UsuarioInicio"].ToString();

            tramite.FechaInicio = Convert.ToDateTime(dataRow["FechaInicio"].ToString());

            tramite.Administrador = Tramite.getAdministradorByString(dataRow["Adm"].ToString());

            tramite.TipoTramite.NombreTramite = dataRow["NomTramite"].ToString();

            tramite.TipoTramite.CodigoTramite = dataRow["CodTramite"].ToString();

            tramite.NroTramite= dataRow["NroTramite"].ToString();

            tramite.Prioridad = Tramite.getPrioridadByString(dataRow["Prioridad"].ToString());

            tramite.CanalIngreso = Tramite.getCanalIngresoByString(dataRow["CanalIngreso"].ToString());

            tramite.Estado = (Estado)Convert.ToByte(dataRow["Estado"].ToString());

            tramite.FechaControl = Convert.ToDateTime(dataRow["FechaControl"].ToString());

            return tramite;
        }

        public DataSet getDataSetTramites(string commandtext)
        {
            DataSet dsTramites = new DataSet();

            OleDbCommand cmd = new OleDbCommand(commandtext, Cn);

            OleDbDataAdapter da = new OleDbDataAdapter(cmd);

            da.Fill(dsTramites);
            if (dsTramites.Tables[0].Rows.Count == 0) throw new Exception("No hay registros.");
             

            return dsTramites;

        }

        public Tramite updateTramite(Tramite tramite)
        {
            throw new NotImplementedException();
        }
    }
}
