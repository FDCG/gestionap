﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    class ProcesoDaoImpl
    {

        private OleDbConnection cn;

        public ProcesoDaoImpl(OleDbConnection cn)
        {
            Cn = cn;

        }

        public ProcesoDaoImpl()
        {

            Cn = new OleDbConnection(Conexion.cnProceso);
        }


        public List<Proceso> getProcesosById (string nroTramite)
        {
            List<Proceso> procesos = new List<Proceso>();

            string cmdText = "SELECT * FROM Procesos WHERE NroTramite=@nroTramite";

            DataSet ds = new DataSet();

            try
            {

                OleDbCommand cmd = new OleDbCommand(cmdText, Cn);

                cmd.Parameters.AddWithValue("@nroTramite", nroTramite);

                OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                da.Fill(ds);

            }
            catch (Exception e)
            {
                throw e;
            }

            if (ds.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dataRow in ds.Tables[0].Rows)
                {
                    procesos.Add(getProcesoFromDataSetRow(dataRow));
                }

            }
            else
            {
                throw new Exception("No se obtuvieron Procesos");
            }

            return procesos;
        }

        public Proceso updateProceso(Proceso proceso, string nroTramite)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("UPDATE Usuarios SET ");

            sb.Append(Query.UPDATE_PROCESO_QUERY);

            sb.Append(" WHERE NroTramite='" + nroTramite + "'");

            OleDbCommand cmd = new OleDbCommand(sb.ToString(), Cn);

            setParameterFromObject(cmd, proceso, nroTramite);

            try
            {
                cn.Open();

                int query = cmd.ExecuteNonQuery() == 0 ? throw new DataException("Error al registrar modificación") : query = 1;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cn.Close();
            }

            return proceso;

        }

        public Proceso createProceso(Proceso proceso, string nroTramite)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("INSERT INTO Procesos ");

            sb.Append(Query.INSERT_PROCESO_FIELDS);

            sb.Append(" VALUES ");

            sb.Append(Query.INSERT_PROCESO_PARAMETERS);

            OleDbCommand cmd = new OleDbCommand(sb.ToString(), Cn);

            setParameterFromObject(cmd, proceso, nroTramite);

            try
            {
                cn.Open();

                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cn.Close();
            }

            return proceso;
        }

        private void setParameterFromObject(OleDbCommand cmd, Proceso proceso, string nroTramite)
        {

            try
            {

                cmd.Parameters.AddWithValue("@NroTramite", nroTramite);
                cmd.Parameters.AddWithValue("@Fecha", proceso.Fecha.ToOADate());
                cmd.Parameters.AddWithValue("@Usuario", proceso.Usuario);
                cmd.Parameters.AddWithValue("@Accion", proceso.AccionProceso);
                cmd.Parameters.AddWithValue("@Detalle", proceso.DetalleProceso);
            }
            catch
            {
                throw new ArgumentException("Error con inserción de parametros");
            }


        }

        private Proceso getProcesoFromDataSetRow(DataRow dataRow)
        {
            Proceso proceso = new Proceso();

            proceso.Usuario = dataRow["Usuario"].ToString();

            proceso.Fecha = Convert.ToDateTime(dataRow["Fecha"].ToString());

            proceso.AccionProceso = (AccionProceso)Convert.ToByte(dataRow["Accion"].ToString());

            proceso.DetalleProceso = dataRow["Detalle"].ToString();

            return proceso;
        }

        public OleDbConnection Cn { get => cn; set => cn = value; }


    }
}
