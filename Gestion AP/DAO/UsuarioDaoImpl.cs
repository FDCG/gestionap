﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.Windows.Forms;

namespace Gestion_AP
{
    class UsuarioDaoImpl : IUsuarioDAO
    {

        private static OleDbConnection cn = new OleDbConnection(Conexion.cnUsuario);


        public Usuario updateUsuario(Usuario usuario)
        {


            StringBuilder sb = new StringBuilder();

            sb.Append("UPDATE Usuarios SET ");

            sb.Append(Query.UPDATE_USUARIO_QUERY);

            sb.Append(" WHERE Usuario='"+usuario.iUsuario+"'");

            OleDbCommand cmd = new OleDbCommand(sb.ToString(), cn);

            setParameterFromObject(cmd, usuario);

            try
            {
                cn.Open();

                int query = cmd.ExecuteNonQuery() == 0 ? throw new DataException ("Error al registrar modificación") : query = 1 ;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                cn.Close();
            }

            return usuario;


        }

        private void setParameterFromObject(OleDbCommand cmd, Usuario usuario)
        {

            try { 

            cmd.Parameters.AddWithValue("@Usuario", usuario.iUsuario);
            cmd.Parameters.AddWithValue("@Contrasena", usuario.Contrasenia);
            cmd.Parameters.AddWithValue("@Nombre", usuario.Nombre);
            cmd.Parameters.AddWithValue("@Apellido", usuario.Apellido);
            cmd.Parameters.AddWithValue("@Nivel", usuario.Nivel);
            cmd.Parameters.AddWithValue("@FechaAlta", setDateFromObjectClassToDBValues(usuario.FechaAlta));
            cmd.Parameters.AddWithValue("@FechaBaja", setDateFromObjectClassToDBValues(usuario.FechaBaja));
            cmd.Parameters.AddWithValue("@UltimoIngreso", setDateFromObjectClassToDBValues(usuario.UltimoIngreso));
            cmd.Parameters.AddWithValue("@UltimoEgreso", setDateFromObjectClassToDBValues(usuario.UltimoEgreso));
            cmd.Parameters.AddWithValue("@Ntdom", usuario.Ntdom);
            cmd.Parameters.AddWithValue("@Equipo", usuario.Equipo);
            cmd.Parameters.AddWithValue("@Dominio", usuario.Dominio);
            cmd.Parameters.AddWithValue("@Activo", usuario.Activo);
            cmd.Parameters.AddWithValue("@VtoClave", setDateFromObjectClassToDBValues(usuario.VtoClave));
            cmd.Parameters.AddWithValue("@Intentos", usuario.IntentosFallidos);
            cmd.Parameters.AddWithValue("@Bloqueado", usuario.Bloqueado);
            }
            catch
            {
                throw new ArgumentException ("Error con inserción de parametros");
            }

             
        }

        private object setDateFromObjectClassToDBValues(DateTime date)
        {

            if(date == new DateTime(1, 1, 1))
            {
                return DBNull.Value;
            }

            return date.ToOADate();
        
        }

        public Usuario createUsuario(Usuario usuario)
        {
            Usuario uAux = getByIdUsuario(usuario.iUsuario);

            if(uAux != null)
            {
                throw new ArgumentException("Usuario existente");
            }


            StringBuilder sb = new StringBuilder();

            sb.Append("INSERT INTO Usuarios ");

            sb.Append(Query.INSERT_USUARIO_FIELDS);

            sb.Append(" VALUES ");

            sb.Append(Query.INSERT_USUARIO_PARAMETERS);

            OleDbCommand cmd = new OleDbCommand(sb.ToString(), cn);

            setParameterFromObject(cmd, usuario);

            try
            {
                cn.Open();

                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                cn.Close();
            }

            return usuario;

        }

        public Usuario deleteUsuario(string iUsuario)
        {
            Usuario usuario = getByIdUsuario(iUsuario);

            if (usuario == null)
            {
                throw new ArgumentException("Usuario inexistente");
            }


            StringBuilder sb = new StringBuilder();

            sb.Append("DELETE FROM Usuarios ");

            sb.Append(string.Format(" WHERE Usuario ='{0}'", iUsuario));

            OleDbCommand cmd = new OleDbCommand(sb.ToString(), cn);

            try
            {
                cn.Open();

                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                cn.Close();
            }

            return usuario;



       
        }

        public List<Usuario> getUsuarios()
        {
            List<Usuario> usuarios = new List<Usuario>();

            string cmdText = "SELECT * FROM Usuarios";

            DataSet ds = new DataSet();

            try
            {

                OleDbCommand cmd = new OleDbCommand(cmdText, cn);

                OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                da.Fill(ds);

            }
            catch (Exception e)
            {
                throw e;
            }

            if(ds.Tables[0].Rows.Count == 0)
            {

                throw new DataException("No hay registros para consultar");

            }
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                usuarios.Add(getUsuarioFromDataSetRow(row));

            }

            return usuarios;

        }

        private Usuario getUsuarioFromDataSetRow(DataRow dataRow)
        {

            Usuario usuario = new Usuario();

            usuario.Id = Convert.ToInt32(dataRow["Id"].ToString());

            usuario.iUsuario = dataRow["Usuario"].ToString();

            usuario.Contrasenia = dataRow["Contrasena"].ToString();

            usuario.Nombre = dataRow["Nombre"].ToString();

            usuario.Apellido = dataRow["Apellido"].ToString();

            usuario.Nivel = dataRow["Nivel"].ToString();

            usuario.FechaAlta = setDateFromDBValuesToObjectClass(dataRow["FechaAlta"]);

            usuario.FechaBaja = setDateFromDBValuesToObjectClass(dataRow["FechaBaja"]);

            usuario.UltimoIngreso = setDateFromDBValuesToObjectClass(dataRow["UltimoIngreso"]);

            usuario.UltimoEgreso = setDateFromDBValuesToObjectClass(dataRow["UltimoEgreso"]);

            usuario.Ntdom = dataRow["Ntdom"].ToString();

            usuario.Equipo = dataRow["Equipo"].ToString();

            usuario.Dominio = dataRow["Dominio"].ToString();

            usuario.Activo = Convert.ToBoolean(dataRow["Activo"].ToString());

            usuario.VtoClave = setDateFromDBValuesToObjectClass(dataRow["VtoClave"]);

            usuario.IntentosFallidos = Convert.ToInt16(dataRow["Intentos"].ToString());

            usuario.Bloqueado = Convert.ToBoolean(dataRow["Bloqueado"].ToString());

            return usuario;

        }

        private DateTime setDateFromDBValuesToObjectClass(object dataField)
        {
            DateTime date = new DateTime();

            if(dataField != DBNull.Value)
            {
                date = Convert.ToDateTime(dataField.ToString());
            }

            return date;
        }

        public Usuario getByIdUsuario(string iUsuario)
        {

            Usuario usuario = null;

            string cmdText = "SELECT * FROM Usuarios WHERE Usuario=@usuario";

            DataSet ds = new DataSet();

            try
            {

                OleDbCommand cmd = new OleDbCommand(cmdText, cn);

                cmd.Parameters.AddWithValue("@usuario", iUsuario);

                OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                da.Fill(ds);

            }
            catch (Exception e)
            {
                throw e;
            }

            if(ds.Tables[0].Rows.Count != 0)
            {
                usuario = getUsuarioFromDataSetRow(ds.Tables[0].Rows[0]);
            }

            return usuario;
        }


    }
}
