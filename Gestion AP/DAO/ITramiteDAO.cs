﻿using System.Collections.Generic;

namespace Gestion_AP
{
    interface ITramiteDAO
    {

        Tramite createTramite(Tramite tramite);
        Tramite deleteTramite(string tramite);
        Tramite updateTramite(Tramite tramite);
        List<Tramite> getTramites();


    }
}