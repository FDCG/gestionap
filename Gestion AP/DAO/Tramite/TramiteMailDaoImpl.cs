﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    class TramiteMailDaoImpl  : TramiteDaoImpl
    {

        TramiteDaoImpl tramiteDao;

        public TramiteMailDaoImpl(TramiteDaoImpl tramiteDao)
        {
            TramiteDao = tramiteDao;
        }
        public TramiteMailDaoImpl()
        {
            TramiteDao = new TramiteDaoImpl();
        }


        public new Tramite createTramite(Tramite tramite) {

            try
            {
                tramiteDao.createTramite(tramite);
                ((TramiteMail)tramite).Archivo.SaveAs(Conexion.cnMails(tramite.NroTramite), false);

            }catch (Exception exception)
            {
                throw exception;
            }
            
            return tramite;
        }

        public TramiteMail getTramiteMailById(string nroTramite)
        {
            TramiteMail tramiteMail;
            try
            {

                tramiteMail = (TramiteMail)tramiteDao.getTramiteById(nroTramite);

                tramiteMail.Archivo.Load(Conexion.cnMails(nroTramite), false);

            }
            catch (Exception e)
            {
                throw e;
            }

            return tramiteMail;

        }





        internal TramiteDaoImpl TramiteDao { get => tramiteDao; set => tramiteDao = value; }
    }
}
