﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Gestion_AP
{
    class TramiteWfDaoImpl : TramiteDaoImpl
    {
        TramiteDaoImpl tramiteDao;

        public TramiteWfDaoImpl(TramiteDaoImpl tramiteDao)
        {
            TramiteDao = tramiteDao;
        }

        public TramiteWfDaoImpl()
        {
            TramiteDao = new TramiteDaoImpl();
        }

        public Tramite createTramite(Tramite tramite, List<Proceso> procesos)
        {


            try
            {
                tramiteDao.createTramite(tramite);


                try
                {
                    TramiteDao.Cn.Open();
                    OleDbTransaction dbTransaction;
                    dbTransaction = Cn.BeginTransaction();

                    OleDbCommand cmdTarjetas, cmdProcesos, cmdWorkflow;


                    try
                    {


                        cmdWorkflow = commandQuery(Query.INSERT_WORKFLOW_FIELDS, Query.INSERT_WORKFLOW_PARAMETERS, "WORKFLOW");
                        setParameterForWorkflow(cmdWorkflow, (TramiteWorkflow)tramite);
                        cmdWorkflow.Transaction = dbTransaction;
                        cmdWorkflow.ExecuteNonQuery();


                        foreach (Tarjeta tarjeta in ((TramiteWorkflow)tramite).Tarjetas)
                        {
                            cmdTarjetas = commandQuery(Query.INSERT_TARJETA_FIELDS, Query.INSERT_TARJETA_PARAMETERS, "Tarjetas");
                            setParameterForTarjeta(cmdTarjetas, tramite.NroTramite, tarjeta);
                            cmdTarjetas.Transaction = dbTransaction;
                            cmdTarjetas.ExecuteNonQuery();
                            cmdTarjetas.Parameters.Clear();
                        }



                        foreach (Proceso proceso in procesos)
                        {
                            cmdProcesos = commandQuery(Query.INSERT_PROCESO_FIELDS, Query.INSERT_PROCESO_PARAMETERS, "Procesos");
                            setParameterForProceso(cmdProcesos, tramite.NroTramite, proceso);
                            cmdProcesos.Transaction = dbTransaction;
                            cmdProcesos.ExecuteNonQuery();
                            cmdProcesos.Parameters.Clear();
                        }


                        dbTransaction.Commit();

                    }
                    catch (OleDbException dbException)
                    {
                        dbTransaction.Rollback();
                        throw dbException;
                    }
                    finally
                    {
                        Cn.Close();
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error al grabar en BBDD. " + e.Message);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }




            return tramite;
        }

        private OleDbCommand commandQuery(string insertStatement, string parameterStatement, string table)
        {

            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("INSERT INTO {0} ", table));

            sb.Append(insertStatement);

            sb.Append(" VALUES ");

            sb.Append(parameterStatement);

            OleDbCommand dbCommand = new OleDbCommand(sb.ToString(), Cn);

            return dbCommand;
        }
        private void setParameterForProceso(OleDbCommand cmd, string nroTramite, Proceso proceso)
        {
            try
            {
                cmd.Parameters.AddWithValue("@NroTramite", nroTramite);
                cmd.Parameters.AddWithValue("@Fecha", proceso.Fecha.ToOADate());
                cmd.Parameters.AddWithValue("@Usuario", proceso.Usuario);
                cmd.Parameters.AddWithValue("@Accion", proceso.AccionProceso);
                cmd.Parameters.AddWithValue("@Detalle", proceso.DetalleProceso);
            }
            catch
            {
                throw new ArgumentException(string.Format("Error con inserción de parametros Procesos"));
            }
        }
        private void setParameterForTarjeta(OleDbCommand cmd, string nroTramite, Tarjeta tarjeta)
        {
            try
            {
                cmd.Parameters.AddWithValue("@NroTramite", nroTramite);
                cmd.Parameters.AddWithValue("@NroTarjeta", tarjeta.NroTarjeta);
                cmd.Parameters.AddWithValue("@Categoria", tarjeta.CategoriaTarjeta);
                cmd.Parameters.AddWithValue("@NombreTJ", tarjeta.NroTarjeta);
                cmd.Parameters.AddWithValue("@TipoDoc", tarjeta.TipoDocumento);
                cmd.Parameters.AddWithValue("@NroDoc", tarjeta.NroDocumento);
                cmd.Parameters.AddWithValue("@Vigencia", tarjeta.VigenciaTarjeta);
                cmd.Parameters.AddWithValue("@Estado", tarjeta.EstadoTarjeta);
            }

            catch
            {
                throw new ArgumentException(string.Format("Error con inserción de parametros Tarjetas"));
            }
        }

        private void setParameterForWorkflow(OleDbCommand cmd, TramiteWorkflow tramite)
        {

            try
            {
                cmd.Parameters.AddWithValue("@NroTramite", tramite.NroTramite);
                cmd.Parameters.AddWithValue("@CuilPersona", tramite.Filiatorio.Cuil);
                cmd.Parameters.AddWithValue("@NomPersona", tramite.Filiatorio.Nombre);
                cmd.Parameters.AddWithValue("@NroCliente", tramite.Filiatorio.Cliente);
                cmd.Parameters.AddWithValue("@NomCliente", tramite.Filiatorio.DenominacionCliente);
                cmd.Parameters.AddWithValue("@NroCtaTJ", tramite.CuentaTarjeta.NroCuentaTarjeta);
                cmd.Parameters.AddWithValue("@TitCtaTJ", tramite.CuentaTarjeta.TitularCuenta);
                cmd.Parameters.AddWithValue("@NroTJ", tramite.CuentaTarjeta.Tarjeta);
                cmd.Parameters.AddWithValue("@EmbozadoTJ", tramite.CuentaTarjeta.EmbozadoTarjeta);
                cmd.Parameters.AddWithValue("@TipoTJ", tramite.CuentaTarjeta.TipoTarjeta);
                cmd.Parameters.AddWithValue("@CtaDebito", tramite.CuentaTarjeta.CuentaDebito);
                cmd.Parameters.AddWithValue("@FormaPago", tramite.CuentaTarjeta.FormaDePago);
                cmd.Parameters.AddWithValue("@LimiteTJ", tramite.CuentaTarjeta.LimiteTarjeta);
                cmd.Parameters.AddWithValue("@LimiteAut", tramite.CuentaTarjeta.LimiteAutorizado);
                cmd.Parameters.AddWithValue("@DomEspecial", tramite.Distribucion.DomicilioEspecial);
                cmd.Parameters.AddWithValue("@SucEntrega", tramite.Distribucion.SucursalEntrega);
                cmd.Parameters.AddWithValue("@DatosAdic", tramite.DatosAdicionales);
                cmd.Parameters.AddWithValue("@MotivoTramite", tramite.MotivoTramite);
                cmd.Parameters.AddWithValue("@ObsTramite", tramite.ObservacionesTramite);

            }
            catch
            {
                throw new ArgumentException(string.Format("Error con inserción de parametros Workflow"));
            }


        }

        public TramiteWorkflow getTramiteWorkflow (Tramite tramite)
        {
            if (tramite == null) throw new Exception("No se pudo obtener trámite");

            TramiteWorkflow tramiteWf = getFullTramiteById(tramite.NroTramite);
            foreach (PropertyInfo prop in tramite.GetType().GetProperties())
            {

                tramiteWf.GetType().GetProperty(prop.Name).SetValue(tramiteWf, prop.GetValue(tramite, null), null);

            }

            return tramiteWf;
        }

        public new TramiteWorkflow getFullTramiteById(string nroTramite)
        {
            TramiteWorkflow tramiteWf = getTramiteWfById(nroTramite);

            TarjetaDaoImpl tarjetaDaoImpl = new TarjetaDaoImpl(Cn);

            try
            {
                tramiteWf.Tarjetas = tarjetaDaoImpl.getTarjetasById(nroTramite);
            }
            catch (Exception e)
            {

                throw e;
            }

            return tramiteWf;
        }

        public TramiteWorkflow getTramiteWfById(string nroTramite)
        {
            TramiteWorkflow tramiteWf;



            string cmdText = "SELECT * FROM Workflow WHERE NroTramite=@nroTramite";

            DataSet ds = new DataSet();

            try
            {

                OleDbCommand cmd = new OleDbCommand(cmdText, Cn);

                cmd.Parameters.AddWithValue("@nroTramite", nroTramite);

                OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                da.Fill(ds);

            }
            catch (Exception e)
            {
                throw e;
            }

            if (ds.Tables[0].Rows.Count != 0)
            {

                tramiteWf = setTramiteWorkflowFromDataSetRow(ds.Tables[0].Rows[0]);
 
            }
            else
            {
                throw new Exception("No se obtuvo Workflow");
            }

            return tramiteWf;

        }



        private TramiteWorkflow setTramiteWorkflowFromDataSetRow(DataRow dataRow)
        {
            TramiteWorkflow tramiteWf = new TramiteWorkflow();

            tramiteWf.Filiatorio.Cuil = dataRow["CuilPersona"].ToString();

            tramiteWf.Filiatorio.Nombre = dataRow["NomPersona"].ToString();

            tramiteWf.Filiatorio.Cliente = dataRow["NroCliente"].ToString();

            tramiteWf.Filiatorio.DenominacionCliente = dataRow["NomCliente"].ToString();

            tramiteWf.CuentaTarjeta.NroCuentaTarjeta = dataRow["NroCtaTJ"].ToString();

            tramiteWf.CuentaTarjeta.Tarjeta = dataRow["NroTJ"].ToString();

            tramiteWf.CuentaTarjeta.TitularCuenta = dataRow["TitCtaTJ"].ToString();

            tramiteWf.CuentaTarjeta.EmbozadoTarjeta = dataRow["EmbozadoTJ"].ToString();

            tramiteWf.CuentaTarjeta.TipoTarjeta = dataRow["TipoTJ"].ToString();

            tramiteWf.CuentaTarjeta.LimiteTarjeta = dataRow["LimiteTJ"].ToString();

            tramiteWf.CuentaTarjeta.LimiteAutorizado = dataRow["LimiteAut"].ToString();

            tramiteWf.CuentaTarjeta.FormaDePago = dataRow["FormaPago"].ToString();

            tramiteWf.CuentaTarjeta.CuentaDebito = dataRow["CtaDebito"].ToString();

            tramiteWf.Distribucion.DomicilioEspecial = dataRow["DomEspecial"].ToString();

            tramiteWf.Distribucion.SucursalEntrega = dataRow["SucEntrega"].ToString();

            tramiteWf.DatosAdicionales = dataRow["DatosAdic"].ToString();

            tramiteWf.MotivoTramite = dataRow["MotivoTramite"].ToString();

            tramiteWf.ObservacionesTramite = dataRow["ObsTramite"].ToString();

            return tramiteWf;

        }



        internal TramiteDaoImpl TramiteDao { get => tramiteDao; set => tramiteDao = value; }
    }
}
