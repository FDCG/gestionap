﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    interface IUsuarioDAO
    {

        Usuario createUsuario(Usuario usuario);
        Usuario deleteUsuario(string usuario);
        Usuario updateUsuario(Usuario usuario);
        List<Usuario> getUsuarios();


    }
}
