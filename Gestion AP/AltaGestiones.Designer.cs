﻿namespace Gestion_AP
{
    partial class AltaGestiones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AltaGestiones));
            this.btnBajas = new System.Windows.Forms.Button();
            this.btnAdjunto = new System.Windows.Forms.Button();
            this.btnImagen = new System.Windows.Forms.Button();
            this.btnMail = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnPurpura = new System.Windows.Forms.Button();
            this.btnPrevalid = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lblDescripcion = new System.Windows.Forms.TextBox();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBajas
            // 
            this.btnBajas.BackColor = System.Drawing.Color.Transparent;
            this.btnBajas.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBajas.Image = global::Gestion_AP.Properties.Resources.Material_Icons_e5c6_0__128;
            this.btnBajas.Location = new System.Drawing.Point(6, 155);
            this.btnBajas.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBajas.Name = "btnBajas";
            this.btnBajas.Size = new System.Drawing.Size(131, 127);
            this.btnBajas.TabIndex = 5;
            this.btnBajas.UseVisualStyleBackColor = false;
            this.btnBajas.Click += new System.EventHandler(this.btnBajas_Click);
            this.btnBajas.MouseEnter += new System.EventHandler(this.mouseEnter);
            this.btnBajas.MouseLeave += new System.EventHandler(this.mouseLeave);
            // 
            // btnAdjunto
            // 
            this.btnAdjunto.BackColor = System.Drawing.Color.Transparent;
            this.btnAdjunto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdjunto.Image = global::Gestion_AP.Properties.Resources.Iconic_e08a_0__128;
            this.btnAdjunto.Location = new System.Drawing.Point(6, 21);
            this.btnAdjunto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAdjunto.Name = "btnAdjunto";
            this.btnAdjunto.Size = new System.Drawing.Size(131, 127);
            this.btnAdjunto.TabIndex = 4;
            this.btnAdjunto.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAdjunto.UseVisualStyleBackColor = false;
            this.btnAdjunto.Click += new System.EventHandler(this.btnAdjunto_Click);
            this.btnAdjunto.MouseEnter += new System.EventHandler(this.mouseEnter);
            this.btnAdjunto.MouseLeave += new System.EventHandler(this.mouseLeave);
            // 
            // btnImagen
            // 
            this.btnImagen.BackColor = System.Drawing.Color.Transparent;
            this.btnImagen.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImagen.Image = global::Gestion_AP.Properties.Resources.alt_image;
            this.btnImagen.Location = new System.Drawing.Point(6, 290);
            this.btnImagen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnImagen.Name = "btnImagen";
            this.btnImagen.Size = new System.Drawing.Size(131, 127);
            this.btnImagen.TabIndex = 6;
            this.btnImagen.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnImagen.UseVisualStyleBackColor = false;
            this.btnImagen.Click += new System.EventHandler(this.btnImagen_Click);
            this.btnImagen.MouseEnter += new System.EventHandler(this.mouseEnter);
            this.btnImagen.MouseLeave += new System.EventHandler(this.mouseLeave);
            // 
            // btnMail
            // 
            this.btnMail.BackColor = System.Drawing.Color.Transparent;
            this.btnMail.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMail.Image = global::Gestion_AP.Properties.Resources.Iconic_2709_0__128;
            this.btnMail.Location = new System.Drawing.Point(6, 290);
            this.btnMail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMail.Name = "btnMail";
            this.btnMail.Size = new System.Drawing.Size(131, 127);
            this.btnMail.TabIndex = 3;
            this.btnMail.UseVisualStyleBackColor = false;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            this.btnMail.MouseEnter += new System.EventHandler(this.mouseEnter);
            this.btnMail.MouseLeave += new System.EventHandler(this.mouseLeave);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.btnBajas);
            this.groupBox5.Controls.Add(this.btnImagen);
            this.groupBox5.Controls.Add(this.btnAdjunto);
            this.groupBox5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox5.Location = new System.Drawing.Point(537, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(143, 427);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            // 
            // btnPurpura
            // 
            this.btnPurpura.BackColor = System.Drawing.Color.Transparent;
            this.btnPurpura.BackgroundImage = global::Gestion_AP.Properties.Resources.Entypo_e731_0__128;
            this.btnPurpura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPurpura.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPurpura.Location = new System.Drawing.Point(6, 21);
            this.btnPurpura.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPurpura.Name = "btnPurpura";
            this.btnPurpura.Size = new System.Drawing.Size(131, 127);
            this.btnPurpura.TabIndex = 1;
            this.btnPurpura.UseVisualStyleBackColor = false;
            this.btnPurpura.Click += new System.EventHandler(this.btnPurpura_Click);
            this.btnPurpura.MouseEnter += new System.EventHandler(this.mouseEnter);
            this.btnPurpura.MouseLeave += new System.EventHandler(this.mouseLeave);
            // 
            // btnPrevalid
            // 
            this.btnPrevalid.BackColor = System.Drawing.Color.Transparent;
            this.btnPrevalid.BackgroundImage = global::Gestion_AP.Properties.Resources.alt_prevalid;
            this.btnPrevalid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPrevalid.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevalid.Location = new System.Drawing.Point(6, 155);
            this.btnPrevalid.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPrevalid.Name = "btnPrevalid";
            this.btnPrevalid.Size = new System.Drawing.Size(131, 127);
            this.btnPrevalid.TabIndex = 2;
            this.btnPrevalid.UseVisualStyleBackColor = false;
            this.btnPrevalid.Click += new System.EventHandler(this.btnPrevalid_Click);
            this.btnPrevalid.MouseEnter += new System.EventHandler(this.mouseEnter);
            this.btnPrevalid.MouseLeave += new System.EventHandler(this.mouseLeave);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnPrevalid);
            this.groupBox1.Controls.Add(this.btnPurpura);
            this.groupBox1.Controls.Add(this.btnMail);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(145, 427);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.BackColor = System.Drawing.SystemColors.Menu;
            this.lblDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblDescripcion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion.Location = new System.Drawing.Point(192, 221);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.ReadOnly = true;
            this.lblDescripcion.Size = new System.Drawing.Size(304, 16);
            this.lblDescripcion.TabIndex = 12;
            this.lblDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AltaGestiones
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(692, 452);
            this.Controls.Add(this.lblDescripcion);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "AltaGestiones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alta de Gestiones";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AltaGestiones_FormClosed);
            this.Load += new System.EventHandler(this.AltaGestiones_Load);
            this.groupBox5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnMail;
        private System.Windows.Forms.Button btnImagen;
        private System.Windows.Forms.Button btnAdjunto;
        private System.Windows.Forms.Button btnBajas;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnPurpura;
        private System.Windows.Forms.Button btnPrevalid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox lblDescripcion;
    }
}