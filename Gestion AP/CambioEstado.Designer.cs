﻿namespace Gestion_AP
{
    partial class CambioEstado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CambioEstado));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Motivo = new System.Windows.Forms.GroupBox();
            this.txtMotivoEstado = new System.Windows.Forms.RichTextBox();
            this.btnAplicarEst = new System.Windows.Forms.Button();
            this.rbtCerrado = new System.Windows.Forms.RadioButton();
            this.rbtRechazado = new System.Windows.Forms.RadioButton();
            this.rbtAbierto = new System.Windows.Forms.RadioButton();
            this.txtTramEstado = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtMotivoPrioridad = new System.Windows.Forms.RichTextBox();
            this.btnAplicarPrio = new System.Windows.Forms.Button();
            this.rbtLiberacion = new System.Windows.Forms.RadioButton();
            this.rbtUrgente = new System.Windows.Forms.RadioButton();
            this.rbtNormal = new System.Windows.Forms.RadioButton();
            this.txtTramPrioridad = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtOperador = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbUsuarios = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtMotivoDelega = new System.Windows.Forms.RichTextBox();
            this.btnDelegacion = new System.Windows.Forms.Button();
            this.txtTramDelega = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox1.SuspendLayout();
            this.Motivo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.groupBox1.Controls.Add(this.Motivo);
            this.groupBox1.Controls.Add(this.btnAplicarEst);
            this.groupBox1.Controls.Add(this.rbtCerrado);
            this.groupBox1.Controls.Add(this.rbtRechazado);
            this.groupBox1.Controls.Add(this.rbtAbierto);
            this.groupBox1.Controls.Add(this.txtTramEstado);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(665, 156);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // Motivo
            // 
            this.Motivo.Controls.Add(this.txtMotivoEstado);
            this.Motivo.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Motivo.Location = new System.Drawing.Point(378, 19);
            this.Motivo.Name = "Motivo";
            this.Motivo.Size = new System.Drawing.Size(274, 119);
            this.Motivo.TabIndex = 6;
            this.Motivo.TabStop = false;
            this.Motivo.Text = "Motivo";
            // 
            // txtMotivoEstado
            // 
            this.txtMotivoEstado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMotivoEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivoEstado.Location = new System.Drawing.Point(3, 17);
            this.txtMotivoEstado.Name = "txtMotivoEstado";
            this.txtMotivoEstado.Size = new System.Drawing.Size(268, 99);
            this.txtMotivoEstado.TabIndex = 0;
            this.txtMotivoEstado.Text = "";
            // 
            // btnAplicarEst
            // 
            this.btnAplicarEst.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAplicarEst.Location = new System.Drawing.Point(124, 123);
            this.btnAplicarEst.Name = "btnAplicarEst";
            this.btnAplicarEst.Size = new System.Drawing.Size(243, 23);
            this.btnAplicarEst.TabIndex = 5;
            this.btnAplicarEst.Text = "Aplicar Cambio de Estado";
            this.btnAplicarEst.UseVisualStyleBackColor = true;
            this.btnAplicarEst.Click += new System.EventHandler(this.btnAplicarEst_Click);
            // 
            // rbtCerrado
            // 
            this.rbtCerrado.AutoSize = true;
            this.rbtCerrado.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtCerrado.Location = new System.Drawing.Point(9, 94);
            this.rbtCerrado.Name = "rbtCerrado";
            this.rbtCerrado.Size = new System.Drawing.Size(245, 19);
            this.rbtCerrado.TabIndex = 4;
            this.rbtCerrado.TabStop = true;
            this.rbtCerrado.Text = "CERRADO       --------> Trámite Finalizado";
            this.rbtCerrado.UseVisualStyleBackColor = true;
            // 
            // rbtRechazado
            // 
            this.rbtRechazado.AutoSize = true;
            this.rbtRechazado.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtRechazado.ForeColor = System.Drawing.Color.Red;
            this.rbtRechazado.Location = new System.Drawing.Point(9, 71);
            this.rbtRechazado.Name = "rbtRechazado";
            this.rbtRechazado.Size = new System.Drawing.Size(325, 19);
            this.rbtRechazado.TabIndex = 3;
            this.rbtRechazado.TabStop = true;
            this.rbtRechazado.Text = "RECHAZADO  --------> Ingresa en Bandeja de Rechazos";
            this.rbtRechazado.UseVisualStyleBackColor = true;
            // 
            // rbtAbierto
            // 
            this.rbtAbierto.AutoSize = true;
            this.rbtAbierto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtAbierto.ForeColor = System.Drawing.Color.Green;
            this.rbtAbierto.Location = new System.Drawing.Point(9, 48);
            this.rbtAbierto.Name = "rbtAbierto";
            this.rbtAbierto.Size = new System.Drawing.Size(312, 19);
            this.rbtAbierto.TabIndex = 2;
            this.rbtAbierto.TabStop = true;
            this.rbtAbierto.Text = "ABIERTO          --------> Ingresa en Bandeja de Control";
            this.rbtAbierto.UseVisualStyleBackColor = true;
            // 
            // txtTramEstado
            // 
            this.txtTramEstado.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTramEstado.Location = new System.Drawing.Point(125, 22);
            this.txtTramEstado.Name = "txtTramEstado";
            this.txtTramEstado.Size = new System.Drawing.Size(243, 21);
            this.txtTramEstado.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "NRO. TRÁMITE ..:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Menu;
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.btnAplicarPrio);
            this.groupBox2.Controls.Add(this.rbtLiberacion);
            this.groupBox2.Controls.Add(this.rbtUrgente);
            this.groupBox2.Controls.Add(this.rbtNormal);
            this.groupBox2.Controls.Add(this.txtTramPrioridad);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(676, 159);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtMotivoPrioridad);
            this.groupBox3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(378, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(273, 119);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Motivo";
            // 
            // txtMotivoPrioridad
            // 
            this.txtMotivoPrioridad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMotivoPrioridad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivoPrioridad.Location = new System.Drawing.Point(3, 17);
            this.txtMotivoPrioridad.Name = "txtMotivoPrioridad";
            this.txtMotivoPrioridad.Size = new System.Drawing.Size(267, 99);
            this.txtMotivoPrioridad.TabIndex = 0;
            this.txtMotivoPrioridad.Text = "";
            // 
            // btnAplicarPrio
            // 
            this.btnAplicarPrio.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAplicarPrio.Location = new System.Drawing.Point(124, 123);
            this.btnAplicarPrio.Name = "btnAplicarPrio";
            this.btnAplicarPrio.Size = new System.Drawing.Size(244, 23);
            this.btnAplicarPrio.TabIndex = 12;
            this.btnAplicarPrio.Text = "Aplicar Cambio de Prioridad";
            this.btnAplicarPrio.UseVisualStyleBackColor = true;
            this.btnAplicarPrio.Click += new System.EventHandler(this.btnAplicarPrio_Click);
            // 
            // rbtLiberacion
            // 
            this.rbtLiberacion.AutoSize = true;
            this.rbtLiberacion.BackColor = System.Drawing.SystemColors.Menu;
            this.rbtLiberacion.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtLiberacion.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rbtLiberacion.Location = new System.Drawing.Point(125, 75);
            this.rbtLiberacion.Name = "rbtLiberacion";
            this.rbtLiberacion.Size = new System.Drawing.Size(117, 19);
            this.rbtLiberacion.TabIndex = 11;
            this.rbtLiberacion.TabStop = true;
            this.rbtLiberacion.Text = "CON LIBERACIÓN";
            this.rbtLiberacion.UseVisualStyleBackColor = false;
            // 
            // rbtUrgente
            // 
            this.rbtUrgente.AutoSize = true;
            this.rbtUrgente.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtUrgente.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rbtUrgente.Location = new System.Drawing.Point(126, 98);
            this.rbtUrgente.Name = "rbtUrgente";
            this.rbtUrgente.Size = new System.Drawing.Size(74, 19);
            this.rbtUrgente.TabIndex = 10;
            this.rbtUrgente.TabStop = true;
            this.rbtUrgente.Text = "URGENTE";
            this.rbtUrgente.UseVisualStyleBackColor = true;
            // 
            // rbtNormal
            // 
            this.rbtNormal.AutoSize = true;
            this.rbtNormal.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtNormal.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rbtNormal.Location = new System.Drawing.Point(125, 52);
            this.rbtNormal.Name = "rbtNormal";
            this.rbtNormal.Size = new System.Drawing.Size(73, 19);
            this.rbtNormal.TabIndex = 9;
            this.rbtNormal.TabStop = true;
            this.rbtNormal.Text = "NORMAL";
            this.rbtNormal.UseVisualStyleBackColor = true;
            // 
            // txtTramPrioridad
            // 
            this.txtTramPrioridad.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTramPrioridad.Location = new System.Drawing.Point(125, 22);
            this.txtTramPrioridad.Name = "txtTramPrioridad";
            this.txtTramPrioridad.Size = new System.Drawing.Size(243, 21);
            this.txtTramPrioridad.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "NRO. TRÁMITE ..:";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Menu;
            this.groupBox4.Controls.Add(this.txtOperador);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.cmbUsuarios);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.btnDelegacion);
            this.groupBox4.Controls.Add(this.txtTramDelega);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(676, 168);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            // 
            // txtOperador
            // 
            this.txtOperador.AutoSize = true;
            this.txtOperador.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOperador.Location = new System.Drawing.Point(124, 91);
            this.txtOperador.Name = "txtOperador";
            this.txtOperador.Size = new System.Drawing.Size(0, 16);
            this.txtOperador.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 16);
            this.label5.TabIndex = 17;
            this.label5.Text = "OPERADOR .......:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "USUARIO ..........:";
            // 
            // cmbUsuarios
            // 
            this.cmbUsuarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUsuarios.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUsuarios.FormattingEnabled = true;
            this.cmbUsuarios.Location = new System.Drawing.Point(124, 55);
            this.cmbUsuarios.Name = "cmbUsuarios";
            this.cmbUsuarios.Size = new System.Drawing.Size(244, 24);
            this.cmbUsuarios.TabIndex = 14;
            this.cmbUsuarios.SelectedValueChanged += new System.EventHandler(this.cmbUsuarios_SelectedValueChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtMotivoDelega);
            this.groupBox5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(378, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(273, 119);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Motivo";
            // 
            // txtMotivoDelega
            // 
            this.txtMotivoDelega.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMotivoDelega.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivoDelega.Location = new System.Drawing.Point(3, 17);
            this.txtMotivoDelega.Name = "txtMotivoDelega";
            this.txtMotivoDelega.Size = new System.Drawing.Size(267, 99);
            this.txtMotivoDelega.TabIndex = 0;
            this.txtMotivoDelega.Text = "";
            // 
            // btnDelegacion
            // 
            this.btnDelegacion.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelegacion.Location = new System.Drawing.Point(124, 123);
            this.btnDelegacion.Name = "btnDelegacion";
            this.btnDelegacion.Size = new System.Drawing.Size(244, 23);
            this.btnDelegacion.TabIndex = 12;
            this.btnDelegacion.Text = "Aplicar Delegación";
            this.btnDelegacion.UseVisualStyleBackColor = true;
            this.btnDelegacion.Click += new System.EventHandler(this.btnDelegacion_Click);
            // 
            // txtTramDelega
            // 
            this.txtTramDelega.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTramDelega.Location = new System.Drawing.Point(125, 22);
            this.txtTramDelega.Name = "txtTramDelega";
            this.txtTramDelega.Size = new System.Drawing.Size(244, 21);
            this.txtTramDelega.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "NRO. TRÁMITE ..:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(1, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(679, 188);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(668, 159);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ESTADO";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(668, 159);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "PRIORIDAD";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(671, 159);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "DELEGACIÓN";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // CambioEstado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(678, 188);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CambioEstado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificar Trámite";
            this.Load += new System.EventHandler(this.CambioEstado_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Motivo.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox Motivo;
        private System.Windows.Forms.RichTextBox txtMotivoEstado;
        private System.Windows.Forms.Button btnAplicarEst;
        private System.Windows.Forms.RadioButton rbtCerrado;
        private System.Windows.Forms.RadioButton rbtRechazado;
        private System.Windows.Forms.RadioButton rbtAbierto;
        private System.Windows.Forms.TextBox txtTramEstado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox txtMotivoPrioridad;
        private System.Windows.Forms.Button btnAplicarPrio;
        private System.Windows.Forms.RadioButton rbtLiberacion;
        private System.Windows.Forms.RadioButton rbtUrgente;
        private System.Windows.Forms.RadioButton rbtNormal;
        private System.Windows.Forms.TextBox txtTramPrioridad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label txtOperador;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbUsuarios;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RichTextBox txtMotivoDelega;
        private System.Windows.Forms.Button btnDelegacion;
        private System.Windows.Forms.TextBox txtTramDelega;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
    }
}