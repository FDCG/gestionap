﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Permissions;
using System.IO;


namespace Gestion_AP
{
    public partial class AltaGestiones : Form
    {
       // [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]

        string [] descripcionBotones = {"Trámites WorkFlow TJ Varios TJ** - TR**",
                                        "Trámites WorkFlow Prevalidación y Rechazos",
                                        "Trámites E-Mails Lotus Notes",
                                        "Archivos Adjuntos",
                                        "Bajas y No Renovaciones",
                                        "Imagenes y Capturas"
                                        };

        public AltaGestiones()
        {
            InitializeComponent();
           
            
       
            
        }

        private void btnImagen_Click(object sender, EventArgs e)
        {
            GestionImagen frm = new GestionImagen();
            frm.Show();

        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            Mails frm = new Mails();
            frm.Show();

        }









        public static bool cargarconcambio = false;
        public static string nuevopath = "";
       

        private void AltaGestiones_Load(object sender, EventArgs e)
        {
           
        }

        private void  mouseLeave(object sender, EventArgs e)
        {
            lblDescripcion.Text = "";
        }

        private void mouseEnter(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            string desc =  btn.Name.ToString();

            switch (desc)
            {
                case "btnBajas":
                    lblDescripcion.Text = descripcionBotones[4];
                    break;
                case "btnPurpura":
                    lblDescripcion.Text = descripcionBotones[0];
                    break;
                case "btnPrevalid":
                    lblDescripcion.Text = descripcionBotones[1];
                    break;
                case "btnMail":
                    lblDescripcion.Text = descripcionBotones[2];
                    break;
                case "btnAdjunto":
                    lblDescripcion.Text = descripcionBotones[3];
                    break;
                case "btnImagen":
                    lblDescripcion.Text = descripcionBotones[5];
                    break;
                default:
                    break;

            }


        }

        //private void btnWorkFlow_Click(object sender, EventArgs e)
        //{
        //    ModuloGris frm = new ModuloGris();
        //    frm.Show();

        //}

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

        }

        private void btnBajas_Click(object sender, EventArgs e)
        {

            BajasFall frm = new BajasFall();
            frm.Show();

        }

        private void btnAltasWF_Click(object sender, EventArgs e)
        {
            ModuloVerde frm = new ModuloVerde();
            frm.Show();
        }

        private void AltaGestiones_FormClosed(object sender, FormClosedEventArgs e)
        {
            Principal frm = new Principal();
            frm.Show();
        }

        private void btnAdjunto_Click(object sender, EventArgs e)
        {
            Adjuntos frm = new Adjuntos();
            frm.Show();
        }

        private void btnPurpura_Click(object sender, EventArgs e)
        {
            ModuloPurpura frm = new ModuloPurpura();
            frm.Show();

        }

        private void btnPrevalid_Click(object sender, EventArgs e)
        {
               ModuloPrevalid frm = new ModuloPrevalid();
            frm.Show();

        }
    }
}
