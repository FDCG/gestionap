﻿namespace Gestion_AP
{
    partial class BajasFall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BajasFall));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAdm = new System.Windows.Forms.TextBox();
            this.txtCuenta = new System.Windows.Forms.TextBox();
            this.txtTarjeta = new System.Windows.Forms.TextBox();
            this.txtSucursal = new System.Windows.Forms.TextBox();
            this.btnCaptura = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.txtGrabar = new System.Windows.Forms.Button();
            this.txtDenominacion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chbNoRen = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtComAdic = new System.Windows.Forms.TextBox();
            this.gbHistorial = new System.Windows.Forms.GroupBox();
            this.dgvHistorial = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.gbHistorial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorial)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ADMINISTRADORA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(42, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "NRO CUENTA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(62, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "SUCURSAL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(73, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "TARJETA";
            // 
            // txtAdm
            // 
            this.txtAdm.Location = new System.Drawing.Point(134, 14);
            this.txtAdm.Name = "txtAdm";
            this.txtAdm.Size = new System.Drawing.Size(386, 23);
            this.txtAdm.TabIndex = 1;
            // 
            // txtCuenta
            // 
            this.txtCuenta.Location = new System.Drawing.Point(134, 73);
            this.txtCuenta.Name = "txtCuenta";
            this.txtCuenta.Size = new System.Drawing.Size(386, 23);
            this.txtCuenta.TabIndex = 3;
            // 
            // txtTarjeta
            // 
            this.txtTarjeta.Location = new System.Drawing.Point(134, 130);
            this.txtTarjeta.Name = "txtTarjeta";
            this.txtTarjeta.Size = new System.Drawing.Size(386, 23);
            this.txtTarjeta.TabIndex = 5;
            // 
            // txtSucursal
            // 
            this.txtSucursal.Location = new System.Drawing.Point(134, 101);
            this.txtSucursal.Name = "txtSucursal";
            this.txtSucursal.Size = new System.Drawing.Size(386, 23);
            this.txtSucursal.TabIndex = 4;
            // 
            // btnCaptura
            // 
            this.btnCaptura.Location = new System.Drawing.Point(526, 41);
            this.btnCaptura.Name = "btnCaptura";
            this.btnCaptura.Size = new System.Drawing.Size(139, 23);
            this.btnCaptura.TabIndex = 8;
            this.btnCaptura.Text = "CAPTURAR DATOS";
            this.btnCaptura.UseVisualStyleBackColor = true;
            this.btnCaptura.Click += new System.EventHandler(this.btnCaptura_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(526, 70);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(139, 23);
            this.btnBorrar.TabIndex = 9;
            this.btnBorrar.Text = "BORRAR";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // txtGrabar
            // 
            this.txtGrabar.Location = new System.Drawing.Point(526, 99);
            this.txtGrabar.Name = "txtGrabar";
            this.txtGrabar.Size = new System.Drawing.Size(139, 23);
            this.txtGrabar.TabIndex = 10;
            this.txtGrabar.Text = "GRABAR TRAMITE";
            this.txtGrabar.UseVisualStyleBackColor = true;
            this.txtGrabar.Click += new System.EventHandler(this.txtGrabar_Click);
            // 
            // txtDenominacion
            // 
            this.txtDenominacion.Location = new System.Drawing.Point(134, 44);
            this.txtDenominacion.Name = "txtDenominacion";
            this.txtDenominacion.Size = new System.Drawing.Size(386, 23);
            this.txtDenominacion.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "DENOMINACION";
            // 
            // chbNoRen
            // 
            this.chbNoRen.AutoSize = true;
            this.chbNoRen.Checked = true;
            this.chbNoRen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbNoRen.Location = new System.Drawing.Point(527, 18);
            this.chbNoRen.Name = "chbNoRen";
            this.chbNoRen.Size = new System.Drawing.Size(141, 21);
            this.chbNoRen.TabIndex = 7;
            this.chbNoRen.Text = "NO RENOVACIÓN";
            this.chbNoRen.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtComAdic);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(134, 160);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(386, 108);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Comentarios Adicionales";
            // 
            // txtComAdic
            // 
            this.txtComAdic.Location = new System.Drawing.Point(6, 22);
            this.txtComAdic.Multiline = true;
            this.txtComAdic.Name = "txtComAdic";
            this.txtComAdic.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtComAdic.Size = new System.Drawing.Size(374, 80);
            this.txtComAdic.TabIndex = 6;
            // 
            // gbHistorial
            // 
            this.gbHistorial.Controls.Add(this.dgvHistorial);
            this.gbHistorial.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbHistorial.Location = new System.Drawing.Point(17, 274);
            this.gbHistorial.Name = "gbHistorial";
            this.gbHistorial.Size = new System.Drawing.Size(648, 124);
            this.gbHistorial.TabIndex = 43;
            this.gbHistorial.TabStop = false;
            this.gbHistorial.Text = "Historial Proceso";
            this.gbHistorial.Visible = false;
            // 
            // dgvHistorial
            // 
            this.dgvHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistorial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvHistorial.Location = new System.Drawing.Point(3, 17);
            this.dgvHistorial.Name = "dgvHistorial";
            this.dgvHistorial.Size = new System.Drawing.Size(642, 104);
            this.dgvHistorial.TabIndex = 0;
            // 
            // BajasFall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(681, 274);
            this.Controls.Add(this.gbHistorial);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chbNoRen);
            this.Controls.Add(this.txtDenominacion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtGrabar);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnCaptura);
            this.Controls.Add(this.txtTarjeta);
            this.Controls.Add(this.txtSucursal);
            this.Controls.Add(this.txtCuenta);
            this.Controls.Add(this.txtAdm);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "BajasFall";
            this.Text = "Bajas y No Renovaciones";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbHistorial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorial)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAdm;
        private System.Windows.Forms.TextBox txtCuenta;
        private System.Windows.Forms.TextBox txtTarjeta;
        private System.Windows.Forms.TextBox txtSucursal;
        private System.Windows.Forms.Button btnCaptura;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button txtGrabar;
        private System.Windows.Forms.TextBox txtDenominacion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chbNoRen;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtComAdic;
        private System.Windows.Forms.GroupBox gbHistorial;
        private System.Windows.Forms.DataGridView dgvHistorial;
    }
}