﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Gestion_AP
{
    public partial class ModuloPrevalid : Form
    {
        public ModuloPrevalid()
        {
            InitializeComponent();
        }

        public ModuloPrevalid(string IdTramite, string Estado, string Prioridad)
        {

            InitializeComponent();
            btnGuardar.Click -= new EventHandler(btnGuardar_Click);
            btnCargar.Click -= new EventHandler(btnCargar_Click);
            chbLiberacion.Enabled = false;

            if (Estado == "ABIERTO")
            {
                btnGuardar.Text = "CONTROLADO";

                btnGuardar.ForeColor = Color.Green;
                btnGuardar.Font = new Font(btnGuardar.Font, FontStyle.Bold);
                btnCargar.Text = "REPROCESAR";

                btnCargar.ForeColor = Color.Red;
                btnCargar.Font = new Font(btnCargar.Font, FontStyle.Bold);
                this.Text = "Control: " + IdTramite;
                btnGuardar.Click += new EventHandler(Controlar);
                btnCargar.Click += new EventHandler(Reprocesar);
                this.Icon = Gestion_AP.Properties.Resources.Icono;

            }
            if (Estado == "RECHAZADO")
            {
                btnGuardar.Text = "PROCESAR";

                btnGuardar.ForeColor = Color.Green;
                btnGuardar.Font = new Font(btnGuardar.Font, FontStyle.Bold);
                btnCargar.Visible = false;
                btnGuardar.Click += new EventHandler(Corregido);
                this.Icon = Gestion_AP.Properties.Resources.ntfPendientes;
                this.Text = "Rechazo: " + IdTramite;



            }
            txtPrioridad.Text = Prioridad;

            CargarTramite(IdTramite);
        }

        private void Controlar(object sender, EventArgs e)
        {
            try
            {

                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST WHERE NRO_TRAMITE='" + txtNroTramite.Text.Trim() + "'";
                cmd.Parameters.AddWithValue("@EST", "CERRADO");

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", "TRÁMITE FINALIZADO CORRECTAMENTE");
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("CONTROL", txtNroTramite.Text.Trim());
                MessageBox.Show("Trámite finalizado correctamente.", "Aceptado", MessageBoxButtons.OK, MessageBoxIcon.Information);

                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Control.EliminarRegistro(txtNroTramite.Text.Trim());
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }


                this.Close();
                cn.Close();



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Logins.LogError(ex);
            }

        }

        private void Reprocesar(object sender, EventArgs e)
        {
            if (txtAdicComent.Text == "")
            {
                MessageBox.Show("Debe ingresar un comentario para rechazar trámite.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    string IdTramite = txtNroTramite.Text;
                    OleDbConnection cn = new OleDbConnection();
                    cn.ConnectionString = Conexion.cnProceso;
                    OleDbCommand cmd = new OleDbCommand();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + txtNroTramite.Text.Trim() + "'";
                    cmd.Parameters.AddWithValue("@EST", "RECHAZADO");
                    cmd.Parameters.AddWithValue("@ACONTROL", DateTime.Today);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                    cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                    cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                    cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                    cmd.Parameters.AddWithValue("@COM", txtAdicComent.Text.Trim());
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    Logins.Estadistica("CONTROL", txtNroTramite.Text.Trim());
                    MessageBox.Show("Trámite enviado a reproceso.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    try
                    {
                        Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                        if (existe != null)
                        {
                            Control.EliminarRegistro(txtNroTramite.Text.Trim());
                        }
                    }
                    catch (Exception ex) { Logins.LogError(ex); }
                    this.Close();
                    cn.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    Logins.LogError(ex);

                }

            }

        }

        private void Corregido(object sender, EventArgs e)
        {
            try
            {
                string IdTramite = txtNroTramite.Text;
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + txtNroTramite.Text.Trim() + "'";
                cmd.Parameters.AddWithValue("@EST", "ABIERTO");
                cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today));
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", txtAdicComent.Text.Trim());
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("REPROCESO", txtNroTramite.Text.Trim());
                MessageBox.Show("Trámite Reprocesado.", "Reproceso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Rechazos").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Rechazos.EliminarRegistro(txtNroTramite.Text.Trim());
                        Rechazos.TotRech--;
                        Rechazos.TotUs--;
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }
                this.Close();
                cn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Logins.LogError(ex);

            }
        }

        public void CargarTramite(string IdTramite)
        {
            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandText = "SELECT * FROM PREVALID WHERE [_NROTRAM]='" + IdTramite + "'";
            DataTable dt = new DataTable();
            DataTable dth = new DataTable();
            DataTable dtc = new DataTable();
            DataTable dtt = new DataTable();
            DataTable dtadic = new DataTable();


            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);
            try
            {
                if (dt.Rows.Count == 1)
                {
                    txtTramite.Text = dt.Rows[0]["_TRAM"].ToString();
                    txtNroTramite.Text = dt.Rows[0]["_NROTRAM"].ToString();
                    txtDesc.Text = dt.Rows[0]["_DESC"].ToString();
                    txtNombre.Text = dt.Rows[0]["_NOM"].ToString();
                    txtCliente.Text = dt.Rows[0]["_CLTE"].ToString();
                    txtPais.Text = dt.Rows[0]["_PAIS"].ToString();
                    txtTipDoc.Text = dt.Rows[0]["_TIPDOC"].ToString();
                    txtDni.Text = dt.Rows[0]["_DOC"].ToString();
                    txtCte_Paq.Text = dt.Rows[0]["_CTECPAQ"].ToString();
                    txtCodPaq.Text = dt.Rows[0]["_CODPAQ"].ToString();
                    txtEstCivil.Text = dt.Rows[0]["_ESTCIV"].ToString();
                    txtSeg.Text = dt.Rows[0]["_SEG"].ToString();
                    txtConv.Text = dt.Rows[0]["_CONV"].ToString();
                    txtFNac.Text = dt.Rows[0]["_FECNAC"].ToString();
                    txtNacion.Text = dt.Rows[0]["_NAC"].ToString();
                    txtTelefono.Text = dt.Rows[0]["_TEL"].ToString();
                    txtSucVta.Text = dt.Rows[0]["_SUCVTA"].ToString();
                    txtPreCa.Text = dt.Rows[0]["_PRECA"].ToString();
                    txtNroCamp.Text = dt.Rows[0]["_NROCAM"].ToString();
                    txtCalle.Text = dt.Rows[0]["_CALLE"].ToString();
                    txtPuerta.Text = dt.Rows[0]["_PUERTA"].ToString();
                    txtPiso.Text = dt.Rows[0]["_PISO"].ToString();
                    txtDpto.Text = dt.Rows[0]["_DEPTO"].ToString();
                    txtCodPos.Text = dt.Rows[0]["_CODPOS"].ToString();
                    txtLocal.Text = dt.Rows[0]["_LOCAL"].ToString();
                    txtProv.Text = dt.Rows[0]["_PROV"].ToString();
                    txtCodGeo.Text = dt.Rows[0]["_CODGEO"].ToString();
                    txtDescGeo.Text = dt.Rows[0]["_DESCGEO"].ToString();
                    txtNroSolCta.Text = dt.Rows[0]["_SOLCTA"].ToString();
                    txtNroSolTj.Text = dt.Rows[0]["_SOLTAR"].ToString();
                    txtFEnvio.Text = dt.Rows[0]["_FECENV"].ToString();
                    txtFRta.Text = dt.Rows[0]["_FECRTA"].ToString();
                    txtModLiq.Text = dt.Rows[0]["_MODLIQ"].ToString();
                    txtS_Mod_ta.Text = dt.Rows[0]["_MODTA"].ToString();
                    txtS_Mod_ca.Text = dt.Rows[0]["_MODCA"].ToString();
                    txtCuentaTJ.Text = dt.Rows[0]["_CTATJ"].ToString();
                    txtCartera.Text = dt.Rows[0]["_CARTERA"].ToString();
                    txtGaff.Text = dt.Rows[0]["_GAFF"].ToString();
                    txtGaffTit.Text = dt.Rows[0]["_GAFFTIT"].ToString();
                    txtSucAdm.Text = dt.Rows[0]["_SUCADM"].ToString();
                    txtTarjeta.Text = dt.Rows[0]["_TJ"].ToString();
                    txt4taLinea.Text = dt.Rows[0]["_4TAL"].ToString();
                    txtEmbozado.Text = dt.Rows[0]["_EMBOZ"].ToString();
                    txtTipoTJ.Text = dt.Rows[0]["_TIPOTJ"].ToString();
                    txtTipoCta.Text = dt.Rows[0]["_TIPOCTA"].ToString();
                    txtLimOto.Text = dt.Rows[0]["_LIMOTO"].ToString();
                    txtLimCom.Text = dt.Rows[0]["_LIMCOM"].ToString();
                    txtCodLim.Text = dt.Rows[0]["_CODLIM"].ToString();
                    txtCCLim.Text = dt.Rows[0]["_CCLIMC"].ToString();
                    txtLimCred.Text = dt.Rows[0]["_LIMCRED"].ToString();
                    txtFPago.Text = dt.Rows[0]["_FPAGO"].ToString();
                    txtCtaDebit.Text = dt.Rows[0]["_CTADB"].ToString();
                    txtIva.Text = dt.Rows[0]["_IVA"].ToString();
                    txtCatBco.Text = dt.Rows[0]["_CATBCO"].ToString();
                    txtCargoBco.Text = dt.Rows[0]["_CARBCO"].ToString();
                    txtHabATM.Text = dt.Rows[0]["_HABATM"].ToString();
                    txtCodAgrup.Text = dt.Rows[0]["_CODAGR"].ToString();
                    txtTipVig.Text = dt.Rows[0]["_TIPOVIG"].ToString();
                    txtVigencia.Text = dt.Rows[0]["_VIGTJ"].ToString();
                    txtProd.Text = dt.Rows[0]["_PROD"].ToString();
                    txtCantBonif.Text = dt.Rows[0]["_CUOBON"].ToString();
                    txtBonifEmi.Text = dt.Rows[0]["_EMIBON"].ToString();
                    txtPromo.Text = dt.Rows[0]["_PROMO"].ToString();
                    txtC_Ser_Cu.Text = dt.Rows[0]["_SERCU"].ToString();
                    txtC_Ser_Bo.Text = dt.Rows[0]["_SERBO"].ToString();
                    txtC_Ser_M.Text = dt.Rows[0]["_SERM"].ToString();
                    txtCoefAde.Text = dt.Rows[0]["_COEFAD"].ToString();
                    txtMod_Au.Text = dt.Rows[0]["_MODAUT"].ToString();
                    txtGtia.Text = dt.Rows[0]["_GTIA"].ToString();
                    txtBonifCred.Text = dt.Rows[0]["_CREDBON"].ToString();
                    txtOcupacion.Text = dt.Rows[0]["_CODOCU"].ToString();
                    txtCodDist.Text = dt.Rows[0]["_CODIST"].ToString();


                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recuperar la tabla PREVALID" + ex.Message);
                Logins.LogError(ex);
            }





            try
            {
                cmd.CommandText = "SELECT * FROM HISTORICO WHERE NRO_TRAMITE='" + IdTramite + "'";
                OleDbDataAdapter dah = new OleDbDataAdapter(cmd);

                dah.Fill(dth);
                dgvComentarios.DataSource = dth;
                dgvComentarios.Columns[0].Visible = false;
                dgvComentarios.Columns[1].Visible = false;
                dgvComentarios.ReadOnly = true;
                AspectoTabla.AspectoDGV(dgvComentarios, Color.LightSalmon);
                dah.Dispose();
                dth.Dispose();
            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recuperar tabla HISTORICOS" + ex.Message);
                Logins.LogError(ex);
            }

            try
            {

                cmd.CommandText = "SELECT * FROM ERR WHERE ID_TRAM='" + IdTramite + "'";
                OleDbDataAdapter dat = new OleDbDataAdapter(cmd);

                dat.Fill(dtt);
                if (dtt.Rows.Count >0)
                {
                    dgvErrPrevalid.DataSource = dtt;
                    dgvErrPrevalid.Columns[0].Visible = false;
                    dgvErrPrevalid.Columns[1].Visible = false;
                    
                    dgvErrPrevalid.ReadOnly = true;
                    AspectoTabla.AspectoDGV(dgvErrPrevalid, Color.LightSalmon);
                    dat.Dispose();
                    dtt.Dispose();

                }



                cmd.CommandText = "SELECT * FROM ADICIONALES WHERE ID_INT='" + IdTramite + "'";
                OleDbDataAdapter daadic = new OleDbDataAdapter(cmd);

                daadic.Fill(dtadic);
                if (dtadic.Rows.Count >0)
                { 
                    dgvAdicionales.DataSource = dtadic;
                    dgvAdicionales.Columns[0].Visible = false;
                    dgvAdicionales.Columns[1].Visible = false;
                    dgvAdicionales.Columns[2].HeaderText = "TIPO DOC";
                    dgvAdicionales.Columns[3].HeaderText = "DOC";
                    dgvAdicionales.Columns[4].HeaderText = "NOMBRE";
                   

                    dgvAdicionales.ReadOnly = true;
                    AspectoTabla.AspectoDGV(dgvAdicionales, Color.LightSeaGreen);
                    daadic.Dispose();
                    dtadic.Dispose();


                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recurerar tabla PERSONAS/Tarjetas" + ex.Message);
                Logins.LogError(ex);
            }

            try
            {
                cmd.CommandText = "SELECT * FROM Procesos WHERE NroTramite='" + IdTramite + "'";
                OleDbDataAdapter dac = new OleDbDataAdapter(cmd);

                dac.Fill(dtc);
                dgvProceso.DataSource = dtc;

                AspectoTabla.AspectoComent(dgvProceso);
                dac.Dispose();
                dtc.Dispose();

            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recuerar tabla Procesos" + ex.Message);
                Logins.LogError(ex);
            }


            da.Dispose();
            dt.Dispose();



        }




        WebBrowser flow = new WebBrowser();

        private void btnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsHTMLDataOnClipboard = Clipboard.ContainsData(DataFormats.Html);


                string htmlData = null;

                if (IsHTMLDataOnClipboard)
                {

                    // **** Para frameworks menores a 4.5 *****

                    htmlData = (string)Clipboard.GetDataObject().GetData(DataFormats.Html, true);

                    htmlData = Encoding.UTF8.GetString(Encoding.Default.GetBytes(htmlData));

                    flow.ScriptErrorsSuppressed = true;
                    flow.DocumentText = htmlData;
                    flow.Document.OpenNew(true);
                    flow.Document.Write(htmlData);
                    flow.Refresh();

                    Datos_tramite();
                    Filiatorios();
                    Domicilio();
                    Datos_solicitud();
                    Parametros();
                    Particularidades();
                    Tabla_Errores();
                    Tabla_Adicionales();
                    Otros_Datos();
                    Tabla_Comentarios_A();
                    Tabla_Comentarios_B();


                }
                else
                {
                    MessageBox.Show("Los datos copiados no contienen estructura WorkFlow");

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error Interno. Algún dato no se pudo recuperar.\nContacte al administrador de la aplicación.\nError: " + ex.Message);
                Logins.LogError(ex);
            }
        }

        public void Otros_Datos()
        {

            // INFORMACION ADICIONAL DEL TRAMITE
            if (flow.Document.GetElementById("_W020INFADI") != null)
            {
                txtAdicInf.Text = flow.Document.GetElementById("_W020INFADI").GetAttribute("value").ToString().Trim();
            }

            
        }

        public void Datos_tramite()

        {
            // TRAMITE Y NRO
            if (flow.Document.GetElementById("_W005TRAMID") != null)
            {
                string strTramite = flow.Document.GetElementById("_W005TRAMID").GetAttribute("value").ToString().Trim();
                txtTramite.Text = strTramite.Substring(0, 4);

                txtNroTramite.Text = strTramite.Substring(4);
            }
            // DESCRIPCION
            if (flow.Document.GetElementById("_W004DESCRI") != null)
            {
                txtDesc.Text = flow.Document.GetElementById("_W004DESCRI").GetAttribute("value").ToString().Trim();
            }
            //PRIORIDAD
            txtPrioridad.Text = "NORMAL";

        }

        public void Filiatorios()
        {


            try
            {



                //CUIL Y DNI
                if (flow.Document.GetElementById("_W020DOCNRO") != null)
                {
                    string strDni = "";
                    strDni = flow.Document.GetElementById("_W020DOCNRO").GetAttribute("value").ToString().Trim();
                    if (strDni.Length == 11)
                    {
                        txtDni.Text = strDni.Substring(2, 8);
                    }
                    else
                    {
                        txtDni.Text = strDni;
                    }

                }
                // NOMBRE
                if (flow.Document.GetElementById("_NOMAPE") != null)
                {
                    txtNombre.Text = flow.Document.GetElementById("_NOMAPE").GetAttribute("value").ToString().Trim();
                }
                //CLIENTE
                if (flow.Document.GetElementById("_W020CLIBT") != null)
                {
                    txtCliente.Text = flow.Document.GetElementById("_W020CLIBT").GetAttribute("value").ToString().Trim();
                }
                //PAIS
                if (flow.Document.GetElementById("_W020DOCPAI") != null)
                {
                    txtPais.Text = flow.Document.GetElementById("_W020DOCPAI").GetAttribute("value").ToString().Trim();
                }
                //TIPO DOCUMENTO
                if (flow.Document.GetElementById("_W020DOCTIP") != null)
                {
                    txtTipDoc.Text = flow.Document.GetElementById("_W020DOCTIP").GetAttribute("value").ToString().Trim();
                }
                //CLIENTE CON PAQUETE
                if (flow.Document.GetElementById("_VCLIENT") != null)
                {
                    txtCte_Paq.Text = flow.Document.GetElementById("_VCLIENT").GetAttribute("value").ToString().Trim();
                }
                //PAQUETE
                if (flow.Document.GetElementById("_WDSCPAQ") != null)
                {
                    txtCodPaq.Text = flow.Document.GetElementById("_WDSCPAQ").GetAttribute("value").ToString().Trim();
                }

                //SEGMENTO
                if (flow.Document.GetElementById("_W020DESSEG") != null)
                {
                    txtSeg.Text = flow.Document.GetElementById("_W020DESSEG").GetAttribute("value").ToString().Trim();
                }

                //ESTADO CIVIL
                if (flow.Document.GetElementById("_W020ESTCIV") != null)
                {
                    if (txtEstCivil.Text == "")
                    {
                        txtEstCivil.Text = flow.Document.GetElementById("_W020ESTCIV").GetAttribute("value").ToString().Trim();

                    }

                }
                if (flow.Document.GetElementById("_ESTCIV") != null)
                {
                    if (txtEstCivil.Text == "")
                    {
                        txtEstCivil.Text = flow.Document.GetElementById("_ESTCIV").GetAttribute("value").ToString().Trim();
                    }
                }
                //CONVENIO / ENTE
                if (flow.Document.GetElementById("_W020NROCON") != null)
                {
                    txtConv.Text = flow.Document.GetElementById("_W020NROCON").GetAttribute("value").ToString().Trim();
                }
                //FECHA NACIMIENTO
                if (flow.Document.GetElementById("_FECHANAC") != null)
                {
                    txtFNac.Text = flow.Document.GetElementById("_FECHANAC").GetAttribute("value").ToString().Trim();
                }

                //NACIONALIDAD 
                if (flow.Document.GetElementById("_W020NAC") != null)
                {
                    txtNacion.Text = flow.Document.GetElementById("_W020NAC").GetAttribute("value").ToString().Trim();
                }


                //TELEFONO
                if (flow.Document.GetElementById("_W020CODARE") != null)
                {
                    txtTelefono.Text = flow.Document.GetElementById("_W020CODARE").GetAttribute("value").ToString().Trim();
                    txtTelefono.AppendText(" " + flow.Document.GetElementById("_W020TEL").GetAttribute("value").ToString().Trim());
                }

                //SUCURSAL DE VENTA
                if (flow.Document.GetElementById("_W020SUCTRA") != null)
                {
                    txtSucVta.Text = flow.Document.GetElementById("_W020SUCTRA").GetAttribute("value").ToString().Trim();
                }
                //PRECALIFICACION
                if (flow.Document.GetElementById("_W020ADIAPE") != null)
                {
                    txtPreCa.Text = flow.Document.GetElementById("_W020ADIAPE").GetAttribute("value").ToString().Trim();
                }
                //CAMPAÑA
                if (flow.Document.GetElementById("_W020CARGO") != null)
                {
                    txtNroCamp.Text = flow.Document.GetElementById("_W020CARGO").GetAttribute("value").ToString().Trim();
                }

            }

            catch (Exception ex)
            {

                MessageBox.Show("Error Interno en 'Filiatorios'. Algún dato no se pudo recuperar.\nContacte al administrador de la aplicación.\nError: " + ex.Message);
                Logins.LogError(ex);
            }


        }

        public void Domicilio()

        {

            //CALLE
            if (flow.Document.GetElementById("_W020CALLE") != null)
            {
                txtCalle.Text = flow.Document.GetElementById("_W020CALLE").GetAttribute("value").ToString().Trim();
            }
            //PUERTA
            if (flow.Document.GetElementById("_W020NUME") != null)
            {
                txtPuerta.Text = flow.Document.GetElementById("_W020NUME").GetAttribute("value").ToString().Trim();
            }
            //PISO
            if (flow.Document.GetElementById("_W020PISO") != null)
            {
                txtPiso.Text = flow.Document.GetElementById("_W020PISO").GetAttribute("value").ToString().Trim();
            }
            //DEPTO
            if (flow.Document.GetElementById("_W020DPTO") != null)
            {
                txtDpto.Text = flow.Document.GetElementById("_W020DPTO").GetAttribute("value").ToString().Trim();
            }
            //CODIGO POSTAL
            if (flow.Document.GetElementById("_W020CPOS") != null)
            {
                txtCodPos.Text = flow.Document.GetElementById("_W020CPOS").GetAttribute("value").ToString().Trim();
            }
            //LOCALIDAD
            if (flow.Document.GetElementById("_W020LOCAL") != null)
            {
                txtLocal.Text = flow.Document.GetElementById("_W020LOCAL").GetAttribute("value").ToString().Trim();
            }
            //PROVINCIA
            if (flow.Document.GetElementById("_DESCPCIA") != null)
            {
                txtProv.Text = flow.Document.GetElementById("_DESCPCIA").GetAttribute("value").ToString().Trim();
            }
            //CODIGO GEOGRAFICO
            if (flow.Document.GetElementById("_W020CGEO") != null)
            {
                txtCodGeo.Text = flow.Document.GetElementById("_W020CGEO").GetAttribute("value").ToString().Trim();
            }
            //DESCRIPCION CODIGO GEOGRAFICO
            if (flow.Document.GetElementById("_DESCCGEO") != null)
            {
                txtDescGeo.Text = flow.Document.GetElementById("_DESCCGEO").GetAttribute("value").ToString().Trim();
            }


        }

        public void Datos_solicitud()

        {

            //SOLICITUD DE CUENTA
            if (flow.Document.GetElementById("_W020SOLCUE") != null)
            {
                txtNroSolCta.Text = flow.Document.GetElementById("_W020SOLCUE").GetAttribute("value").ToString().Trim();
            }
            //SOLICITUD DE TARJETA
            if (flow.Document.GetElementById("_W020SOLTAR") != null)
            {
                txtNroSolTj.Text = flow.Document.GetElementById("_W020SOLTAR").GetAttribute("value").ToString().Trim();
            }

            //FECHA ENVIO ADMINISTRADORA
            if (flow.Document.GetElementById("_FECHAENVIO") != null)
            {
                txtFEnvio.Text = flow.Document.GetElementById("_FECHAENVIO").GetAttribute("value").ToString().Trim();
            }
            //FECHA RESPUESTA
            if (flow.Document.GetElementById("_FECHARTA") != null)
            {
                txtFRta.Text = flow.Document.GetElementById("_FECHARTA").GetAttribute("value").ToString().Trim();
            }



        }


        public void Parametros()

        {
            //MODELO LIQUIDACION
            if (flow.Document.GetElementById("_W020MODLIQ") != null)
            {
                txtModLiq.Text = flow.Document.GetElementById("_W020MODLIQ").GetAttribute("value").ToString().Trim();
            }
            //S MOD TA
            if (flow.Document.GetElementById("_W020SMODTA") != null)
            {
                txtS_Mod_ta.Text = flow.Document.GetElementById("_W020SMODTA").GetAttribute("value").ToString().Trim();
            }
            //S MOD CA
            if (flow.Document.GetElementById("_W020SMODCA") != null)
            {
                txtS_Mod_ca.Text = flow.Document.GetElementById("_W020SMODCA").GetAttribute("value").ToString().Trim();
            }

            //GAFF 
            if (flow.Document.GetElementById("_W020GRUAFI") != null)
            {
                txtGaff.Text = flow.Document.GetElementById("_W020GRUAFI").GetAttribute("value").ToString().Trim();
            }
            //GAFF DEL TITULAR
            if (flow.Document.GetElementById("_GRUAFITIT") != null)
            {
                txtGaffTit.Text = flow.Document.GetElementById("_GRUAFITIT").GetAttribute("value").ToString().Trim();
            }

            //SUCURSAL ADMINISTRADORA
            if (flow.Document.GetElementById("_W020SUCADM") != null)
            {
                txtSucAdm.Text = flow.Document.GetElementById("_W020SUCADM").GetAttribute("value").ToString().Trim();
            }
            //CARTERA
            if (flow.Document.GetElementById("_W020CART") != null)
            {
                txtCartera.Text = flow.Document.GetElementById("_W020CART").GetAttribute("value").ToString().Trim();
            }
            //CUARTA LINEA
            if (flow.Document.GetElementById("_W020CUALIN") != null)
            {
                txt4taLinea.Text = flow.Document.GetElementById("_W020CUALIN").GetAttribute("value").ToString().Trim();
            }
            //NRO DE CUENTA TJ
            if (flow.Document.GetElementById("_W020NROCTA") != null)
            {
                txtCuentaTJ.Text = flow.Document.GetElementById("_W020NROCTA").GetAttribute("value").ToString().Trim();
            }
            //NRO TARJETA
            if (flow.Document.GetElementById("_W020NROTAR") != null)
            {
                txtTarjeta.Text = flow.Document.GetElementById("_W020NROTAR").GetAttribute("value").ToString().Trim();
            }




        }
        public void Particularidades()
        {
            //EMBOZADO
            if (flow.Document.GetElementById("_W020NOMTAR") != null)
            {
                txtEmbozado.Text = flow.Document.GetElementById("_W020NOMTAR").GetAttribute("value").ToString().Trim();
            }
            //TIPO DE PRODUCTO
            if (flow.Document.GetElementById("_TIPPRODUC") != null)
            {
                txtTipoTJ.Text = flow.Document.GetElementById("_TIPPRODUC").GetAttribute("value").ToString().Trim();
            }
            //TIPO DE cuenta
            if (flow.Document.GetElementById("_W020TIPCUE") != null)
            {
                txtTipoCta.Text = flow.Document.GetElementById("_W020TIPCUE").GetAttribute("value").ToString().Trim();
            }
            //LIMITE OTORGADO
            if (flow.Document.GetElementById("_LIMCOMV") != null)
            {
                foreach (HtmlElement Etiqueta in flow.Document.GetElementById("_LIMCOMV").Children)
                {
                    if (Etiqueta.GetAttribute("selected") == "True")
                    {
                        txtLimOto.Text = Etiqueta.InnerText.ToUpper();
                    }
                }
            }
            //_LIMCOMM
            if (flow.Document.GetElementById("_LIMCOMM") != null)
            {
                txtLimCom.Text = flow.Document.GetElementById("_LIMCOMM").GetAttribute("value").ToString().Trim();
            }
            //_W020LIMCOM
            if (flow.Document.GetElementById("_W020LIMCOM") != null)
            {
               txtCodLim.Text = flow.Document.GetElementById("_W020LIMCOM").GetAttribute("value").ToString().Trim();
            }

            //_W020CCLIMC
            if (flow.Document.GetElementById("_W020CCLIMC") != null)
            {
                txtCCLim.Text = flow.Document.GetElementById("_W020CCLIMC").GetAttribute("value").ToString().Trim();
            }
            //LIMITE DE CREDITO
            if (flow.Document.GetElementById("_W020LIMCRE") != null)
            {
                txtCCLim.Text = flow.Document.GetElementById("_W020LIMCRE").GetAttribute("value").ToString().Trim();
            }

            //FORMA DE PAGO 
            if (flow.Document.GetElementById("_W020FPAGO") != null)
            {
                txtFPago.Text = flow.Document.GetElementById("_W020FPAGO").GetAttribute("value").ToString().Trim();
            }

            //CUENTA DEBITO 1
            if (flow.Document.GetElementById("_W020MODCTD") != null)
            {
                txtCtaDebit.Text = flow.Document.GetElementById("_W020MODCTD").GetAttribute("value").ToString().Trim();
            }
            //CUENTA DEBITO 2
            if (flow.Document.GetElementById("_SCMDA") != null)
            {
                txtCtaDebit.AppendText("-"+ flow.Document.GetElementById("_SCMDA").GetAttribute("value").ToString().Trim());
            }
            //CUENTA DEBITO 3
            if (flow.Document.GetElementById("_W020SUCCTD") != null)
            {
                txtCtaDebit.AppendText("-" + flow.Document.GetElementById("_W020SUCCTD").GetAttribute("value").ToString().Trim());
            }
            //CUENTA DEBITO 4
            if (flow.Document.GetElementById("_SCCTA") != null)
            {
                txtCtaDebit.AppendText("-" + flow.Document.GetElementById("_SCCTA").GetAttribute("value").ToString().Trim());
            }
            //CUENTA DEBITO 4
            if (flow.Document.GetElementById("_W020SUBCTD") != null)
            {
                txtCtaDebit.AppendText("-" + flow.Document.GetElementById("_W020SUBCTD").GetAttribute("value").ToString().Trim());
            }
            //CONDICION IVA
            if (flow.Document.GetElementById("_W020TIPIVA") != null)
            {
                txtIva.Text = flow.Document.GetElementById("_W020TIPIVA").GetAttribute("value").ToString().Trim();
            }
            //CATEGORIA BALENCO
            if (flow.Document.GetElementById("_W020CATBAN") != null)
            {
                txtCatBco.Text = flow.Document.GetElementById("_W020CATBAN").GetAttribute("value").ToString().Trim();
            }
            //CARGO BALENCO
            if (flow.Document.GetElementById("_W020CARBAN") != null)
            {
                txtCargoBco.Text = flow.Document.GetElementById("_W020CARBAN").GetAttribute("value").ToString().Trim();
            }
            //HABILITACION ATM
            if (flow.Document.GetElementById("_W020HABATM") != null)
            {
               txtHabATM.Text = flow.Document.GetElementById("_W020HABATM").GetAttribute("value").ToString().Trim();
            }
            //AGRUPACION
            if (flow.Document.GetElementById("_W020AGRU") != null)
            {
                txtCodAgrup.Text = flow.Document.GetElementById("_W020AGRU").GetAttribute("value").ToString().Trim();
            }
            //TIPO DE VIGENCIA
            if (flow.Document.GetElementById("_W020TIPVIG") != null)
            {
               txtTipVig.Text = flow.Document.GetElementById("_W020TIPVIG").GetAttribute("value").ToString().Trim();
            }
            //TIPO DE VIGENCIA
            if (flow.Document.GetElementById("_W020VIGE") != null)
            {
                txtVigencia.Text = flow.Document.GetElementById("_W020VIGE").GetAttribute("value").ToString().Trim();
            }
            //PRODUCTO
            if (flow.Document.GetElementById("_W020PROD") != null)
            {
                txtProd.Text = flow.Document.GetElementById("_W020PROD").GetAttribute("value").ToString().Trim();
            }
            //CANT CUOTAS BONIFICADAS
            if (flow.Document.GetElementById("_W020CCUBON") != null)
            {
                txtCantBonif.Text = flow.Document.GetElementById("_W020CCUBON").GetAttribute("value").ToString().Trim();
            }
            //BONIFICACION EMISION
            if (flow.Document.GetElementById("_W020BONEMI") != null)
            {
                txtBonifEmi.Text = flow.Document.GetElementById("_W020BONEMI").GetAttribute("value").ToString().Trim();
            }
            //PROMOTOR
            if (flow.Document.GetElementById("_W020PROM") != null)
            {
               txtPromo.Text = flow.Document.GetElementById("_W020PROM").GetAttribute("value").ToString().Trim();
            }

            //_W020CSERCU
            if (flow.Document.GetElementById("_W020CSERCU") != null)
            {
                txtC_Ser_Cu.Text = flow.Document.GetElementById("_W020CSERCU").GetAttribute("value").ToString().Trim();
            }
            //GARANTIA
            if (flow.Document.GetElementById("_W020GARANT") != null)
            {
                txtGtia.Text = flow.Document.GetElementById("_W020GARANT").GetAttribute("value").ToString().Trim();
            }

            //COEFICIENTE ADELANTO
            if (flow.Document.GetElementById("_W020COEADE") != null)
            {
               txtCoefAde.Text = flow.Document.GetElementById("_W020COEADE").GetAttribute("value").ToString().Trim();
            }

            //MODELO DE AUTORIZACION
            if (flow.Document.GetElementById("_W020MODAU") != null)
            {
                txtMod_Au.Text = flow.Document.GetElementById("_W020MODAU").GetAttribute("value").ToString().Trim();
            }
            //_W020CSERBO
            if (flow.Document.GetElementById("_W020CSERBO") != null)
            {
               txtC_Ser_Bo.Text = flow.Document.GetElementById("_W020CSERBO").GetAttribute("value").ToString().Trim();
            }
            //_W020BONCRE
            if (flow.Document.GetElementById("_W020BONCRE") != null)
            {
                txtBonifCred.Text = flow.Document.GetElementById("_W020BONCRE").GetAttribute("value").ToString().Trim();
            }

            //_W020CSERM
            if (flow.Document.GetElementById("_W020CSERM") != null)
            {
               txtC_Ser_M.Text = flow.Document.GetElementById("_W020CSERM").GetAttribute("value").ToString().Trim();
            }
            //CODIGO DISTRIBUCION
            if (flow.Document.GetElementById("_W020DIST") != null)
            {
                txtCodDist.Text = flow.Document.GetElementById("_W020DIST").GetAttribute("value").ToString().Trim();
            }
            //CODIGO OCUPACION
            if (flow.Document.GetElementById("_W020CODOCU") != null)
            {
                txtOcupacion.Text = flow.Document.GetElementById("_W020CODOCU").GetAttribute("value").ToString().Trim();
            }





        }
   
        public void Tabla_Comentarios_A()

        {
            int Comentarios = 0;


            foreach (HtmlElement tag in flow.Document.All)
            {
                if (tag.GetAttribute("id").Contains("W0220_VHISTCOMEN_") && !tag.GetAttribute("id").Contains("span"))
                {
                    Comentarios++;
                }

            }


            if (Comentarios > 0)
            {
                try
                {

                    object[,] coment = new object[Comentarios, 5];


                    for (int i = Comentarios; i > 0; i--)
                    {

                        coment[i - 1, 0] = flow.Document.GetElementById("W0220_VFECHA_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        coment[i - 1, 1] = flow.Document.GetElementById("W0220_VTAREA_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        coment[i - 1, 2] = flow.Document.GetElementById("W0220_VUSUARIO_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        coment[i - 1, 3] = flow.Document.GetElementById("W0220_NOMBRE_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        coment[i - 1, 4] = flow.Document.GetElementById("W0220_VHISTCOMEN_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();


                    }





                    DataTable dtc = new DataTable();
                    dtc.Columns.Add("Fecha");
                    dtc.Columns.Add("Tarea");
                    dtc.Columns.Add("Usuario");
                    dtc.Columns.Add("Nombre");
                    dtc.Columns.Add("Comentario");



                    DataRow drc;

                    for (int i = 0; i < Comentarios; i++)

                    {
                        drc = dtc.NewRow();
                        drc["Fecha"] = coment[i, 0];
                        drc["Tarea"] = coment[i, 1];
                        drc["Usuario"] = coment[i, 2];
                        drc["Nombre"] = coment[i, 3];
                        drc["Comentario"] = coment[i, 4];

                        dtc.Rows.Add(drc);


                    }
                    dgvComentarios.DataSource = dtc;

                    dgvComentarios.ReadOnly = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudo obtener datos de Comentarios.\nRevise los datos copiados.");
                    Logins.LogError(ex);
                }

            }




        }
        public void Tabla_Comentarios_B()

        {
            int Comentarios = 0;


            foreach (HtmlElement tag in flow.Document.All)
            {
                if (tag.GetAttribute("id").Contains("W0149_VHISTCOMEN_") && !tag.GetAttribute("id").Contains("span"))
                {
                    Comentarios++;
                }

            }


            if (Comentarios > 0)
            {
                try
                {

                    object[,] coment = new object[Comentarios, 5];


                    for (int i = Comentarios; i > 0; i--)
                    {

                        coment[i - 1, 0] = flow.Document.GetElementById("W0149_VFECHA_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        coment[i - 1, 1] = flow.Document.GetElementById("W0149_VTAREA_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        coment[i - 1, 2] = flow.Document.GetElementById("W0149_VUSUARIO_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        coment[i - 1, 3] = flow.Document.GetElementById("W0149_NOMBRE_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        coment[i - 1, 4] = flow.Document.GetElementById("W0149_VHISTCOMEN_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();


                    }





                    DataTable dtc = new DataTable();
                    dtc.Columns.Add("Fecha");
                    dtc.Columns.Add("Tarea");
                    dtc.Columns.Add("Usuario");
                    dtc.Columns.Add("Nombre");
                    dtc.Columns.Add("Comentario");



                    DataRow drc;

                    for (int i = 0; i < Comentarios; i++)

                    {
                        drc = dtc.NewRow();
                        drc["Fecha"] = coment[i, 0];
                        drc["Tarea"] = coment[i, 1];
                        drc["Usuario"] = coment[i, 2];
                        drc["Nombre"] = coment[i, 3];
                        drc["Comentario"] = coment[i, 4];

                        dtc.Rows.Add(drc);


                    }
                    dgvComentarios.DataSource = dtc;

                    dgvComentarios.ReadOnly = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudo obtener datos de Comentarios.\nRevise los datos copiados.");
                    Logins.LogError(ex);
                }

            }




        }


        public void Tabla_Adicionales()
        {
            int Adicionales = 0;


            foreach (HtmlElement tag in flow.Document.All)
            {
                if (tag.GetAttribute("id").Contains("_TIPOADIC_") && !tag.GetAttribute("id").Contains("span"))
                {
                    Adicionales++;
                }

            }

            if (Adicionales > 0)
            {
                try
                {

                    object[,] adic = new object[Adicionales, 3];


                    for (int i = Adicionales; i > 0; i--)
                    {

                        adic[i - 1, 0] = flow.Document.GetElementById("_TIPOADIC_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        adic[i - 1, 1] = flow.Document.GetElementById("_NRODOCADIC_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        adic[i - 1, 2] = flow.Document.GetElementById("_APENOMADIC_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();


                    }





                    DataTable dta = new DataTable();
                    dta.Columns.Add("Tipo");
                    dta.Columns.Add("Documento");
                    dta.Columns.Add("Apellido y Nombre");



                    DataRow dra;

                    for (int i = 0; i < Adicionales; i++)

                    {
                        dra = dta.NewRow();
                        dra["Tipo"] = adic[i, 0];
                        dra["Documento"] = adic[i, 1];
                        dra["Apellido y Nombre"] = adic[i, 2];

                        dta.Rows.Add(dra);


                    }
                    dgvAdicionales.DataSource = dta;

                    dgvAdicionales.ReadOnly = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudo obtener datos de personas.\nRevise los datos copiados.");
                    Logins.LogError(ex);
                }

            }


        }

        public void Tabla_Errores()

        {
            int Errores = 0;
           

            foreach (HtmlElement tag in flow.Document.All)
            {
                if (tag.GetAttribute("id").Contains("_TIPOMOV_") && !tag.GetAttribute("id").Contains("span"))
                {
                    Errores++;
                }

            }

            if (Errores > 0)
            {
                try
                {

                    object[,] tjs = new object[Errores, 3];


                    for (int i = Errores; i > 0; i--)
                    {

                        tjs[i - 1, 0] = flow.Document.GetElementById("_TIPOMOV_" + i.ToString().PadLeft(4,'0')).GetAttribute("value").ToString();
                        tjs[i - 1, 1] = flow.Document.GetElementById("_CODERR_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        tjs[i - 1, 2] = flow.Document.GetElementById("_DESCERR_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();


                    }





                    DataTable dte = new DataTable();
                    dte.Columns.Add("Tipo.Mov.");
                    dte.Columns.Add("Cod.Error");
                    dte.Columns.Add("Descripcion");
                   


                    DataRow dre;

                    for (int i = 0; i < Errores; i++)

                    {
                        dre = dte.NewRow();
                        dre["Tipo.Mov."] = tjs[i, 0];
                        dre["Cod.Error"] = tjs[i, 1];
                        dre["Descripcion"] = tjs[i, 2];
                        
                        dte.Rows.Add(dre);


                    }
                    dgvErrPrevalid.DataSource = dte;

                    dgvErrPrevalid.ReadOnly = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudo obtener datos de Errores.\nRevise los datos copiados.");
                    Logins.LogError(ex);
                }

            }



        }

        private void label26_Click(object sender, EventArgs e)
        {
            MessageBox.Show("N-R: Nacional/Regional\nI: Internacional\nG: Gold\nP: Platinum\nS-B: Black/Signature","Referencias");
        }

        private void label25_Click(object sender, EventArgs e)
        {
            MessageBox.Show("N-R: Nacional/Regional\nI: Internacional\nP-G: Premier-Gold\nL: Platinum\nS-B: Black/Signature", "Referencias");

        }

        private void label29_Click(object sender, EventArgs e)
        {
            MessageBox.Show("V: Ventanilla\nPM: Pago Minimo\nPT: Pago Total", "Referencias");

        }

        private void label4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1-S: Soltero/a\n2-C: Casado/a\n5-D: Divorciado/a\n4-V: Viudo/a", "Referencias");

        }

        public bool Duplicados(string Tramite)

        {

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT NRO_TRAMITE FROM GESTIONES WHERE NRO_TRAMITE='" + Tramite + "'";
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);


            if (dt.Rows.Count == 1)
            {
                return true;

            }
            else
            {
                return false;
            }



        }
        public void Limpiar()
        {
            txt4taLinea.Text = "";
            txtAdicComent.Text = "";
            txtAdicInf.Text = "";
            txtBonifCred.Text = "";
            txtBonifEmi.Text = "";
            txtCalle.Text = "";
            txtCantBonif.Text = "";
            txtCargoBco.Text = "";
            txtCartera.Text = "";
            txtCatBco.Text = "";
            txtCCLim.Text = "";
            txtCliente.Text = "";
            txtCodAgrup.Text = "";
            txtCodDist.Text = "";
            txtCodGeo.Text = "";
            txtCodLim.Text = "";
            txtCodPaq.Text = "";
            txtCodPos.Text = "";
            txtCoefAde.Text = "";
            txtConv.Text = "";
            txtCtaDebit.Text = "";
            txtCte_Paq.Text = "";
            txtCuentaTJ.Text = "";
            txtC_Ser_Bo.Text = "";
            txtC_Ser_Cu.Text = "";
            txtC_Ser_M.Text = "";
            txtDesc.Text = "";
            txtDescGeo.Text = "";
            txtDni.Text = "";
            txtDpto.Text = "";
            txtEmbozado.Text = "";
            txtEstCivil.Text = "";
            txtFEnvio.Text = "";
            txtFNac.Text = "";
            txtFPago.Text = "";
            txtFRta.Text = "";
            txtGaff.Text = "";
            txtGaffTit.Text = "";
            txtGtia.Text = "";
            txtHabATM.Text = "";
            txtIva.Text = "";
            txtLimCom.Text = "";
            txtLimCred.Text = "";
            txtLimOto.Text = "";
            txtLocal.Text = "";
            txtModLiq.Text = "";
            txtMod_Au.Text = "";
            txtNacion.Text = "";
            txtNombre.Text = "";
            txtNroCamp.Text = "";
            txtNroSolCta.Text = "";
            txtNroSolTj.Text = "";
            txtNroTramite.Text = "";
            txtOcupacion.Text = "";
            txtPais.Text = "";
            txtPiso.Text = "";
            txtPreCa.Text = "";
            txtPrioridad.Text = "";
            txtProd.Text = "";
            txtPromo.Text = "";
            txtProv.Text = "";
            txtPuerta.Text = "";
            txtSeg.Text = "";
            txtSucAdm.Text = "";
            txtSucVta.Text = "";
            txtS_Mod_ca.Text = "";
            txtS_Mod_ta.Text = "";
            txtTarjeta.Text = "";
            txtTelefono.Text = "";
            txtTipDoc.Text = "";
            txtTipoCta.Text = "";
            txtTipoTJ.Text = "";
            txtTipVig.Text = "";
            txtTramite.Text = "";
            txtVigencia.Text = "";
            chbLiberacion.Checked = false;
            dgvAdicionales.DataSource = null;
            dgvComentarios.DataSource = null;
            dgvErrPrevalid.DataSource = null;



        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            if (Duplicados(txtNroTramite.Text))
            {
                MessageBox.Show("No se puede guardar Trámite.\nEl número de trámite ya existe. Verifique en Consulta de Trámites", "Error de grabado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (Metodos.ObtenerMetodo(txtTramite.Text) == "2")
                {
                    if (txtPrioridad.Text != "NORMAL" && txtPrioridad.Text != "URGENTE")
                    {
                        MessageBox.Show("Debe indicar priodidad [NORMAL/URGENTE]", "Prioridad", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        try
                        {
                            OleDbConnection cn = new OleDbConnection();
                            cn.ConnectionString = Conexion.cnProceso;
                            OleDbCommand cmd = new OleDbCommand();
                            cmd.Connection = cn;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "INSERT INTO GESTIONES(US_INICIO,FECHA_INICIO,EMP,TRAMITE,WF,NRO_TRAMITE,PRIORIDAD,CANAL_INGRESO,ESTADO,A_CONTROLAR) VALUES(@US_INICIO,@FECHA_INICIO,@EMP,@TRAMITE,@WF,@NRO_TRAMITE,@PRIORIDAD,@CANAL_INGRESO,@ESTADO,@ACONTROL)";

                            cn.Open();
                            cmd.Parameters.AddWithValue("@US_INICIO", Ingreso.UsuarioLogueado.ToUpper());
                            cmd.Parameters.AddWithValue("@FECHA_INICIO", DateTime.Now.ToOADate());

                            switch (txtTramite.Text.Substring(0,2))
                            {
                                case "TV": cmd.Parameters.AddWithValue("@EMP", "VISA");
                                    break;
                                case "TM": cmd.Parameters.AddWithValue("@EMP", "MASTERCARD");
                                    break;
                                case "TA": cmd.Parameters.AddWithValue("@EMP", "AMERICAN EXPRESS");
                                    break;
                                default:
                                    break;
                            }

                          
                            cmd.Parameters.AddWithValue("@TRAMITE", txtDesc.Text.Trim());
                            cmd.Parameters.AddWithValue("@WF", txtTramite.Text.Trim());
                            cmd.Parameters.AddWithValue("@NRO_TRAMITE", txtNroTramite.Text.Trim());

                            switch (chbLiberacion.Checked)
                            {
                                case true: cmd.Parameters.AddWithValue("@PRIORIDAD", txtPrioridad.Text.Trim()+"+LIBERACION");
                                    break;
                                case false: cmd.Parameters.AddWithValue("@PRIORIDAD", txtPrioridad.Text.Trim());
                                    break;
                                default:
                                    break;
                            }
                            
                            cmd.Parameters.AddWithValue("@CANAL_INGRESO", "ERR-PREVALID");

                            cmd.Parameters.AddWithValue("@ESTADO", "ABIERTO");
                           
                            cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today).ToOADate());


                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                            cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                            cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                            cmd.Parameters.AddWithValue("@COM", txtAdicComent.Text.Trim());
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();



                            OleDbCommand cmdWF = new OleDbCommand();
                            cmdWF.Connection = cn;
                            cmdWF.CommandType = CommandType.Text;
                            cmdWF.CommandText = "INSERT INTO PREVALID (_TRAM,_NROTRAM,_DESC,_NOM,_CLTE," +
                                "_PAIS,_TIPDOC,_DOC,_CTECPAQ,_CODPAQ,_ESTCIV,_SEG,_CONV,_FECNAC,_NAC," +
                                "_TEL,_SUCVTA,_PRECA,_NROCAM,_CALLE,_PUERTA,_PISO,_DEPTO,_CODPOS,_LOCAL," +
                                "_PROV,_CODGEO,_DESCGEO,_SOLCTA,_SOLTAR,_FECENV,_FECRTA,_MODLIQ,_MODTA,_MODCA," +
                                "_CTATJ,_CARTERA,_GAFF,_GAFFTIT,_SUCADM,_TJ,_4TAL,_EMBOZ,_TIPOTJ,_TIPOCTA," +
                                "_LIMOTO,_LIMCOM,_CODLIM,_CCLIMC,_LIMCRED,_FPAGO,_CTADB,_IVA,_CATBCO,_CARBCO," +
                                "_HABATM,_CODAGR,_TIPOVIG,_VIGTJ,_PROD,_CUOBON,_EMIBON,_PROMO,_SERCU,_SERBO," +
                                "_SERM,_COEFAD,_MODAUT,_GTIA,_CREDBON,_CODOCU,_CODIST,_EMPRESA) VALUES (@_TRAM,@_NROTRAM," +
                                "@_DESC,@_NOM,@_CLTE,@_PAIS,@_TIPDOC,@_DOC,@_CTECPAQ,@_CODPAQ,@_ESTCIV,@_SEG," +
                                "@_CONV,@_FECNAC,@_NAC,@_TEL,@_SUCVTA,@_PRECA,@_NROCAM,@_CALLE,@_PUERTA,@_PISO," +
                                "@_DEPTO,@_CODPOS,@_LOCAL,@_PROV,@_CODGEO,@_DESCGEO,@_SOLCTA,@_SOLTAR,@_FECENV," +
                                "@_FECRTA,@_MODLIQ,@_MODTA,@_MODCA,@_CTATJ,@_CARTERA,@_GAFF,@_GAFFTIT,@_SUCADM," +
                                "@_TJ,@_4TAL,@_EMBOZ,@_TIPOTJ,@_TIPOCTA,@_LIMOTO,@_LIMCOM,@_CODLIM,@_CCLIMC," +
                                "@_LIMCRED,@_FPAGO,@_CTADB,@_IVA,@_CATBCO,@_CARBCO,@_HABATM,@_CODAGR,@_TIPOVIG," +
                                "@_VIGTJ,@_PROD,@_CUOBON,@_EMIBON,@_PROMO,@_SERCU,@_SERBO,@_SERM,@_COEFAD," +
                                "@_MODAUT,@_GTIA,@_CREDBON,@_CODOCU,@_CODIST,@_EMPRESA)";



                          

                            cmdWF.Parameters.AddWithValue("@_TRAM", txtTramite.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_NROTRAM",txtNroTramite.Text.Trim());

                            cmdWF.Parameters.AddWithValue("@_DESC",txtDesc.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_NOM",txtNombre.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CLTE",txtCliente.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_PAIS",txtPais.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_TIPDOC",txtTipDoc.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_DOC",txtDni.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CTECPAQ",txtCte_Paq.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CODPAQ",txtCodPaq.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_ESTCIV",txtEstCivil.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_SEG",txtSeg.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CONV",txtConv.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_FECNAC",txtFNac.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_NAC",txtNacion.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_TEL",txtTelefono.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_SUCVTA",txtSucVta.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_PRECA",txtPreCa.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_NROCAM",txtNroCamp.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CALLE",txtCalle.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_PUERTA",txtPuerta.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_PISO",txtPiso.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_DEPTO",txtDpto.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CODPOS",txtCodPos.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_LOCAL",txtLocal.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_PROV",txtProv.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CODGEO",txtCodGeo.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_DESCGEO",txtDescGeo.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_SOLCTA",txtNroSolCta.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_SOLTAR",txtNroSolTj.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_FECENV",txtFEnvio.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_FECRTA",txtFRta.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_MODLIQ",txtModLiq.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_MODTA",txtS_Mod_ta.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_MODCA",txtS_Mod_ca.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CTATJ",txtCuentaTJ.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CARTERA",txtCartera.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_GAFF",txtGaff.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_GAFFTIT",txtGaffTit.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_SUCADM",txtSucAdm.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_TJ",txtTarjeta.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_4TAL",txt4taLinea.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_EMBOZ",txtEmbozado.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_TIPOTJ",txtTipoTJ.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_TIPOCTA",txtTipoCta.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_LIMOTO",txtLimOto.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_LIMCOM",txtLimCom.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CODLIM",txtCodLim.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CCLIMC",txtCCLim.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_LIMCRED",txtLimCred.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_FPAGO",txtFPago.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CTADB",txtCtaDebit.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_IVA",txtIva.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CATBCO",txtCatBco.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CARBCO",txtCargoBco.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_HABATM",txtHabATM.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CODAGR",txtCodAgrup.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_TIPOVIG",txtTipVig.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_VIGTJ",txtVigencia.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_PROD",txtProd.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CUOBON",txtCantBonif.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_EMIBON",txtBonifEmi.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_PROMO",txtPromo.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_SERCU",txtC_Ser_Cu.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_SERBO",txtC_Ser_Bo.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_SERM",txtC_Ser_M.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_COEFAD",txtCoefAde.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_MODAUT",txtMod_Au.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_GTIA",txtGtia.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CREDBON",txtBonifCred.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CODOCU",txtOcupacion.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@_CODIST",txtCodDist.Text.Trim());
                            switch (txtTramite.Text.Substring(0, 2))
                            {
                                case "TV":
                                    cmdWF.Parameters.AddWithValue("@_EMPRESA", "VISA");
                                    break;
                                case "TM":
                                    cmdWF.Parameters.AddWithValue("@_EMPRESA", "MASTERCARD");
                                    break;
                                case "TA":
                                    cmdWF.Parameters.AddWithValue("@_EMPRESA", "AMERICAN EXPRESS");
                                    break;
                                default:
                                    break;
                            }


                            cmdWF.ExecuteNonQuery();
                            cmdWF.Parameters.Clear();

                            OleDbCommand cmdHist = new OleDbCommand();
                            cmdHist.Connection = cn;
                            cmdHist.CommandType = CommandType.Text;
                            cmdHist.CommandText = "INSERT INTO HISTORICO (NRO_TRAMITE,FECHA,TAREA,USUARIO,NOMBRE,COMENTARIO) VALUES (@NRO_TRAMITE,@FECHA,@TAREA,@USUARIO,@NOMBRE,@COMENTARIO)";


                            foreach (DataGridViewRow fila in dgvComentarios.Rows)
                            {

                                cmdHist.Parameters.AddWithValue("@NRO_TRAMITE", txtNroTramite.Text.Trim());
                                cmdHist.Parameters.AddWithValue("@FECHA", Convert.ToString(fila.Cells[0].Value));
                                cmdHist.Parameters.AddWithValue("@TAREA", Convert.ToString(fila.Cells[1].Value));
                                cmdHist.Parameters.AddWithValue("@USUARIO", Convert.ToString(fila.Cells[2].Value));
                                cmdHist.Parameters.AddWithValue("@NOMBRE", Convert.ToString(fila.Cells[3].Value));
                                cmdHist.Parameters.AddWithValue("@COMENTARIO", Convert.ToString(fila.Cells[4].Value));
                                cmdHist.ExecuteNonQuery();
                                cmdHist.Parameters.Clear();

                            }



                            OleDbCommand cmdAD = new OleDbCommand();
                            cmdAD.Connection = cn;
                            cmdAD.CommandType = CommandType.Text;

                           
                                cmdAD.CommandText = "INSERT INTO ADICIONALES (ID_INT,TIPO,DOC,NOM) VALUES (@ID_INT,@TIPO,@DOC,@NOM)";

                                if (dgvAdicionales.Rows.Count > 0)
                                {

                                    foreach (DataGridViewRow fila in dgvAdicionales.Rows)
                                    {

                                        cmdAD.Parameters.AddWithValue("@ID_INT", txtNroTramite.Text.Trim());
                                        cmdAD.Parameters.AddWithValue("@TIPO", Convert.ToString(fila.Cells[0].Value));
                                        cmdAD.Parameters.AddWithValue("@DOC", Convert.ToString(fila.Cells[1].Value));
                                        cmdAD.Parameters.AddWithValue("@NOM", Convert.ToString(fila.Cells[2].Value));
                                        
                                        cmdAD.ExecuteNonQuery();
                                        cmdAD.Parameters.Clear();
                                    }
                                }

                            OleDbCommand cmdER = new OleDbCommand();
                            cmdER.Connection = cn;
                            cmdER.CommandType = CommandType.Text;
                            cmdER.CommandText = "INSERT INTO ERR (ID_TRAM,_MOV,_ERR,_DESC) VALUES (@ID_TRAM,@_MOV,@_ERR,@_DESC)";

                                if (dgvErrPrevalid.Rows.Count > 0)
                                {

                                    foreach (DataGridViewRow row in dgvErrPrevalid.Rows)
                                    {

                                        cmdER.Parameters.AddWithValue("@ID_TRAM", txtNroTramite.Text.Trim());
                                        cmdER.Parameters.AddWithValue("@_MOV", Convert.ToString(row.Cells[0].Value));
                                        cmdER.Parameters.AddWithValue("@_ERR", Convert.ToString(row.Cells[1].Value));
                                        cmdER.Parameters.AddWithValue("@_DESC", Convert.ToString(row.Cells[2].Value));

                                        cmdER.ExecuteNonQuery();
                                        cmdER.Parameters.Clear();
                                    }
                                }

                            
                            Logins.Estadistica("PROCESO", txtNroTramite.Text.Trim());



                            cmd.Parameters.Clear();
                            cmdHist.Parameters.Clear();


                            cn.Close();





                            MessageBox.Show("Gestión grabada exitosamente.\nNro de trámite: " + txtNroTramite.Text.Trim(), "Trámite WorkFlow", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpiar();




                            cn.Close();
                        }
                        catch (Exception EX)

                        {

                            MessageBox.Show(EX.Message + EX.Source);
                            Logins.LogError(EX);
                        }
                    }

                }
                else
                {
                    switch (Metodos.ObtenerMetodo(txtTramite.Text))
                    {
                        case "1":
                            MessageBox.Show("Este trámite no puede ser ingresado en este módulo.\nIngrese por Módulo Púrpura", "Módulo Incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            break;

                        default:
                            break;
                    }
                }
            }




        }

     

      

       

       

        private void dgvErrPrevalid_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            VisorTexto frm = new VisorTexto(dgvErrPrevalid.CurrentCell.Value.ToString());
            frm.Show();
        }

        private void dgvComentarios_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            VisorTexto frm = new VisorTexto(dgvComentarios.CurrentCell.Value.ToString());
            frm.Show();
        }

        private void dgvProceso_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            VisorTexto frm = new VisorTexto(dgvProceso.CurrentCell.Value.ToString());
            frm.Show();
        }
    }
}
