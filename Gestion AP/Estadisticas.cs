﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Gestion_AP
{
    public partial class Estadisticas : Form
    {
        public Estadisticas()
        {
            InitializeComponent();
        }

        public void Limpiar()
        {
            txtTotCon.Text = "";
            txtTotFecha.Text = "";
            txtTotIng.Text = "";
            txtTotRep.Text = "";
            txtTotUs.Text = "";
            txtUsCon.Text = "";
            txtUsIng.Text = "";
            txtUsRep.Text = "";
            dgvTotTram.DataSource = null;
            dgvUsTram.DataSource = null;


        }

        private void btnConsulta_Click(object sender, EventArgs e)
        {

            Limpiar();
            DateTime dtFecha = Convert.ToDateTime(dtpFecha.Value.Day + "/" + dtpFecha.Value.Month + "/" + dtpFecha.Value.Year);

            try
            {
                 //----------------------------------------
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                //----------------------------------------
                //  TOTALES EN ESTADISTICAS
                cmd.CommandText = "SELECT * FROM ESTADISTICAS WHERE FECHA BETWEEN @F1 AND @F2";
                cmd.Parameters.AddWithValue("@F1", dtFecha);
                cmd.Parameters.AddWithValue("@F2", dtFecha.AddDays(1));
                DataTable dtTotFecha = new DataTable();
                OleDbDataAdapter daTotFecha = new OleDbDataAdapter(cmd);
                daTotFecha.Fill(dtTotFecha);
                int TotIngresados = 0;
                int TotControlados = 0;
                int TotReprocesados = 0;
                int TotUsIng = 0;
                int TotUsCon = 0;
                int TotUsRep = 0;


                if (dtTotFecha.Rows.Count != 0)
                {
                    // Total por fecha
                    for (int i = 0; i < dtTotFecha.Rows.Count; i++)
                    {
                        if (dtTotFecha.Rows[i]["ACCION"].ToString() == "PROCESO")
                        {
                            TotIngresados++;
                        }
                        if (dtTotFecha.Rows[i]["ACCION"].ToString() == "CONTROL")
                        {
                            TotControlados++;
                        }
                        if (dtTotFecha.Rows[i]["ACCION"].ToString() == "REPROCESO")
                        {
                            TotReprocesados++;
                        }
                        if (dtTotFecha.Rows[i]["ACCION"].ToString() == "PROCESO" && dtTotFecha.Rows[i]["USUARIO"].ToString() == Ingreso.UsuarioLogueado)
                        {
                            TotUsIng++;
                        }
                        if (dtTotFecha.Rows[i]["ACCION"].ToString() == "CONTROL" && dtTotFecha.Rows[i]["USUARIO"].ToString() == Ingreso.UsuarioLogueado)
                        {
                            TotUsCon++;
                        }
                        if (dtTotFecha.Rows[i]["ACCION"].ToString() == "REPROCESO" && dtTotFecha.Rows[i]["USUARIO"].ToString() == Ingreso.UsuarioLogueado)
                        {
                            TotUsRep++;
                        }
                    }
                }
                dtTotFecha.Dispose();
                daTotFecha.Dispose();
                //----------------------------------------
                //  TOTALES EN FECHA
                txtTotIng.Text = TotIngresados.ToString();
                txtTotCon.Text = TotControlados.ToString();
                txtTotRep.Text = TotReprocesados.ToString();
                txtTotFecha.Text = (TotIngresados + TotControlados + TotReprocesados).ToString();

                //----------------------------------------
                //  TOTALES EN USUARIO
                txtUsIng.Text = TotUsIng.ToString();
                txtUsCon.Text = TotUsCon.ToString();
                txtUsRep.Text = TotUsRep.ToString();
                txtTotUs.Text = (TotUsIng + TotUsCon + TotUsRep).ToString();


                //----------------------------------------
                //  TRAMITES INGRESADOS


               
                cmd.CommandText = "SELECT TRAMITE FROM TRAMITES";
                DataTable dtTramites = new DataTable();
                OleDbDataAdapter daTramites = new OleDbDataAdapter(cmd);
                daTramites.Fill(dtTramites);
                dtTramites.Columns.Add("CANTIDADES");


                cmd.CommandText = "SELECT TRAMITE FROM GESTIONES WHERE FECHA_INICIO BETWEEN @F1 AND @F2";
                cmd.Parameters.AddWithValue("@F1", dtFecha);
                cmd.Parameters.AddWithValue("@F2", dtFecha.AddDays(1));
                DataTable dtGestiones = new DataTable();
                OleDbDataAdapter daGestiones = new OleDbDataAdapter(cmd);
                daGestiones.Fill(dtGestiones);
                

                for (int i = 0; i < dtTramites.Rows.Count; i++)
                {
                    int contador = 0;
                    for (int j = 0; j < dtGestiones.Rows.Count; j++)
                    {
                        if (dtGestiones.Rows[j]["TRAMITE"].ToString()==dtTramites.Rows[i]["TRAMITE"].ToString())
                        {
                            contador++;
                        }

                    }
                    dtTramites.Rows[i]["CANTIDADES"] = contador;
                }

                string fieldName = string.Concat("[", dtTramites.Columns[1].ColumnName, "]");
                
                DataView view = dtTramites.DefaultView;
                view.RowFilter = fieldName + " > 0";
                dgvTotTram.DataSource = view;
                 dgvTotTram.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgvTotTram.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgvTotTram.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvTotTram.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                DataGridViewColumn Q = dgvTotTram.Columns[1];
                dgvTotTram.Sort(Q, ListSortDirection.Descending);
                dtTramites.Dispose();
                daGestiones.Dispose();
                daTramites.Dispose();

                //----------------------------------------
                //  TRAMITES INGRESADOS POR USUARIO



                cmd.CommandText = "SELECT TRAMITE FROM TRAMITES";
                DataTable dtTrams = new DataTable();
                OleDbDataAdapter daTrams = new OleDbDataAdapter(cmd);
                daTrams.Fill(dtTrams);
                dtTrams.Columns.Add("CANTIDADES");


                cmd.CommandText = "SELECT TRAMITE,US_INICIO FROM GESTIONES WHERE FECHA_INICIO BETWEEN @F1 AND @F2 AND US_INICIO='"+Ingreso.UsuarioLogueado+"'";
                cmd.Parameters.AddWithValue("@F1", dtFecha);
                cmd.Parameters.AddWithValue("@F2", dtFecha.AddDays(1));
                DataTable dtGestUs = new DataTable();
                OleDbDataAdapter daGestUs = new OleDbDataAdapter(cmd);
                daGestUs.Fill(dtGestUs);

                if (dtGestUs.Rows.Count!=0)
                {
                    for (int i = 0; i < dtTrams.Rows.Count; i++)
                    {
                        int contador = 0;
                        for (int j = 0; j < dtGestUs.Rows.Count; j++)
                        {
                            if (dtGestUs.Rows[j]["TRAMITE"].ToString() == dtTrams.Rows[i]["TRAMITE"].ToString())
                            {
                                contador++;
                            }

                        }
                        dtTrams.Rows[i]["CANTIDADES"] = contador;
                    }

                    fieldName = string.Concat("[", dtTrams.Columns[1].ColumnName, "]");

                    view = dtTrams.DefaultView;
                    view.RowFilter = fieldName + " > 0";
                    dgvUsTram.DataSource = view;
                    dgvUsTram.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    dgvUsTram.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    dgvUsTram.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvUsTram.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                    DataGridViewColumn Z = dgvUsTram.Columns[1];
                    dgvUsTram.Sort(Z, ListSortDirection.Descending);
                }



                

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                Logins.LogError(ex);
            }
            






        }

        private void btnConsolidado_Click(object sender, EventArgs e)
        {
            //Consolidado frm = new Consolidado();
            //frm.Show();
            MessageBox.Show("En construcción!", "Ups!", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }
}
