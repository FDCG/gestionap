﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Web;
using System.Net;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;


namespace Gestion_AP
{
    public partial class Adjuntos : Form
    {
        public Adjuntos()
        {
            InitializeComponent();
        }

        private void cmbGestion_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT WF FROM TRAMITES WHERE TRAMITE='" + Convert.ToString(cmbGestion.SelectedItem) + "'";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                lblWF_Asociado.Text = Convert.ToString(dt.Rows[0][0]);

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);
            }
        }

        private void btnBuscar_1_Click(object sender, EventArgs e)
        {

            txtFile1.Text = BuscarArchivo("1");

        }

        private static string BuscarArchivo(string file)

        {
            string arc = "";
            OpenFileDialog fd = new OpenFileDialog();
            fd.Title = "Seleccione el archivo Nro: " + file;
            fd.RestoreDirectory = false;

            if (fd.ShowDialog() == DialogResult.OK)
            {

                arc = fd.FileName;



            }
            return arc;
        }

        private void btnBuscar_2_Click(object sender, EventArgs e)
        {
            if (txtFile1.Text == "")
            {
                MessageBox.Show("Utilice Archivo #1");
            }
            else
            {
                txtFile2.Text = BuscarArchivo("2");


            }
        }

        private void btnBuscar_3_Click(object sender, EventArgs e)
        {
            if (txtFile2.Text == "")
            {
                MessageBox.Show("Utilice linea anterior");
            }
            else
            {
                txtFile3.Text = BuscarArchivo("3");


            }
        }

        private void btnBuscar_4_Click(object sender, EventArgs e)
        {
            if (txtFile3.Text == "")
            {
                MessageBox.Show("Utilice linea anterior");
            }
            else
            {
                txtFile4.Text = BuscarArchivo("4");

            }
        }

        private void btnBuscar_5_Click(object sender, EventArgs e)
        {
            if (txtFile4.Text == "")
            {
                MessageBox.Show("Utilice linea anterior");
            }
            else
            {
                txtFile5.Text = BuscarArchivo("5");

            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            try
            {
                if (lblWF_Asociado.Text != "" && cbmAdm.SelectedItem != null && txtFile1.Text!="")
                {
                    string path = UNC.PathUNC(Directory.GetCurrentDirectory()) + @"\03\";
                    string id = "AD" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    int contador = 0;
                    string[] extensiones = new string[5];
                    if (txtFile1.Text != "")
                    {
                        string ext = Path.GetExtension(txtFile1.Text);
                        File.Move(txtFile1.Text, path + id + "1" + ext);
                        contador++;
                        extensiones[0] = ext;
                    }
                    if (txtFile2.Text != "")
                    {
                        string ext = Path.GetExtension(txtFile2.Text);
                        File.Move(txtFile2.Text, path + id + "2" + ext);
                        contador++;
                        extensiones[1] = ext;
                    }
                    if (txtFile3.Text != "")
                    {
                        string ext = Path.GetExtension(txtFile3.Text);
                        File.Move(txtFile3.Text, path + id + "3" + ext);
                        contador++;
                        extensiones[2] = ext;
                    }
                    if (txtFile4.Text != "")
                    {
                        string ext = Path.GetExtension(txtFile4.Text);
                        File.Move(txtFile4.Text, path + id + "4" + ext);
                        contador++;
                        extensiones[3] = ext;
                    }
                    if (txtFile5.Text != "")
                    {
                        string ext = Path.GetExtension(txtFile5.Text);
                        File.Move(txtFile5.Text, path + id + "5" + ext);
                        contador++;
                        extensiones[4] = ext;
                    }
                    try
                    {
                        GrabarGestion(contador, id, path + id, extensiones);
                    }
                    catch (Exception ex) { Logins.LogError(ex); }
                }
                else
                {
                    MessageBox.Show("Faltan completar datos");
                }
            }


            catch (Exception ex) { Logins.LogError(ex); }
        }




        public void GrabarGestion(int cantidad, string strIdInterno, string destino, string[] extensiones)



        {

            string _Prioridad = null;


            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO GESTIONES(US_INICIO,FECHA_INICIO,EMP,TRAMITE,WF,NRO_TRAMITE,PRIORIDAD,CANAL_INGRESO,ESTADO,A_CONTROLAR) VALUES(@US_INICIO,@FECHA_INICIO,@EMP,@TRAMITE,@WF,@NRO_TRAMITE,@PRIORIDAD,@CANAL_INGRESO,@ESTADO,@ACONTROL)";

                cn.Open();
                cmd.Parameters.AddWithValue("@US_INICIO", Ingreso.UsuarioLogueado.ToUpper());
                cmd.Parameters.AddWithValue("@FECHA_INICIO", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@EMP", cbmAdm.SelectedItem.ToString().Trim());
                cmd.Parameters.AddWithValue("@TRAMITE", cmbGestion.Text.ToString());
                cmd.Parameters.AddWithValue("@WF", lblWF_Asociado.Text.ToString());
                cmd.Parameters.AddWithValue("@NRO_TRAMITE", strIdInterno);
                if (chbUrgente.Checked)
                {
                    _Prioridad = "URGENTE";

                }
                else
                {
                    _Prioridad = "NORMAL";

                }

                switch (chbLiberar.Checked)
                {
                    case true:
                        cmd.Parameters.AddWithValue("@PRIORIDAD", _Prioridad + "+LIBERACION");
                        break;
                    case false:
                        cmd.Parameters.AddWithValue("@PRIORIDAD", _Prioridad);
                        break;
                    default:
                        break;
                }

                cmd.Parameters.AddWithValue("@CANAL_INGRESO", "ADJUNTOS");

               
                cmd.Parameters.AddWithValue("@ESTADO", "ABIERTO");
                cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today).ToOADate());
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO ADJUNTOS(NroTramite,ADJUNTO) VALUES(@NroTramite,@ADJUNTO)";
                for (int i = 1; i <= cantidad; i++)
                {
                    cmd.Parameters.AddWithValue("@NroTramite", strIdInterno);
                    cmd.Parameters.AddWithValue("@ADJUNTO", destino + i + extensiones[i - 1]);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                }
                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", strIdInterno);
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", txtComAdic.Text.Trim());
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("PROCESO", strIdInterno);
                



                MessageBox.Show("Gestión grabada exitosamente.\nNro. de control interno: " + strIdInterno+"\nCantidad de archivos registrados: "+cantidad);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo grabar el Trámite.\nError: " + ex.Message + "\nContacte al administrador");
                Logins.LogError(ex);
            }




        }

        private void Adjuntos_Load(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM TRAMITES";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cmbGestion.Items.Add(Convert.ToString(dt.Rows[i]["TRAMITE"]));
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);
            }
        }
    }
}


