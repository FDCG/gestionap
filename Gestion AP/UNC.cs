﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Management;
using System.Management.Instrumentation;
using System.Runtime.InteropServices; 
using System.ComponentModel;  
using System.Data;
using System.Windows.Forms;


namespace Gestion_AP
{

    static class UNC
    {

        public static String PathUNC(String path)
        {
            path = path.TrimEnd('\\', '/') + Path.DirectorySeparatorChar;
            DirectoryInfo d = new DirectoryInfo(path);
            String root = d.Root.FullName.TrimEnd('\\');

            if (!root.StartsWith(@"\\"))
            {
                ManagementObject mo = new ManagementObject();
                mo.Path = new ManagementPath(String.Format("Win32_LogicalDisk='{0}'", root));

                // DriveType 4 = Network Drive
                if (Convert.ToUInt32(mo["DriveType"]) == 4)
                    root = Convert.ToString(mo["ProviderName"]);
                else
                    root = @"\\" + System.Net.Dns.GetHostName() + "\\" + root.TrimEnd(':') + "$\\";
            }

            return Recombine(root, d);
        }

        private static String Recombine(String root, DirectoryInfo d)
        {
            Stack s = new Stack();
            while (d.Parent != null)
            {
                s.Push(d.Name);
                d = d.Parent;
            }

            while (s.Count > 0)
            {
                root = Path.Combine(root, (String)s.Pop());
            }
            return root;
        }

    }
}
