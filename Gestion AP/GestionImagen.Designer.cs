﻿namespace Gestion_AP
{
    partial class GestionImagen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestionImagen));
            this.picPortaImagen = new System.Windows.Forms.PictureBox();
            this.btnPegar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.chbUrgente = new System.Windows.Forms.CheckBox();
            this.txtIdControl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblWF_Asociado = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbGestion = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtComAdic = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbmAdm = new System.Windows.Forms.ComboBox();
            this.chbLiberar = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.picPortaImagen)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // picPortaImagen
            // 
            this.picPortaImagen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picPortaImagen.Location = new System.Drawing.Point(12, 192);
            this.picPortaImagen.Name = "picPortaImagen";
            this.picPortaImagen.Size = new System.Drawing.Size(640, 307);
            this.picPortaImagen.TabIndex = 0;
            this.picPortaImagen.TabStop = false;
            // 
            // btnPegar
            // 
            this.btnPegar.Location = new System.Drawing.Point(502, 10);
            this.btnPegar.Name = "btnPegar";
            this.btnPegar.Size = new System.Drawing.Size(73, 22);
            this.btnPegar.TabIndex = 7;
            this.btnPegar.Text = "PEGAR";
            this.btnPegar.UseVisualStyleBackColor = true;
            this.btnPegar.Click += new System.EventHandler(this.btnPegar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(583, 10);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(70, 22);
            this.btnBorrar.TabIndex = 8;
            this.btnBorrar.Text = "BORRAR";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(533, 39);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(94, 22);
            this.btnGuardar.TabIndex = 9;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // chbUrgente
            // 
            this.chbUrgente.AutoSize = true;
            this.chbUrgente.Location = new System.Drawing.Point(174, 70);
            this.chbUrgente.Name = "chbUrgente";
            this.chbUrgente.Size = new System.Drawing.Size(75, 19);
            this.chbUrgente.TabIndex = 5;
            this.chbUrgente.Text = "URGENTE";
            this.chbUrgente.UseVisualStyleBackColor = true;
            // 
            // txtIdControl
            // 
            this.txtIdControl.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtIdControl.Location = new System.Drawing.Point(116, 8);
            this.txtIdControl.Name = "txtIdControl";
            this.txtIdControl.ReadOnly = true;
            this.txtIdControl.Size = new System.Drawing.Size(170, 23);
            this.txtIdControl.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 19);
            this.label3.TabIndex = 17;
            this.label3.Text = "ID CONTROL";
            // 
            // lblWF_Asociado
            // 
            this.lblWF_Asociado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblWF_Asociado.Location = new System.Drawing.Point(116, 67);
            this.lblWF_Asociado.Name = "lblWF_Asociado";
            this.lblWF_Asociado.ReadOnly = true;
            this.lblWF_Asociado.Size = new System.Drawing.Size(52, 23);
            this.lblWF_Asociado.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 19);
            this.label2.TabIndex = 15;
            this.label2.Text = "WF";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 19);
            this.label1.TabIndex = 14;
            this.label1.Text = "GESTION";
            // 
            // cmbGestion
            // 
            this.cmbGestion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGestion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGestion.FormattingEnabled = true;
            this.cmbGestion.Location = new System.Drawing.Point(116, 37);
            this.cmbGestion.Name = "cmbGestion";
            this.cmbGestion.Size = new System.Drawing.Size(380, 24);
            this.cmbGestion.TabIndex = 3;
            this.cmbGestion.SelectedIndexChanged += new System.EventHandler(this.cmbGestion_SelectedIndexChanged);
            this.cmbGestion.SelectedValueChanged += new System.EventHandler(this.cmbGestion_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(365, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(287, 15);
            this.label4.TabIndex = 20;
            this.label4.Text = "(*)Solo puede guardar 2 (dos) imágenes por Trámite";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtComAdic);
            this.groupBox1.Location = new System.Drawing.Point(12, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(640, 83);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Comentarios Adicionales";
            // 
            // txtComAdic
            // 
            this.txtComAdic.Location = new System.Drawing.Point(6, 20);
            this.txtComAdic.Multiline = true;
            this.txtComAdic.Name = "txtComAdic";
            this.txtComAdic.Size = new System.Drawing.Size(628, 57);
            this.txtComAdic.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(292, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 19);
            this.label5.TabIndex = 23;
            this.label5.Text = "ADM";
            // 
            // cbmAdm
            // 
            this.cbmAdm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbmAdm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbmAdm.FormattingEnabled = true;
            this.cbmAdm.Items.AddRange(new object[] {
            "VISA",
            "MASTERCARD",
            "AMERICAN EXPRESS"});
            this.cbmAdm.Location = new System.Drawing.Point(344, 8);
            this.cbmAdm.Name = "cbmAdm";
            this.cbmAdm.Size = new System.Drawing.Size(152, 24);
            this.cbmAdm.TabIndex = 2;
            // 
            // chbLiberar
            // 
            this.chbLiberar.AutoSize = true;
            this.chbLiberar.Location = new System.Drawing.Point(247, 70);
            this.chbLiberar.Name = "chbLiberar";
            this.chbLiberar.Size = new System.Drawing.Size(118, 19);
            this.chbLiberar.TabIndex = 24;
            this.chbLiberar.Text = "CON LIBERACION";
            this.chbLiberar.UseVisualStyleBackColor = true;
            // 
            // GestionImagen
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(665, 511);
            this.Controls.Add(this.chbLiberar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbmAdm);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chbUrgente);
            this.Controls.Add(this.txtIdControl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblWF_Asociado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbGestion);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnPegar);
            this.Controls.Add(this.picPortaImagen);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GestionImagen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Capturas de Pantalla";
            this.Load += new System.EventHandler(this.GestionImagen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picPortaImagen)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picPortaImagen;
        private System.Windows.Forms.Button btnPegar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.CheckBox chbUrgente;
        private System.Windows.Forms.TextBox txtIdControl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox lblWF_Asociado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbGestion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtComAdic;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbmAdm;
        private System.Windows.Forms.CheckBox chbLiberar;
    }
}