﻿namespace Gestion_AP
{
    partial class VisorTexto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtVisor = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // txtVisor
            // 
            this.txtVisor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVisor.Location = new System.Drawing.Point(0, 0);
            this.txtVisor.Name = "txtVisor";
            this.txtVisor.ReadOnly = true;
            this.txtVisor.Size = new System.Drawing.Size(632, 180);
            this.txtVisor.TabIndex = 0;
            this.txtVisor.Text = "";
            // 
            // VisorTexto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 180);
            this.Controls.Add(this.txtVisor);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VisorTexto";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Visor de Texto";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtVisor;
    }
}