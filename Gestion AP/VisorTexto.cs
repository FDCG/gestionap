﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Gestion_AP
{
    public partial class VisorTexto : Form
    {
        public VisorTexto()
        {
            InitializeComponent();
        }

        public VisorTexto(string Mensaje)
        {
            InitializeComponent();
            try
            {
                txtVisor.Text = Mensaje;
            }
            catch (Exception ex)
            {

                Logins.LogError(ex);
            }
            

        }

       
    }
}
