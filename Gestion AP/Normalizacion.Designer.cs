﻿//namespace Gestion_AP
//{
//    partial class Normalizacion
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.groupBox1 = new System.Windows.Forms.GroupBox();
//            this.chbAbrir_W = new System.Windows.Forms.CheckBox();
//            this.progressBar1 = new System.Windows.Forms.ProgressBar();
//            this.label3 = new System.Windows.Forms.Label();
//            this.label2 = new System.Windows.Forms.Label();
//            this.label1 = new System.Windows.Forms.Label();
//            this.rbtExcel_W = new System.Windows.Forms.RadioButton();
//            this.rbtTxt_W = new System.Windows.Forms.RadioButton();
//            this.rbtCSV_W = new System.Windows.Forms.RadioButton();
//            this.btnExportarW = new System.Windows.Forms.Button();
//            this.btnExaminarW = new System.Windows.Forms.Button();
//            this.txtExportar = new System.Windows.Forms.TextBox();
//            this.txtOrigen = new System.Windows.Forms.TextBox();
//            this.groupBox2 = new System.Windows.Forms.GroupBox();
//            this.lblProgreso = new System.Windows.Forms.Label();
//            this.chbAbrir_B = new System.Windows.Forms.CheckBox();
//            this.txtImpB = new System.Windows.Forms.TextBox();
//            this.txtExpB = new System.Windows.Forms.TextBox();
//            this.label4 = new System.Windows.Forms.Label();
//            this.btnExaminarB = new System.Windows.Forms.Button();
//            this.label5 = new System.Windows.Forms.Label();
//            this.btnExportarB = new System.Windows.Forms.Button();
//            this.label6 = new System.Windows.Forms.Label();
//            this.rbtCSV_B = new System.Windows.Forms.RadioButton();
//            this.rbtExcel_B = new System.Windows.Forms.RadioButton();
//            this.rbtTxt_B = new System.Windows.Forms.RadioButton();
//            this.groupBox3 = new System.Windows.Forms.GroupBox();
//            this.label9 = new System.Windows.Forms.Label();
//            this.btnImportarBPM = new System.Windows.Forms.Button();
//            this.btnExaminarBPM = new System.Windows.Forms.Button();
//            this.txtPathBPM = new System.Windows.Forms.TextBox();
//            this.groupBox1.SuspendLayout();
//            this.groupBox2.SuspendLayout();
//            this.groupBox3.SuspendLayout();
//            this.SuspendLayout();
//            // 
//            // groupBox1
//            // 
//            this.groupBox1.BackColor = System.Drawing.Color.Tan;
//            this.groupBox1.Controls.Add(this.chbAbrir_W);
//            this.groupBox1.Controls.Add(this.progressBar1);
//            this.groupBox1.Controls.Add(this.label3);
//            this.groupBox1.Controls.Add(this.label2);
//            this.groupBox1.Controls.Add(this.label1);
//            this.groupBox1.Controls.Add(this.rbtExcel_W);
//            this.groupBox1.Controls.Add(this.rbtTxt_W);
//            this.groupBox1.Controls.Add(this.rbtCSV_W);
//            this.groupBox1.Controls.Add(this.btnExportarW);
//            this.groupBox1.Controls.Add(this.btnExaminarW);
//            this.groupBox1.Controls.Add(this.txtExportar);
//            this.groupBox1.Controls.Add(this.txtOrigen);
//            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
//            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.groupBox1.ForeColor = System.Drawing.Color.SeaGreen;
//            this.groupBox1.Location = new System.Drawing.Point(12, 130);
//            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
//            this.groupBox1.Name = "groupBox1";
//            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
//            this.groupBox1.Size = new System.Drawing.Size(498, 161);
//            this.groupBox1.TabIndex = 0;
//            this.groupBox1.TabStop = false;
//            this.groupBox1.Text = "BASES BAJAS WEB CALLCENTER";
//            // 
//            // chbAbrir_W
//            // 
//            this.chbAbrir_W.AutoSize = true;
//            this.chbAbrir_W.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.chbAbrir_W.ForeColor = System.Drawing.Color.Navy;
//            this.chbAbrir_W.Location = new System.Drawing.Point(135, 108);
//            this.chbAbrir_W.Name = "chbAbrir_W";
//            this.chbAbrir_W.Size = new System.Drawing.Size(110, 19);
//            this.chbAbrir_W.TabIndex = 6;
//            this.chbAbrir_W.Text = "Abrir al finalizar";
//            this.chbAbrir_W.UseVisualStyleBackColor = true;
//            // 
//            // progressBar1
//            // 
//            this.progressBar1.Location = new System.Drawing.Point(13, 131);
//            this.progressBar1.Name = "progressBar1";
//            this.progressBar1.Size = new System.Drawing.Size(469, 23);
//            this.progressBar1.TabIndex = 8;
//            // 
//            // label3
//            // 
//            this.label3.AutoSize = true;
//            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label3.ForeColor = System.Drawing.Color.SteelBlue;
//            this.label3.Location = new System.Drawing.Point(44, 87);
//            this.label3.Name = "label3";
//            this.label3.Size = new System.Drawing.Size(86, 15);
//            this.label3.TabIndex = 9;
//            this.label3.Text = "Formato Salida";
//            // 
//            // label2
//            // 
//            this.label2.AutoSize = true;
//            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label2.ForeColor = System.Drawing.Color.ForestGreen;
//            this.label2.Location = new System.Drawing.Point(36, 59);
//            this.label2.Name = "label2";
//            this.label2.Size = new System.Drawing.Size(93, 15);
//            this.label2.TabIndex = 8;
//            this.label2.Text = "Carpeta Destino";
//            // 
//            // label1
//            // 
//            this.label1.AutoSize = true;
//            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label1.ForeColor = System.Drawing.Color.DarkOliveGreen;
//            this.label1.Location = new System.Drawing.Point(10, 32);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(120, 15);
//            this.label1.TabIndex = 7;
//            this.label1.Text = "Archivo a Normalizar";
//            // 
//            // rbtExcel_W
//            // 
//            this.rbtExcel_W.AutoSize = true;
//            this.rbtExcel_W.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.rbtExcel_W.ForeColor = System.Drawing.Color.Teal;
//            this.rbtExcel_W.Location = new System.Drawing.Point(249, 85);
//            this.rbtExcel_W.Name = "rbtExcel_W";
//            this.rbtExcel_W.Size = new System.Drawing.Size(124, 19);
//            this.rbtExcel_W.TabIndex = 5;
//            this.rbtExcel_W.TabStop = true;
//            this.rbtExcel_W.Text = "Excel con Formato";
//            this.rbtExcel_W.UseVisualStyleBackColor = true;
//            // 
//            // rbtTxt_W
//            // 
//            this.rbtTxt_W.AutoSize = true;
//            this.rbtTxt_W.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.rbtTxt_W.ForeColor = System.Drawing.Color.DarkSlateGray;
//            this.rbtTxt_W.Location = new System.Drawing.Point(200, 85);
//            this.rbtTxt_W.Name = "rbtTxt_W";
//            this.rbtTxt_W.Size = new System.Drawing.Size(42, 19);
//            this.rbtTxt_W.TabIndex = 4;
//            this.rbtTxt_W.TabStop = true;
//            this.rbtTxt_W.Text = "TXT";
//            this.rbtTxt_W.UseVisualStyleBackColor = true;
//            // 
//            // rbtCSV_W
//            // 
//            this.rbtCSV_W.AutoSize = true;
//            this.rbtCSV_W.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.rbtCSV_W.ForeColor = System.Drawing.Color.Green;
//            this.rbtCSV_W.Location = new System.Drawing.Point(135, 85);
//            this.rbtCSV_W.Name = "rbtCSV_W";
//            this.rbtCSV_W.Size = new System.Drawing.Size(58, 19);
//            this.rbtCSV_W.TabIndex = 3;
//            this.rbtCSV_W.TabStop = true;
//            this.rbtCSV_W.Text = "CSV(,)";
//            this.rbtCSV_W.UseVisualStyleBackColor = true;
//            // 
//            // btnExportarW
//            // 
//            this.btnExportarW.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
//            this.btnExportarW.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
//            this.btnExportarW.ForeColor = System.Drawing.Color.Lavender;
//            this.btnExportarW.Location = new System.Drawing.Point(410, 57);
//            this.btnExportarW.Name = "btnExportarW";
//            this.btnExportarW.Size = new System.Drawing.Size(72, 21);
//            this.btnExportarW.TabIndex = 8;
//            this.btnExportarW.Text = "Exportar";
//            this.btnExportarW.UseVisualStyleBackColor = false;
//            this.btnExportarW.Click += new System.EventHandler(this.btnExportarW_Click);
//            // 
//            // btnExaminarW
//            // 
//            this.btnExaminarW.BackColor = System.Drawing.Color.SeaShell;
//            this.btnExaminarW.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
//            this.btnExaminarW.ForeColor = System.Drawing.Color.Black;
//            this.btnExaminarW.Location = new System.Drawing.Point(410, 30);
//            this.btnExaminarW.Name = "btnExaminarW";
//            this.btnExaminarW.Size = new System.Drawing.Size(72, 21);
//            this.btnExaminarW.TabIndex = 7;
//            this.btnExaminarW.Text = "Examinar...";
//            this.btnExaminarW.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            this.btnExaminarW.UseVisualStyleBackColor = false;
//            this.btnExaminarW.Click += new System.EventHandler(this.btnExaminar_Click);
//            // 
//            // txtExportar
//            // 
//            this.txtExportar.Location = new System.Drawing.Point(135, 57);
//            this.txtExportar.Name = "txtExportar";
//            this.txtExportar.ReadOnly = true;
//            this.txtExportar.Size = new System.Drawing.Size(269, 21);
//            this.txtExportar.TabIndex = 2;
//            // 
//            // txtOrigen
//            // 
//            this.txtOrigen.Location = new System.Drawing.Point(135, 30);
//            this.txtOrigen.Name = "txtOrigen";
//            this.txtOrigen.ReadOnly = true;
//            this.txtOrigen.Size = new System.Drawing.Size(269, 21);
//            this.txtOrigen.TabIndex = 1;
//            // 
//            // groupBox2
//            // 
//            this.groupBox2.BackColor = System.Drawing.Color.RosyBrown;
//            this.groupBox2.Controls.Add(this.lblProgreso);
//            this.groupBox2.Controls.Add(this.chbAbrir_B);
//            this.groupBox2.Controls.Add(this.txtImpB);
//            this.groupBox2.Controls.Add(this.txtExpB);
//            this.groupBox2.Controls.Add(this.label4);
//            this.groupBox2.Controls.Add(this.btnExaminarB);
//            this.groupBox2.Controls.Add(this.label5);
//            this.groupBox2.Controls.Add(this.btnExportarB);
//            this.groupBox2.Controls.Add(this.label6);
//            this.groupBox2.Controls.Add(this.rbtCSV_B);
//            this.groupBox2.Controls.Add(this.rbtExcel_B);
//            this.groupBox2.Controls.Add(this.rbtTxt_B);
//            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
//            this.groupBox2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.groupBox2.ForeColor = System.Drawing.Color.Indigo;
//            this.groupBox2.Location = new System.Drawing.Point(12, 304);
//            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
//            this.groupBox2.Name = "groupBox2";
//            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
//            this.groupBox2.Size = new System.Drawing.Size(498, 156);
//            this.groupBox2.TabIndex = 7;
//            this.groupBox2.TabStop = false;
//            this.groupBox2.Text = "BASES BACK OFFICE LEGAL";
//            // 
//            // lblProgreso
//            // 
//            this.lblProgreso.AutoSize = true;
//            this.lblProgreso.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.lblProgreso.ForeColor = System.Drawing.Color.Red;
//            this.lblProgreso.Location = new System.Drawing.Point(130, 135);
//            this.lblProgreso.Name = "lblProgreso";
//            this.lblProgreso.Size = new System.Drawing.Size(0, 15);
//            this.lblProgreso.TabIndex = 23;
//            // 
//            // chbAbrir_B
//            // 
//            this.chbAbrir_B.AutoSize = true;
//            this.chbAbrir_B.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.chbAbrir_B.Location = new System.Drawing.Point(135, 108);
//            this.chbAbrir_B.Name = "chbAbrir_B";
//            this.chbAbrir_B.Size = new System.Drawing.Size(110, 19);
//            this.chbAbrir_B.TabIndex = 14;
//            this.chbAbrir_B.Text = "Abrir al finalizar";
//            this.chbAbrir_B.UseVisualStyleBackColor = true;
//            // 
//            // txtImpB
//            // 
//            this.txtImpB.Location = new System.Drawing.Point(135, 30);
//            this.txtImpB.Name = "txtImpB";
//            this.txtImpB.ReadOnly = true;
//            this.txtImpB.Size = new System.Drawing.Size(269, 21);
//            this.txtImpB.TabIndex = 9;
//            // 
//            // txtExpB
//            // 
//            this.txtExpB.Location = new System.Drawing.Point(135, 57);
//            this.txtExpB.Name = "txtExpB";
//            this.txtExpB.ReadOnly = true;
//            this.txtExpB.Size = new System.Drawing.Size(269, 21);
//            this.txtExpB.TabIndex = 10;
//            // 
//            // label4
//            // 
//            this.label4.AutoSize = true;
//            this.label4.ForeColor = System.Drawing.Color.Purple;
//            this.label4.Location = new System.Drawing.Point(43, 87);
//            this.label4.Name = "label4";
//            this.label4.Size = new System.Drawing.Size(86, 15);
//            this.label4.TabIndex = 21;
//            this.label4.Text = "Formato Salida";
//            // 
//            // btnExaminarB
//            // 
//            this.btnExaminarB.BackColor = System.Drawing.Color.WhiteSmoke;
//            this.btnExaminarB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
//            this.btnExaminarB.ForeColor = System.Drawing.Color.Black;
//            this.btnExaminarB.Location = new System.Drawing.Point(410, 30);
//            this.btnExaminarB.Name = "btnExaminarB";
//            this.btnExaminarB.Size = new System.Drawing.Size(72, 21);
//            this.btnExaminarB.TabIndex = 15;
//            this.btnExaminarB.Text = "Examinar...";
//            this.btnExaminarB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            this.btnExaminarB.UseVisualStyleBackColor = false;
//            this.btnExaminarB.Click += new System.EventHandler(this.button2_Click);
//            // 
//            // label5
//            // 
//            this.label5.AutoSize = true;
//            this.label5.ForeColor = System.Drawing.Color.MediumBlue;
//            this.label5.Location = new System.Drawing.Point(36, 60);
//            this.label5.Name = "label5";
//            this.label5.Size = new System.Drawing.Size(93, 15);
//            this.label5.TabIndex = 20;
//            this.label5.Text = "Carpeta Destino";
//            // 
//            // btnExportarB
//            // 
//            this.btnExportarB.BackColor = System.Drawing.Color.Black;
//            this.btnExportarB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
//            this.btnExportarB.ForeColor = System.Drawing.Color.White;
//            this.btnExportarB.Location = new System.Drawing.Point(410, 57);
//            this.btnExportarB.Name = "btnExportarB";
//            this.btnExportarB.Size = new System.Drawing.Size(72, 21);
//            this.btnExportarB.TabIndex = 16;
//            this.btnExportarB.Text = "Exportar";
//            this.btnExportarB.UseVisualStyleBackColor = false;
//            this.btnExportarB.Click += new System.EventHandler(this.btnExportarB_Click);
//            // 
//            // label6
//            // 
//            this.label6.AutoSize = true;
//            this.label6.ForeColor = System.Drawing.Color.Maroon;
//            this.label6.Location = new System.Drawing.Point(10, 33);
//            this.label6.Name = "label6";
//            this.label6.Size = new System.Drawing.Size(120, 15);
//            this.label6.TabIndex = 18;
//            this.label6.Text = "Archivo a Normalizar";
//            // 
//            // rbtCSV_B
//            // 
//            this.rbtCSV_B.AutoSize = true;
//            this.rbtCSV_B.ForeColor = System.Drawing.Color.RoyalBlue;
//            this.rbtCSV_B.Location = new System.Drawing.Point(135, 85);
//            this.rbtCSV_B.Name = "rbtCSV_B";
//            this.rbtCSV_B.Size = new System.Drawing.Size(98, 19);
//            this.rbtCSV_B.TabIndex = 11;
//            this.rbtCSV_B.TabStop = true;
//            this.rbtCSV_B.Text = "Delimitado(@)";
//            this.rbtCSV_B.UseVisualStyleBackColor = true;
//            // 
//            // rbtExcel_B
//            // 
//            this.rbtExcel_B.AutoSize = true;
//            this.rbtExcel_B.ForeColor = System.Drawing.Color.Blue;
//            this.rbtExcel_B.Location = new System.Drawing.Point(292, 85);
//            this.rbtExcel_B.Name = "rbtExcel_B";
//            this.rbtExcel_B.Size = new System.Drawing.Size(124, 19);
//            this.rbtExcel_B.TabIndex = 13;
//            this.rbtExcel_B.TabStop = true;
//            this.rbtExcel_B.Text = "Excel con Formato";
//            this.rbtExcel_B.UseVisualStyleBackColor = true;
//            // 
//            // rbtTxt_B
//            // 
//            this.rbtTxt_B.AutoSize = true;
//            this.rbtTxt_B.ForeColor = System.Drawing.Color.Magenta;
//            this.rbtTxt_B.Location = new System.Drawing.Point(243, 85);
//            this.rbtTxt_B.Name = "rbtTxt_B";
//            this.rbtTxt_B.Size = new System.Drawing.Size(42, 19);
//            this.rbtTxt_B.TabIndex = 12;
//            this.rbtTxt_B.TabStop = true;
//            this.rbtTxt_B.Text = "TXT";
//            this.rbtTxt_B.UseVisualStyleBackColor = true;
//            // 
//            // groupBox3
//            // 
//            this.groupBox3.BackColor = System.Drawing.Color.Chocolate;
//            this.groupBox3.Controls.Add(this.label9);
//            this.groupBox3.Controls.Add(this.btnImportarBPM);
//            this.groupBox3.Controls.Add(this.btnExaminarBPM);
//            this.groupBox3.Controls.Add(this.txtPathBPM);
//            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
//            this.groupBox3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.groupBox3.ForeColor = System.Drawing.Color.Blue;
//            this.groupBox3.Location = new System.Drawing.Point(12, 17);
//            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
//            this.groupBox3.Name = "groupBox3";
//            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
//            this.groupBox3.Size = new System.Drawing.Size(498, 98);
//            this.groupBox3.TabIndex = 8;
//            this.groupBox3.TabStop = false;
//            this.groupBox3.Text = "BASE DE RECHAZOS PREVALIDACION BPM";
//            // 
//            // label9
//            // 
//            this.label9.AutoSize = true;
//            this.label9.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label9.ForeColor = System.Drawing.Color.Blue;
//            this.label9.Location = new System.Drawing.Point(10, 32);
//            this.label9.Name = "label9";
//            this.label9.Size = new System.Drawing.Size(120, 15);
//            this.label9.TabIndex = 7;
//            this.label9.Text = "Archivo a Normalizar";
//            // 
//            // btnImportarBPM
//            // 
//            this.btnImportarBPM.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
//            this.btnImportarBPM.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
//            this.btnImportarBPM.ForeColor = System.Drawing.Color.Lavender;
//            this.btnImportarBPM.Location = new System.Drawing.Point(135, 57);
//            this.btnImportarBPM.Name = "btnImportarBPM";
//            this.btnImportarBPM.Size = new System.Drawing.Size(172, 21);
//            this.btnImportarBPM.TabIndex = 8;
//            this.btnImportarBPM.Text = "Importar Rechazos";
//            this.btnImportarBPM.UseVisualStyleBackColor = false;
//            // 
//            // btnExaminarBPM
//            // 
//            this.btnExaminarBPM.BackColor = System.Drawing.Color.SeaShell;
//            this.btnExaminarBPM.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
//            this.btnExaminarBPM.ForeColor = System.Drawing.Color.Black;
//            this.btnExaminarBPM.Location = new System.Drawing.Point(410, 30);
//            this.btnExaminarBPM.Name = "btnExaminarBPM";
//            this.btnExaminarBPM.Size = new System.Drawing.Size(72, 21);
//            this.btnExaminarBPM.TabIndex = 7;
//            this.btnExaminarBPM.Text = "Examinar...";
//            this.btnExaminarBPM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            this.btnExaminarBPM.UseVisualStyleBackColor = false;
//            this.btnExaminarBPM.Click += new System.EventHandler(this.btnExaminarBPM_Click);
//            // 
//            // txtPathBPM
//            // 
//            this.txtPathBPM.Location = new System.Drawing.Point(135, 30);
//            this.txtPathBPM.Name = "txtPathBPM";
//            this.txtPathBPM.ReadOnly = true;
//            this.txtPathBPM.Size = new System.Drawing.Size(269, 21);
//            this.txtPathBPM.TabIndex = 1;
//            // 
//            // Normalizacion
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
//            this.BackColor = System.Drawing.Color.Black;
//            this.ClientSize = new System.Drawing.Size(526, 476);
//            this.Controls.Add(this.groupBox3);
//            this.Controls.Add(this.groupBox2);
//            this.Controls.Add(this.groupBox1);
//            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
//            this.MaximizeBox = false;
//            this.MinimizeBox = false;
//            this.Name = "Normalizacion";
//            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
//            this.Text = "Normalizacion";
//            this.groupBox1.ResumeLayout(false);
//            this.groupBox1.PerformLayout();
//            this.groupBox2.ResumeLayout(false);
//            this.groupBox2.PerformLayout();
//            this.groupBox3.ResumeLayout(false);
//            this.groupBox3.PerformLayout();
//            this.ResumeLayout(false);

//        }

//        #endregion

//        private System.Windows.Forms.GroupBox groupBox1;
//        private System.Windows.Forms.RadioButton rbtExcel_W;
//        private System.Windows.Forms.RadioButton rbtTxt_W;
//        private System.Windows.Forms.RadioButton rbtCSV_W;
//        private System.Windows.Forms.Button btnExportarW;
//        private System.Windows.Forms.Button btnExaminarW;
//        private System.Windows.Forms.TextBox txtExportar;
//        private System.Windows.Forms.TextBox txtOrigen;
//        private System.Windows.Forms.GroupBox groupBox2;
//        private System.Windows.Forms.Label label3;
//        private System.Windows.Forms.Label label2;
//        private System.Windows.Forms.Label label1;
//        public System.Windows.Forms.ProgressBar progressBar1;
//        private System.Windows.Forms.CheckBox chbAbrir_W;
//        private System.Windows.Forms.CheckBox chbAbrir_B;
//        private System.Windows.Forms.TextBox txtImpB;
//        private System.Windows.Forms.TextBox txtExpB;
//        private System.Windows.Forms.Label label4;
//        private System.Windows.Forms.Button btnExaminarB;
//        private System.Windows.Forms.Label label5;
//        private System.Windows.Forms.Button btnExportarB;
//        private System.Windows.Forms.Label label6;
//        private System.Windows.Forms.RadioButton rbtCSV_B;
//        private System.Windows.Forms.RadioButton rbtExcel_B;
//        private System.Windows.Forms.RadioButton rbtTxt_B;
//        private System.Windows.Forms.Label lblProgreso;
//        private System.Windows.Forms.GroupBox groupBox3;
//        private System.Windows.Forms.Label label9;
//        private System.Windows.Forms.Button btnImportarBPM;
//        private System.Windows.Forms.Button btnExaminarBPM;
//        private System.Windows.Forms.TextBox txtPathBPM;
//    }
//}