﻿namespace Gestion_AP
{
    partial class Contador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Contador));
            this.nudCant1 = new System.Windows.Forms.NumericUpDown();
            this.cmbTram1 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.nudCant5 = new System.Windows.Forms.NumericUpDown();
            this.nudCant4 = new System.Windows.Forms.NumericUpDown();
            this.nudCant3 = new System.Windows.Forms.NumericUpDown();
            this.nudCant2 = new System.Windows.Forms.NumericUpDown();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtNov2 = new System.Windows.Forms.TextBox();
            this.cmbTram2 = new System.Windows.Forms.ComboBox();
            this.txtNov3 = new System.Windows.Forms.TextBox();
            this.cmbTram3 = new System.Windows.Forms.ComboBox();
            this.txtNov4 = new System.Windows.Forms.TextBox();
            this.cmbTram4 = new System.Windows.Forms.ComboBox();
            this.txtNov5 = new System.Windows.Forms.TextBox();
            this.cmbTram5 = new System.Windows.Forms.ComboBox();
            this.txtNov1 = new System.Windows.Forms.TextBox();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant2)).BeginInit();
            this.SuspendLayout();
            // 
            // nudCant1
            // 
            this.nudCant1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCant1.Location = new System.Drawing.Point(359, 34);
            this.nudCant1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nudCant1.Name = "nudCant1";
            this.nudCant1.Size = new System.Drawing.Size(51, 23);
            this.nudCant1.TabIndex = 0;
            this.nudCant1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmbTram1
            // 
            this.cmbTram1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTram1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTram1.FormattingEnabled = true;
            this.cmbTram1.Location = new System.Drawing.Point(4, 34);
            this.cmbTram1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbTram1.Name = "cmbTram1";
            this.cmbTram1.Size = new System.Drawing.Size(261, 24);
            this.cmbTram1.TabIndex = 1;
            this.cmbTram1.SelectedIndexChanged += new System.EventHandler(this.cmbTram1_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Blue;
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(7, 8);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(437, 225);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registro de Cantidades";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.18987F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.04431F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.76582F));
            this.tableLayoutPanel1.Controls.Add(this.nudCant5, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.nudCant4, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.nudCant3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.nudCant2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmbTram1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.nudCant1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtNov2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.cmbTram2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtNov3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmbTram3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtNov4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.cmbTram4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtNov5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.cmbTram5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtNov1, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(17, 25);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(414, 177);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // nudCant5
            // 
            this.nudCant5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCant5.Location = new System.Drawing.Point(359, 150);
            this.nudCant5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nudCant5.Name = "nudCant5";
            this.nudCant5.Size = new System.Drawing.Size(51, 23);
            this.nudCant5.TabIndex = 13;
            this.nudCant5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudCant4
            // 
            this.nudCant4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCant4.Location = new System.Drawing.Point(359, 121);
            this.nudCant4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nudCant4.Name = "nudCant4";
            this.nudCant4.Size = new System.Drawing.Size(51, 23);
            this.nudCant4.TabIndex = 10;
            this.nudCant4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudCant3
            // 
            this.nudCant3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCant3.Location = new System.Drawing.Point(359, 92);
            this.nudCant3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nudCant3.Name = "nudCant3";
            this.nudCant3.Size = new System.Drawing.Size(51, 23);
            this.nudCant3.TabIndex = 7;
            this.nudCant3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudCant2
            // 
            this.nudCant2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCant2.Location = new System.Drawing.Point(359, 63);
            this.nudCant2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nudCant2.Name = "nudCant2";
            this.nudCant2.Size = new System.Drawing.Size(51, 23);
            this.nudCant2.TabIndex = 4;
            this.nudCant2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.ForeColor = System.Drawing.Color.Indigo;
            this.textBox3.Location = new System.Drawing.Point(359, 5);
            this.textBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(51, 22);
            this.textBox3.TabIndex = 2;
            this.textBox3.Text = "CANTIDAD";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.Navy;
            this.textBox2.Location = new System.Drawing.Point(272, 5);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(80, 22);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "NOVEDAD";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.DarkGreen;
            this.textBox1.Location = new System.Drawing.Point(4, 5);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(261, 22);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "NOMBRE DEL TRÁMITE";
            // 
            // txtNov2
            // 
            this.txtNov2.Enabled = false;
            this.txtNov2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNov2.Location = new System.Drawing.Point(272, 63);
            this.txtNov2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNov2.Name = "txtNov2";
            this.txtNov2.ReadOnly = true;
            this.txtNov2.Size = new System.Drawing.Size(80, 23);
            this.txtNov2.TabIndex = 6;
            this.txtNov2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmbTram2
            // 
            this.cmbTram2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTram2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTram2.FormattingEnabled = true;
            this.cmbTram2.Location = new System.Drawing.Point(4, 63);
            this.cmbTram2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbTram2.Name = "cmbTram2";
            this.cmbTram2.Size = new System.Drawing.Size(261, 24);
            this.cmbTram2.TabIndex = 5;
            this.cmbTram2.SelectedIndexChanged += new System.EventHandler(this.cmbTram2_SelectedIndexChanged);
            // 
            // txtNov3
            // 
            this.txtNov3.Enabled = false;
            this.txtNov3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNov3.Location = new System.Drawing.Point(272, 92);
            this.txtNov3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNov3.Name = "txtNov3";
            this.txtNov3.ReadOnly = true;
            this.txtNov3.Size = new System.Drawing.Size(80, 23);
            this.txtNov3.TabIndex = 9;
            this.txtNov3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmbTram3
            // 
            this.cmbTram3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTram3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTram3.FormattingEnabled = true;
            this.cmbTram3.Location = new System.Drawing.Point(4, 92);
            this.cmbTram3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbTram3.Name = "cmbTram3";
            this.cmbTram3.Size = new System.Drawing.Size(261, 24);
            this.cmbTram3.TabIndex = 8;
            this.cmbTram3.SelectedIndexChanged += new System.EventHandler(this.cmbTram3_SelectedIndexChanged);
            // 
            // txtNov4
            // 
            this.txtNov4.Enabled = false;
            this.txtNov4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNov4.Location = new System.Drawing.Point(272, 121);
            this.txtNov4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNov4.Name = "txtNov4";
            this.txtNov4.ReadOnly = true;
            this.txtNov4.Size = new System.Drawing.Size(80, 23);
            this.txtNov4.TabIndex = 12;
            this.txtNov4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmbTram4
            // 
            this.cmbTram4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTram4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTram4.FormattingEnabled = true;
            this.cmbTram4.Location = new System.Drawing.Point(4, 121);
            this.cmbTram4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbTram4.Name = "cmbTram4";
            this.cmbTram4.Size = new System.Drawing.Size(261, 24);
            this.cmbTram4.TabIndex = 11;
            this.cmbTram4.SelectedIndexChanged += new System.EventHandler(this.cmbTram4_SelectedIndexChanged);
            // 
            // txtNov5
            // 
            this.txtNov5.Enabled = false;
            this.txtNov5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNov5.Location = new System.Drawing.Point(272, 150);
            this.txtNov5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNov5.Name = "txtNov5";
            this.txtNov5.ReadOnly = true;
            this.txtNov5.Size = new System.Drawing.Size(80, 23);
            this.txtNov5.TabIndex = 15;
            this.txtNov5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmbTram5
            // 
            this.cmbTram5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTram5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTram5.FormattingEnabled = true;
            this.cmbTram5.Location = new System.Drawing.Point(4, 150);
            this.cmbTram5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbTram5.Name = "cmbTram5";
            this.cmbTram5.Size = new System.Drawing.Size(261, 24);
            this.cmbTram5.TabIndex = 14;
            this.cmbTram5.SelectedIndexChanged += new System.EventHandler(this.cmbTram5_SelectedIndexChanged);
            // 
            // txtNov1
            // 
            this.txtNov1.Enabled = false;
            this.txtNov1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNov1.Location = new System.Drawing.Point(272, 34);
            this.txtNov1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNov1.Name = "txtNov1";
            this.txtNov1.ReadOnly = true;
            this.txtNov1.Size = new System.Drawing.Size(80, 23);
            this.txtNov1.TabIndex = 3;
            this.txtNov1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.DarkBlue;
            this.btnAyuda.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAyuda.Location = new System.Drawing.Point(450, 8);
            this.btnAyuda.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(87, 28);
            this.btnAyuda.TabIndex = 3;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.BackColor = System.Drawing.Color.DarkBlue;
            this.btnRegistrar.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrar.ForeColor = System.Drawing.SystemColors.Control;
            this.btnRegistrar.Location = new System.Drawing.Point(450, 80);
            this.btnRegistrar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(87, 28);
            this.btnRegistrar.TabIndex = 4;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = false;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.BackColor = System.Drawing.Color.DarkBlue;
            this.btnBorrar.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrar.ForeColor = System.Drawing.SystemColors.Control;
            this.btnBorrar.Location = new System.Drawing.Point(450, 44);
            this.btnBorrar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(87, 28);
            this.btnBorrar.TabIndex = 5;
            this.btnBorrar.Text = "Borrar Todo";
            this.btnBorrar.UseVisualStyleBackColor = false;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // Contador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.DarkGreen;
            this.ClientSize = new System.Drawing.Size(546, 239);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.btnAyuda);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "Contador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ":: Abaco ::";
            this.Load += new System.EventHandler(this.Contador_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudCant1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nudCant1;
        private System.Windows.Forms.ComboBox cmbTram1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.NumericUpDown nudCant5;
        private System.Windows.Forms.NumericUpDown nudCant4;
        private System.Windows.Forms.NumericUpDown nudCant3;
        private System.Windows.Forms.NumericUpDown nudCant2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtNov2;
        private System.Windows.Forms.ComboBox cmbTram2;
        private System.Windows.Forms.TextBox txtNov3;
        private System.Windows.Forms.ComboBox cmbTram3;
        private System.Windows.Forms.TextBox txtNov4;
        private System.Windows.Forms.ComboBox cmbTram4;
        private System.Windows.Forms.TextBox txtNov5;
        private System.Windows.Forms.ComboBox cmbTram5;
        private System.Windows.Forms.TextBox txtNov1;
        private System.Windows.Forms.Button btnBorrar;
    }
}