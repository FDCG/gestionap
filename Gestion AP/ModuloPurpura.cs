﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
namespace Gestion_AP
{
    public partial class ModuloPurpura : Form
    {
        public ModuloPurpura()
        {
            InitializeComponent();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        WebBrowser flow = new WebBrowser();


        public string[] Modelos = { "091", "092", "914", "314","PBAJAs","919","319" ,"BAJAS", "916", "316", "SVIDA", "500", "RECUP", "MOROS" };

        private void txtCargar_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsHTMLDataOnClipboard = Clipboard.ContainsData(DataFormats.Html);


                string htmlData = null;

                if (IsHTMLDataOnClipboard)
                {

                   // htmlData = Clipboard.GetText(TextDataFormat.Html);
                    //htmlData = Clipboard.GetDataObject().GetData(DataFormats.Html).ToString();
                    //UTF8Encoding utf8 = new UTF8Encoding();
                    //byte[] codificadoBytes = utf8.GetBytes(htmlData);
                    //htmlData=utf8.GetString(codificadoBytes);

                    // **** Para frameworks menores a 4.5 *****

                    htmlData = (string)Clipboard.GetDataObject().GetData(DataFormats.Html, true);
                 
                    htmlData = Encoding.UTF8.GetString(Encoding.Default.GetBytes(htmlData));
                    




                    flow.ScriptErrorsSuppressed = true;
                    flow.DocumentText = htmlData;
                    flow.Document.OpenNew(true);
                    
                    flow.Document.Write(htmlData);
                    flow.Refresh();

                    Datos();
                    DatosTabla();
                    DatosTablaTJ();
                    try
                    {
                        if (txtTramite.Text == "TJRI" || txtTramite.Text == "TJAA" || txtTramite.Text == "TJAI")
                        {
                            if (txtTarjeta.Text.StartsWith("532384"))
                            {

                                MessageBox.Show("Trámite Emisión Alta por Tarjeta de MercadoPago.\nAntes de procesarlo verifique:\n•La tarjeta a reimprimir debe estar habilitada.\n•Estado en Distribución 5190 (Legajo Firmado).\n•La tarjeta anterior no haya sido boletinada por falta de documentación.", "MercadoPago", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                Logins.Login(Ingreso.UsuarioLogueado, "", DateTime.Now, "El usuario registrado ha sido notificado que el trámite " + txtNroTramite.Text + " presenta condición: 'MercadoPago. Revisión adicional'. Confirma su verificación y notificación de antecendentes");

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logins.LogError(ex);
                    }
                    try
                    {
                        if (txtTramite.Text != "TJAV" && txtTramite.Text != "TJAM" && txtTramite.Text != "TJAX")
                        {
                            int inicio;
                            int fin;
                            inicio = txtInfAdic.Text.IndexOf("Liq.:");
                            fin = txtInfAdic.Text.IndexOf("Limite:");
                            string ModLiq = txtInfAdic.Text.Substring(inicio + 5, fin - inicio - 5).Trim();

                            if (Modelos.Contains(ModLiq))
                            {
                                MessageBox.Show("Cuenta con parametría de Baja o Cliente en Gestión.\nAntes de avanzar, verifique:\n✓Modelo de Liquidación\n✓Estado de la Cuenta-Tarjeta\n✓Grupo de Afinidad (VISA/AMEX)\n✓Tarjeta con no Renovación (MASTERCARD)", "Parametría Erronea", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                Logins.Login(Ingreso.UsuarioLogueado, "", DateTime.Now, "El usuario registrado ha sido notificado que el trámite "+txtNroTramite.Text+ " presenta condición: 'Cuenta con parametría de Baja o Cliente en Gestión'. Confirma su verificación y notificación de antecendentes");

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logins.LogError(ex);
                    }
                    try
                    {
                        if (txtTramite.Text != "TJAV" && txtTramite.Text != "TJAM" && txtTramite.Text != "TJAX")
                        {
                            int inicio;
                            int fin;
                            inicio = txtInfAdic.Text.IndexOf("Limite:");
                            fin = txtInfAdic.Text.IndexOf("Gr.Aff:");
                            int Limite = Convert.ToInt32(txtInfAdic.Text.Substring(inicio + 7, fin - inicio - 7).Trim().Replace("$", "").Replace(".", ""));

                            if (Limite <= 300)
                            {
                                MessageBox.Show("Cuenta con Límite $ "+Limite+".\nAntes de avanzar, verifique:\n✓Tarjeta Preembozada\n✓Error en coeficiente(MASTERCARD)\n✓Limites porcentualizados", "Límite de Cuenta Erroneo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                Logins.Login(Ingreso.UsuarioLogueado, "", DateTime.Now, "El usuario registrado ha sido notificado que el trámite " + txtNroTramite.Text + " presenta condición: 'Posible Preembozado'. Confirma su verificación y notificación de antecendentes");

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logins.LogError(ex);
                    }




                    try
                    {
                        if (txtTramite.Text != "TJAV" && txtTramite.Text != "TJAM" && txtTramite.Text != "TJAX")
                        {
                            int inicio;
                            
                            inicio = txtInfAdic.Text.IndexOf("Suc:");
                            
                            string SucRad = txtInfAdic.Text.Substring(inicio + 4, 3).Trim();

                            if (SucRad=="960")
                            {
                                MessageBox.Show("Cuenta con Sucursal Radicación 960.\nNo procesar trámite por discontinuidad operativa", "Parametría Erronea", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                Logins.Login(Ingreso.UsuarioLogueado, "", DateTime.Now, "El usuario registrado ha sido notificado que el trámite " + txtNroTramite.Text + " presenta condición: 'Radicación 960'. Confirma su verificación y notificación de antecendentes");

                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        Logins.LogError(ex);
                    }


                    try
                    {

                        OleDbConnection cn = new OleDbConnection(Conexion.cnProceso);
                        OleDbCommand cmd = new OleDbCommand();
                        cmd.Connection = cn;
                        cmd.CommandText = "SELECT * FROM WORKFLOW WHERE DNI='" + txtDni.Text.Trim() + "'";
                        DataTable dt = new DataTable();
                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(dt);

                        if (dt.Rows.Count >= 1)
                        {
                            Recientes frm = new Recientes(txtDni.Text.Trim());
                            frm.Show();

                        }


                    }
                    catch (Exception ex)
                    {

                        Logins.LogError(ex);
                    }


                }
                else
                {
                    MessageBox.Show("Los datos copiados no contienen estructura WorkFlow");

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error Interno. Algún dato no se pudo recuperar.\nContacte al administrador de la aplicación.\nError: " + ex.Message);
                Logins.LogError(ex);
            }


        }
        private void Datos()


        {

            try
            {
                

                // TRAMITE Y NRO
                if (flow.Document.GetElementById("_W005TRAMID") != null)
                {
                    string strTramite = flow.Document.GetElementById("_W005TRAMID").GetAttribute("value").ToString().Trim();
                    txtTramite.Text = strTramite.Substring(0, 4);

                    txtNroTramite.Text = strTramite.Substring(4);
                }
                // DESCRIPCION
                if (flow.Document.GetElementById("_W004DESCRI") != null)
                {
                    txtDesc.Text = flow.Document.GetElementById("_W004DESCRI").GetAttribute("value").ToString().Trim();
                }

                //CUIL Y DNI
                if (flow.Document.GetElementById("_W005NUMDOC") != null)
                {
                    txtPersona.Text = flow.Document.GetElementById("_W005NUMDOC").GetAttribute("value").ToString().Trim();
                    if (txtPersona.TextLength == 11)
                    {
                        txtDni.Text = txtPersona.Text.Substring(2, 8);
                    }
                    else
                    {
                        txtDni.Text = txtPersona.Text;
                    }

                }
                // NOMBRE
                if (flow.Document.GetElementById("_W005NOMBRE") != null)
                {
                    txtNombre.Text = flow.Document.GetElementById("_W005NOMBRE").GetAttribute("value").ToString().Trim();
                }
                //CLIENTE
                if (flow.Document.GetElementById("_W005CLIENT") != null)
                {
                    txtCliente.Text = flow.Document.GetElementById("_W005CLIENT").GetAttribute("value").ToString().Trim();
                }
                //DENOMINACION CLIENTE
                if (flow.Document.GetElementById("_CTNOM") != null)
                {
                    txtDenomCte.Text = flow.Document.GetElementById("_CTNOM").GetAttribute("value").ToString().Trim();
                }
                //TARJETA
                if (flow.Document.GetElementById("_W005TJ") != null)
                {
                    txtTarjeta.Text = flow.Document.GetElementById("_W005TJ").GetAttribute("value").ToString().Trim();
                    if (txtTarjeta.Text == "" || txtTarjeta.Text == "0")
                    {
                        txtTarjeta.Text = "NO INFORMA";
                    }
                }
                // MOTIVO DEL TRAMITE
                if (flow.Document.GetElementById("_W005TIPITX") != null)
                {
                    txtMotivo.Text = flow.Document.GetElementById("_W005TIPITX").GetAttribute("value").ToString().Trim();
                }
                // ADMINISTRADORA
                if (flow.Document.GetElementById("_W005EMPRTJ") != null)
                {
                    if (flow.Document.GetElementById("_W005EMPRTJ").GetAttribute("value").ToString()=="1")
                    {
                        txtAdministradora.Text = "VISA";
                    }
                    else if (flow.Document.GetElementById("_W005EMPRTJ").GetAttribute("value").ToString() == "2")
                    {
                        txtAdministradora.Text = "MASTERCARD";
                    }
                    else if (flow.Document.GetElementById("_W005EMPRTJ").GetAttribute("value").ToString() == "3")
                    {
                        txtAdministradora.Text = "AMERICAN EXPRESS";
                    }


                }



               
                // EMBOZADO
                if (flow.Document.GetElementById("_NOMTARJETA") != null)
                {
                    txtEmbozado.Text = flow.Document.GetElementById("_NOMTARJETA").GetAttribute("value").ToString().Trim();
                    if (txtEmbozado.Text == "")
                    {
                        txtEmbozado.Text = "NO INFORMA";
                    }
                }
                // CUENTA TARJETA
                if (flow.Document.GetElementById("_W005CUENTJ") != null)
                {
                    txtCuentaTJ.Text = flow.Document.GetElementById("_W005CUENTJ").GetAttribute("value").ToString().Trim();

                    if (txtCuentaTJ.Text == "" || txtCuentaTJ.Text == "0")
                    {
                        txtCuentaTJ.Text = "NO INFORMA";
                    }
                }
                // TITULAR CUENTA
                if (flow.Document.GetElementById("_DEND50") != null)
                {
                    txtTitTJ.Text = flow.Document.GetElementById("_DEND50").GetAttribute("value").ToString().Trim();
                    if (txtTitTJ.Text == "")
                    {
                        txtTitTJ.Text = "NO INFORMA";
                    }
                }
                //TELEFONO
                if (flow.Document.GetElementById("_W005TELEFO") != null)
                {

                    txtTelefono.Text = flow.Document.GetElementById("_W005TELEFO").GetAttribute("value").ToString().Trim();
                    if (txtTelefono.Text == "")
                    {
                        txtTelefono.Text = "NO INFORMA";
                    }
                }

                //LIMITE ACTUAL
                if (flow.Document.GetElementById("_W005IMPORT") != null)
                {

                    txtLim.Text = flow.Document.GetElementById("_W005IMPORT").GetAttribute("value").ToString().Trim();
                    if (txtLim.Text == "")
                    {
                        txtLim.Text = "NO INFORMA";
                    }
                }

                //IMPORTE AUTORIZADO
                if (flow.Document.GetElementById("_W24ADIIMP") != null)
                {

                    txtLimAut.Text = flow.Document.GetElementById("_W24ADIIMP").GetAttribute("value").ToString().Trim();
                    if (txtLimAut.Text == "")
                    {
                        txtLimAut.Text = "NO INFORMA";
                    }
                }
                // MODELO DE LIQUIDACION
                if (flow.Document.GetElementById("_MODLIQ") != null)
                {

                    txtModLiq.Text = flow.Document.GetElementById("_MODLIQ").GetAttribute("value").ToString().Trim();
                    if (txtModLiq.Text == "")
                    {
                        txtModLiq.Text = "NO INFORMA";
                    }
                }
                // TIPO DE TARJETA
                if (flow.Document.GetElementById("_TIPOTARJ") != null)
                {

                    txtTipoTJ.Text = flow.Document.GetElementById("_TIPOTARJ").GetAttribute("value").ToString().Trim();
                    if (txtTipoTJ.Text == "")
                    {
                        txtTipoTJ.Text = "NO INFORMA";
                    }
                }
                // CONVENIO PS
                if (flow.Document.GetElementById("_CONVENIOPS") != null)
                {

                    txtConvPS.Text = flow.Document.GetElementById("_CONVENIOPS").GetAttribute("value").ToString().Trim();
                    if (txtConvPS.Text == "")
                    {
                       txtConvPS.Text = "N/A";
                    }
                }

                // CODIGO DE PAQUETE
                if (flow.Document.GetElementById("_CODPAQUETE") != null)
                {

                    txtCodPaq.Text = flow.Document.GetElementById("_CODPAQUETE").GetAttribute("value").ToString().Trim();
                    if (txtCodPaq.Text == "")
                    {
                       txtCodPaq.Text = "N/A";
                    }
                }

                // FORMA DE PAGO
                if (flow.Document.GetElementById("_FMAPGO") != null)
                {

                    txtFpago.Text = flow.Document.GetElementById("_FMAPGO").GetAttribute("value").ToString().Trim();
                    if (txtFpago.Text=="")
                    {
                        txtFpago.Text = "NO INFORMA";
                    }
                }
                // CUENTA DEBITO
                if (flow.Document.GetElementById("_W005MODULO") != null)
                {
                    txtCtaDeb.Text = flow.Document.GetElementById("_W005MODULO").GetAttribute("value").ToString().Trim() + "-" +
                    flow.Document.GetElementById("_W005MONEDA").GetAttribute("value").ToString().Trim() + "-" +
                    flow.Document.GetElementById("_W005CASA").GetAttribute("value").ToString().Trim() + "-" +
                    flow.Document.GetElementById("_W005CUENTA").GetAttribute("value").ToString().Trim() + "-" +
                    flow.Document.GetElementById("_W005SCUENT").GetAttribute("value").ToString().Trim();
                    if (txtCtaDeb.Text == "0-0-0-0-0")
                    {
                       txtCtaDeb.Text = "NO INFORMA";
                    }
                }
                // DOMICILIO ACTUAL
                if (flow.Document.GetElementById("_VDOMACTUAL") != null)
                {

                   txtDomicilio.Text = flow.Document.GetElementById("_VDOMACTUAL").GetAttribute("value").ToString().Trim();
                    if (txtDomicilio.Text == "")
                    {
                        txtDomicilio.Text = "NO INFORMA";
                    }
                }
                // CODIGO GEOGRAFICO
                if (flow.Document.GetElementById("_CODGEO") != null)
                {

                    txtCodGeo.Text = flow.Document.GetElementById("_CODGEO").GetAttribute("value").ToString().Trim();
                    if (txtCodGeo.Text == "")
                    {
                        txtCodGeo.Text = "NO INFORMA";
                    }
                }
                // DESCRIPCION CODIGO GEOGRAFICO
                if (flow.Document.GetElementById("_DESCCGEO") != null)
                {

                    txtDescGeo.Text = flow.Document.GetElementById("_DESCCGEO").GetAttribute("value").ToString().Trim();
                    if (txtDescGeo.Text == "")
                    {
                        txtDescGeo.Text = "NO INFORMA";
                    }
                }
                // DOMICILIO ESPECIAL BOOLEANO
                if (flow.Document.GetElementById("_VDOMESP") != null)
                {

                    txtDomEsp.Text = flow.Document.GetElementById("_VDOMESP").GetAttribute("value").ToString().Trim();
                    if (txtDomEsp.Text == "")
                    {
                        txtDomEsp.Text = "N/A";
                    }
                }
                // SUCURSAL ESPECIAL
                if (flow.Document.GetElementById("_W005SUCESP") != null)
                {

                    txtSucEntrega.Text = flow.Document.GetElementById("_W005SUCESP").GetAttribute("value").ToString().Trim();
                    if (txtSucEntrega.Text == "0")
                    {
                        txtSucEntrega.Text = "NO INFORMA";
                    }
                }
                // CODIGO DE OCA
                if (flow.Document.GetElementById("_VDESCOCA") != null)
                {

                    txtCodOca.Text = flow.Document.GetElementById("_VDESCOCA").GetAttribute("value").ToString().Trim();
                    if (txtCodOca.Text == "")
                    {
                        txtCodOca.Text = "NO INFORMA";
                    }
                }
                // DOMICILIO
                if (flow.Document.GetElementById("_VDOMICILIO") != null)
                {

                    txtDomEnt.Text = flow.Document.GetElementById("_VDOMICILIO").GetAttribute("value").ToString().Trim();
                    if (txtDomEnt.Text == "0  () - -")
                    {
                        txtDomEnt.Text = "NO INFORMA";
                    }
                }
                // HORARIO DE ENTREGA
                if (flow.Document.GetElementById("_W005HORARI") != null)
                {

                    txtHorEnt.Text = flow.Document.GetElementById("_W005HORARI").GetAttribute("value").ToString().Trim();
                    if (txtHorEnt.Text == "")
                    {
                        txtHorEnt.Text = "NO INFORMA";
                    }
                }
                // INFORMACION ADICIONAL
                if (flow.Document.GetElementById("_W005INFADI") != null)
                {

                    txtInfAdic.Text = flow.Document.GetElementById("_W005INFADI").GetAttribute("value").ToString().Trim();
                    txtInfAdic.Text = txtInfAdic.Text.Replace("Â", "");
                }
                if (flow.Document.GetElementById("_W005TEXTO") != null)
                {

                    txtObservaciones.Text = flow.Document.GetElementById("_W005TEXTO").GetAttribute("value").ToString().Trim();
                    txtObservaciones.Text = txtObservaciones.Text.Replace("Â", "");
                }
                if (flow.Document.GetElementById("_PRIORIDAD")!=null)
                {
                    if (flow.Document.GetElementById("_PRIORIDAD").GetAttribute("value").ToString()=="N")
                    {
                        txtPrioridad.Text = "NORMAL";
                    }
                    if (flow.Document.GetElementById("_PRIORIDAD").GetAttribute("value").ToString() == "U")
                    {
                        txtPrioridad.Text = "URGENTE";
                    }
                }






                try
                {
                    
                    foreach (HtmlElement Etiqueta in flow.Document.All)
                    {


                        if (Etiqueta.GetAttribute("name").Contains("_PRIORIDAD"))
                        {

                            foreach (HtmlElement item in Etiqueta.Children)
                            {
                                if (item.GetAttribute("selected") == "True")
                                {
                                    txtPrioridad.Text = item.InnerText.ToUpper();
                                }
                            }

                        }
                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudieron recuperar algunos datos de Prioridad y/o Usuario.\nConsulte al administrador.");
                    Logins.LogError(ex);

                }
               

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);
            }

        }


        // TABLA DE Procesos HISTORICOS
        private void DatosTabla()

        {
            string Origen = null;
            int Ultimo;
            int Metodo = 0;
            foreach (HtmlElement tag in flow.Document.All)
            {
                if (tag.GetAttribute("id").Contains("span_W0208") && tag.InnerText != null)
                {
                    if (tag.InnerText.Contains("Inicio Sky"))
                    {
                        Origen = tag.GetAttribute("id").ToString().Trim();
                        Metodo = 1;
                        goto Salto;
                    }
                }
                if (tag.GetAttribute("id").Contains("span_W0176") && tag.InnerText != null)
                {
                    if (tag.InnerText.Contains("Inicio Sky"))
                    {
                        Origen = tag.GetAttribute("id").ToString().Trim();
                        Metodo = 2;
                        goto Salto;
                    }
                }


            }
            Salto:
            if (Origen != null)
            {
                try
                {
                    Ultimo = Convert.ToInt32(Origen.Substring(Origen.Length - 1));
                    object[,] historico = new object[Ultimo, 5];

                    if (Metodo == 1)
                    {
                        for (int i = Ultimo; i > 0; i--)
                        {

                            historico[i - 1, 0] = flow.Document.GetElementById("span_W0208_VFECHA_" + i.ToString().PadLeft(4, '0')).InnerText;
                            historico[i - 1, 1] = flow.Document.GetElementById("span_W0208_VTAREA_" + i.ToString().PadLeft(4, '0')).InnerText;
                            historico[i - 1, 2] = flow.Document.GetElementById("span_W0208_VUSUARIO_" + i.ToString().PadLeft(4, '0')).InnerText;
                            historico[i - 1, 3] = flow.Document.GetElementById("span_W0208_NOMBRE_" + i.ToString().PadLeft(4, '0')).InnerText;
                            if (flow.Document.GetElementById("span_W0208_VHISTCOMEN_" + i.ToString().PadLeft(4, '0')).InnerText != null)
                            {
                                historico[i - 1, 4] = flow.Document.GetElementById("span_W0208_VHISTCOMEN_" + i.ToString().PadLeft(4, '0')).InnerText;
                            }
                            else
                            {
                                historico[i - 1, 4] = "-";
                            }

                        }

                    }
                    if (Metodo == 2)
                    {

                        for (int i = Ultimo; i > 0; i--)
                        {

                            historico[i - 1, 0] = flow.Document.GetElementById("span_W0176_VFECHA_" + i.ToString().PadLeft(4, '0')).InnerText;
                            historico[i - 1, 1] = flow.Document.GetElementById("span_W0176_VTAREA_" + i.ToString().PadLeft(4, '0')).InnerText;
                            historico[i - 1, 2] = flow.Document.GetElementById("span_W0176_VUSUARIO_" + i.ToString().PadLeft(4, '0')).InnerText;
                            historico[i - 1, 3] = flow.Document.GetElementById("span_W0176_NOMBRE_" + i.ToString().PadLeft(4, '0')).InnerText;
                            if (flow.Document.GetElementById("span_W0176_VHISTCOMEN_" + i.ToString().PadLeft(4, '0')).InnerText != null)
                            {
                                historico[i - 1, 4] = flow.Document.GetElementById("span_W0176_VHISTCOMEN_" + i.ToString().PadLeft(4, '0')).InnerText;
                            }
                            else
                            {
                                historico[i - 1, 4] = "-";
                            }

                        }

                    }



                    DataTable dt = new DataTable();
                    dt.Columns.Add("Fecha");
                    dt.Columns.Add("Tarea");
                    dt.Columns.Add("Usuario");
                    dt.Columns.Add("Nombre");
                    dt.Columns.Add("Comentario");

                    DataRow dr;

                    for (int i = 0; i < Ultimo; i++)

                    {
                        dr = dt.NewRow();
                        dr["Fecha"] = historico[i, 0];
                        dr["Tarea"] = historico[i, 1];
                        dr["Usuario"] = historico[i, 2];
                        dr["Nombre"] = historico[i, 3];
                        dr["Comentario"] = historico[i, 4];
                        dt.Rows.Add(dr);


                    }
                    dgwHistorico.DataSource = dt;
                    AspectoTabla.AspectoDGV(dgwHistorico, Color.LightBlue);
                    dgwHistorico.ReadOnly = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudo obtener datos historicos del tramite.\nRevise los datos copiados.");
                    Logins.LogError(ex);
                }

            }


        }


        private void DatosTablaTJ()

        {

            int Ultimo = 0;
            int Tarjetas = 0;

            foreach (HtmlElement tag in flow.Document.All)
            {
                if (tag.GetAttribute("id").Contains("_W014PENDOC") && !tag.GetAttribute("id").Contains("span"))
                {
                    Ultimo++;
                }
                if (tag.GetAttribute("id").Contains("_TD65NROTAR") && !tag.GetAttribute("id").Contains("span"))
                {
                   Tarjetas++;
                }


            }

            // METODO PERSONAS
            if (Ultimo > 0)
            {
                try
                {

                    object[,] tjs = new object[Ultimo, 4];


                    for (int i = Ultimo; i > 0; i--)
                    {

                        tjs[i - 1, 0] = flow.Document.GetElementById("span__W014PENDOC_000" + i).InnerText;
                        tjs[i - 1, 1] = flow.Document.GetElementById("span__W014MARCA_000" + i).InnerText;
                        tjs[i - 1, 2] = flow.Document.GetElementById("span__W014NOMBRE_000" + i).InnerText;
                        tjs[i - 1, 3] = flow.Document.GetElementById("span__PFFNAC_000" + i).InnerText;

                    }





                    DataTable dt = new DataTable();
                    dt.Columns.Add("Documento");
                    dt.Columns.Add("Tipo");
                    dt.Columns.Add("Nombre");
                    dt.Columns.Add("Fecha Nacimiento");


                    DataRow dr;

                    for (int i = 0; i < Ultimo; i++)

                    {
                        dr = dt.NewRow();
                        dr["Documento"] = tjs[i, 0];
                        dr["Tipo"] = tjs[i, 1];
                        dr["Nombre"] = tjs[i, 2];
                        dr["Fecha Nacimiento"] = tjs[i, 3];
                        dt.Rows.Add(dr);


                    }
                    dgwTarjetas.DataSource = dt;
                    AspectoTabla.AspectoDGV(dgwTarjetas, Color.LightGoldenrodYellow);
                    dgwTarjetas.ReadOnly = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudo obtener datos de personas.\nRevise los datos copiados.");
                    Logins.LogError(ex);
                }

            }

            // METODO Tarjetas

            if (Tarjetas > 0)
            {
                try
                {

                    object[,] tjs = new object[Tarjetas, 7];
                    

                    for (int i = Tarjetas; i > 0; i--)
                    {

                        tjs[i - 1, 0] = flow.Document.GetElementById("_TD65NROTAR_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        tjs[i - 1, 1] = flow.Document.GetElementById("_TD65CATEGO_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        tjs[i - 1, 2] = flow.Document.GetElementById("_TD65NOMAPE_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        tjs[i - 1, 3] = flow.Document.GetElementById("_TD65TIPDOC_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        tjs[i - 1, 4] = flow.Document.GetElementById("_TD65NRODOC_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        tjs[i - 1, 5] = flow.Document.GetElementById("_FECHADESDE_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();
                        tjs[i - 1, 6] = flow.Document.GetElementById("_TD65MARBAJ_" + i.ToString().PadLeft(4, '0')).GetAttribute("value").ToString();

                    }





                    DataTable dt = new DataTable();
                    dt.Columns.Add("Nro Tarjeta");
                    dt.Columns.Add("Cat.");
                    dt.Columns.Add("Nombre");
                    dt.Columns.Add("Tipo");
                    dt.Columns.Add("Nro.Doc.");
                    dt.Columns.Add("Vigencia");
                    dt.Columns.Add("Estado");


                    DataRow dr;

                    for (int i = 0; i < Tarjetas; i++)

                    {
                        dr = dt.NewRow();
                        dr["Nro Tarjeta"] = tjs[i, 0];
                        dr["Cat."] = tjs[i, 1];
                        dr["Nombre"] = tjs[i, 2];
                        dr["Tipo"] = tjs[i, 3];
                        dr["Nro.Doc."] = tjs[i, 4];
                        dr["Vigencia"] = tjs[i, 5];
                        dr["Estado"] = tjs[i, 6];
                        dt.Rows.Add(dr);


                    }
                    dgwTarjetas.DataSource = dt;
                    AspectoTabla.AspectoDGV(dgwTarjetas, Color.LightSteelBlue);
                    dgwTarjetas.ReadOnly = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudo obtener tarjetas de la cuenta.\nRevise los datos copiados.");
                    Logins.LogError(ex);
                }

            }


        }

        public bool Duplicados(string Tramite)

        {

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT NRO_TRAMITE FROM GESTIONES WHERE NRO_TRAMITE='" + Tramite + "'";
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);


            if (dt.Rows.Count == 1)
            {
                return true;

            }
            else
            {
                return false;
            }



        }



        public void Limpiar()
        {
            txtAdicComent.Text = "";
            txtAdministradora.Text = "";
            txtCliente.Text = "";
            txtCodGeo.Text = "";
            txtCodOca.Text = "";
            txtCodPaq.Text = "";
            txtConvPS.Text = "";
            txtCtaDeb.Text = "";
            txtCuentaTJ.Text = "";
            txtDenomCte.Text = "";
            txtDesc.Text = "";
            txtDescGeo.Text = "";
            txtDni.Text = "";
            txtDomEnt.Text = "";
            txtDomEsp.Text = "";
            txtDomicilio.Text = "";
            txtEmbozado.Text = "";
            txtFpago.Text = "";
            txtHorEnt.Text = "";
            txtInfAdic.Text = "";
            txtLim.Text = "";
            txtLimAut.Text = "";
            txtModLiq.Text = "";
            txtMotivo.Text = "";
            txtNombre.Text = "";
            txtNroTramite.Text = "";
            txtObservaciones.Text = "";
            txtPersona.Text = "";
            txtPrioridad.Text = "";
            txtSucEntrega.Text = "";
            txtTarjeta.Text = "";
            txtTelefono.Text = "";
            txtTipoTJ.Text = "";
            txtTitTJ.Text = "";
            txtTramite.Text = "";

            chbLiberacion.Checked = false;
            dgvProcesos.DataSource = null;
            dgwHistorico.DataSource = null;
            dgwTarjetas.DataSource = null;

        }



        private void btnGuardar_Click(object sender, EventArgs e)
        {

            if (Duplicados(txtNroTramite.Text))
            {
                MessageBox.Show("No se puede guardar Trámite.\nEl número de trámite ya existe. Verifique en Consulta de Trámites", "Error de grabado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (Metodos.ObtenerMetodo(txtTramite.Text) == "1")
                {
                    if (txtPrioridad.Text != "NORMAL" && txtPrioridad.Text != "URGENTE")
                    {
                        MessageBox.Show("Debe indicar priodidad [NORMAL/URGENTE]", "Prioridad", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        try
                        {
                            OleDbConnection cn = new OleDbConnection();
                            cn.ConnectionString = Conexion.cnProceso;
                            OleDbCommand cmd = new OleDbCommand();
                            cmd.Connection = cn;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "INSERT INTO GESTIONES(US_INICIO,FECHA_INICIO,EMP,TRAMITE,WF,NRO_TRAMITE,PRIORIDAD,CANAL_INGRESO,ESTADO,A_CONTROLAR) VALUES(@US_INICIO,@FECHA_INICIO,@EMP,@TRAMITE,@WF,@NRO_TRAMITE,@PRIORIDAD,@CANAL_INGRESO,@ESTADO,@ACONTROL)";

                            cn.Open();
                            cmd.Parameters.AddWithValue("@US_INICIO", Ingreso.UsuarioLogueado.ToUpper());
                            cmd.Parameters.AddWithValue("@FECHA_INICIO", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@EMP", txtAdministradora.Text.Trim());
                            cmd.Parameters.AddWithValue("@TRAMITE", txtDesc.Text.Trim());
                            cmd.Parameters.AddWithValue("@WF", txtTramite.Text.Trim());
                            cmd.Parameters.AddWithValue("@NRO_TRAMITE", txtNroTramite.Text.Trim());
                            switch (chbLiberacion.Checked)
                            {
                                case true: cmd.Parameters.AddWithValue("@PRIORIDAD", txtPrioridad.Text.Trim()+"+LIBERACION");
                                    break;
                                case false: cmd.Parameters.AddWithValue("@PRIORIDAD", txtPrioridad.Text.Trim());
                                    break;
                                default:
                                    break;
                            }
                            

                            cmd.Parameters.AddWithValue("@CANAL_INGRESO", "WORKFLOW");

                            cmd.Parameters.AddWithValue("@ESTADO", "ABIERTO");
                            if (txtTramite.Text=="TJLS" || txtTramite.Text == "TJLC")
                            {

                                cmd.Parameters.AddWithValue("@ACONTROL", DateTime.Today.ToOADate());

                            }
                            else
                            {

                                if (Metodos.Postdatado(txtTramite.Text) && txtAdministradora.Text == "MASTERCARD")
                                {
                                    cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(Calendario.DiaLabPosterior(DateTime.Today)).ToOADate());
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today).ToOADate());

                                }



                            }


                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                            cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                            cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                            cmd.Parameters.AddWithValue("@COM", txtAdicComent.Text.Trim());
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();



                            OleDbCommand cmdWF = new OleDbCommand();
                            cmdWF.Connection = cn;
                            cmdWF.CommandType = CommandType.Text;
                            cmdWF.CommandText = "INSERT INTO WORKFLOW(TIPO_TRAMITE,NRO_TRAMITE,DESCRIPCION," +
                                 "CUIL,DNI,NOMBRE,DENOM_CLTE,TELEFONO,CTADEBIT,FORMA_PAGO,DOM_ESP," +
                                 "SUC_ENTREGA,COD_OCA,CLIENTEBT,TITCTATJ,TARJETA,EMBOZADO,MARCA,CUENTATJ," +
                                 "DATOS_ADIC,MOTIVO,OBSERVACIONES,TIPOTJ,LIMITETJ,LIMAUT,DOM_ACT,CODGEO,CONVPS,CODPAQ,MODLIQ,DESCGEO,DOMENT,HORENT)"+
                                 "VALUES (@TIPO_TRAMITE,@NRO_TRAMITE,@DESCRIPCION,@CUIL,@DNI,@NOMBRE,@DENOM_CLTE,@TELEFONO,@CTADEBIT," +
                                 "@FORMA_PAGO,@DOM_ESP,@SUC_ENTREGA,@COD_OCA,@CLIENTEBT,@TITCTATJ,@TARJETA,@EMBOZADO,@MARCA,@CUENTATJ,@DATOS_ADIC,@MOTIVO,@OBSERVACIONES," +
                                 "@TIPOTJ,@LIMITETJ,@LIMAUT,@DOM_ACT,@CODGEO,@CONVPS,@CODPAQ,@MODLIQ,@DESCGEO,@DOMENT,@HORENT)";
                            cmdWF.Parameters.AddWithValue("@TIPO_TRAMITE", txtTramite.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@NRO_TRAMITE", txtNroTramite.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DESCRIPCION", txtDesc.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CUIL", txtPersona.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DNI", txtDni.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@NOMBRE", txtNombre.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DENOM_CLTE", txtDenomCte.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@TELEFONO", txtTelefono.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CTADEBIT", txtCtaDeb.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@FORMA_PAGO", txtFpago.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DOM_ESP", txtDomEsp.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@SUC_ENTREGA", txtSucEntrega.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@COD_OCA", txtCodOca.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CLIENTEBT", txtCliente.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@TITCTATJ", txtTitTJ.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@TARJETA", txtTarjeta.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@EMBOZADO", txtEmbozado.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@MARCA", txtAdministradora.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CUENTATJ", txtCuentaTJ.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DATOS_ADIC", txtInfAdic.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@MOTIVO", txtMotivo.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@OBSERVACIONES", txtObservaciones.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@TIPOTJ", txtTipoTJ.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@LIMITETJ", txtLim.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@LIMAUT", txtLimAut.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DOM_ACT", txtDomicilio.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CODGEO", txtCodGeo.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CONVPS", txtConvPS.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CODPAQ", txtCodPaq.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@MODLIQ", txtModLiq.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DESCGEO", txtDescGeo.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DOMENT", txtDomEnt.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@HORENT", txtHorEnt.Text.Trim());


                            cmdWF.ExecuteNonQuery();
                            cmdWF.Parameters.Clear();

                            OleDbCommand cmdHist = new OleDbCommand();
                            cmdHist.Connection = cn;
                            cmdHist.CommandType = CommandType.Text;
                            cmdHist.CommandText = "INSERT INTO HISTORICO (NRO_TRAMITE,FECHA,TAREA,USUARIO,NOMBRE,COMENTARIO) VALUES (@NRO_TRAMITE,@FECHA,@TAREA,@USUARIO,@NOMBRE,@COMENTARIO)";


                            foreach (DataGridViewRow fila in dgwHistorico.Rows)
                            {

                                cmdHist.Parameters.AddWithValue("@NRO_TRAMITE", txtNroTramite.Text.Trim());
                                cmdHist.Parameters.AddWithValue("@FECHA", Convert.ToString(fila.Cells[0].Value));
                                cmdHist.Parameters.AddWithValue("@TAREA", Convert.ToString(fila.Cells[1].Value));
                                cmdHist.Parameters.AddWithValue("@USUARIO", Convert.ToString(fila.Cells[2].Value));
                                cmdHist.Parameters.AddWithValue("@NOMBRE", Convert.ToString(fila.Cells[3].Value));
                                cmdHist.Parameters.AddWithValue("@COMENTARIO", Convert.ToString(fila.Cells[4].Value));
                                cmdHist.ExecuteNonQuery();
                                cmdHist.Parameters.Clear();

                            }



                            OleDbCommand cmdTJ = new OleDbCommand();
                            cmdTJ.Connection = cn;
                            cmdTJ.CommandType = CommandType.Text;

                            if (txtTramite.Text=="TJAV" || txtTramite.Text == "TJAM" || txtTramite.Text == "TJAX")
                            {
                                cmdTJ.CommandText = "INSERT INTO PERSONAS (NroTramite,DOC,NOMBRE,TIPO,FEC_NAC) VALUES (@NroTramite,@DOC,@NOMBRE,@TIPO,@FEC_NAC)";

                                if (dgwTarjetas.Rows.Count > 0)
                                {

                                    foreach (DataGridViewRow fila in dgwTarjetas.Rows)
                                    {

                                        cmdTJ.Parameters.AddWithValue("@NroTramite", txtNroTramite.Text.Trim());
                                        cmdTJ.Parameters.AddWithValue("@DOC", Convert.ToString(fila.Cells[0].Value));
                                        cmdTJ.Parameters.AddWithValue("@NOMBRE", Convert.ToString(fila.Cells[1].Value));
                                        cmdTJ.Parameters.AddWithValue("@TIPO", Convert.ToString(fila.Cells[2].Value));
                                        cmdTJ.Parameters.AddWithValue("@FEC_NAC", Convert.ToString(fila.Cells[3].Value));
                                        cmdTJ.ExecuteNonQuery();
                                        cmdTJ.Parameters.Clear();
                                    }
                                }
                            }
                            else
                            {
                                cmdTJ.CommandText = "INSERT INTO Tarjetas (NroTramite,TJ,CAT,NOMBRE,TIPO,DOC,VIG,EST) VALUES (@NroTramite,@TJ,@CAT,@NOMBRE,@TIPO,@DOC,@VIG,@EST)";

                                if (dgwTarjetas.Rows.Count > 0)
                                {

                                    foreach (DataGridViewRow fila in dgwTarjetas.Rows)
                                    {

                                        cmdTJ.Parameters.AddWithValue("@NroTramite", txtNroTramite.Text.Trim());
                                        cmdTJ.Parameters.AddWithValue("@TJ", Convert.ToString(fila.Cells[0].Value));
                                        cmdTJ.Parameters.AddWithValue("@CAT", Convert.ToString(fila.Cells[1].Value));
                                        cmdTJ.Parameters.AddWithValue("@NOMBRE", Convert.ToString(fila.Cells[2].Value));
                                        cmdTJ.Parameters.AddWithValue("@TIPO", Convert.ToString(fila.Cells[3].Value));
                                        cmdTJ.Parameters.AddWithValue("@DOC", Convert.ToString(fila.Cells[4].Value));
                                        cmdTJ.Parameters.AddWithValue("@VIG", Convert.ToString(fila.Cells[5].Value));
                                        cmdTJ.Parameters.AddWithValue("@EST", Convert.ToString(fila.Cells[6].Value));
                                        cmdTJ.ExecuteNonQuery();
                                        cmdTJ.Parameters.Clear();
                                    }
                                }

                            }
                            Logins.Estadistica("PROCESO", txtNroTramite.Text.Trim());



                            cmd.Parameters.Clear();
                            cmdHist.Parameters.Clear();


                            cn.Close();





                            MessageBox.Show("Gestión grabada exitosamente.\nNro de trámite: " + txtNroTramite.Text.Trim(), "Trámite WorkFlow", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpiar();




                            cn.Close();
                        }
                        catch (Exception EX)
                        {

                            MessageBox.Show(EX.Message + EX.Source);
                            Logins.LogError(EX);
                        }
                    }

                }
                else
                {
                    switch (Metodos.ObtenerMetodo(txtTramite.Text))
                    {
                        case "2":
                            MessageBox.Show("Este trámite no puede ser ingresado en este módulo.\nIngrese por Módulo Púrpura", "Módulo Incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            break;

                        default:
                            break;
                    }
                }
            }

        }



        public ModuloPurpura(string IdTramite, string Estado, string Prioridad)

        {



            InitializeComponent();
            btnGuardar.Click -= new EventHandler(btnGuardar_Click);
            txtCargar.Click -= new EventHandler(txtCargar_Click);
            chbLiberacion.Enabled = false;

            if (Estado == "ABIERTO")
            {
                btnGuardar.Text = "CONTROLADO";
              
                btnGuardar.ForeColor = Color.Green;
                btnGuardar.Font = new Font(btnGuardar.Font, FontStyle.Bold);
                txtCargar.Text = "REPROCESAR";
              
                txtCargar.ForeColor = Color.Red;
                txtCargar.Font = new Font(txtCargar.Font, FontStyle.Bold);
                this.Text = "Control: " + IdTramite;
                btnGuardar.Click += new EventHandler(Controlar);
                txtCargar.Click += new EventHandler(Reprocesar);
                this.Icon = Gestion_AP.Properties.Resources.Icono;

            }
            if (Estado == "RECHAZADO")
            {
                btnGuardar.Text = "PROCESAR";
              
                btnGuardar.ForeColor = Color.Green;
                btnGuardar.Font = new Font(btnGuardar.Font, FontStyle.Bold);
                txtCargar.Visible = false;
                btnGuardar.Click += new EventHandler(Corregido);
                this.Icon = Gestion_AP.Properties.Resources.ntfPendientes;
                this.Text = "Rechazo: " + IdTramite;



            }
            txtPrioridad.Text = Prioridad;

            CargarTramite(IdTramite);

        }


        private void Corregido(object sender, EventArgs e)
        {
            try
            {
                string IdTramite = txtNroTramite.Text;
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + txtNroTramite.Text.Trim() + "'";
                cmd.Parameters.AddWithValue("@EST", "ABIERTO");
                if (txtTramite.Text=="TJLS" || txtTramite.Text == "TJLC")
                {
                    cmd.Parameters.AddWithValue("@ACONTROL", DateTime.Today.ToOADate());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today).ToOADate());

                }
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", txtAdicComent.Text.Trim());
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("REPROCESO", txtNroTramite.Text.Trim());
                MessageBox.Show("Trámite Reprocesado.", "Reproceso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Rechazos").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Rechazos.EliminarRegistro(txtNroTramite.Text.Trim());
                        Rechazos.TotRech--;
                        Rechazos.TotUs--;
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }
                this.Close();
                cn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Logins.LogError(ex);

            }
        }


        private void Reprocesar(object sender, EventArgs e)
        {

            if (txtAdicComent.Text == "")
            {
                MessageBox.Show("Debe ingresar un comentario para rechazar trámite.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    string IdTramite = txtNroTramite.Text;
                    OleDbConnection cn = new OleDbConnection();
                    cn.ConnectionString = Conexion.cnProceso;
                    OleDbCommand cmd = new OleDbCommand();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + txtNroTramite.Text.Trim() + "'";
                    cmd.Parameters.AddWithValue("@EST", "RECHAZADO");
                    cmd.Parameters.AddWithValue("@ACONTROL", DateTime.Today);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                    cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                    cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                    cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                    cmd.Parameters.AddWithValue("@COM", txtAdicComent.Text.Trim());
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    Logins.Estadistica("CONTROL", txtNroTramite.Text.Trim());
                    MessageBox.Show("Trámite enviado a reproceso.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    try
                    {
                        Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                        if (existe != null)
                        {
                            Control.EliminarRegistro(txtNroTramite.Text.Trim());
                        }
                    }
                    catch (Exception ex) { Logins.LogError(ex); }
                    this.Close();
                    cn.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    Logins.LogError(ex);

                }

            }

        }


        private void Controlar(object sender, EventArgs e)
        {

            try
            {

                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST WHERE NRO_TRAMITE='" + txtNroTramite.Text.Trim() + "'";
                cmd.Parameters.AddWithValue("@EST", "CERRADO");

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", "TRÁMITE FINALIZADO CORRECTAMENTE");
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("CONTROL", txtNroTramite.Text.Trim());
                MessageBox.Show("Trámite finalizado correctamente.", "Aceptado", MessageBoxButtons.OK, MessageBoxIcon.Information);

                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Control.EliminarRegistro(txtNroTramite.Text.Trim());
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }


                this.Close();
                cn.Close();



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Logins.LogError(ex);
            }
        }


        public void CargarTramite(string IdTramite)

        {

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandText = "SELECT * FROM WORKFLOW WHERE NRO_TRAMITE='" + IdTramite + "'";
            DataTable dt = new DataTable();
            DataTable dth = new DataTable();
            DataTable dtc = new DataTable();
            DataTable dtt = new DataTable();
        
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);
            try
            {
                if (dt.Rows.Count == 1)
                {
                    txtNroTramite.Text = dt.Rows[0]["NRO_TRAMITE"].ToString();
                    txtTramite.Text = dt.Rows[0]["TIPO_TRAMITE"].ToString();
                    txtDesc.Text = dt.Rows[0]["DESCRIPCION"].ToString();
                    txtPersona.Text = dt.Rows[0]["CUIL"].ToString();
                    txtDni.Text = dt.Rows[0]["DNI"].ToString();
                    txtNombre.Text = dt.Rows[0]["NOMBRE"].ToString();
                    txtDenomCte.Text = dt.Rows[0]["DENOM_CLTE"].ToString();
                    txtTelefono.Text = dt.Rows[0]["TELEFONO"].ToString();
                    txtCtaDeb.Text = dt.Rows[0]["CTADEBIT"].ToString();
                    txtTipoTJ.Text = dt.Rows[0]["TIPOTJ"].ToString();
                    txtDomEsp.Text = dt.Rows[0]["DOM_ESP"].ToString();
                    txtSucEntrega.Text = dt.Rows[0]["SUC_ENTREGA"].ToString();
                    txtCodOca.Text = dt.Rows[0]["COD_OCA"].ToString();
                    txtCliente.Text = dt.Rows[0]["CLIENTEBT"].ToString();
                    txtLim.Text = dt.Rows[0]["LIMITETJ"].ToString();
                    txtLimAut.Text= dt.Rows[0]["LIMAUT"].ToString();
                    txtAdministradora.Text = dt.Rows[0]["MARCA"].ToString();
                    txtInfAdic.Text = dt.Rows[0]["DATOS_ADIC"].ToString();
                    txtMotivo.Text = dt.Rows[0]["MOTIVO"].ToString();
                    txtObservaciones.Text = dt.Rows[0]["OBSERVACIONES"].ToString();
                    txtDomicilio.Text= dt.Rows[0]["DOM_ACT"].ToString();
                    txtCodGeo.Text= dt.Rows[0]["CODGEO"].ToString();
                    txtConvPS.Text= dt.Rows[0]["CONVPS"].ToString();
                    txtCodPaq.Text= dt.Rows[0]["CODPAQ"].ToString();
                    txtModLiq.Text= dt.Rows[0]["MODLIQ"].ToString();
                    txtFpago.Text= dt.Rows[0]["FORMA_PAGO"].ToString();
                    txtDescGeo.Text= dt.Rows[0]["DESCGEO"].ToString();
                    txtDomEnt.Text= dt.Rows[0]["DOMENT"].ToString();
                    txtHorEnt.Text= dt.Rows[0]["HORENT"].ToString();
                    txtTarjeta.Text = dt.Rows[0]["TARJETA"].ToString();
                    txtEmbozado.Text= dt.Rows[0]["EMBOZADO"].ToString();
                    txtCuentaTJ.Text= dt.Rows[0]["CUENTATJ"].ToString();
                    txtTitTJ.Text= dt.Rows[0]["TITCTATJ"].ToString();

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recuperar la tabla WORKFLOW" + ex.Message);
                Logins.LogError(ex);
            }

        



            try
            {
                cmd.CommandText = "SELECT * FROM HISTORICO WHERE NRO_TRAMITE='" + IdTramite + "'";
                OleDbDataAdapter dah = new OleDbDataAdapter(cmd);

                dah.Fill(dth);
                dgwHistorico.DataSource = dth;
                dgwHistorico.Columns[0].Visible = false;
                dgwHistorico.Columns[1].Visible = false;
                dgwHistorico.ReadOnly = true;
                AspectoTabla.AspectoDGV(dgwHistorico, Color.LightGray);
                dah.Dispose();
                dth.Dispose();
            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recurerar tabla HISTORICOS" + ex.Message);
                Logins.LogError(ex);
            }

            try
            {
                if (txtTramite.Text=="TJAV" || txtTramite.Text == "TJAM" || txtTramite.Text == "TJAX")
                {

                    cmd.CommandText = "SELECT * FROM PERSONAS WHERE NroTramite='" + IdTramite + "'";
                    OleDbDataAdapter dat = new OleDbDataAdapter(cmd);

                    dat.Fill(dtt);
                    dgwTarjetas.DataSource = dtt;
                    dgwTarjetas.Columns[0].Visible = false;
                    dgwTarjetas.Columns[1].Visible = false;
                    dgwTarjetas.Columns[5].HeaderText = "FECHA NACIMIENTO";
                    dgwTarjetas.ReadOnly = true;
                    AspectoTabla.AspectoDGV(dgwTarjetas, Color.LightGoldenrodYellow);
                    dat.Dispose();
                    dtt.Dispose();

                }
                else
                {
                    cmd.CommandText = "SELECT * FROM Tarjetas WHERE NroTramite='" + IdTramite + "'";
                    OleDbDataAdapter dat = new OleDbDataAdapter(cmd);

                    dat.Fill(dtt);
                    dgwTarjetas.DataSource = dtt;
                    dgwTarjetas.Columns[0].Visible = false;
                    dgwTarjetas.Columns[1].Visible = false;
                    dgwTarjetas.Columns[2].HeaderText = "TARJETA";
                    dgwTarjetas.Columns[3].HeaderText = "CAT.";
                    dgwTarjetas.Columns[4].HeaderText = "NOMBRE";
                    dgwTarjetas.Columns[5].HeaderText = "TIPO";
                    dgwTarjetas.Columns[6].HeaderText = "DOCUMENTO";
                    dgwTarjetas.Columns[7].HeaderText = "VIGENCIA";
                    dgwTarjetas.Columns[8].HeaderText = "ESTADO";
                    dgwTarjetas.ReadOnly = true;
                    AspectoTabla.AspectoDGV(dgwTarjetas, Color.LightBlue);
                    dat.Dispose();
                    dtt.Dispose();


                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recurerar tabla PERSONAS/Tarjetas" + ex.Message);
                Logins.LogError(ex);
            }

            try
            {
                cmd.CommandText = "SELECT * FROM Procesos WHERE NroTramite='" + IdTramite + "'";
                OleDbDataAdapter dac = new OleDbDataAdapter(cmd);

                dac.Fill(dtc);
                dgvProcesos.DataSource = dtc;

                AspectoTabla.AspectoComent(dgvProcesos);
                dac.Dispose();
                dtc.Dispose();

            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recuerar tabla Procesos" + ex.Message);
                Logins.LogError(ex);
            }


            da.Dispose();
            dt.Dispose();


        }


        private void tpDistribucion_Click(object sender, EventArgs e)
        {

        }

        private void ModuloPurpura_Load(object sender, EventArgs e)
        {

        }

       
       

     

        private void dgwTarjetas_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button==MouseButtons.Right)
            {
                VisorTexto frm = new VisorTexto(dgvProcesos.CurrentCell.Value.ToString());
                frm.Show();

            }
        }

       

       

        private void dgwHistorico_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            VisorTexto frm = new VisorTexto(dgwHistorico.CurrentCell.Value.ToString());
            frm.Show();
        }

       

        private void dgvProcesos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            VisorTexto frm = new VisorTexto(dgvProcesos.CurrentCell.Value.ToString());
            frm.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Si el embozado contiene caracteres erróneos, puede corregirlos manualmente tal cual figuran en el trámite original.", "Caracteres inválidos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtEmbozado.Focus();

        }
    }
}
