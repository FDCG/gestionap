﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gestion_AP
{
    class Historial
    {

        private DateTime fecha;
        private string tarea;
        private string usuario;
        private string nombreUsuario;
        private string comentario;

        public Historial()
        {
        }

        public Historial(DateTime fecha, string tarea, string usuario, string nombreUsuario, string comentario)
        {
            Fecha = fecha;
            Tarea = tarea;
            Usuario = usuario;
            NombreUsuario = nombreUsuario;
            Comentario = comentario;
        }



        public DateTime Fecha { get => fecha; set => fecha = value; }
        public string Tarea { get => tarea; set => tarea = value; }
        public string Usuario { get => usuario; set => usuario = value; }
        public string NombreUsuario { get => nombreUsuario; set => nombreUsuario = value; }
        public string Comentario { get => comentario; set => comentario = value; }
    }
}
