﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Data.OleDb;

using System.Net;




namespace Gestion_AP
{

    public partial class ModuloVerde : Form
    {



        public ModuloVerde()
        {
            InitializeComponent();
            tabControl1.TabPages.Remove(pagProceso);
        }

        public ModuloVerde(string IdTramite, string Estado, string Prioridad)

        {

            

            InitializeComponent();
            btnGuardar.Click -= new EventHandler(btnGuardar_Click);
            txtCargar.Click -= new EventHandler(txtCargar_Click);
            
            if (Estado == "ABIERTO")
            {
                btnGuardar.Text = "CONTROLADO";
                btnGuardar.Size = new Size(100, 22);
                btnGuardar.Location = new Point(603, 12);
                btnGuardar.ForeColor = Color.Green;
                btnGuardar.Font = new Font(btnGuardar.Font, FontStyle.Bold);
                txtCargar.Text = "REPROCESAR";
                txtCargar.Size = new Size(100, 22);
                txtCargar.Location = new Point(497, 12);
                txtCargar.ForeColor = Color.Red;
                txtCargar.Font = new Font(txtCargar.Font, FontStyle.Bold);
                this.Text = "Control: " + IdTramite;
                btnGuardar.Click += new EventHandler(Controlar);
                txtCargar.Click += new EventHandler(Reprocesar);
                this.Icon = Gestion_AP.Properties.Resources.Icono;

            }
            if (Estado == "RECHAZADO")
            {
                btnGuardar.Text = "PROCESAR";
                btnGuardar.Size = new Size(97, 22);
                btnGuardar.Location = new Point(599, 12);
                btnGuardar.ForeColor = Color.Green;
                btnGuardar.Font = new Font(btnGuardar.Font, FontStyle.Bold);
                txtCargar.Visible = false;
                btnGuardar.Click += new EventHandler(Corregido);
                this.Icon = Gestion_AP.Properties.Resources.ntfPendientes;
                this.Text = "Rechazo: " + IdTramite;



            }
            txtPrioridad.Text = Prioridad;

            CargarTramite(IdTramite);

        }

        private void Corregido(object sender, EventArgs e)
        {
            try
            {
                string IdTramite = txtNroTramite.Text;
                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + txtNroTramite.Text.Trim() + "'";
                cmd.Parameters.AddWithValue("@EST", "ABIERTO");
                cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today));
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", txtAdicComent.Text.Trim());
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("REPROCESO", txtNroTramite.Text.Trim());
                MessageBox.Show("Trámite Reprocesado.", "Reproceso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Rechazos").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Rechazos.EliminarRegistro(txtNroTramite.Text.Trim());
                        Rechazos.TotRech--;
                        Rechazos.TotUs--;
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }
                this.Close();
                cn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Logins.LogError(ex);

            }
        }

        private void Reprocesar(object sender, EventArgs e)
        {

            if (txtAdicComent.Text == "")
            {
                MessageBox.Show("Debe ingresar un comentario para rechazar trámite.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    string IdTramite = txtNroTramite.Text;
                    OleDbConnection cn = new OleDbConnection();
                    cn.ConnectionString = Conexion.cnProceso;
                    OleDbCommand cmd = new OleDbCommand();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST,A_CONTROLAR=@ACONTROL WHERE NRO_TRAMITE='" + txtNroTramite.Text.Trim() + "'";
                    cmd.Parameters.AddWithValue("@EST", "RECHAZADO");
                    cmd.Parameters.AddWithValue("@ACONTROL", DateTime.Today);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                    cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                    cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                    cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                    cmd.Parameters.AddWithValue("@COM", txtAdicComent.Text.Trim());
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    Logins.Estadistica("CONTROL", txtNroTramite.Text.Trim());
                    MessageBox.Show("Trámite enviado a reproceso.", "Rechazo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    try
                    {
                        Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                        if (existe != null)
                        {
                            Control.EliminarRegistro(txtNroTramite.Text.Trim());
                        }
                    }
                    catch (Exception ex) { Logins.LogError(ex); }
                    this.Close();
                    cn.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    Logins.LogError(ex);

                }

            }

        }

        private void Controlar(object sender, EventArgs e)
        {

            try
            {

                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = Conexion.cnProceso;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "UPDATE GESTIONES SET ESTADO=@EST WHERE NRO_TRAMITE='" + txtNroTramite.Text.Trim() + "'";
                cmd.Parameters.AddWithValue("@EST", "CERRADO");

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                cmd.Parameters.AddWithValue("@COM", "TRÁMITE FINALIZADO CORRECTAMENTE");
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                Logins.Estadistica("CONTROL", txtNroTramite.Text.Trim());
                MessageBox.Show("Trámite finalizado correctamente.", "Aceptado", MessageBoxButtons.OK, MessageBoxIcon.Information);

                try
                {
                    Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == "Control").SingleOrDefault<Form>();
                    if (existe != null)
                    {
                        Control.EliminarRegistro(txtNroTramite.Text.Trim());
                    }
                }
                catch (Exception ex) { Logins.LogError(ex); }


                this.Close();
                cn.Close();



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Logins.LogError(ex);
            }
        }

        public void CargarTramite(string IdTramite)

        {

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandText = "SELECT * FROM WORKFLOW WHERE NRO_TRAMITE='" + IdTramite + "'";
            DataTable dt = new DataTable();
            DataTable dth = new DataTable();
            DataTable dtc = new DataTable();
            DataTable dtt = new DataTable();
            DataTable dtd = new DataTable();
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);
            try
            {
                if (dt.Rows.Count == 1)
                {
                    txtNroTramite.Text = dt.Rows[0]["NRO_TRAMITE"].ToString();
                    txtTramite.Text = dt.Rows[0]["TIPO_TRAMITE"].ToString();
                    txtDesc.Text = dt.Rows[0]["DESCRIPCION"].ToString();
                    txtPersona.Text = dt.Rows[0]["CUIL"].ToString();
                    txtDni.Text = dt.Rows[0]["DNI"].ToString();
                    txtNombre.Text = dt.Rows[0]["NOMBRE"].ToString();
                    txtDenomCte.Text = dt.Rows[0]["DENOM_CLTE"].ToString();
                    txtTelefono.Text = dt.Rows[0]["TELEFONO"].ToString();
                    txtCtaDeb.Text = dt.Rows[0]["CTADEBIT"].ToString();
                    txtTipoTJ.Text= dt.Rows[0]["TIPOTJ"].ToString();
                    txtDomEsp.Text = dt.Rows[0]["DOM_ESP"].ToString();
                    txtSucEntrega.Text = dt.Rows[0]["SUC_ENTREGA"].ToString();
                    txtCodOca.Text = dt.Rows[0]["COD_OCA"].ToString();
                    txtCliente.Text = dt.Rows[0]["CLIENTEBT"].ToString();
                    txtLimite.Text= dt.Rows[0]["LIMITETJ"].ToString();

                    txtAdministradora.Text = dt.Rows[0]["MARCA"].ToString();
              
                    txtInfAdic.Text = dt.Rows[0]["DATOS_ADIC"].ToString();
                    txtMotivo.Text = dt.Rows[0]["MOTIVO"].ToString();
                    txtObservaciones.Text = dt.Rows[0]["OBSERVACIONES"].ToString();
                    txtUserAlta.Text = dt.Rows[0]["USUARIO_ALTA"].ToString();


                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recuperar la tabla WORKFLOW" + ex.Message);
                Logins.LogError(ex);
            }

            try
            {
                cmd.CommandText = "SELECT * FROM DISTRIBUCION WHERE NroTramite='" + IdTramite + "'";
                OleDbDataAdapter dad = new OleDbDataAdapter(cmd);

                dad.Fill(dtd);
                txtCodGeo.Text = dtd.Rows[0]["COD_GEO"].ToString();
                txtDomicilio.Text = dtd.Rows[0]["DOM_ACT"].ToString();
            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recurerar tabla DISTRIBUCION" + ex.Message);
                Logins.LogError(ex);
            }



            try
            {
                cmd.CommandText = "SELECT * FROM HISTORICO WHERE NRO_TRAMITE='" + IdTramite + "'";
                OleDbDataAdapter dah = new OleDbDataAdapter(cmd);

                dah.Fill(dth);
                dgwHistorico.DataSource = dth;
                dgwHistorico.Columns[0].Visible = false;
                dgwHistorico.Columns[1].Visible = false;
                dgwHistorico.ReadOnly = true;
                dah.Dispose();
                dth.Dispose();
            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recurerar tabla HISTORICOS" + ex.Message);
                Logins.LogError(ex);
            }

            try
            {
                cmd.CommandText = "SELECT * FROM Tarjetas WHERE NroTramite='" + IdTramite + "'";
                OleDbDataAdapter dat = new OleDbDataAdapter(cmd);

                dat.Fill(dtt);
                dgwTarjetas.DataSource = dtt;
                dgwTarjetas.Columns[0].Visible = false;
                dgwTarjetas.Columns[1].Visible = false;
                dgwTarjetas.Columns[5].HeaderText = "FECHA NACIMIENTO";
                dgwTarjetas.ReadOnly = true;
                dat.Dispose();
                dtt.Dispose();
            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recurerar tabla Tarjetas" + ex.Message);
                Logins.LogError(ex);
            }

            try
            {
                cmd.CommandText = "SELECT * FROM Procesos WHERE NroTramite='" + IdTramite + "'";
                OleDbDataAdapter dac = new OleDbDataAdapter(cmd);

                dac.Fill(dtc);
                dgvProcesos.DataSource = dtc;

                AspectoTabla.AspectoComent(dgvProcesos);
                dac.Dispose();
                dtc.Dispose();

            }
            catch (Exception ex)
            {

                MessageBox.Show("No se pudo recuerar tabla Procesos" + ex.Message);
                Logins.LogError(ex);
            }


            da.Dispose();
            dt.Dispose();


        }

        

        


        WebBrowser flow = new WebBrowser();
        public static string TextoObserv1 = null;

        public static string TextoObserv2 = null;


        private void txtCargar_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsHTMLDataOnClipboard = Clipboard.ContainsData(DataFormats.Html);


                string htmlData = null;

                if (IsHTMLDataOnClipboard)
                {

                    htmlData = Clipboard.GetText(TextDataFormat.Html);
                    TextoObserv1 = Clipboard.GetText(TextDataFormat.Text);
                    TextoObserv2 = Clipboard.GetText(TextDataFormat.Text);
                    flow.ScriptErrorsSuppressed = true;
                    flow.DocumentText = htmlData;
                    flow.Document.OpenNew(true);
                    flow.Document.Write(htmlData);
                    flow.Refresh();

                    Datos();
                    DatosTabla();
                    DatosTablaTJ();

                }
                else
                {
                    MessageBox.Show("Los datos copiados no contienen estructura WorkFlow");

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error Interno. Algún dato no se pudo recuperar.\nContacte al administrador de la aplicación.\nError: " + ex.Message);
            }


        }
        private void Datos()


        {

            try
            {
                if (flow.Document.GetElementById("span__W005TRAMID") != null)
                {
                    string strTramite = flow.Document.GetElementById("span__W005TRAMID").InnerText;
                    txtTramite.Text = strTramite.Substring(0, 4);

                    txtNroTramite.Text = strTramite.Substring(4);
                }
                if (flow.Document.GetElementById("span__W005MODULO") != null)
                {
                    txtCtaDeb.Text = flow.Document.GetElementById("span__W005MODULO").InnerText + "-" +
                    flow.Document.GetElementById("span__W005MONEDA").InnerText + "-" +
                    flow.Document.GetElementById("span__W005CASA").InnerText + "-" +
                    flow.Document.GetElementById("span__W005CUENTA").InnerText + "-" +
                    flow.Document.GetElementById("span__W005SCUENT").InnerText;
                }
                if (flow.Document.GetElementById("span__W004DESCRI") != null)
                {
                    txtDesc.Text = flow.Document.GetElementById("span__W004DESCRI").InnerText.Trim();
                }
                if (flow.Document.GetElementById("span__W005NUMDOC") != null)
                {
                    txtPersona.Text = flow.Document.GetElementById("span__W005NUMDOC").InnerText.Trim();
                    if (txtPersona.TextLength == 11)
                    {
                        txtDni.Text = txtPersona.Text.Substring(2, 8);
                    }
                    else
                    {
                        txtDni.Text = txtPersona.Text;
                    }

                }
                if (flow.Document.GetElementById("span__W005NOMBRE") != null)
                {
                    txtNombre.Text = flow.Document.GetElementById("span__W005NOMBRE").InnerText.Trim();
                }
                if (flow.Document.GetElementById("span__W005CLIENT") != null)
                {
                    txtCliente.Text = flow.Document.GetElementById("span__W005CLIENT").InnerText.Trim();
                }
                if (flow.Document.GetElementById("span__CTNOM") != null)
                {
                    txtDenomCte.Text = flow.Document.GetElementById("span__CTNOM").InnerText.Trim();
                }

                if (flow.Document.GetElementById("span__W005TIPITX") != null)
                {
                    txtMotivo.Text = flow.Document.GetElementById("span__W005TIPITX").InnerText.Trim();
                }
                //if (flow.Document.GetElementById("span__W005USUALT") != null)
                //{
                //    txtUserAlta.Text = flow.Document.GetElementById("span__W005USUALT").InnerText.Trim();
                //}


                if (flow.Document.GetElementById("span__VDOMACTUAL") != null)
                {
                    txtDomicilio.Text = flow.Document.GetElementById("span__VDOMACTUAL").InnerText.Trim();
                }
                if (flow.Document.GetElementById("span__CODGEO") != null)
                {
                    txtCodGeo.Text = flow.Document.GetElementById("span__CODGEO").InnerText.Trim();
                    if (flow.Document.GetElementById("span__DESCCGEO") != null)
                    {
                        txtCodGeo.AppendText("-" + (flow.Document.GetElementById("span__DESCCGEO").InnerText.Trim()));
                    }
                }
                if (flow.Document.GetElementById("span__VDESCOCA") != null)
                {
                    txtCodOca.Text = flow.Document.GetElementById("span__VDESCOCA").InnerText.Trim();
                }
                if (flow.Document.GetElementById("span__W005SUCESP") != null)
                {
                    txtSucEntrega.Text = flow.Document.GetElementById("span__W005SUCESP").InnerText.Trim();
                    if (flow.Document.GetElementById("span__SCNOM") != null)
                    {
                        txtSucEntrega.AppendText("-" + (flow.Document.GetElementById("span__SCNOM").InnerText.Trim()));
                    }

                }

                if (flow.Document.GetElementById("_W005EMPRTJ") != null)
                {
                    string strMarca = flow.Document.GetElementById("_W005EMPRTJ").GetAttribute("value").ToString();
                    if (strMarca == "3")
                    {
                        txtAdministradora.Text = "AMERICAN EXPRESS";
                    }
                    else if (strMarca == "1")
                    {
                        txtAdministradora.Text = "VISA";
                    }
                    else if (strMarca == "2")
                    {
                        txtAdministradora.Text = "MASTERCARD";
                    }

                }




                if (flow.Document.GetElementById("span__W005TELEFO") != null)
                {

                    txtTelefono.Text = flow.Document.GetElementById("span__W005TELEFO").InnerText.Trim();
                }
                if (flow.Document.GetElementById("_VDOMESP") != null)
                {
                    txtDomEsp.Text = flow.Document.GetElementById("_VDOMESP").GetAttribute("value").ToString().Trim();

                }

                if (flow.Document.GetElementById("_W005INFADI") != null)
                {
                    txtInfAdic.Text = flow.Document.GetElementById("_W005INFADI").GetAttribute("value").ToString().Trim();

                }
                if (flow.Document.GetElementById("_PRIORIDAD") != null)
                {
                    string prioridad = flow.Document.GetElementById("_PRIORIDAD").GetAttribute("value").ToString().Trim();
                    if (prioridad == "N")
                    {
                        txtPrioridad.Text = "NORMAL";
                    }
                    else if (prioridad == "U")
                    {
                        txtPrioridad.Text = "URGENTE";
                    }

                }

                if (flow.Document.GetElementById("_TIPOTARJ") != null)
                {
                    txtTipoTJ.Text = flow.Document.GetElementById("_TIPOTARJ").GetAttribute("value").ToString().Trim();
                }
                if (flow.Document.GetElementById("span__W005IMPORT") != null)
                {
                    txtLimite.Text = "$" + flow.Document.GetElementById("_W005IMPORT").GetAttribute("value").ToString().Trim();
                }

                try
                {
                    string usuario = "";
                    foreach (HtmlElement Etiqueta in flow.Document.All)
                    {


                        if (Etiqueta.GetAttribute("name").Contains("_PRIORIDAD"))
                        {

                            foreach (HtmlElement item in Etiqueta.Children)
                            {
                                if (item.GetAttribute("selected") == "True")
                                {
                                    txtPrioridad.Text = item.InnerText.ToUpper();
                                }
                            }

                        }
                        //if (Etiqueta.GetAttribute("class").Contains("NoBorderAttribute"))
                        //{
                        //    if (Etiqueta.InnerText.Contains("Normal"))
                        //    {
                        //        txtPrioridad.Text = "NORMAL";
                        //    }
                        //    else if (Etiqueta.InnerText.Contains("Urgente"))
                        //    {
                        //        txtPrioridad.Text = "URGENTE";
                        //    }

                        //}




                        if (Etiqueta.GetAttribute("id").Contains("_VTAREA_000") && Etiqueta.InnerText != null)
                        {
                            if (Etiqueta.InnerText.Contains("Inicio Sky"))
                            {
                                string tarea = Etiqueta.GetAttribute("id").ToString().Trim();
                                usuario = tarea.Replace("TAREA", "USUARIO");
                            }

                        }


                        if (Etiqueta.Id == usuario && Etiqueta.Id != null)
                        {
                            txtUserAlta.Text = Etiqueta.InnerText.Trim();
                        }

                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudieron recuperar algunos datos de Prioridad y/o Usuario.\nConsulte al administrador.");
                    Logins.LogError(ex);

                }
                try
                {
                    if (flow.Document.GetElementById("_W005TEXTO") != null)
                    {
                        txtObservaciones.Text = flow.Document.GetElementById("_W005TEXTO").GetAttribute("value").ToString().Trim();
                        txtObservaciones.Text = txtObservaciones.Text.Replace("Â", "");

                    }
                    else
                    {
                        if (TextoObserv1.Contains("Observaciones"))
                        {
                            int inicio = TextoObserv1.IndexOf("Observaciones");
                            int fin = TextoObserv1.IndexOf("Tarjetas");
                            txtObservaciones.Text = TextoObserv1.Substring(inicio + 13, fin - inicio - 13).Trim();
                            txtObservaciones.Text = txtObservaciones.Text.Replace("Comentarios", "");
                            txtObservaciones.Text = txtObservaciones.Text.Replace("Â", "");
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se capturaron observaciones, revise datos copiados");
                    Logins.LogError(ex);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                Logins.LogError(ex);
            }

        }


        // TABLA DE Procesos HISTORICOS
        private void DatosTabla()

        {
            string Origen = null;
            int Ultimo;
            int Metodo = 0;
            foreach (HtmlElement tag in flow.Document.All)
            {
                if (tag.GetAttribute("id").Contains("span_W0208") && tag.InnerText != null)
                {
                    if (tag.InnerText.Contains("Inicio Sky"))
                    {
                        Origen = tag.GetAttribute("id").ToString().Trim();
                        Metodo = 1;
                    }
                }
                if (tag.GetAttribute("id").Contains("span_W0176") && tag.InnerText != null)
                {
                    if (tag.InnerText.Contains("Inicio Sky"))
                    {
                        Origen = tag.GetAttribute("id").ToString().Trim();
                        Metodo = 2;
                    }
                }


            }
            if (Origen != null)
            {
                try
                {
                    Ultimo = Convert.ToInt32(Origen.Substring(Origen.Length - 1));
                    object[,] historico = new object[Ultimo, 5];

                    if (Metodo == 1)
                    {
                        for (int i = Ultimo; i > 0; i--)
                        {

                            historico[i - 1, 0] = flow.Document.GetElementById("span_W0208_VFECHA_000" + i).InnerText;
                            historico[i - 1, 1] = flow.Document.GetElementById("span_W0208_VTAREA_000" + i).InnerText;
                            historico[i - 1, 2] = flow.Document.GetElementById("span_W0208_VUSUARIO_000" + i).InnerText;
                            historico[i - 1, 3] = flow.Document.GetElementById("span_W0208_NOMBRE_000" + i).InnerText;
                            if (flow.Document.GetElementById("span_W0208_VHISTCOMEN_000" + i).InnerText != null)
                            {
                                historico[i - 1, 4] = flow.Document.GetElementById("span_W0208_VHISTCOMEN_000" + i).InnerText;
                            }
                            else
                            {
                                historico[i - 1, 4] = "-";
                            }

                        }

                    }
                    if (Metodo == 2)
                    {

                        for (int i = Ultimo; i > 0; i--)
                        {

                            historico[i - 1, 0] = flow.Document.GetElementById("span_W0176_VFECHA_000" + i).InnerText;
                            historico[i - 1, 1] = flow.Document.GetElementById("span_W0176_VTAREA_000" + i).InnerText;
                            historico[i - 1, 2] = flow.Document.GetElementById("span_W0176_VUSUARIO_000" + i).InnerText;
                            historico[i - 1, 3] = flow.Document.GetElementById("span_W0176_NOMBRE_000" + i).InnerText;
                            if (flow.Document.GetElementById("span_W0176_VHISTCOMEN_000" + i).InnerText != null)
                            {
                                historico[i - 1, 4] = flow.Document.GetElementById("span_W0176_VHISTCOMEN_000" + i).InnerText;
                            }
                            else
                            {
                                historico[i - 1, 4] = "-";
                            }

                        }

                    }



                    DataTable dt = new DataTable();
                    dt.Columns.Add("Fecha");
                    dt.Columns.Add("Tarea");
                    dt.Columns.Add("Usuario");
                    dt.Columns.Add("Nombre");
                    dt.Columns.Add("Comentario");

                    DataRow dr;

                    for (int i = 0; i < Ultimo; i++)

                    {
                        dr = dt.NewRow();
                        dr["Fecha"] = historico[i, 0];
                        dr["Tarea"] = historico[i, 1];
                        dr["Usuario"] = historico[i, 2];
                        dr["Nombre"] = historico[i, 3];
                        dr["Comentario"] = historico[i, 4];
                        dt.Rows.Add(dr);


                    }
                    dgwHistorico.DataSource = dt;
                    AspectoTabla.AspectoDGV(dgwHistorico, Color.FloralWhite);
                    dgwHistorico.ReadOnly = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudo obtener datos historicos del tramite.\nRevise los datos copiados.");
                    Logins.LogError(ex);
                }

            }


        }

        private void DatosTablaTJ()

        {

            int Ultimo = 0;

            foreach (HtmlElement tag in flow.Document.All)
            {
                if (tag.GetAttribute("id").Contains("span__W014PENDOC") && tag.InnerText != null)
                {
                    Ultimo++;
                }



            }
            if (Ultimo > 0)
            {
                try
                {

                    object[,] tjs = new object[Ultimo, 4];


                    for (int i = Ultimo; i > 0; i--)
                    {

                        tjs[i - 1, 0] = flow.Document.GetElementById("span__W014PENDOC_000" + i).InnerText;
                        tjs[i - 1, 1] = flow.Document.GetElementById("span__W014MARCA_000" + i).InnerText;
                        tjs[i - 1, 2] = flow.Document.GetElementById("span__W014NOMBRE_000" + i).InnerText;
                        tjs[i - 1, 3] = flow.Document.GetElementById("span__PFFNAC_000" + i).InnerText;

                    }





                    DataTable dt = new DataTable();
                    dt.Columns.Add("Documento");
                    dt.Columns.Add("Tipo");
                    dt.Columns.Add("Nombre");
                    dt.Columns.Add("Fecha Nacimiento");


                    DataRow dr;

                    for (int i = 0; i < Ultimo; i++)

                    {
                        dr = dt.NewRow();
                        dr["Documento"] = tjs[i, 0];
                        dr["Tipo"] = tjs[i, 1];
                        dr["Nombre"] = tjs[i, 2];
                        dr["Fecha Nacimiento"] = tjs[i, 3];
                        dt.Rows.Add(dr);


                    }
                    dgwTarjetas.DataSource = dt;
                    AspectoTabla.AspectoDGV(dgwTarjetas, Color.AntiqueWhite);
                    dgwTarjetas.ReadOnly = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se pudo obtener datos historicos del tramite.\nRevise los datos copiados.");
                    Logins.LogError(ex);
                }

            }


        }

        public bool Duplicados(string Tramite)

        {

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT NRO_TRAMITE FROM GESTIONES WHERE NRO_TRAMITE='" + Tramite + "'";
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);


            if (dt.Rows.Count == 1)
            {
                return true;

            }
            else
            {
                return false;
            }



        }

        public void Limpiar()
        {
            txtAdicComent.Text = "";
            txtAdministradora.Text = "";
            txtCliente.Text = "";
            txtCodGeo.Text = "";
            txtCodOca.Text = "";
            txtCtaDeb.Text = "";
            txtDenomCte.Text = "";
            txtDesc.Text = "";
            txtDni.Text = "";
            txtDomEsp.Text = "";
            txtDomicilio.Text = "";
            txtInfAdic.Text = "";
            txtLimite.Text = "";
            txtMotivo.Text = "";
            txtNombre.Text = "";
            txtNroTramite.Text = "";
            txtObservaciones.Text = "";
            txtPersona.Text = "";
            txtPrioridad.Text = "";
            txtSucEntrega.Text = "";
            txtTelefono.Text = "";
            txtTipoTJ.Text = "";
            txtTramite.Text = "";
            txtUserAlta.Text = "";

            dgvProcesos.DataSource = null;
            dgwHistorico.DataSource = null;
            dgwTarjetas.DataSource = null;


        }



        private void btnGuardar_Click(object sender, EventArgs e)
        {

            if (Duplicados(txtNroTramite.Text))
            {
                MessageBox.Show("No se puede guardar Trámite.\nEl número de trámite ya existe. Verifique en Consulta de Trámites", "Error de grabado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (Metodos.ObtenerMetodo(txtTramite.Text) == "2")
                {

                    if (txtPrioridad.Text != "NORMAL" && txtPrioridad.Text != "URGENTE")
                    {
                        MessageBox.Show("Debe indicar priodidad [NORMAL/URGENTE]", "Prioridad", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        try
                        {
                            OleDbConnection cn = new OleDbConnection();
                            cn.ConnectionString = Conexion.cnProceso;
                            OleDbCommand cmd = new OleDbCommand();
                            cmd.Connection = cn;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "INSERT INTO GESTIONES(US_INICIO,FECHA_INICIO,EMP,TRAMITE,WF,NRO_TRAMITE,PRIORIDAD,CANAL_INGRESO,ESTADO,A_CONTROLAR) VALUES(@US_INICIO,@FECHA_INICIO,@EMP,@TRAMITE,@WF,@NRO_TRAMITE,@PRIORIDAD,@CANAL_INGRESO,@ESTADO,@ACONTROL)";

                            cn.Open();
                            cmd.Parameters.AddWithValue("@US_INICIO", Ingreso.UsuarioLogueado.ToUpper());
                            cmd.Parameters.AddWithValue("@FECHA_INICIO", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@EMP", txtAdministradora.Text.Trim());
                            cmd.Parameters.AddWithValue("@TRAMITE", txtDesc.Text.Trim());
                            cmd.Parameters.AddWithValue("@WF", txtTramite.Text.Trim());
                            cmd.Parameters.AddWithValue("@NRO_TRAMITE", txtNroTramite.Text.Trim());
                            cmd.Parameters.AddWithValue("@PRIORIDAD", txtPrioridad.Text.Trim());
                            cmd.Parameters.AddWithValue("@CANAL_INGRESO", "WORKFLOW");

                            cmd.Parameters.AddWithValue("@ESTADO", "ABIERTO");
                            if (Metodos.Postdatado(txtTramite.Text) && txtAdministradora.Text == "MASTERCARD")
                            {
                                cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(Calendario.DiaLabPosterior(DateTime.Today)).ToOADate());
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ACONTROL", Calendario.DiaLabPosterior(DateTime.Today).ToOADate());

                            }




                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            cmd.CommandText = "INSERT INTO Procesos(NroTramite,FECHA,USUARIO,COMENTARIO) VALUES (@ID,@FEC,@US,@COM)";
                            cmd.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                            cmd.Parameters.AddWithValue("@FEC", DateTime.Now.ToOADate());
                            cmd.Parameters.AddWithValue("@US", Ingreso.UsuarioLogueado);
                            cmd.Parameters.AddWithValue("@COM", txtAdicComent.Text.Trim());
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            OleDbCommand cmdWF = new OleDbCommand();
                            cmdWF.Connection = cn;
                            cmdWF.CommandType = CommandType.Text;
                            cmdWF.CommandText = "INSERT INTO WORKFLOW(TIPO_TRAMITE,NRO_TRAMITE,DESCRIPCION" +
                                 ",CUIL,DNI,NOMBRE,DENOM_CLTE,TELEFONO,CTADEBIT,FORMA_PAGO,DOM_ESP,SUC_ENTREGA" +
                                 ",COD_OCA,CLIENTEBT,MARCA,DATOS_ADIC,MOTIVO,OBSERVACIONES" +
                                 ",USUARIO_ALTA,TIPOTJ,LIMITETJ) VALUES (@TIPO_TRAMITE,@NRO_TRAMITE,@DESCRIPCION,@CUIL,@DNI,@NOMBRE," +
                                 "@DENOM_CLTE,@TELEFONO,@CTADEBIT,@FORMA_PAGO,@DOM_ESP,@SUC_ENTREGA,@COD_OCA,@CLIENTEBT" +
                                 ",@MARCA,@DATOS_ADIC,@MOTIVO,@OBSERVACIONES,@US_ALTA,@TIPOTJ,@LIMITETJ)";
                            cmdWF.Parameters.AddWithValue("@TIPO_TRAMITE", txtTramite.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@NRO_TRAMITE", txtNroTramite.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DESCRIPCION", txtDesc.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CUIL", txtPersona.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DNI", txtDni.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@NOMBRE", txtNombre.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DENOM_CLTE", txtDenomCte.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@TELEFONO", txtTelefono.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CTADEBIT", txtCtaDeb.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@FORMA_PAGO", "-");
                            cmdWF.Parameters.AddWithValue("@DOM_ESP", txtDomEsp.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@SUC_ENTREGA", txtSucEntrega.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@COD_OCA", txtCodOca.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@CLIENTEBT", txtCliente.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@MARCA", txtAdministradora.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DATOS_ADIC", txtInfAdic.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@MOTIVO", txtMotivo.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@OBSERVACIONES", txtObservaciones.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@US_ALTA", txtUserAlta.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@TIPOTJ", txtTipoTJ.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@LIMITETJ", txtLimite.Text.Trim());

                            cmdWF.ExecuteNonQuery();
                            cmdWF.Parameters.Clear();

                            cmdWF.CommandText = "INSERT INTO DISTRIBUCION(NroTramite,DOM_ACT,COD_GEO) VALUES(@ID,@DOM,@GEO)";
                            cmdWF.Parameters.AddWithValue("@ID", txtNroTramite.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@DOM", txtDomicilio.Text.Trim());
                            cmdWF.Parameters.AddWithValue("@GEO", txtCodGeo.Text.Trim());

                            cmdWF.ExecuteNonQuery();
                            cmdWF.Parameters.Clear();

                            OleDbCommand cmdHist = new OleDbCommand();
                            cmdHist.Connection = cn;
                            cmdHist.CommandType = CommandType.Text;
                            cmdHist.CommandText = "INSERT INTO HISTORICO (NRO_TRAMITE,FECHA,TAREA,USUARIO,NOMBRE,COMENTARIO) VALUES (@NRO_TRAMITE,@FECHA,@TAREA,@USUARIO,@NOMBRE,@COMENTARIO)";


                            foreach (DataGridViewRow fila in dgwHistorico.Rows)
                            {

                                cmdHist.Parameters.AddWithValue("@NRO_TRAMITE", txtNroTramite.Text.Trim());
                                cmdHist.Parameters.AddWithValue("@FECHA", Convert.ToString(fila.Cells[0].Value));
                                cmdHist.Parameters.AddWithValue("@TAREA", Convert.ToString(fila.Cells[1].Value));
                                cmdHist.Parameters.AddWithValue("@USUARIO", Convert.ToString(fila.Cells[2].Value));
                                cmdHist.Parameters.AddWithValue("@NOMBRE", Convert.ToString(fila.Cells[3].Value));
                                cmdHist.Parameters.AddWithValue("@COMENTARIO", Convert.ToString(fila.Cells[4].Value));
                                cmdHist.ExecuteNonQuery();
                                cmdHist.Parameters.Clear();

                            }


                            OleDbCommand cmdTJ = new OleDbCommand();
                            cmdTJ.Connection = cn;
                            cmdTJ.CommandType = CommandType.Text;
                            cmdTJ.CommandText = "INSERT INTO Tarjetas (NroTramite,DOC,NOMBRE,TIPO,FEC_NAC) VALUES (@NroTramite,@DOC,@NOMBRE,@TIPO,@FEC_NAC)";

                            if (dgwTarjetas.Rows.Count > 0)
                            {

                                foreach (DataGridViewRow fila in dgwTarjetas.Rows)
                                {

                                    cmdTJ.Parameters.AddWithValue("@NroTramite", txtNroTramite.Text.Trim());
                                    cmdTJ.Parameters.AddWithValue("@DOC", Convert.ToString(fila.Cells[0].Value));
                                    cmdTJ.Parameters.AddWithValue("@NOMBRE", Convert.ToString(fila.Cells[1].Value));
                                    cmdTJ.Parameters.AddWithValue("@TIPO", Convert.ToString(fila.Cells[2].Value));
                                    cmdTJ.Parameters.AddWithValue("@FEC_NAC", Convert.ToString(fila.Cells[3].Value));
                                    cmdTJ.ExecuteNonQuery();
                                    cmdTJ.Parameters.Clear();
                                }
                            }

                            cmd.Parameters.Clear();
                            cmdHist.Parameters.Clear();


                            cn.Close();

                            MessageBox.Show("Gestión grabada exitosamente.\nNro de trámite: " + txtNroTramite.Text.Trim(), "Trámite WorkFlow", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpiar();
                        }
                        catch (Exception EX)
                        {

                            MessageBox.Show(EX.Message);
                            Logins.LogError(EX);
                        }
                    }
                }
                else
                {
                    switch (Metodos.ObtenerMetodo(txtTramite.Text))
                    {
                        case "1": MessageBox.Show("Este trámite no puede ser ingresado en este módulo.\nIngrese por Módulo Gris","Módulo Incorrecto",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                            break;
                        case "3": MessageBox.Show("Este trámite no puede ser ingresado en este módulo.\nIngrese por Módulo Púrpura", "Módulo Incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            break;
                        default:
                            break;
                    }
                    
                }




            }




        }


    }
}
