﻿namespace Gestion_AP
{
    partial class Adjuntos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Adjuntos));
            this.label5 = new System.Windows.Forms.Label();
            this.cbmAdm = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtComAdic = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chbUrgente = new System.Windows.Forms.CheckBox();
            this.txtIdControl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblWF_Asociado = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbGestion = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnBuscar_5 = new System.Windows.Forms.Button();
            this.btnBuscar_4 = new System.Windows.Forms.Button();
            this.btnBuscar_3 = new System.Windows.Forms.Button();
            this.btnBuscar_2 = new System.Windows.Forms.Button();
            this.btnBuscar_1 = new System.Windows.Forms.Button();
            this.txtFile5 = new System.Windows.Forms.TextBox();
            this.txtFile4 = new System.Windows.Forms.TextBox();
            this.txtFile3 = new System.Windows.Forms.TextBox();
            this.txtFile2 = new System.Windows.Forms.TextBox();
            this.txtFile1 = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.gbHistorial = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.chbLiberar = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbHistorial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(341, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 19);
            this.label5.TabIndex = 34;
            this.label5.Text = "ADM";
            // 
            // cbmAdm
            // 
            this.cbmAdm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbmAdm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbmAdm.FormattingEnabled = true;
            this.cbmAdm.Items.AddRange(new object[] {
            "VISA",
            "MASTERCARD",
            "AMERICAN EXPRESS"});
            this.cbmAdm.Location = new System.Drawing.Point(401, 10);
            this.cbmAdm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbmAdm.Name = "cbmAdm";
            this.cbmAdm.Size = new System.Drawing.Size(177, 24);
            this.cbmAdm.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtComAdic);
            this.groupBox1.Location = new System.Drawing.Point(19, 282);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(559, 93);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Comentarios Adicionales";
            // 
            // txtComAdic
            // 
            this.txtComAdic.Location = new System.Drawing.Point(7, 23);
            this.txtComAdic.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtComAdic.Multiline = true;
            this.txtComAdic.Name = "txtComAdic";
            this.txtComAdic.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtComAdic.Size = new System.Drawing.Size(546, 62);
            this.txtComAdic.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(150, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(325, 15);
            this.label4.TabIndex = 31;
            this.label4.Text = "(*)Solo puede adjuntar hasta 5 (cinco) archivos por Trámite";
            // 
            // chbUrgente
            // 
            this.chbUrgente.AutoSize = true;
            this.chbUrgente.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbUrgente.ForeColor = System.Drawing.Color.Red;
            this.chbUrgente.Location = new System.Drawing.Point(18, 70);
            this.chbUrgente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chbUrgente.Name = "chbUrgente";
            this.chbUrgente.Size = new System.Drawing.Size(75, 19);
            this.chbUrgente.TabIndex = 30;
            this.chbUrgente.Text = "URGENTE";
            this.chbUrgente.UseVisualStyleBackColor = true;
            // 
            // txtIdControl
            // 
            this.txtIdControl.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtIdControl.Location = new System.Drawing.Point(113, 10);
            this.txtIdControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIdControl.Name = "txtIdControl";
            this.txtIdControl.ReadOnly = true;
            this.txtIdControl.Size = new System.Drawing.Size(220, 23);
            this.txtIdControl.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 19);
            this.label3.TabIndex = 28;
            this.label3.Text = "ID CONTROL";
            // 
            // lblWF_Asociado
            // 
            this.lblWF_Asociado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblWF_Asociado.Location = new System.Drawing.Point(518, 47);
            this.lblWF_Asociado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblWF_Asociado.Name = "lblWF_Asociado";
            this.lblWF_Asociado.ReadOnly = true;
            this.lblWF_Asociado.Size = new System.Drawing.Size(60, 23);
            this.lblWF_Asociado.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(481, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 19);
            this.label2.TabIndex = 26;
            this.label2.Text = "WF";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 19);
            this.label1.TabIndex = 25;
            this.label1.Text = "TRAMITE";
            // 
            // cmbGestion
            // 
            this.cmbGestion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGestion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGestion.FormattingEnabled = true;
            this.cmbGestion.Location = new System.Drawing.Point(113, 46);
            this.cmbGestion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbGestion.Name = "cmbGestion";
            this.cmbGestion.Size = new System.Drawing.Size(362, 24);
            this.cmbGestion.TabIndex = 24;
            this.cmbGestion.SelectedIndexChanged += new System.EventHandler(this.cmbGestion_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 15);
            this.label6.TabIndex = 35;
            this.label6.Text = "Archivo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 15);
            this.label7.TabIndex = 36;
            this.label7.Text = "Archivo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 15);
            this.label8.TabIndex = 37;
            this.label8.Text = "Archivo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 15);
            this.label9.TabIndex = 38;
            this.label9.Text = "Archivo";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 15);
            this.label10.TabIndex = 39;
            this.label10.Text = "Archivo";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnBuscar_5);
            this.groupBox2.Controls.Add(this.btnBuscar_4);
            this.groupBox2.Controls.Add(this.btnBuscar_3);
            this.groupBox2.Controls.Add(this.btnBuscar_2);
            this.groupBox2.Controls.Add(this.btnBuscar_1);
            this.groupBox2.Controls.Add(this.txtFile5);
            this.groupBox2.Controls.Add(this.txtFile4);
            this.groupBox2.Controls.Add(this.txtFile3);
            this.groupBox2.Controls.Add(this.txtFile2);
            this.groupBox2.Controls.Add(this.txtFile1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(18, 119);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(560, 156);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Adjuntos";
            // 
            // btnBuscar_5
            // 
            this.btnBuscar_5.BackgroundImage = global::Gestion_AP.Properties.Resources.Buscar;
            this.btnBuscar_5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscar_5.Location = new System.Drawing.Point(517, 127);
            this.btnBuscar_5.Name = "btnBuscar_5";
            this.btnBuscar_5.Size = new System.Drawing.Size(27, 21);
            this.btnBuscar_5.TabIndex = 49;
            this.btnBuscar_5.UseVisualStyleBackColor = true;
            this.btnBuscar_5.Click += new System.EventHandler(this.btnBuscar_5_Click);
            // 
            // btnBuscar_4
            // 
            this.btnBuscar_4.BackgroundImage = global::Gestion_AP.Properties.Resources.Buscar;
            this.btnBuscar_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscar_4.Location = new System.Drawing.Point(517, 100);
            this.btnBuscar_4.Name = "btnBuscar_4";
            this.btnBuscar_4.Size = new System.Drawing.Size(27, 21);
            this.btnBuscar_4.TabIndex = 48;
            this.btnBuscar_4.UseVisualStyleBackColor = true;
            this.btnBuscar_4.Click += new System.EventHandler(this.btnBuscar_4_Click);
            // 
            // btnBuscar_3
            // 
            this.btnBuscar_3.BackgroundImage = global::Gestion_AP.Properties.Resources.Buscar;
            this.btnBuscar_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscar_3.Location = new System.Drawing.Point(517, 74);
            this.btnBuscar_3.Name = "btnBuscar_3";
            this.btnBuscar_3.Size = new System.Drawing.Size(27, 21);
            this.btnBuscar_3.TabIndex = 47;
            this.btnBuscar_3.UseVisualStyleBackColor = true;
            this.btnBuscar_3.Click += new System.EventHandler(this.btnBuscar_3_Click);
            // 
            // btnBuscar_2
            // 
            this.btnBuscar_2.BackgroundImage = global::Gestion_AP.Properties.Resources.Buscar;
            this.btnBuscar_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscar_2.Location = new System.Drawing.Point(517, 47);
            this.btnBuscar_2.Name = "btnBuscar_2";
            this.btnBuscar_2.Size = new System.Drawing.Size(27, 21);
            this.btnBuscar_2.TabIndex = 46;
            this.btnBuscar_2.UseVisualStyleBackColor = true;
            this.btnBuscar_2.Click += new System.EventHandler(this.btnBuscar_2_Click);
            // 
            // btnBuscar_1
            // 
            this.btnBuscar_1.BackgroundImage = global::Gestion_AP.Properties.Resources.Buscar;
            this.btnBuscar_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscar_1.Location = new System.Drawing.Point(517, 20);
            this.btnBuscar_1.Name = "btnBuscar_1";
            this.btnBuscar_1.Size = new System.Drawing.Size(27, 21);
            this.btnBuscar_1.TabIndex = 45;
            this.btnBuscar_1.UseVisualStyleBackColor = true;
            this.btnBuscar_1.Click += new System.EventHandler(this.btnBuscar_1_Click);
            // 
            // txtFile5
            // 
            this.txtFile5.Location = new System.Drawing.Point(63, 127);
            this.txtFile5.Name = "txtFile5";
            this.txtFile5.ReadOnly = true;
            this.txtFile5.Size = new System.Drawing.Size(448, 21);
            this.txtFile5.TabIndex = 44;
            // 
            // txtFile4
            // 
            this.txtFile4.Location = new System.Drawing.Point(63, 100);
            this.txtFile4.Name = "txtFile4";
            this.txtFile4.ReadOnly = true;
            this.txtFile4.Size = new System.Drawing.Size(448, 21);
            this.txtFile4.TabIndex = 43;
            // 
            // txtFile3
            // 
            this.txtFile3.Location = new System.Drawing.Point(63, 74);
            this.txtFile3.Name = "txtFile3";
            this.txtFile3.ReadOnly = true;
            this.txtFile3.Size = new System.Drawing.Size(448, 21);
            this.txtFile3.TabIndex = 42;
            // 
            // txtFile2
            // 
            this.txtFile2.Location = new System.Drawing.Point(63, 47);
            this.txtFile2.Name = "txtFile2";
            this.txtFile2.ReadOnly = true;
            this.txtFile2.Size = new System.Drawing.Size(448, 21);
            this.txtFile2.TabIndex = 41;
            // 
            // txtFile1
            // 
            this.txtFile1.Location = new System.Drawing.Point(63, 20);
            this.txtFile1.Name = "txtFile1";
            this.txtFile1.ReadOnly = true;
            this.txtFile1.Size = new System.Drawing.Size(448, 21);
            this.txtFile1.TabIndex = 40;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(485, 83);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(93, 23);
            this.btnGuardar.TabIndex = 41;
            this.btnGuardar.Text = "GRABAR";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // gbHistorial
            // 
            this.gbHistorial.Controls.Add(this.dataGridView1);
            this.gbHistorial.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbHistorial.Location = new System.Drawing.Point(19, 382);
            this.gbHistorial.Name = "gbHistorial";
            this.gbHistorial.Size = new System.Drawing.Size(559, 124);
            this.gbHistorial.TabIndex = 42;
            this.gbHistorial.TabStop = false;
            this.gbHistorial.Text = "Historial Proceso";
            this.gbHistorial.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 17);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(553, 104);
            this.dataGridView1.TabIndex = 0;
            // 
            // chbLiberar
            // 
            this.chbLiberar.AutoSize = true;
            this.chbLiberar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbLiberar.ForeColor = System.Drawing.Color.OrangeRed;
            this.chbLiberar.Location = new System.Drawing.Point(18, 92);
            this.chbLiberar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chbLiberar.Name = "chbLiberar";
            this.chbLiberar.Size = new System.Drawing.Size(118, 19);
            this.chbLiberar.TabIndex = 43;
            this.chbLiberar.Text = "CON LIBERACION";
            this.chbLiberar.UseVisualStyleBackColor = true;
            // 
            // Adjuntos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(601, 384);
            this.Controls.Add(this.chbLiberar);
            this.Controls.Add(this.gbHistorial);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbmAdm);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chbUrgente);
            this.Controls.Add(this.txtIdControl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblWF_Asociado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbGestion);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Adjuntos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adjuntos";
            this.Load += new System.EventHandler(this.Adjuntos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbHistorial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbmAdm;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtComAdic;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chbUrgente;
        private System.Windows.Forms.TextBox txtIdControl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox lblWF_Asociado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbGestion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnBuscar_1;
        private System.Windows.Forms.TextBox txtFile5;
        private System.Windows.Forms.TextBox txtFile4;
        private System.Windows.Forms.TextBox txtFile3;
        private System.Windows.Forms.TextBox txtFile2;
        private System.Windows.Forms.TextBox txtFile1;
        private System.Windows.Forms.Button btnBuscar_5;
        private System.Windows.Forms.Button btnBuscar_4;
        private System.Windows.Forms.Button btnBuscar_3;
        private System.Windows.Forms.Button btnBuscar_2;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.GroupBox gbHistorial;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.CheckBox chbLiberar;
    }
}