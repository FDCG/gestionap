﻿namespace Gestion_AP
{
    partial class BPMPrisma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.txtPuerta = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtTipCta = new System.Windows.Forms.TextBox();
            this.txtDenCta = new System.Windows.Forms.TextBox();
            this.txtSolicitud = new System.Windows.Forms.TextBox();
            this.txtSucBco = new System.Windows.Forms.TextBox();
            this.txtCodBco = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.txtPiso = new System.Windows.Forms.TextBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.txtLOD = new System.Windows.Forms.TextBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.txtNLOD = new System.Windows.Forms.TextBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.txtCodPos = new System.Windows.Forms.TextBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.txtCodGeo = new System.Windows.Forms.TextBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.txtPartido = new System.Windows.Forms.TextBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.txtDDN = new System.Windows.Forms.TextBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.txtPromotor = new System.Windows.Forms.TextBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.txtLimCpra = new System.Windows.Forms.TextBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.txtLimFin = new System.Windows.Forms.TextBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.txtModLiq = new System.Windows.Forms.TextBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.txtFinUSS = new System.Windows.Forms.TextBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.txtFPago = new System.Windows.Forms.TextBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.txtCartera = new System.Windows.Forms.TextBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.txtPorcExigPM = new System.Windows.Forms.TextBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.txtLimExcPM = new System.Windows.Forms.TextBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.txtSucCtaDb = new System.Windows.Forms.TextBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.txtSucDlsDb = new System.Windows.Forms.TextBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.txtTipoCtaDb = new System.Windows.Forms.TextBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.txtTipDlsDb = new System.Windows.Forms.TextBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.txtNroCtaDb = new System.Windows.Forms.TextBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.txtNroDlsDb = new System.Windows.Forms.TextBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.Sellados = new System.Windows.Forms.TextBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.txtCalleCorr = new System.Windows.Forms.TextBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.txtPuertaCorr = new System.Windows.Forms.TextBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.txtPisoCorr = new System.Windows.Forms.TextBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.txtLODCor = new System.Windows.Forms.TextBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.txtNLODCorr = new System.Windows.Forms.TextBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.txtCodPosCorr = new System.Windows.Forms.TextBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.txtCodGeoCorr = new System.Windows.Forms.TextBox();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.txtPartidoCorr = new System.Windows.Forms.TextBox();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.txtProd = new System.Windows.Forms.TextBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.txtLPtmo = new System.Windows.Forms.TextBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.txtCatCaj = new System.Windows.Forms.TextBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.txtAffGr = new System.Windows.Forms.TextBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.txtSocio = new System.Windows.Forms.TextBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.txtAgrup = new System.Windows.Forms.TextBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.txtSeg = new System.Windows.Forms.TextBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.txtFLCC = new System.Windows.Forms.TextBox();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.txtSuc2da = new System.Windows.Forms.TextBox();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.txtTipCta2da = new System.Windows.Forms.TextBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.txtCta2da = new System.Windows.Forms.TextBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.txtVtoTar = new System.Windows.Forms.TextBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.txtEmpresa = new System.Windows.Forms.TextBox();
            this.pictureBox54 = new System.Windows.Forms.PictureBox();
            this.txtPlanta = new System.Windows.Forms.TextBox();
            this.pictureBox55 = new System.Windows.Forms.PictureBox();
            this.txtTipoIva = new System.Windows.Forms.TextBox();
            this.pictureBox56 = new System.Windows.Forms.PictureBox();
            this.txtCuit1 = new System.Windows.Forms.TextBox();
            this.pictureBox57 = new System.Windows.Forms.PictureBox();
            this.txtCuit2 = new System.Windows.Forms.TextBox();
            this.pictureBox58 = new System.Windows.Forms.PictureBox();
            this.txtCuit3 = new System.Windows.Forms.TextBox();
            this.pictureBox59 = new System.Windows.Forms.PictureBox();
            this.txtSecBco = new System.Windows.Forms.TextBox();
            this.pictureBox60 = new System.Windows.Forms.PictureBox();
            this.txtMarcaId = new System.Windows.Forms.TextBox();
            this.pictureBox61 = new System.Windows.Forms.PictureBox();
            this.txtNroEmp = new System.Windows.Forms.TextBox();
            this.pictureBox62 = new System.Windows.Forms.PictureBox();
            this.txtCanalVta = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.pictureBox104 = new System.Windows.Forms.PictureBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.pictureBox105 = new System.Windows.Forms.PictureBox();
            this.pictureBox106 = new System.Windows.Forms.PictureBox();
            this.pictureBox107 = new System.Windows.Forms.PictureBox();
            this.pictureBox108 = new System.Windows.Forms.PictureBox();
            this.pictureBox109 = new System.Windows.Forms.PictureBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.pictureBox110 = new System.Windows.Forms.PictureBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.pictureBox111 = new System.Windows.Forms.PictureBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.pictureBox95 = new System.Windows.Forms.PictureBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.pictureBox96 = new System.Windows.Forms.PictureBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.pictureBox97 = new System.Windows.Forms.PictureBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.pictureBox86 = new System.Windows.Forms.PictureBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.pictureBox88 = new System.Windows.Forms.PictureBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.pictureBox87 = new System.Windows.Forms.PictureBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.pictureBox89 = new System.Windows.Forms.PictureBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.pictureBox90 = new System.Windows.Forms.PictureBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.pictureBox78 = new System.Windows.Forms.PictureBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.pictureBox79 = new System.Windows.Forms.PictureBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.pictureBox80 = new System.Windows.Forms.PictureBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.pictureBox81 = new System.Windows.Forms.PictureBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.pictureBox82 = new System.Windows.Forms.PictureBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.pictureBox83 = new System.Windows.Forms.PictureBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.pictureBox84 = new System.Windows.Forms.PictureBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox84)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.ForeColor = System.Drawing.Color.Cyan;
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(742, 72);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FORMULARIO PARA ALTA DE CUENTA - TARJETA PRISMA MEDIOS DE PAGO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(11, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "NRO. DE TRÁMITE:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(123, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(166, 20);
            this.textBox1.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(13, 92);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(742, 531);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Black;
            this.tabPage1.Controls.Add(this.label68);
            this.tabPage1.Controls.Add(this.pictureBox62);
            this.tabPage1.Controls.Add(this.txtCanalVta);
            this.tabPage1.Controls.Add(this.pictureBox61);
            this.tabPage1.Controls.Add(this.txtNroEmp);
            this.tabPage1.Controls.Add(this.pictureBox60);
            this.tabPage1.Controls.Add(this.txtMarcaId);
            this.tabPage1.Controls.Add(this.pictureBox59);
            this.tabPage1.Controls.Add(this.txtSecBco);
            this.tabPage1.Controls.Add(this.pictureBox58);
            this.tabPage1.Controls.Add(this.txtCuit3);
            this.tabPage1.Controls.Add(this.pictureBox57);
            this.tabPage1.Controls.Add(this.txtCuit2);
            this.tabPage1.Controls.Add(this.pictureBox56);
            this.tabPage1.Controls.Add(this.txtCuit1);
            this.tabPage1.Controls.Add(this.pictureBox55);
            this.tabPage1.Controls.Add(this.txtTipoIva);
            this.tabPage1.Controls.Add(this.pictureBox54);
            this.tabPage1.Controls.Add(this.txtPlanta);
            this.tabPage1.Controls.Add(this.pictureBox53);
            this.tabPage1.Controls.Add(this.txtEmpresa);
            this.tabPage1.Controls.Add(this.pictureBox52);
            this.tabPage1.Controls.Add(this.txtVtoTar);
            this.tabPage1.Controls.Add(this.pictureBox51);
            this.tabPage1.Controls.Add(this.txtCta2da);
            this.tabPage1.Controls.Add(this.pictureBox50);
            this.tabPage1.Controls.Add(this.txtTipCta2da);
            this.tabPage1.Controls.Add(this.pictureBox49);
            this.tabPage1.Controls.Add(this.txtSuc2da);
            this.tabPage1.Controls.Add(this.pictureBox48);
            this.tabPage1.Controls.Add(this.txtFLCC);
            this.tabPage1.Controls.Add(this.pictureBox47);
            this.tabPage1.Controls.Add(this.txtSeg);
            this.tabPage1.Controls.Add(this.pictureBox46);
            this.tabPage1.Controls.Add(this.txtAgrup);
            this.tabPage1.Controls.Add(this.pictureBox45);
            this.tabPage1.Controls.Add(this.txtSocio);
            this.tabPage1.Controls.Add(this.pictureBox44);
            this.tabPage1.Controls.Add(this.txtAffGr);
            this.tabPage1.Controls.Add(this.pictureBox43);
            this.tabPage1.Controls.Add(this.txtCatCaj);
            this.tabPage1.Controls.Add(this.pictureBox42);
            this.tabPage1.Controls.Add(this.txtLPtmo);
            this.tabPage1.Controls.Add(this.pictureBox41);
            this.tabPage1.Controls.Add(this.txtProd);
            this.tabPage1.Controls.Add(this.pictureBox40);
            this.tabPage1.Controls.Add(this.txtPartidoCorr);
            this.tabPage1.Controls.Add(this.pictureBox39);
            this.tabPage1.Controls.Add(this.txtCodGeoCorr);
            this.tabPage1.Controls.Add(this.pictureBox38);
            this.tabPage1.Controls.Add(this.txtCodPosCorr);
            this.tabPage1.Controls.Add(this.pictureBox37);
            this.tabPage1.Controls.Add(this.txtNLODCorr);
            this.tabPage1.Controls.Add(this.pictureBox36);
            this.tabPage1.Controls.Add(this.txtLODCor);
            this.tabPage1.Controls.Add(this.pictureBox35);
            this.tabPage1.Controls.Add(this.txtPisoCorr);
            this.tabPage1.Controls.Add(this.pictureBox34);
            this.tabPage1.Controls.Add(this.txtPuertaCorr);
            this.tabPage1.Controls.Add(this.pictureBox33);
            this.tabPage1.Controls.Add(this.txtCalleCorr);
            this.tabPage1.Controls.Add(this.pictureBox32);
            this.tabPage1.Controls.Add(this.txtCelular);
            this.tabPage1.Controls.Add(this.pictureBox31);
            this.tabPage1.Controls.Add(this.Sellados);
            this.tabPage1.Controls.Add(this.pictureBox30);
            this.tabPage1.Controls.Add(this.txtNroDlsDb);
            this.tabPage1.Controls.Add(this.pictureBox29);
            this.tabPage1.Controls.Add(this.txtNroCtaDb);
            this.tabPage1.Controls.Add(this.pictureBox28);
            this.tabPage1.Controls.Add(this.txtTipDlsDb);
            this.tabPage1.Controls.Add(this.pictureBox27);
            this.tabPage1.Controls.Add(this.txtTipoCtaDb);
            this.tabPage1.Controls.Add(this.pictureBox26);
            this.tabPage1.Controls.Add(this.txtSucDlsDb);
            this.tabPage1.Controls.Add(this.pictureBox25);
            this.tabPage1.Controls.Add(this.txtSucCtaDb);
            this.tabPage1.Controls.Add(this.pictureBox24);
            this.tabPage1.Controls.Add(this.txtLimExcPM);
            this.tabPage1.Controls.Add(this.pictureBox23);
            this.tabPage1.Controls.Add(this.txtPorcExigPM);
            this.tabPage1.Controls.Add(this.pictureBox22);
            this.tabPage1.Controls.Add(this.txtCartera);
            this.tabPage1.Controls.Add(this.pictureBox21);
            this.tabPage1.Controls.Add(this.txtFPago);
            this.tabPage1.Controls.Add(this.pictureBox20);
            this.tabPage1.Controls.Add(this.txtFinUSS);
            this.tabPage1.Controls.Add(this.pictureBox19);
            this.tabPage1.Controls.Add(this.txtModLiq);
            this.tabPage1.Controls.Add(this.pictureBox18);
            this.tabPage1.Controls.Add(this.txtLimFin);
            this.tabPage1.Controls.Add(this.pictureBox17);
            this.tabPage1.Controls.Add(this.txtLimCpra);
            this.tabPage1.Controls.Add(this.pictureBox16);
            this.tabPage1.Controls.Add(this.txtPromotor);
            this.tabPage1.Controls.Add(this.pictureBox15);
            this.tabPage1.Controls.Add(this.txtDDN);
            this.tabPage1.Controls.Add(this.pictureBox14);
            this.tabPage1.Controls.Add(this.txtTelefono);
            this.tabPage1.Controls.Add(this.pictureBox13);
            this.tabPage1.Controls.Add(this.txtPartido);
            this.tabPage1.Controls.Add(this.pictureBox12);
            this.tabPage1.Controls.Add(this.txtCodGeo);
            this.tabPage1.Controls.Add(this.pictureBox11);
            this.tabPage1.Controls.Add(this.txtCodPos);
            this.tabPage1.Controls.Add(this.pictureBox10);
            this.tabPage1.Controls.Add(this.txtNLOD);
            this.tabPage1.Controls.Add(this.pictureBox9);
            this.tabPage1.Controls.Add(this.txtLOD);
            this.tabPage1.Controls.Add(this.pictureBox8);
            this.tabPage1.Controls.Add(this.txtPiso);
            this.tabPage1.Controls.Add(this.pictureBox7);
            this.tabPage1.Controls.Add(this.txtPuerta);
            this.tabPage1.Controls.Add(this.pictureBox6);
            this.tabPage1.Controls.Add(this.txtCalle);
            this.tabPage1.Controls.Add(this.pictureBox5);
            this.tabPage1.Controls.Add(this.pictureBox4);
            this.tabPage1.Controls.Add(this.pictureBox3);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.txtTipCta);
            this.tabPage1.Controls.Add(this.txtDenCta);
            this.tabPage1.Controls.Add(this.txtSolicitud);
            this.tabPage1.Controls.Add(this.txtSucBco);
            this.tabPage1.Controls.Add(this.txtCodBco);
            this.tabPage1.Controls.Add(this.label64);
            this.tabPage1.Controls.Add(this.label65);
            this.tabPage1.Controls.Add(this.label66);
            this.tabPage1.Controls.Add(this.label67);
            this.tabPage1.Controls.Add(this.label63);
            this.tabPage1.Controls.Add(this.label62);
            this.tabPage1.Controls.Add(this.label61);
            this.tabPage1.Controls.Add(this.label49);
            this.tabPage1.Controls.Add(this.label54);
            this.tabPage1.Controls.Add(this.label59);
            this.tabPage1.Controls.Add(this.label60);
            this.tabPage1.Controls.Add(this.label55);
            this.tabPage1.Controls.Add(this.label56);
            this.tabPage1.Controls.Add(this.label57);
            this.tabPage1.Controls.Add(this.label58);
            this.tabPage1.Controls.Add(this.label47);
            this.tabPage1.Controls.Add(this.label48);
            this.tabPage1.Controls.Add(this.label50);
            this.tabPage1.Controls.Add(this.label51);
            this.tabPage1.Controls.Add(this.label52);
            this.tabPage1.Controls.Add(this.label53);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.label42);
            this.tabPage1.Controls.Add(this.label43);
            this.tabPage1.Controls.Add(this.label44);
            this.tabPage1.Controls.Add(this.label45);
            this.tabPage1.Controls.Add(this.label46);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Controls.Add(this.label34);
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.ForeColor = System.Drawing.Color.Lime;
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(734, 504);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "* * *  50 - ALTA DE CUENTA  * * *";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox7.Location = new System.Drawing.Point(380, 109);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(50, 1);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 91;
            this.pictureBox7.TabStop = false;
            // 
            // txtPuerta
            // 
            this.txtPuerta.BackColor = System.Drawing.Color.Black;
            this.txtPuerta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPuerta.ForeColor = System.Drawing.Color.Blue;
            this.txtPuerta.Location = new System.Drawing.Point(381, 93);
            this.txtPuerta.MaxLength = 5;
            this.txtPuerta.Name = "txtPuerta";
            this.txtPuerta.Size = new System.Drawing.Size(50, 15);
            this.txtPuerta.TabIndex = 90;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox6.Location = new System.Drawing.Point(66, 109);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(280, 1);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 89;
            this.pictureBox6.TabStop = false;
            // 
            // txtCalle
            // 
            this.txtCalle.BackColor = System.Drawing.Color.Black;
            this.txtCalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCalle.ForeColor = System.Drawing.Color.Blue;
            this.txtCalle.Location = new System.Drawing.Point(68, 93);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(280, 15);
            this.txtCalle.TabIndex = 88;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox5.Location = new System.Drawing.Point(559, 87);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(30, 1);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 87;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox4.Location = new System.Drawing.Point(100, 86);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(380, 1);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 86;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox3.Location = new System.Drawing.Point(622, 33);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(60, 1);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 85;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox2.Location = new System.Drawing.Point(425, 34);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 84;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox1.Location = new System.Drawing.Point(63, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 1);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 83;
            this.pictureBox1.TabStop = false;
            // 
            // txtTipCta
            // 
            this.txtTipCta.BackColor = System.Drawing.Color.Black;
            this.txtTipCta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTipCta.ForeColor = System.Drawing.Color.Blue;
            this.txtTipCta.Location = new System.Drawing.Point(560, 71);
            this.txtTipCta.MaxLength = 1;
            this.txtTipCta.Name = "txtTipCta";
            this.txtTipCta.Size = new System.Drawing.Size(50, 15);
            this.txtTipCta.TabIndex = 77;
            // 
            // txtDenCta
            // 
            this.txtDenCta.BackColor = System.Drawing.Color.Black;
            this.txtDenCta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDenCta.ForeColor = System.Drawing.Color.Blue;
            this.txtDenCta.Location = new System.Drawing.Point(102, 70);
            this.txtDenCta.Name = "txtDenCta";
            this.txtDenCta.Size = new System.Drawing.Size(380, 15);
            this.txtDenCta.TabIndex = 75;
            // 
            // txtSolicitud
            // 
            this.txtSolicitud.BackColor = System.Drawing.Color.Black;
            this.txtSolicitud.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSolicitud.ForeColor = System.Drawing.Color.Blue;
            this.txtSolicitud.Location = new System.Drawing.Point(624, 17);
            this.txtSolicitud.MaxLength = 6;
            this.txtSolicitud.Name = "txtSolicitud";
            this.txtSolicitud.Size = new System.Drawing.Size(80, 15);
            this.txtSolicitud.TabIndex = 73;
            // 
            // txtSucBco
            // 
            this.txtSucBco.BackColor = System.Drawing.Color.Black;
            this.txtSucBco.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSucBco.ForeColor = System.Drawing.Color.Blue;
            this.txtSucBco.Location = new System.Drawing.Point(426, 17);
            this.txtSucBco.MaxLength = 3;
            this.txtSucBco.Name = "txtSucBco";
            this.txtSucBco.Size = new System.Drawing.Size(30, 15);
            this.txtSucBco.TabIndex = 71;
            // 
            // txtCodBco
            // 
            this.txtCodBco.BackColor = System.Drawing.Color.Black;
            this.txtCodBco.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodBco.ForeColor = System.Drawing.Color.Blue;
            this.txtCodBco.Location = new System.Drawing.Point(64, 17);
            this.txtCodBco.MaxLength = 3;
            this.txtCodBco.Name = "txtCodBco";
            this.txtCodBco.Size = new System.Drawing.Size(50, 15);
            this.txtCodBco.TabIndex = 69;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(466, 436);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(35, 14);
            this.label64.TabIndex = 68;
            this.label64.Text = "CVta";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(358, 436);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(42, 14);
            this.label65.TabIndex = 67;
            this.label65.Text = "N.Emp";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(148, 436);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(63, 14);
            this.label66.TabIndex = 66;
            this.label66.Text = "Marca Id";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(20, 436);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(70, 14);
            this.label67.TabIndex = 65;
            this.label67.Text = "Sec.Banco";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(645, 409);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(14, 14);
            this.label63.TabIndex = 64;
            this.label63.Text = "-";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(548, 409);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(14, 14);
            this.label62.TabIndex = 63;
            this.label62.Text = "-";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(456, 409);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(63, 14);
            this.label61.TabIndex = 62;
            this.label61.Text = "Nro.CUIT";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(368, 409);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(63, 14);
            this.label49.TabIndex = 61;
            this.label49.Text = "Tipo IVA";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(233, 409);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(49, 14);
            this.label54.TabIndex = 60;
            this.label54.Text = "Planta";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(121, 409);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(56, 14);
            this.label59.TabIndex = 59;
            this.label59.Text = "Empresa";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(20, 409);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(63, 14);
            this.label60.TabIndex = 58;
            this.label60.Text = "Vto.tar.";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(404, 384);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(112, 14);
            this.label55.TabIndex = 57;
            this.label55.Text = "Nro.Cta  $ 2da.";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(269, 384);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(98, 14);
            this.label56.TabIndex = 56;
            this.label56.Text = "T.Cta  $ 2da.";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(134, 384);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(84, 14);
            this.label57.TabIndex = 55;
            this.label57.Text = "Suc.  $ 2da";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(20, 384);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(63, 14);
            this.label58.TabIndex = 54;
            this.label58.Text = "F.L.C.C.";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(583, 359);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(28, 14);
            this.label47.TabIndex = 53;
            this.label47.Text = "Seg";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(490, 359);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(49, 14);
            this.label48.TabIndex = 52;
            this.label48.Text = "Agrup.";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(352, 359);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(42, 14);
            this.label50.TabIndex = 50;
            this.label50.Text = "Socio";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(241, 359);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(56, 14);
            this.label51.TabIndex = 49;
            this.label51.Text = "Aff.Gr.";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(102, 359);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(112, 14);
            this.label52.TabIndex = 48;
            this.label52.Text = "Categ.p/cajeros";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(20, 359);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(49, 14);
            this.label53.TabIndex = 47;
            this.label53.Text = "L.Ptmo";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(20, 335);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(693, 14);
            this.label38.TabIndex = 46;
            this.label38.Text = "---------------------------------------------------------------------------------" +
    "-----------------";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(578, 310);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 14);
            this.label37.TabIndex = 45;
            this.label37.Text = "Prod.";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(297, 310);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(91, 14);
            this.label39.TabIndex = 43;
            this.label39.Text = "Dpto-Partido";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(139, 310);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(91, 14);
            this.label40.TabIndex = 42;
            this.label40.Text = "C.Geografico";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(20, 310);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(63, 14);
            this.label41.TabIndex = 41;
            this.label41.Text = "C.Postal";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(578, 287);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(42, 14);
            this.label42.TabIndex = 40;
            this.label42.Text = "N.LOD";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(508, 287);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(28, 14);
            this.label43.TabIndex = 39;
            this.label43.Text = "LOD";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(435, 287);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(35, 14);
            this.label44.TabIndex = 38;
            this.label44.Text = "Piso";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(354, 287);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 14);
            this.label45.TabIndex = 37;
            this.label45.Text = "Nro";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(20, 287);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(42, 14);
            this.label46.TabIndex = 36;
            this.label46.Text = "Calle";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(20, 263);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(693, 14);
            this.label36.TabIndex = 35;
            this.label36.Text = "---------------------  Domicilio para la entrega de correspondencia  ------------" +
    "-----------------";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(535, 239);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(56, 14);
            this.label29.TabIndex = 34;
            this.label29.Text = "Celular";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(254, 239);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(84, 14);
            this.label33.TabIndex = 33;
            this.label33.Text = "Nro.Cta U$S";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(147, 239);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(70, 14);
            this.label34.TabIndex = 32;
            this.label34.Text = "T.Cta U$S";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(20, 239);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(91, 14);
            this.label35.TabIndex = 31;
            this.label35.Text = "Cod.Suc. U$S";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(556, 217);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(63, 14);
            this.label28.TabIndex = 30;
            this.label28.Text = "Sellados";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(254, 217);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(77, 14);
            this.label30.TabIndex = 28;
            this.label30.Text = "Nro.Cta  $";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(147, 217);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(63, 14);
            this.label31.TabIndex = 27;
            this.label31.Text = "T.Cta  $";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(20, 217);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(84, 14);
            this.label32.TabIndex = 26;
            this.label32.Text = "Cod.Suc.  $";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(20, 187);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(693, 14);
            this.label25.TabIndex = 25;
            this.label25.Text = "------------------------------  Datos de la Cuenta Bancaria  --------------------" +
    "-----------------";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(173, 165);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(112, 14);
            this.label26.TabIndex = 24;
            this.label26.Text = "Lim.Exc.Pag.Min";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(20, 165);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(112, 14);
            this.label27.TabIndex = 23;
            this.label27.Text = "Porc.Exig.P.Min";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(519, 141);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 14);
            this.label23.TabIndex = 22;
            this.label23.Text = "F.Pago";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(434, 141);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 14);
            this.label24.TabIndex = 21;
            this.label24.Text = "Fin.U$S";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(601, 141);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 14);
            this.label18.TabIndex = 20;
            this.label18.Text = "C.Cart.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(333, 141);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 14);
            this.label19.TabIndex = 19;
            this.label19.Text = "Mod.Liq";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(235, 141);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 14);
            this.label20.TabIndex = 18;
            this.label20.Text = "Lim.Fin";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(141, 141);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 14);
            this.label21.TabIndex = 17;
            this.label21.Text = "Lim.Cpra";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(20, 141);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 14);
            this.label22.TabIndex = 16;
            this.label22.Text = "Promotor";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(602, 116);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 14);
            this.label13.TabIndex = 15;
            this.label13.Text = "DDN";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(486, 116);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 14);
            this.label14.TabIndex = 14;
            this.label14.Text = "Tel";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(212, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 14);
            this.label15.TabIndex = 13;
            this.label15.Text = "Dpto-Partido";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(107, 116);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 14);
            this.label16.TabIndex = 12;
            this.label16.Text = "C.Geog.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(20, 116);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 14);
            this.label17.TabIndex = 11;
            this.label17.Text = "C.P.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(573, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 14);
            this.label12.TabIndex = 10;
            this.label12.Text = "N.LOD";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(503, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 14);
            this.label11.TabIndex = 9;
            this.label11.Text = "LOD";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(435, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 14);
            this.label10.TabIndex = 8;
            this.label10.Text = "Piso";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(351, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 14);
            this.label9.TabIndex = 7;
            this.label9.Text = "Nro";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 14);
            this.label8.TabIndex = 6;
            this.label8.Text = "Calle";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(507, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 14);
            this.label7.TabIndex = 5;
            this.label7.Text = "T.Cta.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 14);
            this.label6.TabIndex = 4;
            this.label6.Text = "Denominac.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(700, 14);
            this.label5.TabIndex = 3;
            this.label5.Text = "------------------------------  Datos de la Cuenta VISA/AMEX  -------------------" +
    "------------------";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(518, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 14);
            this.label4.TabIndex = 2;
            this.label4.Text = "Nro.Solicitud";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(395, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "Suc";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Banco";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Black;
            this.tabPage2.Controls.Add(this.pictureBox84);
            this.tabPage2.Controls.Add(this.textBox23);
            this.tabPage2.Controls.Add(this.label91);
            this.tabPage2.Controls.Add(this.pictureBox83);
            this.tabPage2.Controls.Add(this.textBox22);
            this.tabPage2.Controls.Add(this.label90);
            this.tabPage2.Controls.Add(this.pictureBox82);
            this.tabPage2.Controls.Add(this.textBox21);
            this.tabPage2.Controls.Add(this.label89);
            this.tabPage2.Controls.Add(this.pictureBox81);
            this.tabPage2.Controls.Add(this.textBox20);
            this.tabPage2.Controls.Add(this.label88);
            this.tabPage2.Controls.Add(this.pictureBox80);
            this.tabPage2.Controls.Add(this.textBox19);
            this.tabPage2.Controls.Add(this.label87);
            this.tabPage2.Controls.Add(this.pictureBox79);
            this.tabPage2.Controls.Add(this.textBox18);
            this.tabPage2.Controls.Add(this.label86);
            this.tabPage2.Controls.Add(this.pictureBox78);
            this.tabPage2.Controls.Add(this.textBox17);
            this.tabPage2.Controls.Add(this.label85);
            this.tabPage2.Controls.Add(this.pictureBox89);
            this.tabPage2.Controls.Add(this.textBox28);
            this.tabPage2.Controls.Add(this.pictureBox90);
            this.tabPage2.Controls.Add(this.textBox29);
            this.tabPage2.Controls.Add(this.label98);
            this.tabPage2.Controls.Add(this.label99);
            this.tabPage2.Controls.Add(this.label97);
            this.tabPage2.Controls.Add(this.pictureBox87);
            this.tabPage2.Controls.Add(this.textBox26);
            this.tabPage2.Controls.Add(this.pictureBox86);
            this.tabPage2.Controls.Add(this.textBox25);
            this.tabPage2.Controls.Add(this.pictureBox88);
            this.tabPage2.Controls.Add(this.textBox27);
            this.tabPage2.Controls.Add(this.label94);
            this.tabPage2.Controls.Add(this.label95);
            this.tabPage2.Controls.Add(this.label96);
            this.tabPage2.Controls.Add(this.pictureBox97);
            this.tabPage2.Controls.Add(this.textBox36);
            this.tabPage2.Controls.Add(this.pictureBox96);
            this.tabPage2.Controls.Add(this.textBox35);
            this.tabPage2.Controls.Add(this.pictureBox95);
            this.tabPage2.Controls.Add(this.textBox34);
            this.tabPage2.Controls.Add(this.pictureBox111);
            this.tabPage2.Controls.Add(this.textBox50);
            this.tabPage2.Controls.Add(this.label121);
            this.tabPage2.Controls.Add(this.label116);
            this.tabPage2.Controls.Add(this.pictureBox110);
            this.tabPage2.Controls.Add(this.textBox49);
            this.tabPage2.Controls.Add(this.label120);
            this.tabPage2.Controls.Add(this.pictureBox104);
            this.tabPage2.Controls.Add(this.textBox43);
            this.tabPage2.Controls.Add(this.pictureBox105);
            this.tabPage2.Controls.Add(this.pictureBox106);
            this.tabPage2.Controls.Add(this.pictureBox107);
            this.tabPage2.Controls.Add(this.pictureBox108);
            this.tabPage2.Controls.Add(this.pictureBox109);
            this.tabPage2.Controls.Add(this.textBox44);
            this.tabPage2.Controls.Add(this.textBox45);
            this.tabPage2.Controls.Add(this.textBox46);
            this.tabPage2.Controls.Add(this.textBox47);
            this.tabPage2.Controls.Add(this.textBox48);
            this.tabPage2.Controls.Add(this.label106);
            this.tabPage2.Controls.Add(this.label107);
            this.tabPage2.Controls.Add(this.label108);
            this.tabPage2.Controls.Add(this.label113);
            this.tabPage2.Controls.Add(this.label114);
            this.tabPage2.Controls.Add(this.label115);
            this.tabPage2.Controls.Add(this.label117);
            this.tabPage2.Controls.Add(this.label118);
            this.tabPage2.Controls.Add(this.label119);
            this.tabPage2.ForeColor = System.Drawing.Color.Lime;
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(734, 504);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "* * *  51 - ALTA DE TARJETA  * * *";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox8.Location = new System.Drawing.Point(474, 110);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(20, 1);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 93;
            this.pictureBox8.TabStop = false;
            // 
            // txtPiso
            // 
            this.txtPiso.BackColor = System.Drawing.Color.Black;
            this.txtPiso.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPiso.ForeColor = System.Drawing.Color.Blue;
            this.txtPiso.Location = new System.Drawing.Point(475, 94);
            this.txtPiso.MaxLength = 2;
            this.txtPiso.Name = "txtPiso";
            this.txtPiso.Size = new System.Drawing.Size(20, 15);
            this.txtPiso.TabIndex = 92;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox9.Location = new System.Drawing.Point(535, 110);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(20, 1);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 95;
            this.pictureBox9.TabStop = false;
            // 
            // txtLOD
            // 
            this.txtLOD.BackColor = System.Drawing.Color.Black;
            this.txtLOD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLOD.ForeColor = System.Drawing.Color.Blue;
            this.txtLOD.Location = new System.Drawing.Point(536, 94);
            this.txtLOD.MaxLength = 2;
            this.txtLOD.Name = "txtLOD";
            this.txtLOD.Size = new System.Drawing.Size(20, 15);
            this.txtLOD.TabIndex = 94;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox10.Location = new System.Drawing.Point(621, 110);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(20, 1);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 97;
            this.pictureBox10.TabStop = false;
            // 
            // txtNLOD
            // 
            this.txtNLOD.BackColor = System.Drawing.Color.Black;
            this.txtNLOD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNLOD.ForeColor = System.Drawing.Color.Blue;
            this.txtNLOD.Location = new System.Drawing.Point(622, 94);
            this.txtNLOD.MaxLength = 2;
            this.txtNLOD.Name = "txtNLOD";
            this.txtNLOD.Size = new System.Drawing.Size(20, 15);
            this.txtNLOD.TabIndex = 96;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox11.Location = new System.Drawing.Point(53, 131);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(50, 1);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 99;
            this.pictureBox11.TabStop = false;
            // 
            // txtCodPos
            // 
            this.txtCodPos.BackColor = System.Drawing.Color.Black;
            this.txtCodPos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodPos.ForeColor = System.Drawing.Color.Blue;
            this.txtCodPos.Location = new System.Drawing.Point(54, 115);
            this.txtCodPos.MaxLength = 4;
            this.txtCodPos.Name = "txtCodPos";
            this.txtCodPos.Size = new System.Drawing.Size(50, 15);
            this.txtCodPos.TabIndex = 98;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox12.Location = new System.Drawing.Point(159, 132);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(50, 1);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 101;
            this.pictureBox12.TabStop = false;
            // 
            // txtCodGeo
            // 
            this.txtCodGeo.BackColor = System.Drawing.Color.Black;
            this.txtCodGeo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodGeo.ForeColor = System.Drawing.Color.Blue;
            this.txtCodGeo.Location = new System.Drawing.Point(160, 116);
            this.txtCodGeo.MaxLength = 5;
            this.txtCodGeo.Name = "txtCodGeo";
            this.txtCodGeo.Size = new System.Drawing.Size(50, 15);
            this.txtCodGeo.TabIndex = 100;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox13.Location = new System.Drawing.Point(308, 131);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(175, 1);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 103;
            this.pictureBox13.TabStop = false;
            // 
            // txtPartido
            // 
            this.txtPartido.BackColor = System.Drawing.Color.Black;
            this.txtPartido.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPartido.ForeColor = System.Drawing.Color.Blue;
            this.txtPartido.Location = new System.Drawing.Point(309, 115);
            this.txtPartido.MaxLength = 5;
            this.txtPartido.Name = "txtPartido";
            this.txtPartido.Size = new System.Drawing.Size(175, 15);
            this.txtPartido.TabIndex = 102;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox14.Location = new System.Drawing.Point(514, 131);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(80, 1);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 105;
            this.pictureBox14.TabStop = false;
            // 
            // txtTelefono
            // 
            this.txtTelefono.BackColor = System.Drawing.Color.Black;
            this.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelefono.ForeColor = System.Drawing.Color.Blue;
            this.txtTelefono.Location = new System.Drawing.Point(516, 115);
            this.txtTelefono.MaxLength = 8;
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(80, 15);
            this.txtTelefono.TabIndex = 104;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox15.Location = new System.Drawing.Point(635, 132);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(50, 1);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 107;
            this.pictureBox15.TabStop = false;
            // 
            // txtDDN
            // 
            this.txtDDN.BackColor = System.Drawing.Color.Black;
            this.txtDDN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDDN.ForeColor = System.Drawing.Color.Blue;
            this.txtDDN.Location = new System.Drawing.Point(636, 116);
            this.txtDDN.MaxLength = 5;
            this.txtDDN.Name = "txtDDN";
            this.txtDDN.Size = new System.Drawing.Size(50, 15);
            this.txtDDN.TabIndex = 106;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox16.Location = new System.Drawing.Point(83, 155);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(50, 1);
            this.pictureBox16.TabIndex = 109;
            this.pictureBox16.TabStop = false;
            // 
            // txtPromotor
            // 
            this.txtPromotor.BackColor = System.Drawing.Color.Black;
            this.txtPromotor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPromotor.ForeColor = System.Drawing.Color.Blue;
            this.txtPromotor.Location = new System.Drawing.Point(84, 140);
            this.txtPromotor.MaxLength = 4;
            this.txtPromotor.Name = "txtPromotor";
            this.txtPromotor.Size = new System.Drawing.Size(50, 15);
            this.txtPromotor.TabIndex = 108;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox17.Location = new System.Drawing.Point(205, 156);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(20, 1);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 111;
            this.pictureBox17.TabStop = false;
            // 
            // txtLimCpra
            // 
            this.txtLimCpra.BackColor = System.Drawing.Color.Black;
            this.txtLimCpra.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLimCpra.ForeColor = System.Drawing.Color.Blue;
            this.txtLimCpra.Location = new System.Drawing.Point(209, 140);
            this.txtLimCpra.MaxLength = 2;
            this.txtLimCpra.Name = "txtLimCpra";
            this.txtLimCpra.Size = new System.Drawing.Size(20, 15);
            this.txtLimCpra.TabIndex = 110;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox18.Location = new System.Drawing.Point(293, 156);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(30, 1);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 113;
            this.pictureBox18.TabStop = false;
            // 
            // txtLimFin
            // 
            this.txtLimFin.BackColor = System.Drawing.Color.Black;
            this.txtLimFin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLimFin.ForeColor = System.Drawing.Color.Blue;
            this.txtLimFin.Location = new System.Drawing.Point(295, 139);
            this.txtLimFin.MaxLength = 3;
            this.txtLimFin.Name = "txtLimFin";
            this.txtLimFin.Size = new System.Drawing.Size(30, 15);
            this.txtLimFin.TabIndex = 112;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox19.Location = new System.Drawing.Point(392, 155);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(30, 1);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 115;
            this.pictureBox19.TabStop = false;
            // 
            // txtModLiq
            // 
            this.txtModLiq.BackColor = System.Drawing.Color.Black;
            this.txtModLiq.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtModLiq.ForeColor = System.Drawing.Color.Blue;
            this.txtModLiq.Location = new System.Drawing.Point(394, 138);
            this.txtModLiq.MaxLength = 3;
            this.txtModLiq.Name = "txtModLiq";
            this.txtModLiq.Size = new System.Drawing.Size(30, 15);
            this.txtModLiq.TabIndex = 114;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox20.Location = new System.Drawing.Point(491, 155);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(20, 1);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 117;
            this.pictureBox20.TabStop = false;
            // 
            // txtFinUSS
            // 
            this.txtFinUSS.BackColor = System.Drawing.Color.Black;
            this.txtFinUSS.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFinUSS.ForeColor = System.Drawing.Color.Blue;
            this.txtFinUSS.Location = new System.Drawing.Point(492, 139);
            this.txtFinUSS.MaxLength = 2;
            this.txtFinUSS.Name = "txtFinUSS";
            this.txtFinUSS.Size = new System.Drawing.Size(20, 15);
            this.txtFinUSS.TabIndex = 116;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox21.Location = new System.Drawing.Point(570, 155);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(20, 1);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 119;
            this.pictureBox21.TabStop = false;
            // 
            // txtFPago
            // 
            this.txtFPago.BackColor = System.Drawing.Color.Black;
            this.txtFPago.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFPago.ForeColor = System.Drawing.Color.Blue;
            this.txtFPago.Location = new System.Drawing.Point(571, 139);
            this.txtFPago.MaxLength = 2;
            this.txtFPago.Name = "txtFPago";
            this.txtFPago.Size = new System.Drawing.Size(20, 15);
            this.txtFPago.TabIndex = 118;
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox22.Location = new System.Drawing.Point(660, 155);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(20, 1);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox22.TabIndex = 121;
            this.pictureBox22.TabStop = false;
            // 
            // txtCartera
            // 
            this.txtCartera.BackColor = System.Drawing.Color.Black;
            this.txtCartera.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCartera.ForeColor = System.Drawing.Color.Blue;
            this.txtCartera.Location = new System.Drawing.Point(661, 139);
            this.txtCartera.MaxLength = 2;
            this.txtCartera.Name = "txtCartera";
            this.txtCartera.Size = new System.Drawing.Size(20, 15);
            this.txtCartera.TabIndex = 120;
            // 
            // pictureBox23
            // 
            this.pictureBox23.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox23.Location = new System.Drawing.Point(142, 180);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(20, 1);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox23.TabIndex = 123;
            this.pictureBox23.TabStop = false;
            // 
            // txtPorcExigPM
            // 
            this.txtPorcExigPM.BackColor = System.Drawing.Color.Black;
            this.txtPorcExigPM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPorcExigPM.ForeColor = System.Drawing.Color.Blue;
            this.txtPorcExigPM.Location = new System.Drawing.Point(143, 164);
            this.txtPorcExigPM.MaxLength = 2;
            this.txtPorcExigPM.Name = "txtPorcExigPM";
            this.txtPorcExigPM.Size = new System.Drawing.Size(20, 15);
            this.txtPorcExigPM.TabIndex = 122;
            // 
            // pictureBox24
            // 
            this.pictureBox24.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox24.Location = new System.Drawing.Point(290, 181);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(20, 1);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox24.TabIndex = 125;
            this.pictureBox24.TabStop = false;
            // 
            // txtLimExcPM
            // 
            this.txtLimExcPM.BackColor = System.Drawing.Color.Black;
            this.txtLimExcPM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLimExcPM.ForeColor = System.Drawing.Color.Blue;
            this.txtLimExcPM.Location = new System.Drawing.Point(291, 165);
            this.txtLimExcPM.MaxLength = 2;
            this.txtLimExcPM.Name = "txtLimExcPM";
            this.txtLimExcPM.Size = new System.Drawing.Size(20, 15);
            this.txtLimExcPM.TabIndex = 124;
            // 
            // pictureBox25
            // 
            this.pictureBox25.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox25.Location = new System.Drawing.Point(113, 230);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(30, 1);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox25.TabIndex = 127;
            this.pictureBox25.TabStop = false;
            // 
            // txtSucCtaDb
            // 
            this.txtSucCtaDb.BackColor = System.Drawing.Color.Black;
            this.txtSucCtaDb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSucCtaDb.ForeColor = System.Drawing.Color.Blue;
            this.txtSucCtaDb.Location = new System.Drawing.Point(115, 213);
            this.txtSucCtaDb.MaxLength = 3;
            this.txtSucCtaDb.Name = "txtSucCtaDb";
            this.txtSucCtaDb.Size = new System.Drawing.Size(30, 15);
            this.txtSucCtaDb.TabIndex = 126;
            // 
            // pictureBox26
            // 
            this.pictureBox26.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox26.Location = new System.Drawing.Point(113, 251);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(30, 1);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox26.TabIndex = 129;
            this.pictureBox26.TabStop = false;
            // 
            // txtSucDlsDb
            // 
            this.txtSucDlsDb.BackColor = System.Drawing.Color.Black;
            this.txtSucDlsDb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSucDlsDb.ForeColor = System.Drawing.Color.Blue;
            this.txtSucDlsDb.Location = new System.Drawing.Point(115, 234);
            this.txtSucDlsDb.MaxLength = 3;
            this.txtSucDlsDb.Name = "txtSucDlsDb";
            this.txtSucDlsDb.Size = new System.Drawing.Size(30, 15);
            this.txtSucDlsDb.TabIndex = 128;
            // 
            // pictureBox27
            // 
            this.pictureBox27.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox27.Location = new System.Drawing.Point(225, 231);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(20, 1);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox27.TabIndex = 131;
            this.pictureBox27.TabStop = false;
            // 
            // txtTipoCtaDb
            // 
            this.txtTipoCtaDb.BackColor = System.Drawing.Color.Black;
            this.txtTipoCtaDb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTipoCtaDb.ForeColor = System.Drawing.Color.Blue;
            this.txtTipoCtaDb.Location = new System.Drawing.Point(226, 215);
            this.txtTipoCtaDb.MaxLength = 2;
            this.txtTipoCtaDb.Name = "txtTipoCtaDb";
            this.txtTipoCtaDb.Size = new System.Drawing.Size(20, 15);
            this.txtTipoCtaDb.TabIndex = 130;
            // 
            // pictureBox28
            // 
            this.pictureBox28.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox28.Location = new System.Drawing.Point(225, 251);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(20, 1);
            this.pictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox28.TabIndex = 133;
            this.pictureBox28.TabStop = false;
            // 
            // txtTipDlsDb
            // 
            this.txtTipDlsDb.BackColor = System.Drawing.Color.Black;
            this.txtTipDlsDb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTipDlsDb.ForeColor = System.Drawing.Color.Blue;
            this.txtTipDlsDb.Location = new System.Drawing.Point(226, 235);
            this.txtTipDlsDb.MaxLength = 2;
            this.txtTipDlsDb.Name = "txtTipDlsDb";
            this.txtTipDlsDb.Size = new System.Drawing.Size(20, 15);
            this.txtTipDlsDb.TabIndex = 132;
            // 
            // pictureBox29
            // 
            this.pictureBox29.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox29.Location = new System.Drawing.Point(345, 231);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(190, 1);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox29.TabIndex = 135;
            this.pictureBox29.TabStop = false;
            // 
            // txtNroCtaDb
            // 
            this.txtNroCtaDb.BackColor = System.Drawing.Color.Black;
            this.txtNroCtaDb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNroCtaDb.ForeColor = System.Drawing.Color.Blue;
            this.txtNroCtaDb.Location = new System.Drawing.Point(346, 215);
            this.txtNroCtaDb.MaxLength = 5;
            this.txtNroCtaDb.Name = "txtNroCtaDb";
            this.txtNroCtaDb.Size = new System.Drawing.Size(190, 15);
            this.txtNroCtaDb.TabIndex = 134;
            // 
            // pictureBox30
            // 
            this.pictureBox30.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox30.Location = new System.Drawing.Point(345, 250);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(175, 1);
            this.pictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox30.TabIndex = 137;
            this.pictureBox30.TabStop = false;
            // 
            // txtNroDlsDb
            // 
            this.txtNroDlsDb.BackColor = System.Drawing.Color.Black;
            this.txtNroDlsDb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNroDlsDb.ForeColor = System.Drawing.Color.Blue;
            this.txtNroDlsDb.Location = new System.Drawing.Point(346, 234);
            this.txtNroDlsDb.MaxLength = 5;
            this.txtNroDlsDb.Name = "txtNroDlsDb";
            this.txtNroDlsDb.Size = new System.Drawing.Size(175, 15);
            this.txtNroDlsDb.TabIndex = 136;
            // 
            // pictureBox31
            // 
            this.pictureBox31.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox31.Location = new System.Drawing.Point(623, 233);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(20, 1);
            this.pictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox31.TabIndex = 139;
            this.pictureBox31.TabStop = false;
            // 
            // Sellados
            // 
            this.Sellados.BackColor = System.Drawing.Color.Black;
            this.Sellados.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Sellados.ForeColor = System.Drawing.Color.Blue;
            this.Sellados.Location = new System.Drawing.Point(624, 217);
            this.Sellados.MaxLength = 2;
            this.Sellados.Name = "Sellados";
            this.Sellados.Size = new System.Drawing.Size(20, 15);
            this.Sellados.TabIndex = 138;
            // 
            // pictureBox32
            // 
            this.pictureBox32.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox32.Location = new System.Drawing.Point(595, 253);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(80, 1);
            this.pictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox32.TabIndex = 141;
            this.pictureBox32.TabStop = false;
            // 
            // txtCelular
            // 
            this.txtCelular.BackColor = System.Drawing.Color.Black;
            this.txtCelular.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCelular.ForeColor = System.Drawing.Color.Blue;
            this.txtCelular.Location = new System.Drawing.Point(597, 237);
            this.txtCelular.MaxLength = 8;
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(80, 15);
            this.txtCelular.TabIndex = 140;
            // 
            // pictureBox33
            // 
            this.pictureBox33.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox33.Location = new System.Drawing.Point(66, 301);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(280, 1);
            this.pictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox33.TabIndex = 143;
            this.pictureBox33.TabStop = false;
            // 
            // txtCalleCorr
            // 
            this.txtCalleCorr.BackColor = System.Drawing.Color.Black;
            this.txtCalleCorr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCalleCorr.ForeColor = System.Drawing.Color.Blue;
            this.txtCalleCorr.Location = new System.Drawing.Point(68, 285);
            this.txtCalleCorr.Name = "txtCalleCorr";
            this.txtCalleCorr.Size = new System.Drawing.Size(280, 15);
            this.txtCalleCorr.TabIndex = 142;
            // 
            // pictureBox34
            // 
            this.pictureBox34.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox34.Location = new System.Drawing.Point(383, 301);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(50, 1);
            this.pictureBox34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox34.TabIndex = 145;
            this.pictureBox34.TabStop = false;
            // 
            // txtPuertaCorr
            // 
            this.txtPuertaCorr.BackColor = System.Drawing.Color.Black;
            this.txtPuertaCorr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPuertaCorr.ForeColor = System.Drawing.Color.Blue;
            this.txtPuertaCorr.Location = new System.Drawing.Point(384, 285);
            this.txtPuertaCorr.MaxLength = 5;
            this.txtPuertaCorr.Name = "txtPuertaCorr";
            this.txtPuertaCorr.Size = new System.Drawing.Size(50, 15);
            this.txtPuertaCorr.TabIndex = 144;
            // 
            // pictureBox35
            // 
            this.pictureBox35.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox35.Location = new System.Drawing.Point(476, 302);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(20, 1);
            this.pictureBox35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox35.TabIndex = 147;
            this.pictureBox35.TabStop = false;
            // 
            // txtPisoCorr
            // 
            this.txtPisoCorr.BackColor = System.Drawing.Color.Black;
            this.txtPisoCorr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPisoCorr.ForeColor = System.Drawing.Color.Blue;
            this.txtPisoCorr.Location = new System.Drawing.Point(477, 286);
            this.txtPisoCorr.MaxLength = 2;
            this.txtPisoCorr.Name = "txtPisoCorr";
            this.txtPisoCorr.Size = new System.Drawing.Size(20, 15);
            this.txtPisoCorr.TabIndex = 146;
            // 
            // pictureBox36
            // 
            this.pictureBox36.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox36.Location = new System.Drawing.Point(547, 303);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(20, 1);
            this.pictureBox36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox36.TabIndex = 149;
            this.pictureBox36.TabStop = false;
            // 
            // txtLODCor
            // 
            this.txtLODCor.BackColor = System.Drawing.Color.Black;
            this.txtLODCor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLODCor.ForeColor = System.Drawing.Color.Blue;
            this.txtLODCor.Location = new System.Drawing.Point(548, 287);
            this.txtLODCor.MaxLength = 2;
            this.txtLODCor.Name = "txtLODCor";
            this.txtLODCor.Size = new System.Drawing.Size(20, 15);
            this.txtLODCor.TabIndex = 148;
            // 
            // pictureBox37
            // 
            this.pictureBox37.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox37.Location = new System.Drawing.Point(625, 304);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(20, 1);
            this.pictureBox37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox37.TabIndex = 151;
            this.pictureBox37.TabStop = false;
            // 
            // txtNLODCorr
            // 
            this.txtNLODCorr.BackColor = System.Drawing.Color.Black;
            this.txtNLODCorr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNLODCorr.ForeColor = System.Drawing.Color.Blue;
            this.txtNLODCorr.Location = new System.Drawing.Point(626, 288);
            this.txtNLODCorr.MaxLength = 2;
            this.txtNLODCorr.Name = "txtNLODCorr";
            this.txtNLODCorr.Size = new System.Drawing.Size(20, 15);
            this.txtNLODCorr.TabIndex = 150;
            // 
            // pictureBox38
            // 
            this.pictureBox38.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox38.Location = new System.Drawing.Point(82, 324);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(50, 1);
            this.pictureBox38.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox38.TabIndex = 153;
            this.pictureBox38.TabStop = false;
            // 
            // txtCodPosCorr
            // 
            this.txtCodPosCorr.BackColor = System.Drawing.Color.Black;
            this.txtCodPosCorr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodPosCorr.ForeColor = System.Drawing.Color.Blue;
            this.txtCodPosCorr.Location = new System.Drawing.Point(83, 308);
            this.txtCodPosCorr.MaxLength = 4;
            this.txtCodPosCorr.Name = "txtCodPosCorr";
            this.txtCodPosCorr.Size = new System.Drawing.Size(50, 15);
            this.txtCodPosCorr.TabIndex = 152;
            // 
            // pictureBox39
            // 
            this.pictureBox39.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox39.Location = new System.Drawing.Point(234, 324);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(50, 1);
            this.pictureBox39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox39.TabIndex = 155;
            this.pictureBox39.TabStop = false;
            // 
            // txtCodGeoCorr
            // 
            this.txtCodGeoCorr.BackColor = System.Drawing.Color.Black;
            this.txtCodGeoCorr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodGeoCorr.ForeColor = System.Drawing.Color.Blue;
            this.txtCodGeoCorr.Location = new System.Drawing.Point(235, 308);
            this.txtCodGeoCorr.MaxLength = 5;
            this.txtCodGeoCorr.Name = "txtCodGeoCorr";
            this.txtCodGeoCorr.Size = new System.Drawing.Size(50, 15);
            this.txtCodGeoCorr.TabIndex = 154;
            // 
            // pictureBox40
            // 
            this.pictureBox40.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox40.Location = new System.Drawing.Point(392, 325);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(175, 1);
            this.pictureBox40.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox40.TabIndex = 157;
            this.pictureBox40.TabStop = false;
            // 
            // txtPartidoCorr
            // 
            this.txtPartidoCorr.BackColor = System.Drawing.Color.Black;
            this.txtPartidoCorr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPartidoCorr.ForeColor = System.Drawing.Color.Blue;
            this.txtPartidoCorr.Location = new System.Drawing.Point(393, 309);
            this.txtPartidoCorr.MaxLength = 5;
            this.txtPartidoCorr.Name = "txtPartidoCorr";
            this.txtPartidoCorr.Size = new System.Drawing.Size(175, 15);
            this.txtPartidoCorr.TabIndex = 156;
            // 
            // pictureBox41
            // 
            this.pictureBox41.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox41.Location = new System.Drawing.Point(625, 325);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(50, 1);
            this.pictureBox41.TabIndex = 159;
            this.pictureBox41.TabStop = false;
            // 
            // txtProd
            // 
            this.txtProd.BackColor = System.Drawing.Color.Black;
            this.txtProd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtProd.ForeColor = System.Drawing.Color.Blue;
            this.txtProd.Location = new System.Drawing.Point(626, 310);
            this.txtProd.MaxLength = 4;
            this.txtProd.Name = "txtProd";
            this.txtProd.Size = new System.Drawing.Size(50, 15);
            this.txtProd.TabIndex = 158;
            // 
            // pictureBox42
            // 
            this.pictureBox42.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox42.Location = new System.Drawing.Point(73, 373);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(20, 1);
            this.pictureBox42.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox42.TabIndex = 161;
            this.pictureBox42.TabStop = false;
            // 
            // txtLPtmo
            // 
            this.txtLPtmo.BackColor = System.Drawing.Color.Black;
            this.txtLPtmo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLPtmo.ForeColor = System.Drawing.Color.Blue;
            this.txtLPtmo.Location = new System.Drawing.Point(74, 357);
            this.txtLPtmo.MaxLength = 2;
            this.txtLPtmo.Name = "txtLPtmo";
            this.txtLPtmo.Size = new System.Drawing.Size(20, 15);
            this.txtLPtmo.TabIndex = 160;
            // 
            // pictureBox43
            // 
            this.pictureBox43.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox43.Location = new System.Drawing.Point(216, 373);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(20, 1);
            this.pictureBox43.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox43.TabIndex = 163;
            this.pictureBox43.TabStop = false;
            // 
            // txtCatCaj
            // 
            this.txtCatCaj.BackColor = System.Drawing.Color.Black;
            this.txtCatCaj.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCatCaj.ForeColor = System.Drawing.Color.Blue;
            this.txtCatCaj.Location = new System.Drawing.Point(217, 357);
            this.txtCatCaj.MaxLength = 2;
            this.txtCatCaj.Name = "txtCatCaj";
            this.txtCatCaj.Size = new System.Drawing.Size(20, 15);
            this.txtCatCaj.TabIndex = 162;
            // 
            // pictureBox44
            // 
            this.pictureBox44.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox44.Location = new System.Drawing.Point(296, 372);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(50, 1);
            this.pictureBox44.TabIndex = 165;
            this.pictureBox44.TabStop = false;
            // 
            // txtAffGr
            // 
            this.txtAffGr.BackColor = System.Drawing.Color.Black;
            this.txtAffGr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAffGr.ForeColor = System.Drawing.Color.Blue;
            this.txtAffGr.Location = new System.Drawing.Point(297, 357);
            this.txtAffGr.MaxLength = 4;
            this.txtAffGr.Name = "txtAffGr";
            this.txtAffGr.Size = new System.Drawing.Size(50, 15);
            this.txtAffGr.TabIndex = 164;
            // 
            // pictureBox45
            // 
            this.pictureBox45.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox45.Location = new System.Drawing.Point(397, 373);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(80, 1);
            this.pictureBox45.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox45.TabIndex = 167;
            this.pictureBox45.TabStop = false;
            // 
            // txtSocio
            // 
            this.txtSocio.BackColor = System.Drawing.Color.Black;
            this.txtSocio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSocio.ForeColor = System.Drawing.Color.Blue;
            this.txtSocio.Location = new System.Drawing.Point(399, 357);
            this.txtSocio.MaxLength = 8;
            this.txtSocio.Name = "txtSocio";
            this.txtSocio.Size = new System.Drawing.Size(80, 15);
            this.txtSocio.TabIndex = 166;
            // 
            // pictureBox46
            // 
            this.pictureBox46.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox46.Location = new System.Drawing.Point(544, 375);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(20, 1);
            this.pictureBox46.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox46.TabIndex = 169;
            this.pictureBox46.TabStop = false;
            // 
            // txtAgrup
            // 
            this.txtAgrup.BackColor = System.Drawing.Color.Black;
            this.txtAgrup.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAgrup.ForeColor = System.Drawing.Color.Blue;
            this.txtAgrup.Location = new System.Drawing.Point(545, 359);
            this.txtAgrup.MaxLength = 2;
            this.txtAgrup.Name = "txtAgrup";
            this.txtAgrup.Size = new System.Drawing.Size(20, 15);
            this.txtAgrup.TabIndex = 168;
            // 
            // pictureBox47
            // 
            this.pictureBox47.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox47.Location = new System.Drawing.Point(621, 374);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(50, 1);
            this.pictureBox47.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox47.TabIndex = 171;
            this.pictureBox47.TabStop = false;
            // 
            // txtSeg
            // 
            this.txtSeg.BackColor = System.Drawing.Color.Black;
            this.txtSeg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSeg.ForeColor = System.Drawing.Color.Blue;
            this.txtSeg.Location = new System.Drawing.Point(622, 358);
            this.txtSeg.MaxLength = 4;
            this.txtSeg.Name = "txtSeg";
            this.txtSeg.Size = new System.Drawing.Size(50, 15);
            this.txtSeg.TabIndex = 170;
            // 
            // pictureBox48
            // 
            this.pictureBox48.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox48.Location = new System.Drawing.Point(81, 399);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(50, 1);
            this.pictureBox48.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox48.TabIndex = 173;
            this.pictureBox48.TabStop = false;
            // 
            // txtFLCC
            // 
            this.txtFLCC.BackColor = System.Drawing.Color.Black;
            this.txtFLCC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFLCC.ForeColor = System.Drawing.Color.Blue;
            this.txtFLCC.Location = new System.Drawing.Point(82, 383);
            this.txtFLCC.MaxLength = 5;
            this.txtFLCC.Name = "txtFLCC";
            this.txtFLCC.Size = new System.Drawing.Size(50, 15);
            this.txtFLCC.TabIndex = 172;
            // 
            // pictureBox49
            // 
            this.pictureBox49.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox49.Location = new System.Drawing.Point(224, 398);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(30, 1);
            this.pictureBox49.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox49.TabIndex = 175;
            this.pictureBox49.TabStop = false;
            // 
            // txtSuc2da
            // 
            this.txtSuc2da.BackColor = System.Drawing.Color.Black;
            this.txtSuc2da.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSuc2da.ForeColor = System.Drawing.Color.Blue;
            this.txtSuc2da.Location = new System.Drawing.Point(226, 381);
            this.txtSuc2da.MaxLength = 3;
            this.txtSuc2da.Name = "txtSuc2da";
            this.txtSuc2da.Size = new System.Drawing.Size(30, 15);
            this.txtSuc2da.TabIndex = 174;
            // 
            // pictureBox50
            // 
            this.pictureBox50.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox50.Location = new System.Drawing.Point(373, 399);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(20, 1);
            this.pictureBox50.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox50.TabIndex = 177;
            this.pictureBox50.TabStop = false;
            // 
            // txtTipCta2da
            // 
            this.txtTipCta2da.BackColor = System.Drawing.Color.Black;
            this.txtTipCta2da.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTipCta2da.ForeColor = System.Drawing.Color.Blue;
            this.txtTipCta2da.Location = new System.Drawing.Point(374, 383);
            this.txtTipCta2da.MaxLength = 2;
            this.txtTipCta2da.Name = "txtTipCta2da";
            this.txtTipCta2da.Size = new System.Drawing.Size(20, 15);
            this.txtTipCta2da.TabIndex = 176;
            // 
            // pictureBox51
            // 
            this.pictureBox51.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox51.Location = new System.Drawing.Point(516, 398);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(190, 1);
            this.pictureBox51.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox51.TabIndex = 179;
            this.pictureBox51.TabStop = false;
            // 
            // txtCta2da
            // 
            this.txtCta2da.BackColor = System.Drawing.Color.Black;
            this.txtCta2da.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCta2da.ForeColor = System.Drawing.Color.Blue;
            this.txtCta2da.Location = new System.Drawing.Point(517, 382);
            this.txtCta2da.MaxLength = 5;
            this.txtCta2da.Name = "txtCta2da";
            this.txtCta2da.Size = new System.Drawing.Size(190, 15);
            this.txtCta2da.TabIndex = 178;
            // 
            // pictureBox52
            // 
            this.pictureBox52.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox52.Location = new System.Drawing.Point(84, 422);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(20, 1);
            this.pictureBox52.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox52.TabIndex = 181;
            this.pictureBox52.TabStop = false;
            // 
            // txtVtoTar
            // 
            this.txtVtoTar.BackColor = System.Drawing.Color.Black;
            this.txtVtoTar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVtoTar.ForeColor = System.Drawing.Color.Blue;
            this.txtVtoTar.Location = new System.Drawing.Point(85, 406);
            this.txtVtoTar.MaxLength = 2;
            this.txtVtoTar.Name = "txtVtoTar";
            this.txtVtoTar.Size = new System.Drawing.Size(20, 15);
            this.txtVtoTar.TabIndex = 180;
            // 
            // pictureBox53
            // 
            this.pictureBox53.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox53.Location = new System.Drawing.Point(177, 423);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(50, 1);
            this.pictureBox53.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox53.TabIndex = 183;
            this.pictureBox53.TabStop = false;
            // 
            // txtEmpresa
            // 
            this.txtEmpresa.BackColor = System.Drawing.Color.Black;
            this.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpresa.ForeColor = System.Drawing.Color.Blue;
            this.txtEmpresa.Location = new System.Drawing.Point(178, 407);
            this.txtEmpresa.MaxLength = 4;
            this.txtEmpresa.Name = "txtEmpresa";
            this.txtEmpresa.Size = new System.Drawing.Size(50, 15);
            this.txtEmpresa.TabIndex = 182;
            // 
            // pictureBox54
            // 
            this.pictureBox54.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox54.Location = new System.Drawing.Point(283, 422);
            this.pictureBox54.Name = "pictureBox54";
            this.pictureBox54.Size = new System.Drawing.Size(50, 1);
            this.pictureBox54.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox54.TabIndex = 185;
            this.pictureBox54.TabStop = false;
            // 
            // txtPlanta
            // 
            this.txtPlanta.BackColor = System.Drawing.Color.Black;
            this.txtPlanta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPlanta.ForeColor = System.Drawing.Color.Blue;
            this.txtPlanta.Location = new System.Drawing.Point(284, 406);
            this.txtPlanta.MaxLength = 4;
            this.txtPlanta.Name = "txtPlanta";
            this.txtPlanta.Size = new System.Drawing.Size(50, 15);
            this.txtPlanta.TabIndex = 184;
            // 
            // pictureBox55
            // 
            this.pictureBox55.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox55.Location = new System.Drawing.Point(434, 422);
            this.pictureBox55.Name = "pictureBox55";
            this.pictureBox55.Size = new System.Drawing.Size(20, 1);
            this.pictureBox55.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox55.TabIndex = 187;
            this.pictureBox55.TabStop = false;
            // 
            // txtTipoIva
            // 
            this.txtTipoIva.BackColor = System.Drawing.Color.Black;
            this.txtTipoIva.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTipoIva.ForeColor = System.Drawing.Color.Blue;
            this.txtTipoIva.Location = new System.Drawing.Point(435, 406);
            this.txtTipoIva.MaxLength = 2;
            this.txtTipoIva.Name = "txtTipoIva";
            this.txtTipoIva.Size = new System.Drawing.Size(20, 15);
            this.txtTipoIva.TabIndex = 186;
            // 
            // pictureBox56
            // 
            this.pictureBox56.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox56.Location = new System.Drawing.Point(526, 423);
            this.pictureBox56.Name = "pictureBox56";
            this.pictureBox56.Size = new System.Drawing.Size(20, 1);
            this.pictureBox56.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox56.TabIndex = 189;
            this.pictureBox56.TabStop = false;
            // 
            // txtCuit1
            // 
            this.txtCuit1.BackColor = System.Drawing.Color.Black;
            this.txtCuit1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCuit1.ForeColor = System.Drawing.Color.Blue;
            this.txtCuit1.Location = new System.Drawing.Point(527, 407);
            this.txtCuit1.MaxLength = 2;
            this.txtCuit1.Name = "txtCuit1";
            this.txtCuit1.Size = new System.Drawing.Size(20, 15);
            this.txtCuit1.TabIndex = 188;
            // 
            // pictureBox57
            // 
            this.pictureBox57.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox57.Location = new System.Drawing.Point(562, 424);
            this.pictureBox57.Name = "pictureBox57";
            this.pictureBox57.Size = new System.Drawing.Size(80, 1);
            this.pictureBox57.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox57.TabIndex = 191;
            this.pictureBox57.TabStop = false;
            // 
            // txtCuit2
            // 
            this.txtCuit2.BackColor = System.Drawing.Color.Black;
            this.txtCuit2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCuit2.ForeColor = System.Drawing.Color.Blue;
            this.txtCuit2.Location = new System.Drawing.Point(564, 408);
            this.txtCuit2.MaxLength = 8;
            this.txtCuit2.Name = "txtCuit2";
            this.txtCuit2.Size = new System.Drawing.Size(80, 15);
            this.txtCuit2.TabIndex = 190;
            // 
            // pictureBox58
            // 
            this.pictureBox58.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox58.Location = new System.Drawing.Point(659, 424);
            this.pictureBox58.Name = "pictureBox58";
            this.pictureBox58.Size = new System.Drawing.Size(10, 1);
            this.pictureBox58.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox58.TabIndex = 193;
            this.pictureBox58.TabStop = false;
            // 
            // txtCuit3
            // 
            this.txtCuit3.BackColor = System.Drawing.Color.Black;
            this.txtCuit3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCuit3.ForeColor = System.Drawing.Color.Blue;
            this.txtCuit3.Location = new System.Drawing.Point(660, 408);
            this.txtCuit3.MaxLength = 1;
            this.txtCuit3.Name = "txtCuit3";
            this.txtCuit3.Size = new System.Drawing.Size(10, 15);
            this.txtCuit3.TabIndex = 192;
            // 
            // pictureBox59
            // 
            this.pictureBox59.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox59.Location = new System.Drawing.Point(91, 449);
            this.pictureBox59.Name = "pictureBox59";
            this.pictureBox59.Size = new System.Drawing.Size(50, 1);
            this.pictureBox59.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox59.TabIndex = 195;
            this.pictureBox59.TabStop = false;
            // 
            // txtSecBco
            // 
            this.txtSecBco.BackColor = System.Drawing.Color.Black;
            this.txtSecBco.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSecBco.ForeColor = System.Drawing.Color.Blue;
            this.txtSecBco.Location = new System.Drawing.Point(92, 433);
            this.txtSecBco.MaxLength = 4;
            this.txtSecBco.Name = "txtSecBco";
            this.txtSecBco.Size = new System.Drawing.Size(50, 15);
            this.txtSecBco.TabIndex = 194;
            // 
            // pictureBox60
            // 
            this.pictureBox60.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox60.Location = new System.Drawing.Point(216, 449);
            this.pictureBox60.Name = "pictureBox60";
            this.pictureBox60.Size = new System.Drawing.Size(10, 1);
            this.pictureBox60.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox60.TabIndex = 197;
            this.pictureBox60.TabStop = false;
            // 
            // txtMarcaId
            // 
            this.txtMarcaId.BackColor = System.Drawing.Color.Black;
            this.txtMarcaId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMarcaId.ForeColor = System.Drawing.Color.Blue;
            this.txtMarcaId.Location = new System.Drawing.Point(217, 433);
            this.txtMarcaId.MaxLength = 1;
            this.txtMarcaId.Name = "txtMarcaId";
            this.txtMarcaId.Size = new System.Drawing.Size(10, 15);
            this.txtMarcaId.TabIndex = 196;
            // 
            // pictureBox61
            // 
            this.pictureBox61.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox61.Location = new System.Drawing.Point(403, 449);
            this.pictureBox61.Name = "pictureBox61";
            this.pictureBox61.Size = new System.Drawing.Size(50, 1);
            this.pictureBox61.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox61.TabIndex = 199;
            this.pictureBox61.TabStop = false;
            // 
            // txtNroEmp
            // 
            this.txtNroEmp.BackColor = System.Drawing.Color.Black;
            this.txtNroEmp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNroEmp.ForeColor = System.Drawing.Color.Blue;
            this.txtNroEmp.Location = new System.Drawing.Point(404, 433);
            this.txtNroEmp.MaxLength = 4;
            this.txtNroEmp.Name = "txtNroEmp";
            this.txtNroEmp.Size = new System.Drawing.Size(50, 15);
            this.txtNroEmp.TabIndex = 198;
            // 
            // pictureBox62
            // 
            this.pictureBox62.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox62.Location = new System.Drawing.Point(504, 450);
            this.pictureBox62.Name = "pictureBox62";
            this.pictureBox62.Size = new System.Drawing.Size(50, 1);
            this.pictureBox62.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox62.TabIndex = 201;
            this.pictureBox62.TabStop = false;
            // 
            // txtCanalVta
            // 
            this.txtCanalVta.BackColor = System.Drawing.Color.Black;
            this.txtCanalVta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCanalVta.ForeColor = System.Drawing.Color.Blue;
            this.txtCanalVta.Location = new System.Drawing.Point(505, 434);
            this.txtCanalVta.MaxLength = 4;
            this.txtCanalVta.Name = "txtCanalVta";
            this.txtCanalVta.Size = new System.Drawing.Size(50, 15);
            this.txtCanalVta.TabIndex = 200;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label68.Location = new System.Drawing.Point(23, 471);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(322, 14);
            this.label68.TabIndex = 202;
            this.label68.Text = "Haga click sobre cada campo para más detalles";
            // 
            // pictureBox104
            // 
            this.pictureBox104.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox104.Location = new System.Drawing.Point(134, 126);
            this.pictureBox104.Name = "pictureBox104";
            this.pictureBox104.Size = new System.Drawing.Size(190, 1);
            this.pictureBox104.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox104.TabIndex = 234;
            this.pictureBox104.TabStop = false;
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.Color.Black;
            this.textBox43.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox43.ForeColor = System.Drawing.Color.Blue;
            this.textBox43.Location = new System.Drawing.Point(136, 110);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(190, 15);
            this.textBox43.TabIndex = 233;
            // 
            // pictureBox105
            // 
            this.pictureBox105.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox105.Location = new System.Drawing.Point(539, 103);
            this.pictureBox105.Name = "pictureBox105";
            this.pictureBox105.Size = new System.Drawing.Size(20, 1);
            this.pictureBox105.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox105.TabIndex = 232;
            this.pictureBox105.TabStop = false;
            // 
            // pictureBox106
            // 
            this.pictureBox106.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox106.Location = new System.Drawing.Point(135, 102);
            this.pictureBox106.Name = "pictureBox106";
            this.pictureBox106.Size = new System.Drawing.Size(160, 1);
            this.pictureBox106.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox106.TabIndex = 231;
            this.pictureBox106.TabStop = false;
            // 
            // pictureBox107
            // 
            this.pictureBox107.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox107.Location = new System.Drawing.Point(463, 33);
            this.pictureBox107.Name = "pictureBox107";
            this.pictureBox107.Size = new System.Drawing.Size(60, 1);
            this.pictureBox107.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox107.TabIndex = 230;
            this.pictureBox107.TabStop = false;
            // 
            // pictureBox108
            // 
            this.pictureBox108.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox108.Location = new System.Drawing.Point(65, 55);
            this.pictureBox108.Name = "pictureBox108";
            this.pictureBox108.Size = new System.Drawing.Size(30, 1);
            this.pictureBox108.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox108.TabIndex = 229;
            this.pictureBox108.TabStop = false;
            // 
            // pictureBox109
            // 
            this.pictureBox109.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox109.Location = new System.Drawing.Point(126, 34);
            this.pictureBox109.Name = "pictureBox109";
            this.pictureBox109.Size = new System.Drawing.Size(100, 1);
            this.pictureBox109.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox109.TabIndex = 228;
            this.pictureBox109.TabStop = false;
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.Color.Black;
            this.textBox44.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox44.ForeColor = System.Drawing.Color.Blue;
            this.textBox44.Location = new System.Drawing.Point(540, 87);
            this.textBox44.MaxLength = 1;
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(20, 15);
            this.textBox44.TabIndex = 227;
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.Color.Black;
            this.textBox45.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox45.ForeColor = System.Drawing.Color.Blue;
            this.textBox45.Location = new System.Drawing.Point(137, 86);
            this.textBox45.MaxLength = 16;
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(160, 15);
            this.textBox45.TabIndex = 226;
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.Color.Black;
            this.textBox46.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox46.ForeColor = System.Drawing.Color.Blue;
            this.textBox46.Location = new System.Drawing.Point(465, 17);
            this.textBox46.MaxLength = 6;
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(80, 15);
            this.textBox46.TabIndex = 225;
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.Color.Black;
            this.textBox47.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox47.ForeColor = System.Drawing.Color.Blue;
            this.textBox47.Location = new System.Drawing.Point(66, 38);
            this.textBox47.MaxLength = 3;
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(30, 15);
            this.textBox47.TabIndex = 224;
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.Color.Black;
            this.textBox48.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox48.ForeColor = System.Drawing.Color.Blue;
            this.textBox48.Location = new System.Drawing.Point(127, 18);
            this.textBox48.MaxLength = 3;
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(100, 15);
            this.textBox48.TabIndex = 223;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(445, 137);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(63, 14);
            this.label106.TabIndex = 185;
            this.label106.Text = "Vigencia";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(240, 137);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(70, 14);
            this.label107.TabIndex = 184;
            this.label107.Text = "Categoria";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(20, 137);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(77, 14);
            this.label108.TabIndex = 183;
            this.label108.Text = "T. Tarjeta";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(20, 112);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(91, 14);
            this.label113.TabIndex = 178;
            this.label113.Text = "Denominacion";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(444, 87);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(91, 14);
            this.label114.TabIndex = 177;
            this.label114.Text = "Distribucion";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(20, 87);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(112, 14);
            this.label115.TabIndex = 176;
            this.label115.Text = "Nro. de Tarjeta";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(359, 19);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(98, 14);
            this.label117.TabIndex = 174;
            this.label117.Text = "Nro.Solicitud";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(20, 40);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(42, 14);
            this.label118.TabIndex = 173;
            this.label118.Text = "Banco";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(20, 19);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(105, 14);
            this.label119.TabIndex = 172;
            this.label119.Text = "Nro. de Cuenta";
            // 
            // pictureBox110
            // 
            this.pictureBox110.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox110.Location = new System.Drawing.Point(462, 53);
            this.pictureBox110.Name = "pictureBox110";
            this.pictureBox110.Size = new System.Drawing.Size(30, 1);
            this.pictureBox110.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox110.TabIndex = 319;
            this.pictureBox110.TabStop = false;
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.Color.Black;
            this.textBox49.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox49.ForeColor = System.Drawing.Color.Blue;
            this.textBox49.Location = new System.Drawing.Point(463, 36);
            this.textBox49.MaxLength = 3;
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(30, 15);
            this.textBox49.TabIndex = 318;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(359, 40);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(35, 14);
            this.label120.TabIndex = 317;
            this.label120.Text = "Casa";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(18, 62);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(693, 14);
            this.label116.TabIndex = 320;
            this.label116.Text = "---------------------------------------------------------------------------------" +
    "-----------------";
            // 
            // pictureBox111
            // 
            this.pictureBox111.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox111.Location = new System.Drawing.Point(627, 101);
            this.pictureBox111.Name = "pictureBox111";
            this.pictureBox111.Size = new System.Drawing.Size(10, 1);
            this.pictureBox111.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox111.TabIndex = 323;
            this.pictureBox111.TabStop = false;
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.Color.Black;
            this.textBox50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox50.ForeColor = System.Drawing.Color.Blue;
            this.textBox50.Location = new System.Drawing.Point(628, 85);
            this.textBox50.MaxLength = 1;
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(10, 15);
            this.textBox50.TabIndex = 322;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(580, 88);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(35, 14);
            this.label121.TabIndex = 321;
            this.label121.Text = "Foto";
            // 
            // pictureBox95
            // 
            this.pictureBox95.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox95.Location = new System.Drawing.Point(133, 151);
            this.pictureBox95.Name = "pictureBox95";
            this.pictureBox95.Size = new System.Drawing.Size(10, 1);
            this.pictureBox95.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox95.TabIndex = 325;
            this.pictureBox95.TabStop = false;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.Color.Black;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox34.ForeColor = System.Drawing.Color.Blue;
            this.textBox34.Location = new System.Drawing.Point(134, 135);
            this.textBox34.MaxLength = 1;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(10, 15);
            this.textBox34.TabIndex = 324;
            // 
            // pictureBox96
            // 
            this.pictureBox96.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox96.Location = new System.Drawing.Point(344, 150);
            this.pictureBox96.Name = "pictureBox96";
            this.pictureBox96.Size = new System.Drawing.Size(10, 1);
            this.pictureBox96.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox96.TabIndex = 327;
            this.pictureBox96.TabStop = false;
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.Color.Black;
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox35.ForeColor = System.Drawing.Color.Blue;
            this.textBox35.Location = new System.Drawing.Point(345, 134);
            this.textBox35.MaxLength = 1;
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(10, 15);
            this.textBox35.TabIndex = 326;
            // 
            // pictureBox97
            // 
            this.pictureBox97.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox97.Location = new System.Drawing.Point(539, 149);
            this.pictureBox97.Name = "pictureBox97";
            this.pictureBox97.Size = new System.Drawing.Size(20, 1);
            this.pictureBox97.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox97.TabIndex = 329;
            this.pictureBox97.TabStop = false;
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.Color.Black;
            this.textBox36.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox36.ForeColor = System.Drawing.Color.Blue;
            this.textBox36.Location = new System.Drawing.Point(540, 133);
            this.textBox36.MaxLength = 1;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(20, 15);
            this.textBox36.TabIndex = 328;
            // 
            // pictureBox86
            // 
            this.pictureBox86.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox86.Location = new System.Drawing.Point(539, 176);
            this.pictureBox86.Name = "pictureBox86";
            this.pictureBox86.Size = new System.Drawing.Size(60, 1);
            this.pictureBox86.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox86.TabIndex = 338;
            this.pictureBox86.TabStop = false;
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.Color.Black;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox25.ForeColor = System.Drawing.Color.Blue;
            this.textBox25.Location = new System.Drawing.Point(540, 160);
            this.textBox25.MaxLength = 1;
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(60, 15);
            this.textBox25.TabIndex = 337;
            // 
            // pictureBox88
            // 
            this.pictureBox88.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox88.Location = new System.Drawing.Point(133, 175);
            this.pictureBox88.Name = "pictureBox88";
            this.pictureBox88.Size = new System.Drawing.Size(20, 1);
            this.pictureBox88.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox88.TabIndex = 334;
            this.pictureBox88.TabStop = false;
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.Color.Black;
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox27.ForeColor = System.Drawing.Color.Blue;
            this.textBox27.Location = new System.Drawing.Point(134, 159);
            this.textBox27.MaxLength = 1;
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(20, 15);
            this.textBox27.TabIndex = 333;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(445, 163);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(42, 14);
            this.label94.TabIndex = 332;
            this.label94.Text = "Prod.";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(240, 164);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(49, 14);
            this.label95.TabIndex = 331;
            this.label95.Text = "Cuotas";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(20, 163);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(91, 14);
            this.label96.TabIndex = 330;
            this.label96.Text = "Bonificacion";
            // 
            // pictureBox87
            // 
            this.pictureBox87.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox87.Location = new System.Drawing.Point(344, 177);
            this.pictureBox87.Name = "pictureBox87";
            this.pictureBox87.Size = new System.Drawing.Size(20, 1);
            this.pictureBox87.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox87.TabIndex = 340;
            this.pictureBox87.TabStop = false;
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.Color.Black;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox26.ForeColor = System.Drawing.Color.Blue;
            this.textBox26.Location = new System.Drawing.Point(345, 161);
            this.textBox26.MaxLength = 1;
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(20, 15);
            this.textBox26.TabIndex = 339;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(20, 192);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(693, 14);
            this.label97.TabIndex = 341;
            this.label97.Text = "---------------------------------------------------------------------------------" +
    "-----------------";
            // 
            // pictureBox89
            // 
            this.pictureBox89.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox89.Location = new System.Drawing.Point(346, 227);
            this.pictureBox89.Name = "pictureBox89";
            this.pictureBox89.Size = new System.Drawing.Size(100, 1);
            this.pictureBox89.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox89.TabIndex = 347;
            this.pictureBox89.TabStop = false;
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.Color.Black;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox28.ForeColor = System.Drawing.Color.Blue;
            this.textBox28.Location = new System.Drawing.Point(347, 211);
            this.textBox28.MaxLength = 1;
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(100, 15);
            this.textBox28.TabIndex = 346;
            // 
            // pictureBox90
            // 
            this.pictureBox90.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox90.Location = new System.Drawing.Point(135, 227);
            this.pictureBox90.Name = "pictureBox90";
            this.pictureBox90.Size = new System.Drawing.Size(30, 1);
            this.pictureBox90.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox90.TabIndex = 345;
            this.pictureBox90.TabStop = false;
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.Color.Black;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox29.ForeColor = System.Drawing.Color.Blue;
            this.textBox29.Location = new System.Drawing.Point(136, 211);
            this.textBox29.MaxLength = 1;
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(30, 15);
            this.textBox29.TabIndex = 344;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(240, 213);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(98, 14);
            this.label98.TabIndex = 343;
            this.label98.Text = "Nro.Documento";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(22, 213);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(105, 14);
            this.label99.TabIndex = 342;
            this.label99.Text = "Tipo Documento";
            // 
            // pictureBox78
            // 
            this.pictureBox78.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox78.Location = new System.Drawing.Point(135, 253);
            this.pictureBox78.Name = "pictureBox78";
            this.pictureBox78.Size = new System.Drawing.Size(20, 1);
            this.pictureBox78.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox78.TabIndex = 350;
            this.pictureBox78.TabStop = false;
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.Color.Black;
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox17.ForeColor = System.Drawing.Color.Blue;
            this.textBox17.Location = new System.Drawing.Point(136, 237);
            this.textBox17.MaxLength = 1;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(20, 15);
            this.textBox17.TabIndex = 349;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(22, 241);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(91, 14);
            this.label85.TabIndex = 348;
            this.label85.Text = "Nacionalidad";
            // 
            // pictureBox79
            // 
            this.pictureBox79.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox79.Location = new System.Drawing.Point(346, 254);
            this.pictureBox79.Name = "pictureBox79";
            this.pictureBox79.Size = new System.Drawing.Size(10, 1);
            this.pictureBox79.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox79.TabIndex = 353;
            this.pictureBox79.TabStop = false;
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.Color.Black;
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox18.ForeColor = System.Drawing.Color.Blue;
            this.textBox18.Location = new System.Drawing.Point(346, 238);
            this.textBox18.MaxLength = 1;
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(10, 15);
            this.textBox18.TabIndex = 352;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(240, 241);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(35, 14);
            this.label86.TabIndex = 351;
            this.label86.Text = "Sexo";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(445, 241);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(91, 14);
            this.label87.TabIndex = 354;
            this.label87.Text = "Estado Civil";
            // 
            // pictureBox80
            // 
            this.pictureBox80.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox80.Location = new System.Drawing.Point(541, 254);
            this.pictureBox80.Name = "pictureBox80";
            this.pictureBox80.Size = new System.Drawing.Size(10, 1);
            this.pictureBox80.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox80.TabIndex = 356;
            this.pictureBox80.TabStop = false;
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.Color.Black;
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox19.ForeColor = System.Drawing.Color.Blue;
            this.textBox19.Location = new System.Drawing.Point(542, 238);
            this.textBox19.MaxLength = 1;
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(10, 15);
            this.textBox19.TabIndex = 355;
            // 
            // pictureBox81
            // 
            this.pictureBox81.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox81.Location = new System.Drawing.Point(133, 282);
            this.pictureBox81.Name = "pictureBox81";
            this.pictureBox81.Size = new System.Drawing.Size(60, 1);
            this.pictureBox81.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox81.TabIndex = 359;
            this.pictureBox81.TabStop = false;
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.Color.Black;
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox20.ForeColor = System.Drawing.Color.Blue;
            this.textBox20.Location = new System.Drawing.Point(134, 266);
            this.textBox20.MaxLength = 1;
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(60, 15);
            this.textBox20.TabIndex = 358;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(22, 269);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(112, 14);
            this.label88.TabIndex = 357;
            this.label88.Text = "Fecha de Nacim.";
            // 
            // pictureBox82
            // 
            this.pictureBox82.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox82.Location = new System.Drawing.Point(346, 282);
            this.pictureBox82.Name = "pictureBox82";
            this.pictureBox82.Size = new System.Drawing.Size(60, 1);
            this.pictureBox82.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox82.TabIndex = 362;
            this.pictureBox82.TabStop = false;
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.Color.Black;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox21.ForeColor = System.Drawing.Color.Blue;
            this.textBox21.Location = new System.Drawing.Point(347, 266);
            this.textBox21.MaxLength = 1;
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(60, 15);
            this.textBox21.TabIndex = 361;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(243, 269);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(84, 14);
            this.label89.TabIndex = 360;
            this.label89.Text = "C.Ocupacion";
            // 
            // pictureBox83
            // 
            this.pictureBox83.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox83.Location = new System.Drawing.Point(540, 281);
            this.pictureBox83.Name = "pictureBox83";
            this.pictureBox83.Size = new System.Drawing.Size(20, 1);
            this.pictureBox83.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox83.TabIndex = 365;
            this.pictureBox83.TabStop = false;
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.Color.Black;
            this.textBox22.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox22.ForeColor = System.Drawing.Color.Blue;
            this.textBox22.Location = new System.Drawing.Point(541, 265);
            this.textBox22.MaxLength = 1;
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(20, 15);
            this.textBox22.TabIndex = 364;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(444, 269);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(91, 14);
            this.label90.TabIndex = 363;
            this.label90.Text = "Habilitacion";
            // 
            // pictureBox84
            // 
            this.pictureBox84.Image = global::Gestion_AP.Properties.Resources.line_bt;
            this.pictureBox84.Location = new System.Drawing.Point(627, 281);
            this.pictureBox84.Name = "pictureBox84";
            this.pictureBox84.Size = new System.Drawing.Size(10, 1);
            this.pictureBox84.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox84.TabIndex = 368;
            this.pictureBox84.TabStop = false;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.Color.Black;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox23.ForeColor = System.Drawing.Color.Blue;
            this.textBox23.Location = new System.Drawing.Point(628, 265);
            this.textBox23.MaxLength = 1;
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(10, 15);
            this.textBox23.TabIndex = 367;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(580, 268);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(42, 14);
            this.label91.TabIndex = 366;
            this.label91.Text = "Cargo";
            // 
            // BPMPrisma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(772, 635);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Name = "BPMPrisma";
            this.Text = "BPMPrisma";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox84)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtCodBco;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtTipCta;
        private System.Windows.Forms.TextBox txtDenCta;
        private System.Windows.Forms.TextBox txtSolicitud;
        private System.Windows.Forms.TextBox txtSucBco;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.TextBox txtPuerta;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.TextBox txtNLOD;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.TextBox txtLOD;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TextBox txtPiso;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.TextBox txtLimExcPM;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.TextBox txtPorcExigPM;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.TextBox txtCartera;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.TextBox txtFPago;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.TextBox txtFinUSS;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.TextBox txtModLiq;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.TextBox txtLimFin;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.TextBox txtLimCpra;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.TextBox txtPromotor;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.TextBox txtDDN;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.TextBox txtPartido;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.TextBox txtCodGeo;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.TextBox txtCodPos;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.TextBox txtCalleCorr;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.TextBox Sellados;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.TextBox txtNroDlsDb;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.TextBox txtNroCtaDb;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.TextBox txtTipDlsDb;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.TextBox txtTipoCtaDb;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.TextBox txtSucDlsDb;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.TextBox txtSucCtaDb;
        private System.Windows.Forms.PictureBox pictureBox62;
        private System.Windows.Forms.TextBox txtCanalVta;
        private System.Windows.Forms.PictureBox pictureBox61;
        private System.Windows.Forms.TextBox txtNroEmp;
        private System.Windows.Forms.PictureBox pictureBox60;
        private System.Windows.Forms.TextBox txtMarcaId;
        private System.Windows.Forms.PictureBox pictureBox59;
        private System.Windows.Forms.TextBox txtSecBco;
        private System.Windows.Forms.PictureBox pictureBox58;
        private System.Windows.Forms.TextBox txtCuit3;
        private System.Windows.Forms.PictureBox pictureBox57;
        private System.Windows.Forms.TextBox txtCuit2;
        private System.Windows.Forms.PictureBox pictureBox56;
        private System.Windows.Forms.TextBox txtCuit1;
        private System.Windows.Forms.PictureBox pictureBox55;
        private System.Windows.Forms.TextBox txtTipoIva;
        private System.Windows.Forms.PictureBox pictureBox54;
        private System.Windows.Forms.TextBox txtPlanta;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.TextBox txtEmpresa;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.TextBox txtVtoTar;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.TextBox txtCta2da;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.TextBox txtTipCta2da;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.TextBox txtSuc2da;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.TextBox txtFLCC;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.TextBox txtSeg;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.TextBox txtAgrup;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.TextBox txtSocio;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.TextBox txtAffGr;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.TextBox txtCatCaj;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.TextBox txtLPtmo;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.TextBox txtProd;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.TextBox txtPartidoCorr;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.TextBox txtCodGeoCorr;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.TextBox txtCodPosCorr;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.TextBox txtNLODCorr;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.TextBox txtLODCor;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.TextBox txtPisoCorr;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.TextBox txtPuertaCorr;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.PictureBox pictureBox84;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.PictureBox pictureBox83;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.PictureBox pictureBox82;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.PictureBox pictureBox81;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.PictureBox pictureBox80;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.PictureBox pictureBox79;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.PictureBox pictureBox78;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.PictureBox pictureBox89;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.PictureBox pictureBox90;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.PictureBox pictureBox87;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.PictureBox pictureBox86;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.PictureBox pictureBox88;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.PictureBox pictureBox97;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.PictureBox pictureBox96;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.PictureBox pictureBox95;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.PictureBox pictureBox111;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.PictureBox pictureBox110;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.PictureBox pictureBox104;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.PictureBox pictureBox105;
        private System.Windows.Forms.PictureBox pictureBox106;
        private System.Windows.Forms.PictureBox pictureBox107;
        private System.Windows.Forms.PictureBox pictureBox108;
        private System.Windows.Forms.PictureBox pictureBox109;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
    }
}