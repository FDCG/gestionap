﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace Gestion_AP
{
    class Metodos
    {

        private static readonly List<string> Metodo = new List<string>();


        public static string ObtenerMetodo(string Tramite)
        {
		// ABRO CONEXION Y OBTENGO TABLA DE TRAMITES
            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select METODO FROM TRAMITES WHERE WF='"+Tramite+"'";

            DataTable dt = new DataTable();
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);

            if (dt.Rows.Count==1)
            {
                Tramite = dt.Rows[0][0].ToString();
                
            }
            else
            {
                Tramite = "99";
            }
            return Tramite;


        }

        public static bool Postdatado(string Tramite)
        {
		//METODO PARA POSTADATAR TRAMITES DE MASTERCARD EN WORKFLOW

            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = Conexion.cnProceso;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select POSTDATA FROM TRAMITES WHERE WF='" + Tramite + "'";

            DataTable dt = new DataTable();
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);

            if (dt.Rows.Count == 1 && Convert.ToBoolean(dt.Rows[0][0])==true)
            {
                return true;
		//DEVUELVE SI ES MASTERCARD Y EN TABLA ES POSTADATADO TRUE

            }
            else
            {
                return false;
            }
            



        }


    }
}
